<?php

return [
    'Stripe Payment Mode' => [
        "Live" => "Live",
        "Test" => "Test"
    ],
    'Website Status' => [
        "Online" => "Online",
        "Offline" => "Offline"
    ],
];

