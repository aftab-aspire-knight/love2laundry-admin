<?php

return [
    'yes_no' => [
        "Yes" => "Yes",
        "No" => "No"
    ],
    'roles' => [
        "administrator" => "Administrator",
        "driver" => "Driver"
    ],
    'register_from' => [
        "desktop" => "Desktop",
        "mobile" => "Mobile"
    ],
    'delivery_options' => [
        "None" => "None",
        "Saturday Pickup Delivery To Monday After 3" => "Saturday Pickup Delivery To Monday After 3",
        "Saturday Sunday Both Pickup Delivery To Monday After 3" => "Saturday Sunday Both Pickup Delivery To Monday After 3",
        "Franchise Hours" => "Franchise Hours",
    ],
    "franchise_timings" => [
        "05:00 pm" => "05:00 pm",
        "06:00 pm" => "06:00 pm",
        "07:00 pm" => "07:00 pm",
        "08:00 pm" => "08:00 pm",
        "09:00 pm" => "09:00 pm",
        "10:00 pm" => "10:00 pm",
        "11:00 pm" => "11:00 pm",
        "12:00 am" => "12:00 am",
        "01:00 am" => "01:00 am",
        "03:00 am" => "03:00 am",
        "04:00 am" => "04:00 am",
        "02:00 am" => "02:00 am",
        "03:00 am" => "03:00 am",
        "04:00 am" => "04:00 am",
        "05:00 am" => "05:00 am",
        "06:00 am" => "06:00 am",
        "07:00 am" => "07:00 am",
        "08:00 am" => "08:00 am",
        "09:00 am" => "09:00 am",
        "10:00 am" => "10:00 am",
        "11:00 am" => "11:00 am",
        "12:00 pm" => "12:00 pm",
        "01:00 pm" => "01:00 pm",
        "02:00 pm" => "02:00 pm",
        "03:00 pm" => "03:00 pm",
        "04:00 pm" => "04:00 pm",
    ],
    "days" => [
        "Monday" => "Monday",
        "Tuesday" => "Tuesday",
        "Wenesday" => "Wenesday",
        "Thusday" => "Thusday",
        "Friday" => "Friday",
        "Saturday" => "Saturday",
        "Sunday" => "Sunday",
    ],
    'online_offline' => [
        "Online" => "Online",
        "Offline" => "Offline"
    ],
    'live_test' => [
        "Live" => "Live",
        "Test" => "Test"
    ],
    'unavailable_times_types' => [
        "Pickup" => "Pickup",
        "Delivery" => "Delivery",
        "Both" => "Both"
    ],
    'template' => [
        "None" => "None",
        "Area" => "Area",
        "Area Detail" => "Area Detail"
    ],
    "discounts" => [
        'types' => [
            "Percentage" => "Percentage",
            "Price" => "Price"
        ],
        'statuses' => [
            "Active" => "Active",
            "Expire" => "Expire",
            "Used" => "Used"
        ],
        'for' => [
            "Discount" => "Discount",
            "Referral" => "Referral",
            "Loyalty" => "Loyalty"
        ],
        'used' => [
            "One Time" => "One Time",
            "Multiple Time" => "Multiple Time"
        ]
    ],
    "order_from" => [
        "Desktop" => "Desktop",
        "Mobile" => "Mobile",
    ],
    "payment_methods" => [
        "Stripe" => "Stripe",
        "Cash" => "Cash",
    ],
    "invoice_statuses" => [
        "Pending" => "Pending",
        "Processed" => "Processed",
        "Processed" => "Processed",
        "Cancel" => "Cancel",
        "Completed" => "Completed",
    ],
    'invoice_types' => [
        "After" => "After",
        "Items" => "Items"
    ],
    "regularly" => ["Just once", "Weekly", "Every two weeks"],
    "timings" => [
        "05:00-06:00" => "05:00-06:00 am",
        "06:00-07:00" => "06:00-07:00 am",
        "07:00-08:00" => "07:00-08:00 am",
        "08:00-09:00" => "08:00-09:00 am",
        "09:00-10:00" => "09:00-10:00 am",
        "10:00-11:00" => "10:00-11:00 am",
        "11:00-12:00" => "11:00-12:00 pm",
        "12:00-13:00" => "12:00-01:00 pm",
        "13:00-14:00" => "01:00-02:00 pm",
        "14:00-15:00" => "02:00-03:00 pm",
        "15:00-16:00" => "03:00-04:00 pm",
        "16:00-17:00" => "04:00-05:00 pm",
        "17:00-18:00" => "05:00-06:00 am",
        "18:00-19:00" => "06:00-07:00 am",
        "19:00-20:00" => "07:00-08:00 am",
        "20:00-21:00" => "08:00-09:00 am",
        "21:00-22:00" => "09:00-10:00 am",
        "22:00-23:00" => "10:00-11:00 am",
    ],
    "orders_from" => [
        "Desktop" => "Desktop",
        "Mobile" => "Mobile",
    ],
    "payment_status" => [
        "Pending" => "Pending",
        "Processed" => "Processed",
        "Cancel" => "Cancel",
        "Completed" => "Completed"
    ],
    "dir_upload_category" => [
        "thumb" => "uploads/categories/thumb/",
        "without_thumb" => "uploads/categories/"
    ],
    "dir_upload_service" => [
        "thumb" => "uploads/services/thumb/",
        "without_thumb" => "uploads/services/"
    ],
    "dir_upload_banners" => [
        "thumb" => "uploads/banners/thumb/",
        "without_thumb" => "uploads/banners/"
    ],
    "dir_upload_widgets" => [
        "thumb" => "uploads/widgets/thumb/",
        "without_thumb" => "uploads/widgets/"
    ],
    
];
