<?php


Route::get('/', 'DashboardController@index');
Route::get('dashboard', 'DashboardController@index');
Route::get('dashboard/twomonthsorders', 'DashboardController@twomonthsorders');
Route::get('dashboard/districtorders', 'DashboardController@districtorders');
Route::get('dashboard/districtmembers', 'DashboardController@districtmembers');
Route::get('dashboard/franchisesorders', 'DashboardController@franchisesorders');
Route::get('dashboard/districtorderslist', 'DashboardController@districtorderslist');
Route::get('dashboard/ordersbylocations', 'DashboardController@ordersbylocations');
Route::get('dashboard/membersnotinorders', 'DashboardController@membersnotinorders');
Route::get('dashboard/productsanatytics', 'DashboardController@productsanatytics');

Route::get('logout', 'LogoutController@logout');

Route::resources(['administrators' => 'AdministratorsController']);
Route::post('/administrators/save', 'AdministratorsController@save');
Route::get('/administrators/edit/{id}', 'AdministratorsController@edit');
Route::post('/administrators/update/{id}', 'AdministratorsController@update');
Route::get('/administrators/show/{id}', 'AdministratorsController@show');
Route::get('/administrators/delete/{id}', 'AdministratorsController@delete');

Route::resources(['franchises' => 'FranchisesController']);
Route::post('/franchises/save', 'FranchisesController@save');
Route::get('/franchises/edit/{id}', 'FranchisesController@edit');
Route::post('/franchises/update/{id}', 'FranchisesController@update');
Route::get('/franchises/show/{id}', 'FranchisesController@show');
Route::get('/franchises/delete/{id}', 'FranchisesController@delete');

Route::resources(['users' => 'FranchiseUsersController']);
Route::post('/users/save', 'FranchiseUsersController@save');
Route::get('/users/edit/{id}', 'FranchiseUsersController@edit');
Route::post('/users/update/{id}', 'FranchiseUsersController@update');
Route::get('/users/show/{id}', 'FranchiseUsersController@show');
Route::get('/users/delete/{id}', 'FranchiseUsersController@delete');

Route::get('members', 'MembersController@index');
Route::get('members/create', 'MembersController@create');
Route::post('members/save', 'MembersController@save');
Route::get('members/edit/{id}', 'MembersController@edit');
Route::post('members/update/{id}', 'MembersController@update');
Route::get('members/show/{id}', 'MembersController@show');
Route::get('members/delete/{id}', 'MembersController@delete');
Route::get('members/json/', 'MembersController@json');


Route::resources(['payments' => 'OwnerPaymentController']);
Route::post('/payments/save', 'OwnerPaymentController@save');
Route::get('/payments/edit/{id}', 'OwnerPaymentController@edit');
Route::post('/payments/update/{id}', 'OwnerPaymentController@update');
Route::get('/payments/show/{id}', 'OwnerPaymentController@show');
Route::get('/payments/delete/{id}', 'OwnerPaymentController@delete');
Route::get('/payments/json', 'OwnerPaymentController@json');

//settings
Route::resources(['settings' => 'SettingsController']);
Route::post('/settings/update', 'SettingsController@update');
//cms
Route::get('/manager', 'FileManagerController@index');


Route::resources(['banners' => 'BannersController']);
Route::post('/banners/save', 'BannersController@save');
Route::get('/banners/edit/{id}', 'BannersController@edit');
Route::post('/banners/update/{id}', 'BannersController@update');
Route::get('/banners/show/{id}', 'BannersController@show');
Route::get('/banners/delete/{id}', 'BannersController@delete');
Route::get('/banners/remove/{id}', 'BannersController@RemoveImage');


Route::resources(['widgets' => 'WidgetsController']);
Route::post('/widgets/save', 'WidgetsController@save');
Route::get('/widgets/edit/{id}', 'WidgetsController@edit');
Route::post('/widgets/update/{id}', 'WidgetsController@update');
Route::get('/widgets/show/{id}', 'WidgetsController@show');
Route::get('/widgets/delete/{id}', 'WidgetsController@delete');
Route::get('/widgets/remove/{id}', 'WidgetsController@RemoveImage');

Route::resources(['widgetsections' => 'WidgetSectionController']);
Route::post('/widgetsections/save', 'WidgetSectionController@save');
Route::get('/widgetsections/edit/{id}', 'WidgetSectionController@edit');
Route::post('/widgetsections/update/{id}', 'WidgetSectionController@update');
Route::get('/widgetsections/show/{id}', 'WidgetSectionController@show');
Route::get('/widgetsections/delete/{id}', 'WidgetSectionController@delete');

Route::resources(['testimonials' => 'TestimonialController']);
Route::post('/testimonials/save', 'TestimonialController@save');
Route::get('/testimonials/edit/{id}', 'TestimonialController@edit');
Route::post('/testimonials/update/{id}', 'TestimonialController@update');
Route::get('/testimonials/show/{id}', 'TestimonialController@show');
Route::get('/testimonials/delete/{id}', 'TestimonialController@delete');

Route::resources(['subscribers' => 'SubscriberController']);
Route::post('/subscribers/save', 'SubscriberController@save');
Route::get('/subscribers/edit/{id}', 'SubscriberController@edit');
Route::post('/subscribers/update/{id}', 'SubscriberController@update');
Route::get('/subscribers/show/{id}', 'SubscriberController@show');
Route::get('/subscribers/delete/{id}', 'SubscriberController@delete');


Route::resources(['categories' => 'CategoriesController']);
Route::post('/categories/save', 'CategoriesController@save');
Route::get('/categories/edit/{id}', 'CategoriesController@edit');
Route::post('/categories/update/{id}', 'CategoriesController@update');
Route::get('/categories/show/{id}', 'CategoriesController@show');
Route::get('/categories/delete/{id}', 'CategoriesController@delete');
Route::get('/categories/remove/{id}', 'CategoriesController@RemoveImage');

Route::resources(['services' => 'ServiceController']);
Route::post('/services/save', 'ServiceController@save');
Route::get('/services/edit/{id}', 'ServiceController@edit');
Route::post('/services/update/{id}', 'ServiceController@update');
Route::get('/services/show/{id}', 'ServiceController@show');
Route::get('/services/delete/{id}', 'ServiceController@delete');
Route::get('/services/remove-mob/{id}', 'ServiceController@RemoveMobileImage');
Route::get('/services/remove/{id}', 'ServiceController@RemoveDesktopImage');

Route::resources(['locations' => 'LocationController']);
Route::post('/locations/save', 'LocationController@save');
Route::get('/locations/edit/{id}', 'LocationController@edit');
Route::post('/locations/update/{id}', 'LocationController@update');
Route::get('/locations/show/{id}', 'LocationController@show');
Route::get('/locations/delete/{id}', 'LocationController@delete');

Route::resources(['emailresponders' => 'EmailrespondersController']);
Route::post('/emailresponders/save', 'EmailrespondersController@save');
Route::get('/emailresponders/edit/{id}', 'EmailrespondersController@edit');
Route::post('/emailresponders/update/{id}', 'EmailrespondersController@update');
Route::get('/emailresponders/show/{id}', 'EmailrespondersController@show');
Route::get('/emailresponders/delete/{id}', 'EmailrespondersController@delete');

Route::resources(['tags' => 'TagsController']);
Route::post('/tags/save', 'TagsController@save');
Route::get('/tags/edit/{id}', 'TagsController@edit');
Route::post('/tags/update/{id}', 'TagsController@update');
Route::get('/tags/show/{id}', 'TagsController@show');
Route::get('/tags/delete/{id}', 'TagsController@delete');

Route::resources(['smsresponders' => 'SMSResponderController']);
Route::post('/smsresponders/save', 'SMSResponderController@save');
Route::get('/smsresponders/edit/{id}', 'SMSResponderController@edit');
Route::post('/smsresponders/update/{id}', 'SMSResponderController@update');
Route::get('/smsresponders/show/{id}', 'SMSResponderController@show');
Route::get('/smsresponders/delete/{id}', 'SMSResponderController@delete');
// Route::get('administrators/{id}', 'AdministratorsController@show');
// Route Reset Password
Route::post('/recover_password', 'ResetPasswordController@recover_password');
Route::post('reset', 'ResetPasswordController@change_password');

Route::get('/get_statistics', 'DashboardController@statistics');
Route::get('user-profile-page', 'UserController@user_profile_page');
Route::post('profile-update', 'UserController@update_profile');

Route::get('update-user-password-page', 'UserController@update_user_password_page');
Route::post('update-password', 'UserController@update_user_password');

Route::resources(['franchises' => 'FranchisesController']);
Route::post('/franchises/save', 'FranchisesController@save');
Route::get('/franchises/edit/{id}', 'FranchisesController@edit');
Route::post('/franchises/update/{id}', 'FranchisesController@update');
Route::get('/franchises/show/{id}', 'FranchisesController@show');
Route::get('/franchises/delete/{id}', 'FranchisesController@delete');
Route::get('/franchises/services/{id}', 'FranchisesController@services');
Route::get('/franchises/getservices/{id}', 'FranchisesController@getservices');
Route::post('/franchises/saveservice', 'FranchisesController@saveservice');
Route::post('/franchises/removeservice', 'FranchisesController@removeservice');
Route::post('/franchises/addservice', 'FranchisesController@addservice');
Route::post('/franchises/codes', 'FranchisesController@codes');

Route::get('/franchises/unavailabletimes/{franchise_id}', 'UnavailableTimesController@index');
Route::get('/franchises/unavailabletimes/create/{id}', 'UnavailableTimesController@create');
Route::post('franchises/unavailabletimes/save', 'UnavailableTimesController@save');

Route::get('/franchises/unavailabletimes/edit/{franchise_id}/{id}', 'UnavailableTimesController@edit');
Route::post('/franchises/unavailabletimes/update/{id}', 'UnavailableTimesController@update');
Route::get('/franchises/unavailabletimes/delete/{franchise_id}/{id}', 'UnavailableTimesController@delete');


Route::resources(['discounts' => 'DiscountsController']);
Route::post('/discounts/save', 'DiscountsController@save');
Route::get('/discounts/edit/{id}', 'DiscountsController@edit');
Route::post('/discounts/update/{id}', 'DiscountsController@update');
Route::get('/discounts/show/{id}', 'DiscountsController@show');
Route::get('/discounts/delete/{id}', 'DiscountsController@delete');

Route::resources(['locations' => 'LocationsController']);
Route::post('/locations/save', 'LocationsController@save');
Route::get('/locations/edit/{id}', 'LocationsController@edit');
Route::post('/locations/update/{id}', 'LocationsController@update');
Route::get('/locations/show/{id}', 'LocationsController@show');
Route::get('/locations/delete/{id}', 'LocationsController@delete');

Route::resources(['preferences' => 'PreferencesController']);
Route::post('/preferences/save', 'PreferencesController@save');
Route::get('/preferences/edit/{id}', 'PreferencesController@edit');
Route::post('/preferences/update/{id}', 'PreferencesController@update');
Route::get('/preferences/show/{id}', 'PreferencesController@show');
Route::get('/preferences/delete/{id}', 'PreferencesController@delete');

//Route::resources(['invoices' => 'InvoicesController']);
Route::get('/invoices', 'InvoicesController@index');
Route::get('/invoices/create/{member_id}/{franchise_id}/{postcode}', 'InvoicesController@create');
Route::post('/invoices/save', 'InvoicesController@save');
Route::get('/invoices/edit/{id}', 'InvoicesController@edit');
Route::post('/invoices/update/{id}', 'InvoicesController@update');
Route::get('/invoices/show/{id}', 'InvoicesController@show');
Route::get('/invoices/delete/{id}', 'InvoicesController@delete');
Route::get('/invoices/json', 'InvoicesController@json');
Route::get('/invoices/member', 'InvoicesController@member');
Route::post('/invoices/checkfranchise', 'InvoicesController@checkfranchise');
Route::post('/invoices/refund', 'InvoicesController@refund');
Route::get('/invoices/get-refunds/{invoice_id}', 'InvoicesController@getRefunds');

Route::get('/invoicefilters', 'InvoiceFiltersController@index');
Route::get('/invoicefilters/search', 'InvoiceFiltersController@search');
Route::get('/invoicefilters/getmembers', 'InvoiceFiltersController@getmembers');

Route::resources(['navigations' => 'NavigationsController']);
Route::post('/navigations/save', 'NavigationsController@save');
Route::get('/navigations/edit/{id}', 'NavigationsController@edit');
Route::post('/navigations/update/{id}', 'NavigationsController@update');
Route::get('/navigations/show/{id}', 'NavigationsController@show');
Route::get('/navigations/delete/{id}', 'NavigationsController@delete');


Route::get('/payments', 'OwnerPaymentController@index');
Route::post('/payments/save', 'OwnerPaymentController@save');
Route::get('payments/update', 'OwnerPaymentController@UpdatePaymentStatus');
Route::get('payments/update-unpaid', 'OwnerPaymentController@UpdatePaymentStatusToUnPaid');
//// Reporting 
Route::get('/reporting/client-retention', 'ReportingController@clientretention');
Route::get('/reporting/search-client-retention', 'ReportingController@searchclientretention');
Route::get('/reporting/customers-retargeting', 'ReportingController@customersretargeting');
Route::get('/reporting/search-customers-retargeting', 'ReportingController@searchcustomersretargeting');
Route::get('/reporting/order-analysis', 'ReportingController@orderanalysis');
Route::get('/reporting/search-order-analysis', 'ReportingController@searchorderanalysis');
Route::get('/reporting/rolling-sales', 'ReportingController@rollingsales');
Route::get('/reporting/search-rolling-sales', 'ReportingController@searchrollingsales');
Route::post('/invoices/move-status', 'InvoicesController@movestatus');


Route::get('/members/export', 'MembersController@get_export');
Route::get('postcodelogs', 'PostCodeLogsController@index');
Route::get('/postcodelogs/json/', 'PostCodeLogsController@json');
Route::get('/postcodelogs/delete/{id}', 'PostCodeLogsController@delete');
Route::get('/postcodelogs/deleteAll', 'PostCodeLogsController@deleteAll');

Route::resources(['pages' => 'CmsPagesController']);
Route::post('/pages/save', 'CmsPagesController@save');
Route::get('/pages/edit/{id}', 'CmsPagesController@edit');
Route::post('/pages/update/{id}', 'CmsPagesController@update');
Route::get('/pages/show/{id}', 'CmsPagesController@show');
Route::get('/pages/delete/{id}', 'CmsPagesController@delete');
Route::get('/pages/remove-header/{id}', 'CmsPagesController@RemoveHeaderImage');
Route::get('/pages/remove-header1/{id}', 'CmsPagesController@RemoveHeaderImage1');
Route::get('/pages/remove-header2/{id}', 'CmsPagesController@RemoveHeaderImage2');

/*
Route::get('smslogs', function(){return View::make('errors.index');});
*/
?>
