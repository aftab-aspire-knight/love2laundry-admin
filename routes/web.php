<?php

Auth::routes();
Route::post('/login/validate', 'Auth\LoginController@validate_api');

Route::get('/auth-login', 'AuthenticationController@login');
Route::get('/auth-register', 'AuthenticationController@register');
Route::get('/auth-forgot-password', 'AuthenticationController@forgot_password');
Route::get('/auth-reset-password', 'AuthenticationController@reset_password');
Route::get('/auth-lock-screen', 'AuthenticationController@lock_screen');
Route::get('/', 'AuthenticationController@login');
Route::post('check-login', 'AuthenticationController@checklogin');


Route::group([
    'middleware' => 'auth:web'
        ], function() {
    include("admin.php");
});
