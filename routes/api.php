<?php

Route::group([
    'prefix' => 'auth',
        ], function () {
    Route::post('login', 'Api\LoginController@index');
    Route::post('register', 'Api\RegistrationController@index');
    Route::post('searchfranchise', 'Api\HomeController@searchfranchise');
    Route::get('timings/pickup/{id}', 'Api\TimingsController@pickup');
    Route::get('timings/delivery', 'Api\TimingsController@delivery');
    Route::post('checkout', 'Api\OrderController@checkout');



    Route::group([
        'middleware' => 'auth:api',
            ], function() {
        Route::get('account', 'Api\AccountController@index');
        Route::post('account/update', 'Api\AccountController@update');
        Route::post('account/changepassword', 'Api\AccountController@changepassword');
        Route::get('logout', 'Api\AccountController@logout');

    });
    Route::group(['middleware' => ['api']], function () {
     Route::post('account/forgotpassword', 'Api\AccountController@forgotpassword');
    Route::get('account/resetpassword', 'Api\AccountController@resetpasswordview');
    Route::post('account/reset', 'Api\AccountController@reset');
});
   

});

