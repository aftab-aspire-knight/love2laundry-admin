function displayErrors(response, id) {
    // alert(id);
    var errorsHtml = '<div class="alert alert-danger alert-dismissible fade show" role="alert">';
    $.each(response.errors, function (key, value) {
        errorsHtml += '<p class="mb-0" >' + value + '</p>'; //showing only the first error.
    });
    errorsHtml += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>';
    $('#' + id).html(errorsHtml).show();
}

function ajaxRequest(url, type, dataType, data, callBack, errorCallBack) {

    $.ajax({
        url: url,
        type: type,
        data: data,
        dataType: dataType,
        success: function (result) {
            callBack(result);
        },
        error: function (jqXHR, error, errorThrown) {
            if (dataType == "json") {
                displayErrors(jqXHR.responseJSON, "errors");
            } else {
                errorCallBack(jqXHR, error, errorThrown);
            }
        }
    });
}

$('.confirm-delete').on('click', function () {
    var id = $(this).data('id');
    var href = $(this).data('href');
    confirmDelete(id, href);
});



function confirmDelete(id, href) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-danger ml-1',
        buttonsStyling: false,
    }).then(function (result) {
        if (result.value) {
            // alert(id);
            window.top.location = href;
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire({
                title: 'Cancelled',
                text: '',
                type: 'error',
                confirmButtonClass: 'btn btn-success',
            })
        }
    })
}

function alertMessage(title, msg, type) {
    Swal.fire(title, msg, type);
}
$(document).ready(function () {
    $('.listing thead tr').clone(true).appendTo('.listing thead');
    var length = $('.listing thead tr:eq(1) th').length;
//console.log(" length "+length);

    $('.listing thead tr:eq(1) th').each(function (i) {

        if (i == length - 1) {
            return false;
        }

        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');

        $('input', this).on('keyup change', function () {
            if (table.column(i).search() !== this.value) {
                table
                        .column(i)
                        .search(this.value)
                        .draw();
            }
        });
    });

    var table = $('.listing').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        pageLength: pageLength,
        columnDefs: [{"orderable": false, "targets": length - 1}]
    });

});