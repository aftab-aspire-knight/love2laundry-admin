<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberCards extends Model
{
    protected $table = 'ws_member_cards';
    protected $primaryKey = 'PKCardID';
    public $timestamps = false;
   
}
