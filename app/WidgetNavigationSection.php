<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WidgetNavigationSection extends Model
{
    protected $table = 'cms_widget_navigation_sections';
    protected $primaryKey = 'PKNavigationID';
    public $timestamps = false;

}
