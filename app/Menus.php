<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model {


    protected $table = 'cms_menus';
    
    protected $primaryKey = 'PKMenuID';
    
    protected $casts = [];
    public $timestamps = false;
    
    
    
}
