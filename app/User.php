<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use HasApiTokens,
        Notifiable;

    protected $table = 'cms_users';
    protected $primaryKey = 'PKUserID';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
            //'email_verified_at' => 'datetime',
    ];

    public function menus() {
        return $this->hasMany('App\UserMenus',"FKUserID");
    }
    
    public function data()
    {
        return $this->hasManyThrough('App\Menus', 'App\UserMenus',"FKUserID","PKMenuID","PKUserID","FKMenuID");
    }
    
    public function franchise()
    {
        return $this->belongsTo('App\Franchises','FKFranchiseID','PKFranchiseID');
    }
       public function invoice(){
    
        return $this->belongsToMany('App\Invoices');
    }
    public function franchiseInvoices(){
        if($this->franchise()->where('PKFranchiseID','!=',0)->first()){
            return true;
        }
        return false;
    }
   

}