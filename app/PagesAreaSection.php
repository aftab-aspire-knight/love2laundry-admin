<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagesAreaSection extends Model
{
    protected $table = 'cms_page_area_sections';
    protected $primaryKey = 'PKSectionID';
    public $timestamps = false;
      public function pages()
    {
        return $this->belongsTo('App\CMSPages','FKPageID','PKSectionID');
    }
   
}
