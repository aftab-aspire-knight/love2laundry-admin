<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSPages extends Model
{
    protected $table = 'cms_pages';
    protected $primaryKey = 'PKPageID';
    public $timestamps = false;
    
   
}
