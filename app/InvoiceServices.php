<?php

namespace App;
use App\Members;

use Illuminate\Database\Eloquent\Model;

class InvoiceServices extends Model
{
    protected $table = 'ws_invoice_services';
    protected $primaryKey = 'PKInvoiceServiceID';
    public $timestamps = false;
    
    public function service()
    {
        return $this->belongsTo(Services::class, 'FKServiceID', 'PKServiceID');           
    }
}
