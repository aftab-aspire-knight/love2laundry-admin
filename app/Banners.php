<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = 'ws_banners';
    protected $primaryKey = 'PKBannerID';
    public $timestamps = false;

}
