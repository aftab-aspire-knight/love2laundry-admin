<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchises extends Model {

    protected $table = 'ws_franchises';
    protected $primaryKey = 'PKFranchiseID';
    protected $casts = [];
    public $timestamps = false;

    public function user() {
        return $this->hasOne('App\User', 'FKFranchiseID', 'PKFranchiseID');
    }
/*
    public function postcodes() {
        return $this->hasOne(PostCodeLogs::class, 'FKFranchiseID', 'PKFranchiseID');
    }
*/
    public function timings() {
        return $this->hasMany('App\FranchiseTimings', "FKFranchiseID");
    }

    public function postCodes() {
        return $this->hasMany('App\FranchisePostCodes', "FKFranchiseID");
    }
    
    public function services() {
        return $this->hasMany('App\FranchiseServices', "FKFranchiseID");
    }
   
    

}
