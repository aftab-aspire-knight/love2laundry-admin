<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaDetail extends Model
{
    protected $table = 'cms_page_areas';
    protected $primaryKey = 'PKPageAreaID';
    public $timestamps = false;
    
    public function pages()
    {
        return $this->belongsTo(CMSPages::class,'FKPageID','PKPageAreaID');
    }
}
