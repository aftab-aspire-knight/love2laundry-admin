<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentInformations extends Model
{
    protected $table = 'ws_payment_informations';
    protected $primaryKey = 'PKInformationID';
   
    public $timestamps = false;

}
