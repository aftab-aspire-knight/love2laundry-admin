<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    public $timestamps = false;
    protected $table = 'cms_token';
    
    protected $primaryKey = 'PKTokenID';

}
