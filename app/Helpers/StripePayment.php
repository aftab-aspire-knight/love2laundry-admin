<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use App\Settings;
use Cartalyst\Stripe\Stripe;

class StripePayment {

    public static function setApiKey() {

        $setting = Settings::where("Title", "Stripe Payment Mode")->first();

        if ($setting->Content == "Test") {
            $setting = Settings::where("Title", "Stripe API Test Key")->first();
        } else {
            $setting = Settings::where("Title", "Stripe API Live Key")->first();
        }
        \Stripe\Stripe::setApiKey($setting->Content);
    }

    public static function purchase($data) {

        $response = array();
        $response['error'] = true;
        
        self::setApiKey();

        try {
            $intent = \Stripe\PaymentIntent::create($data);
            $response = $intent;
        } catch (\Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage() ;
        } catch (\Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage() ;
        } catch (\Stripe\ApiOperations $e) {
            $response['message'] = $e->getMessage() ;
        } catch (\Stripe\Exception $e) {
            $response['message'] = $e->getMessage() ;
        } catch (\Stripe\Exception\AuthenticationException $e){
             $response['message'] = $e->getMessage() ;
        } catch (Exception $e){
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function updatePurchase($pi, $data) {

        $response = array();
        $response['error'] = true;
        self::setApiKey();

        try {

            $intent = \Stripe\PaymentIntent::update($pi,$data);
            $response = $intent;
        } catch (\Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\ApiOperations $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function refund($id, $amount) {

        $response = array();
        $response['error'] = true;
        self::setApiKey();

        try {

            $intent = \Stripe\PaymentIntent::retrieve($id);
            $response = \Stripe\Refund::create([
                        'charge' => $intent->charges->data[0]->id,
                        'amount' => $amount,
            ]);
        } catch (\Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\UnexpectedValueException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    public static function getRefunds($id) {

        $response = array();
        $response['error'] = true;
        self::setApiKey();

        try {
            $intent = \Stripe\PaymentIntent::retrieve($id);
            $response = $intent;
        } catch (\Stripe\Exception\CardException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\RateLimitException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $response['message'] = $e->getMessage();
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

}
