<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



if (!function_exists('showDecimal')) {

    function showDecimal($number) {
        return number_format((float) $number, 2, '.', '');
    }

}



if (!function_exists('getCountry')) {

    function getCountry() {

        if (isset($_SERVER["COUNTRY"])) {
            $server = $_SERVER["SERVER_NAME"];
        } else {
            $server = "www.love2laundry.com";
        }

        $country = "uk";
        $last = explode(".", $server);
        $last = end($last);

        switch ($last) {
            case "com";
                $country = "uk";
                break;
            case "ae";
                $country = "uae";
                break;
            case "nl";
                $country = "nl";
                break;
            case "au";
                $country = "nl";
                break;
            default:
                $country = "uk";
        }

        return $country;
    }

}


if (!function_exists('showYearMonth')) {

    function showYearMonth($date) {
        return date("m-Y", strtotime($date));
    }

}

if (!function_exists('currentDateTime')) {

    function currentDateTime() {
        return date("Y-m-d H:i:s");
    }

}

if (!function_exists('d')) {

    function d($data = '', $status = 0) {
        if (($data)) {
            echo '<pre>';
            print_r($data);
            '</pre>';
        }

        if ($status == 1)
            die;
    }

}

