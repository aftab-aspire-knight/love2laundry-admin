<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

class EmailTemplates {

    public static function setServicesTemplate($services) {

        $html="";
        if (count($services) > 0) {
            $html = '<h4 style="margin-bottom:0">Shopping Cart</h4>';
            $html .= '<table border="1" cellpadding="5" cellspacing="0" bordercolor="#DDDDDD" style="margin-bottom: 20px; width: 100%;">';
            $html .= '<thead>';
            $html .= '<tr>';
            $html .= '<th width="55%" style="text-align:left">Title</th>';
            $html .= '<th width="15%" style="text-align:center">Quantity</th>';
            $html .= '<th width="15%" style="text-align:center">Price</th>';
            $html .= '<th width="15%" style="text-align:center">Total</th>';
            $html .= '</tr>';
            $html .= '</thead>';
            $html .= '<tbody>';
            foreach ($services as $service) {
                $html .= '<tr>';
                $html .= '<td>' . $service->Title . '</td>';
                $html .= '<td style="text-align:right">' . $service->Quantity . '</td>';
                $html .= '<td style="text-align:right">' . env('CURRENCY_SYMBOL') . $service->Price . '</td>';
                $html .= '<td style="text-align:right">' . env('CURRENCY_SYMBOL') . $service->Total . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
        }
        return $html;
    }

    public static function setPreferencesTemplate($preferences) {
        
        $first_heading_text = 'Wash and Fold';
        $second_heading_text = 'Dry Cleaning and Laundered Shirt Options';

        $html="";
        if (count($preferences) > 0) {
            $html = '<h4 style="margin-bottom:0">Customer Preferences</h4>';
            $html .= '<table  border="0" cellpadding="5" cellspacing="0" bordercolor="#DDDDDD" style="margin-bottom: 20px; width: 100%;">';
            $html .= '<tbody>';
            $up_area = "";
            $down_area = "";
            $count = 0;
            $bg_count = 0;
            foreach ($preferences as $record) {
                if ($record->ParentTitle == "Starch on Shirts") {
                    $down_area .= '<tr>';
                    $down_area .= '<td width="20%">' . $record->ParentTitle . '</td>';
                    $down_area .= '<td width="30%">' . $record->Title . '</td>';
                    $down_area .= '</tr>';
                } else {
                    if ($count == 0) {
                        $up_area .= '<tr>';
                    }
                    $up_area .= '<td width="20%"';
                    if ($bg_count < 2) {
                        $up_area .= ' bgcolor="#EAEAEA"';
                    }
                    $up_area .= '>' . $record->ParentTitle . '</td>';
                    $up_area .= '<td width="30%"';
                    if ($bg_count < 2) {
                        $up_area .= ' bgcolor="#EAEAEA"';
                    }
                    $up_area .= '>' . $record->Title . '</td>';
                    if ($count == 1) {
                        $up_area .= '</tr>';
                        $count = 0;
                    } else {
                        $count = 1;
                    }
                    $bg_count += 1;
                    if ($bg_count == 4) {
                        $bg_count = 0;
                    }
                }
            }
            $html .= '<tr>';
            $html .= '<th class="text-center" colspan="4" bgcolor="#444"> <h5 style="margin:0; text-transform:uppercase; color:#fff;"><strong>' . $first_heading_text . '</strong></h5>';
            $html .= '</th>';
            $html .= '</tr>';
            $html .= $up_area;
            if (!empty($down_area)) {
                $html .= '<tr>';
                $html .= '<th class="text-center" colspan="4" bgcolor="#444"> <h5 style="margin:0; text-transform:uppercase; color:#fff;"><strong>' . $second_heading_text . '</strong></h5>';
                $html .= '</th>';
                $html .= '</tr>';
                $html .= $down_area;
            }
            $html .= '</tbody>';
            $html .= '</table>';
        }

        return $html;
    }

}