<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use App\Settings;
use App\Navigations;
use App\NavigationSections;

class NavHelpers {

    public static function getEmailHeaderMenus() {

        $model = NavigationSections::where("Title", "Email Header Menus")->first();

        $menu_html = '';
        foreach ($model->navigations as $menu) {
          //  $menu_html .= '<li style="display: table-cell; auto; text-align: center; margin-left:10px;"><a href="' . env("APP_URL") . $menu->LinkWithURL . '" target="_blank" style="color:#fff;text-transform:uppercase;padding:10px;font-size:12px;text-decoration:none;border:1px solid #fff;margin:0 5px;display:inline-block">' . $menu->Name . '</a></li>';
                    $menu_html .= '<li style="display: table-cell; "><a href="' . env("APP_URL") . $menu->LinkWithURL . '" target="_blank" style="color:#fff;padding:10px;font-size:12px;text-decoration:none;border:1px solid #fff;margin:5px;display:inline-block">' . strtoupper($menu->Name) .'</a></li>';
//                    $menu_html .= '<li style="display: table-cell; padding:0 5px;"><a href="#" style="color:#fff;">Contact Us</a></li>';

        }
        return $menu_html;
    }

    public static function getEmailFooterMenus() {

        $model = NavigationSections::where("Title", "Email Footer Menus")->first();

        $menu_html = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
        $menu_total_count = round(sizeof($model->navigations) / 4);
        $menu_count = 0;
        foreach ($model->navigations as $menu) {
            if ($menu_count == 0) {
                $menu_html .= '<tr>';
            }
            $menu_html .= '<td><a href="' . env("APP_URL") . $menu->LinkWithURL . '" target="_blank" style="color:#fff;text-transform:capitalize;font-size:11px;text-decoration:none;">' . $menu->Name . '</a></td>';
            $menu_count += 1;
            if ($menu_count == $menu_total_count) {
                $menu_html .= '</tr>';
                $menu_count = 0;
            }
        }
        if ($menu_count != 0) {
            $menu_html .= '</tr>';
        }
        $menu_html .= '</table>';
        return $menu_html;
    }

}
