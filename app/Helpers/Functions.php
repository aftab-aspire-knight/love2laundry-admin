<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use App\Settings;

class Functions {

    public static function replaces($data, $replaces) {

        foreach ($replaces as $key => $replace) {
            $data['body'] = str_replace("[" . $key . "]", $replace, $data['body']);
            $data['title'] = str_replace("[" . $key . "]", $replace, $data['title']);
            $data['subject'] = str_replace("[" . $key . "]", $replace, $data['subject']);
        }

        return $data;
    }
    
    public static function rawAmount($amount) {
        $float_amount = (float) $amount;
        $exponent = 2;
        $int_amount = (int) round($float_amount * pow(10, $exponent));
        return (string) $int_amount;
    }

    public static function getTimeRanges() {

        for ($i = 0; $i < 24; $i++) {

            $j = $i + 1;
            $time = sprintf('%02d', $i) . ":00";
            //$time = date("h:i a", strtotime($time));

            $time2 = sprintf('%02d', $j) . ":00";
            //$time2 = date("h:i a", strtotime($time2));
            if ($time2 == "24:00") {
                $time2 = "00:00";
            }

            $range[$time . "-" . $time2] = date("h:i a", strtotime($time)) . "-" . date("h:i a", strtotime($time2));
        }
        return $range;
    }

    public static function getSettings() {

        $model = Settings::get();

        $settings = array();
        foreach ($model as $row) {
            $settings[$row->Title] = $row->Content;
        }
        return $settings;
    }

}
