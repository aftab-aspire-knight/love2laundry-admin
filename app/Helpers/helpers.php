<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use Illuminate\Support\Str;
use App\Menus;
use App\UserMenus;

class Helper {

    public static function getConfigData() {

        if (isset($_SERVER["COUNTRY"])) {
            $country = strtolower($_SERVER["COUNTRY"]);
        } else {
            $country = "uk";
        }
        return Config::get($country);
    }

    public static function getPaymentMethods() {


        if (isset($_SERVER["COUNTRY"])) {
            $country = strtolower($_SERVER["COUNTRY"]);
        } else {
            $country = "uk";
        }
        return Config::get($country . ".payment_methods");
    }

    public static function getSideBar($user, $request) {

        $userMenus = UserMenus::where("FKUserID", $user->PKUserID)
                ->where("ParentMenuID", 0)
                ->orderBy("Position", "asc")
                ->get();

        $body = '<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation"><li class="navigation-header open"><span>Apps</span></li>';
        foreach ($userMenus as $userMenu) {

            $subMenus = UserMenus::where("ParentMenuID", $userMenu->PKAdminMenuID)->get();
            $menu = $userMenu->menu;
            if (count($subMenus) == 0) {

                $class = $request->is($menu->AccessURL) ? 'active' : '';
                $body .= '<li class="nav-item ' . $class . '"><a href="' . url($menu->AccessURL) . '" ><i class="' . $menu->MenuIcon . '"></i><span class="title">' . $menu->Name . '</span></a></li>';
            } else {

                //$class = $request->is($subMenu->menu->AccessURL) ? 'open' : '';
                $class = $request->is($menu->AccessURL) ? 'open' : '';
                $body .= '<li class="' . $class . '"><a href="javascript:;"><i class="fa fa-user"></i><span class="title">' . $menu->Name . '</span><span class="arrow ' . $class . '"></span></a><ul class="sub-menu">';

                foreach ($subMenus as $subMenu) {

                    $class = $request->is($subMenu->menu->AccessURL) ? 'active' : '';
                    $body .= '<li class="nav-item ' . $class . '"><a href="' . url($subMenu->menu->AccessURL) . '" ><i class="' . $subMenu->menu->MenuIcon . '"></i><span class="title">' . $subMenu->menu->Name . '</span></a></li>';
                }

                $body .= "<li></ul>";
            }
        }

        //$body .='<a href="javascript:;"><i class="fa fa-user"></i><span class="title">Users</span><span class="arrow open"></span></a><ul class="sub-menu" style="display: block;"><li><a href="https://www.love2laundry.com/admin/administrators.html"><i class="fa fa-user"></i><span class="title">Administrators</span></a></li><li><a href="https://www.love2laundry.com/admin/members.html"><i class="fa fa-user"></i><span class="title">Members</span></a></li><li><a href="https://www.love2laundry.com/admin/users.html"><i class="fa fa-user"></i><span class="title">Franchise Users</span></a></li></ul></li>';
        return $body .= '</ul>';
    }

//SELECT * FROM `cms_menus` ORDER BY `cms_menus`.`AccessURL` ASC

    public static function applClasses() {

        //dd(auth()->user());
        //return false;
        $data = config('custom.custom');

        $layoutClasses = [
            'theme' => $data['theme'],
            'sidebarCollapsed' => $data['sidebarCollapsed'],
            'navbarColor' => $data['navbarColor'],
            'menuType' => $data['menuType'],
            'navbarType' => $data['navbarType'],
            'footerType' => $data['footerType'],
            'sidebarClass' => 'menu-expanded',
            'bodyClass' => $data['bodyClass'],
            'pageHeader' => $data['pageHeader'],
            'blankPage' => $data['blankPage'],
            'blankPageClass' => '',
            'contentLayout' => $data['contentLayout'],
            'sidebarPositionClass' => '',
            'contentsidebarClass' => '',
            'mainLayoutType' => $data['mainLayoutType'],
        ];



        //Theme
        if ($layoutClasses['theme'] == 'dark')
            $layoutClasses['theme'] = "dark-layout";
        elseif ($layoutClasses['theme'] == 'semi-dark')
            $layoutClasses['theme'] = "semi-dark-layout";
        else
            $layoutClasses['theme'] = "light";

        //menu Type
        switch ($layoutClasses['menuType']) {
            case "static":
                $layoutClasses['menuType'] = "menu-static";
                break;
            default:
                $layoutClasses['menuType'] = "menu-fixed";
        }


        //navbar
        switch ($layoutClasses['navbarType']) {
            case "static":
                $layoutClasses['navbarType'] = "navbar-static";
                $layoutClasses['navbarClass'] = "navbar-static-top";
                break;
            case "sticky":
                $layoutClasses['navbarType'] = "navbar-sticky";
                $layoutClasses['navbarClass'] = "fixed-top";
                break;
            case "hidden":
                $layoutClasses['navbarType'] = "navbar-hidden";
                break;
            default:
                $layoutClasses['navbarType'] = "navbar-floating";
                $layoutClasses['navbarClass'] = "floating-nav";
        }

        // sidebar Collapsed
        if ($layoutClasses['sidebarCollapsed'] == 'true')
            $layoutClasses['sidebarClass'] = "menu-collapsed";

        // sidebar Collapsed
        if ($layoutClasses['blankPage'] == 'true')
            $layoutClasses['blankPageClass'] = "blank-page";

        //footer
        switch ($layoutClasses['footerType']) {
            case "sticky":
                $layoutClasses['footerType'] = "fixed-footer";
                break;
            case "hidden":
                $layoutClasses['footerType'] = "footer-hidden";
                break;
            default:
                $layoutClasses['footerType'] = "footer-static";
        }

        //Cotntent Sidebar
        switch ($layoutClasses['contentLayout']) {
            case "content-left-sidebar":
                $layoutClasses['sidebarPositionClass'] = "sidebar-left";
                $layoutClasses['contentsidebarClass'] = "content-right";
                break;
            case "content-right-sidebar":
                $layoutClasses['sidebarPositionClass'] = "sidebar-right";
                $layoutClasses['contentsidebarClass'] = "content-left";
                break;
            case "content-detached-left-sidebar":
                $layoutClasses['sidebarPositionClass'] = "sidebar-detached sidebar-left";
                $layoutClasses['contentsidebarClass'] = "content-detached content-right";
                break;
            case "content-detached-right-sidebar":
                $layoutClasses['sidebarPositionClass'] = "sidebar-detached sidebar-right";
                $layoutClasses['contentsidebarClass'] = "content-detached content-left";
                break;
            default:
                $layoutClasses['sidebarPositionClass'] = "";
                $layoutClasses['contentsidebarClass'] = "";
        }

        return $layoutClasses;
    }

    public static function updatePageConfig($pageConfigs) {
        $demo = 'custom';
        if (isset($pageConfigs)) {
            if (count($pageConfigs) > 0) {
                foreach ($pageConfigs as $config => $val) {
                    Config::set('custom.' . $demo . '.' . $config, $val);
                }
            }
        }
    }

}
