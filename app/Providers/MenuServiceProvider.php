<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Route;
use App\Http\Controllers\UserController;
use App\MenuHelper\Menus;
use Illuminate\Http\Request;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        $verticalMenuJson = file_get_contents(base_path('resources/json/verticalMenu.json'));
        $verticalMenuData = json_decode($verticalMenuJson);
        $horizontalMenuJson = file_get_contents(base_path('resources/json/horizontalMenu.json'));
        $horizontalMenuData = json_decode($horizontalMenuJson);
        
        \View::share('menuData',[$verticalMenuData, $horizontalMenuData]);
            //GET VALUES IN CONTROLLER
        $this->app->bind(Menus::class, function($app){
			
            return new Menus([json_decode(file_get_contents(base_path('resources/json/horizontalMenu.json'))),
                json_decode(file_get_contents(base_path('resources/json/verticalMenu.json')))]);
			
	}); 
    }
}