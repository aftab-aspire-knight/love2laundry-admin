<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Services\Country;

class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {

        
        $this->app->singleton(Country::class, function($app) {
            //
            
            
            $country = getCountry();
            
            $class = "App\Services\\" . strtoupper($country);

            $instance = new $class();
            return $instance;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Schema::defaultStringLength(191);
        Passport::routes();
    }

}
