<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NavigationSections extends Model
{
    protected $table = 'cms_navigation_sections';
    protected $primaryKey = 'PKSectionID';
   
    public $timestamps = false;
    
    public function navigations() {
        return $this->hasMany('App\Navigations', "FKSectionID");
    }
    

}
