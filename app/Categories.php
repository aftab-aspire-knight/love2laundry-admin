<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'ws_categories';
    protected $primaryKey = 'PKCategoryID';
    public $timestamps = false;
    
    public function services() {
        return $this->hasMany(Services::class, 'FKCategoryID', 'PKCategoryID');
    }

}
