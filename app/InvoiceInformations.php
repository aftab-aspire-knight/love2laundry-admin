<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceInformations extends Model
{
    protected $table = 'ws_invoice_payment_informations';
    protected $primaryKey = 'PKInvoicePaymentID';
   
    public $timestamps = false;

}
