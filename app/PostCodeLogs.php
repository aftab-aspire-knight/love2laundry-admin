<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCodeLogs extends Model
{
    protected $table = 'ws_search_post_codes';
    protected $primaryKey = 'PKSearchID';
    public $timestamps = false;
    
     public function franchises()
    {
        return $this->belongsTo(Franchises::class, 'FKFranchiseID', 'PKFranchiseID');           
    }
}
