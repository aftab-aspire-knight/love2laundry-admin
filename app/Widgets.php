<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widgets extends Model
{
    protected $table = 'cms_widgets';
    protected $primaryKey = 'PKWidgetID';
    public $timestamps = false;
}
