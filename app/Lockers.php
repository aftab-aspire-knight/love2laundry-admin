<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lockers extends Model
{
    protected $table = 'ws_lockers';
    protected $primaryKey = 'PKLockerID';
    public $timestamps = false;
}
