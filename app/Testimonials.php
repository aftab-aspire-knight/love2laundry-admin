<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    protected $table = 'ws_testimonials';
    protected $primaryKey = 'PKTestimonialID';
    public $timestamps = false;
}
