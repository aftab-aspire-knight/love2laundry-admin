<?php
namespace App\Traits;


trait EloquentTrait
{
    public static function table()
    {
        return ((new self)->getTable());
    }
}