<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseServices extends Model {

    protected $table = 'ws_franchise_services';
    protected $primaryKey = 'PKFranchiseServiceID';
    protected $casts = [];
    public $timestamps = false;

    public function service() {
        return $this->hasOne(Services::class, 'PKServiceID', 'FKServiceID');
    }
    
    
    public function category()
    {
        return $this->hasOneThrough('App\Categories', 'App\Services',"PKServiceID","PKCategoryID","FKServiceID","FKCategoryID");
    }

}
