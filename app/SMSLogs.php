<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSLogs extends Model
{
    protected $table = 'ws_sms_logs';
    protected $primaryKey = 'PKSmsLogID	';
    public $timestamps = false;
}
