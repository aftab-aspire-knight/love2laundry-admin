<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Members;
use App\Invoices;
use Carbon\Carbon;
use DB;
use App\Services\Country;
use App\Franchises;
use App\Helpers\Helper;
use Gate;
use Auth;
class DashboardController extends Controller {

    private $from;
    private $to;

    public function __construct(Country $country) {
        $this->middleware('auth');
        $this->from = date("Y-m-d", strtotime("-1 month"));
        $this->to = date("Y-m-d");
    }

    public function index(Country $country) {
        if(Gate::denies('show-invoices')){
        $count_member = Members::where("Status", "Enabled")->count();
        $count_invoices = Invoices::where("OrderStatus", "!=", "Cancel")->count();
        $count_invoices_pending = Invoices::where('OrderStatus', '=', 'Pending')->count();
        $count_invoices_completed = Invoices::where('OrderStatus', '=', 'Completed')->count();

        $franchises = Franchises::where('Status', "Enabled")->pluck("Title", "PKFranchiseId");
        $areas = $country->getAreasList();

        $range["from"] = $this->from;
        $range["to"] = $this->to;
        $data = $range;
        $data["areas"] = $areas;
        $data["franchises"] = $franchises;
        $data["pageHeader"] = TRUE;
        $data["count_member"] = $count_member;
        $data["count_invoices"] = $count_invoices;
        $data["count_invoices_pending"] = $count_invoices_pending;
        $data["count_invoices_completed"] = $count_invoices_completed;
        $data["title"] = 'Orders comparison from last month';
        $data["products_analytics_view"] = $country->productsAnalyticsView($range, $franchises);
        $data["only_members_view"] = $country->nonCustomersView($range, $franchises);


        return view('/dashboard/index', $data);
        
        }
        $user = Auth::user();
        $fk_ids= explode(",",$user->FKFranchiseID);
    
        $count_member = Members::where("Status", "Enabled")->whereIn('FKFranchiseID',$fk_ids)->count();
        $count_invoices = Invoices::where("OrderStatus", "!=", "Cancel")->whereIn('FKFranchiseID',$fk_ids)->count();
        $count_invoices_pending = Invoices::where('OrderStatus', '=', 'Pending')->whereIn('FKFranchiseID',$fk_ids)->count();
        $count_invoices_completed = Invoices::where('OrderStatus', '=', 'Completed')->whereIn('FKFranchiseID',$fk_ids)->count();

        $franchises = Franchises::where('Status', "Enabled")->whereIn('PKFranchiseID',$fk_ids)->pluck("Title", "PKFranchiseId");
        $areas = $country->getAreasList();

        $range["from"] = $this->from;
        $range["to"] = $this->to;
        $data = $range;
        $data["areas"] = $areas;
        $data["franchises"] = $franchises;
        $data["pageHeader"] = TRUE;
        $data["count_member"] = $count_member;
        $data["count_invoices"] = $count_invoices;
        $data["count_invoices_pending"] = $count_invoices_pending;
        $data["count_invoices_completed"] = $count_invoices_completed;
        $data["title"] = 'Orders comparison from last month';
        $data["products_analytics_view"] = $country->productsAnalyticsView($range, $franchises);
        $data["only_members_view"] = $country->nonCustomersView($range, $franchises);


        return view('/dashboard/index', $data);
    }

    public function membersnotinorders(Request $request) {

        $from = $request->from;
        $to = $request->to;
        $codes = $request->codes;
        $franchises = $request->franchises;
        $limit = $request->limit;

        $subString = "SUBSTRING(ws_members.PostalCode,1,length(ws_members.PostalCode)-3)";

        $model = Members::whereBetween('ws_members.CreatedDateTime', [$from . " 00:00:00", $to . " 23:59:59"]);


        $model = $model->select(\DB::raw("$subString as district"), "ws_members.PKMemberID", "ws_members.EmailAddress", "ws_members.FirstName", "ws_members.LastName", "ws_members.Phone", "ws_members.CreatedDateTime", "ws_members.PostalCode","ws_members.FKFranchiseID","f.Title",\DB::raw("max(wi.CreatedDateTime) as lastOrdered"));

        $model->leftJoin("ws_franchises as f", "f.PKFranchiseID", "ws_members.FKFranchiseID");
        $model->leftJoin("ws_invoices as wi", "wi.FKMemberID", "ws_members.PKMemberID");
        
        if (!empty($limit)) {
            
            $date = date("Y-m-d 00:00:00", strtotime("- $limit"));
            $model = $model->whereNotIn('PKMemberID', function($q) use ($date) {
                $q->select('FKMemberID')->from('ws_invoices')->whereBetween('CreatedDateTime', [$date, date("Y-m-d H:i:s")]);
            });
        } else {
            $model->leftJoin("ws_invoices as i", "i.FKMemberID", "ws_members.PKMemberID");
            $model = $model->whereNull("i.FKMemberID");
        }

        if (!empty($franchises) && !in_array("", $franchises)) {
            $model = $model->whereIn("ws_members.FKFranchiseID", $franchises);
        }
        
        if (!empty($codes) && !in_array("", $codes)) {
            $model = $model->whereIn(\DB::raw($subString), $codes);
        }
        $model= $model->groupBy("ws_members.PKMemberID");
        //dd($model->getQuery());
        $model = $model->get();
        //d($model,1);
        $data['model'] = $model;
        return view("/dashboard/ajax/members_not_in_orders", $data);
    }

    public function twomonthsorders() {

        $from = date("Y-m", strtotime("-1 month")) . "-01 00:00:00";
        $to = date("Y-m-d H:i:s");

        $data["from"] = $from;
        $data["to"] = $to;

        $invoices = Invoices::whereBetween("CreatedDateTime", [$from, $to])
                ->where('OrderStatus', "!=", 'Cancel')
                ->groupBy(\DB::raw('Date(CreatedDateTime)'))
                ->select(\DB::raw('count(PKINvoiceId) as orders'), \DB::raw('Date(CreatedDateTime) as date'))
                ->get();


        $from = date("Y-m", strtotime($from));
        $to = date("Y-m", strtotime($to));

        $dateOrders[$from] = array();
        $dateOrders[$to] = array();

        foreach ($invoices as $invoice) {
            $dateOrders[date("Y-m", strtotime($invoice->date))][(int) date("d", strtotime($invoice->date))] = $invoice->orders;
        }


        foreach ($dateOrders as $key => $orders) {
            $l = date("t", strtotime($key));
            for ($i = 1; $i <= ($l); $i++) {
                if (!key_exists($i, $orders)) {
                    $dateOrders[$key][$i] = 0;
                }
            }
            ksort($dateOrders[$key]);
        }

        $data["orders"] = $dateOrders;
        return view('/dashboard/ajax/two_months_orders', $data);
    }

    public function franchisesorders(Request $request) {

        $franchise_ids = $request->franchises;
        $to = $request->to;
        $from = $request->from;

        $model = Invoices::
                select(\DB::raw('count(PKInvoiceId) as orders'), "FKFranchiseID", \DB::raw('Date(CreatedDateTime) as date'))
                ->whereBetween("CreatedDateTime", [$from, $to])
                ->where('OrderStatus', "!=", 'Cancel')
                ->groupBy(\DB::raw('Date(CreatedDateTime)'));

        $charts = array();

        //d($franchise_ids,1);
        if (!empty($franchise_ids) && !empty($franchise_ids[0])) {
            $model = $model->whereIn("FKFranchiseID", $franchise_ids);
            $model = $model->groupBy("FKFranchiseID")->get();
            $i = 0;
            $franchise_ids = array();
            foreach ($model as $row) {
                $franchise_ids[$row->FKFranchiseID] = $row->franchises->Title;
                $charts[$row->FKFranchiseID]["title"] = $row->franchises->Title;
                $charts[$row->FKFranchiseID]["dates"][strtotime($row->date)] = $row->orders;
                $i++;
            }
        } else {
            $model = $model->get();
            $franchise_ids[0] = 0;
            $charts[0]["title"] = env("APP_NAME");
            foreach ($model as $row) {
                $charts[0]["dates"][strtotime($row->date)] = $row->orders;
            }
        }

        $date = Carbon::parse($from);
        $now = Carbon::parse($to);
        $diff = $date->diffInDays($now);

        $response = array();
        for ($i = 0; $i <= $diff; $i++) {
            $d = strtotime($from) + ( 86400 * $i );

            foreach ($franchise_ids as $key => $franchise_id) {

                if (!isset($charts[$key]["dates"][$d])) {
                    $charts[$key]["dates"][$d] = 0;
                }
                ksort($charts[$key]["dates"]);
            }
            $categories[] = date("j M, y", $d);
        }
        $i = 0;
        $series = array();
        foreach ($charts as $chart) {
            $series[$i]["name"] = $chart["title"];
            $series[$i]["data"] = array_values($chart["dates"]);
            $i++;
        }

        $data["categories"] = json_encode(array_values($categories));
        $data["series"] = json_encode($series);
        $data["chart"] = "franchisesorders";
        return view("/dashboard/ajax/chart", $data);
    }

    public function ordersbylocations(Request $request, Country $country) {


        $locations = $request->locations;
        $to = $request->to;
        $from = $request->from;
        $data = $country->ordersByLocations($from, $to, $locations);
        $data["chart"] = "ordersbylocations";
        return view("/dashboard/ajax/chart", $data);
    }

    public function districtorders(Request $request, Country $country) {

        $from = $request->from;
        $to = $request->to;
        $title = $request->title;
        $data = $country->getAreaOrders($from, $to);
        $data['title'] = $title;
        return view("/dashboard/ajax/district_orders", $data);
    }

    public function districtmembers(Request $request, Country $country) {

        $from = $request->from;
        $to = $request->to;
        $title = $request->title;
        $data = $country->getAreaMembers($from, $to);
        $data['title'] = $title;
        return view("/dashboard/ajax/district_members", $data);
    }

    public function productsanatytics(Request $request, Country $country) {

        $from = $request->from;
        $to = $request->to;

        $input = $request->all();
        $data["model"] = $country->getProductsAnatytics($input);
        return view("/dashboard/ajax/products_anatytics", $data);
    }

    public function statistics(Request $request) {
    
        if (isset($_GET['Period'])) {
            $period = $_GET['Period'];
        } else {
            $period = 7;
        }
     
        
        if(Gate::denies('show-invoices')){
            $date = date("Y-m-d 00:00:00", strtotime("- $period days"));
            $newCustomers = Members::get()->where('CreatedDateTime', '>', $date);

            $customerIds = array();
            foreach ($newCustomers as $customer) {
                $customerIds[] = $customer->PKMemberID;
            }
            $newOrders = Invoices::get()
                    ->whereIn("PaymentStatus", array("Completed"))
                    ->where('CreatedDateTime', '>', $date);

            $newOrderSum = 0.00;

            foreach ($newOrders as $o) {
                $newOrderSum = $newOrderSum + $o->GrandTotal;
            }

            $orderNewCustomers = Invoices::whereIn("FKMemberID", $customerIds)
                    ->whereIn("PaymentStatus", array("Completed"))
                    //->where('members.CreatedDateTime', '>', Carbon::now()->subDays($period))
                    ->get();

            $orderIds = array();
            $orderNewCustomersSum = 0.00;
            foreach ($orderNewCustomers as $o) {
                $orderNewCustomersSum = $orderNewCustomersSum + $o->GrandTotal;
                $orderIds[$o->FKMemberID][] = $o;
            }

            // repeated customers using model
            $repeated_members = Invoices::where('CreatedDateTime', '>', $date)
                    ->groupBy('FKMemberID')
                    ->havingRaw('count("FKMemberID") >1')
                    ->get();

            $data = array(
                "New_customers" => count($customerIds),
                "No_of_orders" => count($newOrders),
                "Amount_of_orders" => number_format((float) $newOrderSum, 2, '.', ''),
                "No_of_order_of_new_Customers" => count($orderNewCustomers),
                "Amount_of_order_of_new_Customers" => number_format((float) $orderNewCustomersSum, 2, '.', ''),
                "Repeated_Customers" => $repeated_members->count());

            $newJsonString = json_encode($data, JSON_PRETTY_PRINT);

            file_put_contents(base_path('resources/lang/en.json'), stripslashes($newJsonString));

            print_r($newJsonString);
    }else{
        $date = date("Y-m-d 00:00:00", strtotime("- $period days"));
        $user = Auth::user();
        $fk_ids= explode(",",$user->FKFranchiseID);
        $newCustomers = Members::get()->where('CreatedDateTime', '>', $date)->whereIn('FKFranchiseID',$fk_ids);

        $customerIds = array();
        foreach ($newCustomers as $customer) {
            $customerIds[] = $customer->PKMemberID;
        }
        $newOrders = Invoices::get()
                ->whereIn("PaymentStatus", array("Completed"))
                ->whereIn('FKFranchiseID',$fk_ids)
                ->where('CreatedDateTime', '>', $date);

        $newOrderSum = 0.00;

        foreach ($newOrders as $o) {
            $newOrderSum = $newOrderSum + $o->GrandTotal;
        }

        $orderNewCustomers = Invoices::whereIn("FKMemberID", $customerIds)
                ->whereIn('FKFranchiseID',$fk_ids)
                ->whereIn("PaymentStatus", array("Completed"))
                //->where('members.CreatedDateTime', '>', Carbon::now()->subDays($period))
                ->get();

        $orderIds = array();
        $orderNewCustomersSum = 0.00;
        foreach ($orderNewCustomers as $o) {
            $orderNewCustomersSum = $orderNewCustomersSum + $o->GrandTotal;
            $orderIds[$o->FKMemberID][] = $o;
        }

        // repeated customers using model
        $repeated_members = Invoices::where('CreatedDateTime', '>', $date)
                ->whereIn('FKFranchiseID',$fk_ids)
                ->groupBy('FKMemberID')
                ->havingRaw('count("FKMemberID") >1')
                ->get();

        $data = array(
            "New_customers" => count($customerIds),
            "No_of_orders" => count($newOrders),
            "Amount_of_orders" => number_format((float) $newOrderSum, 2, '.', ''),
            "No_of_order_of_new_Customers" => count($orderNewCustomers),
            "Amount_of_order_of_new_Customers" => number_format((float) $orderNewCustomersSum, 2, '.', ''),
            "Repeated_Customers" => $repeated_members->count());

        $newJsonString = json_encode($data, JSON_PRETTY_PRINT);

        file_put_contents(base_path('resources/lang/en.json'), stripslashes($newJsonString));

          print_r($newJsonString);
    }
    }

}
