<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Settings;

class SettingsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['url'] = "settings";
        $this->breadcrumbs[1]['name'] = "Settings";
    }

    public function index() {

        $setting_record = Settings::get();
        
        $data["pageHeader"] = false;
        $data["model"] = $setting_record;
        $data["title"] = "Settings";
        $data["SubTitle"] = "settings";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('settings/index', $data);
    }

    public function update(Request $postData) {

        $active_tab = $postData->active_tab;

        $setting_record = Settings::where('Title', 'App Settings Version')->first();

        $input = $postData->all();
        foreach ($input['IDS'] as $key => $ids) {
            $data['Content'] = $input['Content'][$key];
            $data["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $data["UpdatedBy"] = Auth::user()->PKUserID;
            $data["PKSettingID"] = $ids;

            Settings::where('PKSettingID', '=', $ids)->update($data);
        }
        \Session::flash('success', 'Settings Updated successfully!');
        return redirect('settings');
       // d($input);
    }

}
