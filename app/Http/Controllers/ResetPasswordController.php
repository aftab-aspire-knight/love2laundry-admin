<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\Token;
use Carbon\Carbon;
use Mail;
use Redirect;
use DB;
use App\Helpers\PasswordEncryptHelper;
class ResetPasswordController extends Controller
{
       //Recover Password
    public function recover_password(Request $request){
        
        $rules = [
           
            'email' => 'required|string',           
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
        
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            //generate token
            $rand_token= Str::random(12);
            $users = User::where("EmailAddress","=",$input['email'])->first();
            $mytime = Carbon::now();
            if(empty($tokens)){
                DB::beginTransaction();
                try {
                    $token= new Token();
                    $token->FKUserID=$users->PKUserID;
                    $token->Token=$rand_token;
                    $token->CreatedDateTime=$mytime->toDateTimeString();
                    $token->save();

                    DB::commit();
                     // all good
                } catch (\Exception $e) {
                     DB::rollback();
                     // something went wrong
                }
            }else{
                DB::beginTransaction();
                try {
                    $token_val = Token::find($tokens->FKUserID);
                    $token_val->Token=$rand_token;
                    $token_val->CreatedDateTime=$mytime->toDateTimeString();
                    $token_val->save();

                    DB::commit();
                    // all good
                } catch (\Exception $e) {
                     DB::rollback();
                      // something went wrong
                 } 
            }
            $data = array('user'=>$users, 'token'=>$rand_token);
            Mail::send('user.password reset.email_layout', $data, function ($m) use ($users) {
                $m->from(env("MAIL_FROM_ADDRESS"), env("MAIL_FROM_NAME"));
                $m->to($users->EmailAddress, $users->FirstName . " " . $users->LastName)->subject("Request Password Reset");
                                          
                });
                   return redirect("login");
        }
                               
    }
    
    public function change_password(Request $request){
        $rules = [
           
            'user-email' => 'required',
            'user-password'=>'required',
            'user-confrim-password'=>'same:user-password'          
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else{
            $mytime = Carbon::now();
            $token=$request->get('token');              
            $token_val= Token::where("Token","=",$token)->first();
            DB::beginTransaction();

            try {
                $user_val= User::find($token_val->FKUserID);
                $user_val->EmailAddress=$request->get('user-email');
                $user_val->Password=PasswordEncryptHelper::encrypt_decrypt('encrypt',$request->get('user-password'));
                $user_val->UpdatedDateTime=$mytime->toDateTimeString();
                $user_val->save();

                DB::commit();
                 // all good
            } catch (\Exception $e) {
               DB::rollback();
                 // something went wrong
            }
            return redirect("login");
                         
        }
       
   }
}
    