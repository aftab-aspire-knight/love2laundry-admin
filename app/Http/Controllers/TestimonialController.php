<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonials;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends BaseController
{
    public function __construct() {
          
        $this->middleware('auth');
        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "testimonials";
        $this->breadcrumbs[1]['name'] = "Testimonials";
    }
    
    public function index() {
         
        $testimonials= Testimonials::get();
        $data["pageHeader"] = false;
        $data["model"] = $testimonials;
        $data["title"] = "Testimonials";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('testimonials/index', $data);
       
    }
    
    public function create() {
           
        $data["pageHeader"] = false;
        $data["title"] = "Testimonials";
        $data["subTitle"] = "Create";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
   

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('testimonials/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
            'Title' => 'required|string',
            'Name' => 'required|string',
            'EmailAddress' => 'required|string|email',
            'Content'=>'required|string',
            'TestimonialDate'=>'required|string',

        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            $input["TestimonialDate"]=str_replace(","," ",$input["TestimonialDate"]);

            $input["TestimonialDate"] = date("Y-m-d", strtotime($input["TestimonialDate"]));

            Testimonials::insertGetId($input);

            \Session::flash('success', 'Testimonial Added Succesfully!');
            return redirect('testimonials');
            //d($input);
          
        }
    }

    public function show($id) {
          
        $q = new Testimonials();
        $testimonials=$q->find($id);
        
        $data['model'] =$testimonials;
        $data["title"] = "Testimonials";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
   
       return view('testimonials/show', $data);
  
    }
    public function delete($id) {

        if ($id != 1) {
            Testimonials::where('PKTestimonialID', $id)->delete();
        
        }
        \Session::flash('error', 'Testimonial deleted successfully!');
        return redirect('testimonials');
    }
    
    public function edit($id) {

        $q = new Testimonials();
        $testimonials=$q->find($id);
 
        $data['model'] =$testimonials;

        $data["title"] = "Testimonials";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('testimonials/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

         $rules = [
           
            'Title' => 'required|string',
            'Name' => 'required|string',
            'EmailAddress' => 'required|string|email',
            'Content'=>'required|string',
            'TestimonialDate'=>'required|string',

        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
                  
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }


            unset($input["_token"]);
            $input["TestimonialDate"]=str_replace(","," ",$input["TestimonialDate"]);

            $input["TestimonialDate"] = date("Y-m-d", strtotime($input["TestimonialDate"]));

            Testimonials::where('PKTestimonialID', '=', $id)->update($input);
            

            \Session::flash('success', 'Testimonial Updated successfully!');
            return redirect('testimonials');
                 

        }
    }

   
}
