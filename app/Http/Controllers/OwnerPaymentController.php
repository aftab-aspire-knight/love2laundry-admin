<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Franchises;
use Illuminate\Support\Facades\Validator;
use App\Invoices;
use App\Members;
use DataTables;
class OwnerPaymentController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "payments";
        $this->breadcrumbs[1]['name'] = "Payment";
    }
    
    public function index() {

        $data["franchise"]= Franchises::pluck('Title','PKFranchiseID');
        $data["date"]=date("j F, Y");
        $data["pageHeader"] = false;
        $data["model"] = null;
        $data["title"] = "Owner Payment";
        $data["SubTitle"] = "";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('payments/index', $data);

    }
    
    public function save(Request $request) {
        
        $rules = [
            'FKFranchiseID' => 'required',
            'not-paid' => 'required',
            'paid' => 'required',
            'StartDate'=>'required',
            'EndDate'=>'required',
        ];
        $input = $request->all();
        
        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $response['messages']=$errors;
            $header = 402;
        } else {
            $input["StartDate"]=str_replace(","," ",$input["StartDate"]);
            $input["EndDate"] = str_replace(",", " ",  $input["EndDate"] );
            $input["StartDate"] = date("Y-m-d", strtotime($input["StartDate"]));
            $input["EndDate"] = date("Y-m-d", strtotime($input["EndDate"]));

            $fk_franchise=$input["FKFranchiseID"];

            $invoices=Invoices::with('members')->whereBetween('CreatedDateTime', [$input["StartDate"], $input["EndDate"]])
                    ->whereIn('FKFranchiseID',$fk_franchise)
                    ->get();
            
            $members_array=array();
  
              $response["message"] = "Invoice added successfully!";
              $response["value"] = json_encode($invoices);
                    $header = 200;
                    $error = false;
        }
         return response()->json($response, $header);

    } 
    public function UpdatePaymentStatus(){
         $data = $_GET['IDS'];
         $invoices=array();
         foreach ($data as $row){
            
         $invoices=  Invoices::where('PKInvoiceID', '=', $row)->update(['OwnerPaymentStatus'=>'Yes']);
         echo $invoices;

         }
           
   
    }
    public function UpdatePaymentStatusToUnPaid(){
           $data = $_GET['IDS'];
         $invoices=array();
         foreach ($data as $row){
            
         $invoices=  Invoices::where('PKInvoiceID', '=', $row)->update(['OwnerPaymentStatus'=>'No']);
         echo $invoices;

         } 
    }

}
