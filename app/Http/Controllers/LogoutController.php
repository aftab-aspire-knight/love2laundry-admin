<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class LogoutController extends Controller {

    // Dashboard - Analytics

    public function __construct() {
        $this->middleware('auth');
    }

    public function logout(Request $request)
    {
        //$request->user()->token()->revoke();
        Auth::logout();
        return redirect('login');

    }
}