<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;
use Carbon\Carbon;
use DB;
use App\Helpers\PasswordEncryptHelper;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    public function user_profile_page() {
        $data["pageHeader"] = false;
        $data["title"] = "Profile";

        $data["record"] = Auth::user();
        return view('/user/profile/user-profile-page', $data);
    }

    public function update_profile(Request $request) {

        $validator = Validator::make($request->all(), [
                    'fname' => 'required',
                    'lname' => 'required',
                    'email' => 'required',
                        ], [
                    'fname.required' => 'First Name required',
                    'lname.required' => 'Last Namre required',
                    'email.required' => ' Email required'
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        } else {
            DB::beginTransaction();

            try {
                $id = $request->get('invisible');
                $mytime = Carbon::now();
                $user = User::find($id);
                $user->FirstName = $request->get('fname');
                $user->LastName = $request->get('lname');
                $user->EmailAddress = $request->get('email');
                // $user->Password=PasswordEncryptHelper::encrypt_decrypt('encrypt',$request->get('password'));
                $user->UpdatedDateTime = $mytime->toDateTimeString();
                //echo 'hello'.$user;
                $user->save();
                //echo 'hello'.$user;

                DB::commit();
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
            }
            Auth::logout();

            return redirect('login');
        }
    }

    public function update_user_password_page() {
        $data["pageHeader"] = false;
        $data["title"] = "Change Password";

        $data["record"] = Auth::user();
        return view('/user/profile/update-user-password', $data);
    }

    public function update_user_password(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required',
                    'conf-password' => 'same:password'
                        ], [
                    'email.required' => ' Email required',
                    'password.required' => 'Password required'
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator->errors());
        } else {

            DB::beginTransaction();

            try {
                $id = $request->get('id');
                $mytime = Carbon::now();
                $user = User::find($id);
                $user->EmailAddress = $request->get('email');
                $user->Password = PasswordEncryptHelper::encrypt_decrypt('encrypt', $request->get('password'));
                $user->UpdatedDateTime = $mytime->toDateTimeString();
                $user->save();
                //  echo 'hello'.$user;

                DB::commit();
                // all good
            } catch (Exception $e) {
                DB::rollback();
                // something went wrong
            }
            Auth::logout();

            return redirect('login');
        }
    }

//    
//    public function check(\App\MenuHelper\Menus $menus){
//          $arr_menu= $menus->charge();
//           // dd($arr_menu);
////          foreach($arr_menu as $men){
////              dd($men->menu);
////          }
//          echo "<pre>".print_r($arr_menu, true)."</pre>";
//       
//    }
}
