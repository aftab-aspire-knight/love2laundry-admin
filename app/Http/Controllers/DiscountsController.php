<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Discounts;
use App\Members;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;

class DiscountsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "discounts";
        $this->breadcrumbs[1]['name'] = "Discounts";
    }

    public function index() {


        $model = Discounts::orderBy("PKDiscountID", "desc")->get();
        $data["pageHeader"] = false;

        $data["title"] = "Discounts";
        $data["SubTitle"] = "List";
        $data["model"] = $model;
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('discounts/index', $data);
    }

    public function create() {

        $model = new Discounts();

        $data["pageHeader"] = false;
        $data["title"] = "Discount";
        $data["SubTitle"] = "List";
        $data["model"] = $model;

        $members = Members::where("Status", "Enabled")->pluck("EmailAddress", "PKMemberId");
        $members[""] = "None";
        $members = json_decode(json_encode($members), true);
        $data["members"] = array_reverse($members, true);
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('discounts/create', $data);
    }

    public function save(Request $request) {
        $rules = [
            'Code' => 'required|string',
            'Worth' => 'required|numeric',
            'MinimumOrderAmount' => 'required|numeric'
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            unset($input["_token"]);

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;



            if (!empty($input["StartDate"])) {
                $startDate = date("Y-m-d", strtotime($input["StartDate"]));
                $input["StartDate"] = $startDate;
            }


            if (!empty($input["StartDate"])) {
                $expireDate = date("Y-m-d", strtotime($input["ExpireDate"]));
                $input["ExpireDate"] = $expireDate;
            }

            $response["id"] = Discounts::insertGetId($input);


            $response["message"] = "Discount added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function edit($id) {

        $model = Discounts::find($id);

        if (!empty($model->StartDate)) {
            $model->StartDate = date("j F,Y", strtotime($model->StartDate));
        }
        if (!empty($model->ExpireDate)) {
            $model->ExpireDate = date("j F,Y", strtotime($model->ExpireDate));
        }

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Discount (" . $model->Code . ")";
        $data["SubTitle"] = "Edit";


        $members = Members::where("Status", "Enabled")->pluck("EmailAddress", "PKMemberId");
        $members[""] = "None";
        $members = json_decode(json_encode($members), true);
        $data["members"] = array_reverse($members, true);

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit (" . $model->Code . ")";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('discounts/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {



        $rules = [
            'Code' => 'required|string',
            'Worth' => 'required|numeric',
            'MinimumOrderAmount' => 'required|numeric'
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            unset($input["_token"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["UpdatedBy"] = Auth::user()->PKUserID;



            if (!empty($input["StartDate"])) {
                $startDate = date("Y-m-d", strtotime($input["StartDate"]));
                $input["StartDate"] = $startDate;
            } else {
                $input["StartDate"] = "";
            }


            if (!empty($input["StartDate"])) {
                $expireDate = date("Y-m-d", strtotime($input["ExpireDate"]));
                $input["ExpireDate"] = $expireDate;
            } else {
                $input["ExpireDate"] = "";
            }

            $response["id"] = Discounts::where("PKDiscountID", $id)->update($input);


            $response["message"] = "Discount added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function show($id) {

        $model = Discounts::find($id);

        if (!empty($model->StartDate)) {
            $model->StartDate = date("j F,Y", strtotime($model->StartDate));
        }
        if (!empty($model->ExpireDate)) {
            $model->ExpireDate = date("j F,Y", strtotime($model->ExpireDate));
        }

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Discount (" . $model->Code . ")";
        $data["SubTitle"] = "View";


        $data["member"] = $model->member;
        
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $model->Code;

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('discounts/show', $data)->with('id', $id);
    }
    
    public function delete($id) {
        
        $model = Discounts::where("PKDiscountID",$id)->delete();
        
        \Session::flash('error', 'Discount Code deleted successfully!');
        return redirect('discounts');
        
    }

}
