<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Franchises;
use App\DisableDates;
use App\DisableTimes;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Functions;

class UnavailableTimesController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "franchises";
        $this->breadcrumbs[1]['name'] = "Franchises";
    }

    public function index($id) {

        $franchise = Franchises::find($id);

        $data["pageHeader"] = false;
        $data["franchise"] = $franchise;
        $data["title"] = "Franchise";
        $data["SubTitle"] = "List";

        $this->breadcrumbs[2]['link'] = "franchises/unavailabletimes/" . $id;
        $this->breadcrumbs[2]['name'] = "Unavailable Times (" . $franchise->Title . ")";

        $model = DisableDates::where("FKFranchiseID", $id)->orderBy("PKDateID", "desc")->get();

        $data["model"] = $model;
        $data['breadcrumbs'] = $this->breadcrumbs;
        $data["franchise_id"] = $id;

        return view('unavailable_times/index', $data)->with('id', $id);
    }

    public function create($franchise_id) {

        $franchise = Franchises::find($franchise_id);

        $range = Functions::getTimeRanges();
        $data["range"] = $range;
        $data["pageHeader"] = false;
        $data["franchise"] = $franchise;
        $data["title"] = "Franchise";
        $data["SubTitle"] = "List";
        $data["time"] = "All Day";

        $data["selectedTime"] = array();
        $data["franchise_id"] = $franchise_id;
        $this->breadcrumbs[2]['link'] = "franchises/unavailabletimes/" . $franchise_id;
        $this->breadcrumbs[2]['name'] = "Unavailable Times (" . $franchise->Title . ")";

        $this->breadcrumbs[3]['link'] = "#_";
        $this->breadcrumbs[3]['name'] = "Create";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('unavailable_times/create', $data)->with('franchise_id', $franchise_id);
    }

    public function save(Request $request) {
        $rules = [
            'DisableDateFrom' => 'required|string',
            'DisableDateTo' => 'required|string',
            'Type' => 'required|string',
            'Time' => 'required|string',
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {
            $time = $input["Time"];
            $timings = array();

            if ($time == "Specific") {
                $timings = $input["timings"];
            }
            unset($input["timings"]);
            unset($input["Time"]);
            unset($input["_token"]);

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;


            $disableDateFrom = date("Y-m-d", strtotime($input["DisableDateFrom"]));
            $disableDateTo = date("Y-m-d", strtotime($input["DisableDateTo"]));

            $input["DisableDateFrom"] = $disableDateFrom;
            $input["DisableDateTo"] = $disableDateTo;
            $response["id"] = DisableDates::insertGetId($input);

            $dateFromUnix = strtotime($disableDateFrom); // Convert date to a UNIX timestamp
            $dateToUnix = strtotime($disableDateTo); // Convert date to a UNIX timestamp

            for ($i = $dateFromUnix; $i <= $dateToUnix; $i += 86400) {
                $date = date("Y-m-d", $i);
                $disableTimes = array();
                if ($time == "Specific") {

                    foreach ($timings as $time) {
                        $insert = array();
                        $insert["FKFranchiseID"] = $input["FKFranchiseID"];
                        $insert["FKDateID"] = $response["id"];
                        $insert["Time"] = $time;
                        $insert["Type"] = $input["Type"];
                        $insert["Date"] = $date;
                        $disableTimes[] = $insert;
                    }
                } else {
                    $insert = array();
                    $insert["FKFranchiseID"] = $input["FKFranchiseID"];
                    $insert["FKDateID"] = $response["id"];
                    $insert["Time"] = $time;
                    $insert["Type"] = $input["Type"];
                    $insert["Date"] = $date;
                    $disableTimes[] = $insert;
                }
                DisableTimes::insert($disableTimes);
            }

            $response["message"] = "Timing added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function edit($franchise_id, $id) {

        $franchise = Franchises::find($franchise_id);

        $range = Functions::getTimeRanges($franchise_id);

        $model = DisableDates::find($id);

        $model->DisableDateFrom = date("j F, Y", strtotime($model->DisableDateFrom));
        $model->DisableDateTo = date("j F, Y", strtotime($model->DisableDateTo));
        $data["franchise_id"] = $franchise_id;

        $modelDisableTimes = DisableTimes::where("FKDateID", $id)->get();
        $selectedTime = array();
        foreach ($modelDisableTimes as $t) {
            $selectedTime[$t->Time] = $t->Time;
        }

        $time = "Specific";
        if (in_array("All Day", $selectedTime)) {
            $time = "All Day";
        }

        $data["selectedTime"] = $selectedTime;
        $data["model"] = $model;
        $data["time"] = $time;
        $data["range"] = $range;
        $data["pageHeader"] = false;
        $data["franchise"] = $franchise;
        $data["title"] = "Franchise";
        $data["SubTitle"] = "List";
        $data["id"] = $id;


        $this->breadcrumbs[2]['link'] = "franchises/unavailabletimes/" . $franchise_id;
        $this->breadcrumbs[2]['name'] = "Unavailable Times (" . $franchise->Title . ")";

        $this->breadcrumbs[3]['link'] = "#_";
        $this->breadcrumbs[3]['name'] = "Edit";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('unavailable_times/edit', $data)->with('franchise_id', $franchise_id);
    }

    public function update($id, Request $request) {

        $rules = [
            'DisableDateFrom' => 'required|string',
            'DisableDateTo' => 'required|string',
            'Type' => 'required|string',
            'Time' => 'required|string',
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            $time = $input["Time"];
            $timings = array();

            if ($time == "Specific") {
                $timings = $input["timings"];
            }
            unset($input["timings"]);
            unset($input["Time"]);
            unset($input["_token"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["UpdatedBy"] = Auth::user()->PKUserID;
            $disableDateFrom = date("Y-m-d", strtotime($input["DisableDateFrom"]));
            $disableDateTo = date("Y-m-d", strtotime($input["DisableDateTo"]));

            $input["DisableDateFrom"] = $disableDateFrom;
            $input["DisableDateTo"] = $disableDateTo;
            DisableDates::where("PKDateID", $id)->update($input);


            $dateFromUnix = strtotime($disableDateFrom); // Convert date to a UNIX timestamp
            $dateToUnix = strtotime($disableDateTo); // Convert date to a UNIX timestamp


            DisableTimes::where("FKDateID", $id)->delete();

            for ($i = $dateFromUnix; $i <= $dateToUnix; $i += 86400) {
                $date = date("Y-m-d", $i);
                $disableTimes = array();
                if ($time == "Specific") {

                    foreach ($timings as $time) {
                        $insert = array();
                        $insert["FKFranchiseID"] = $input["FKFranchiseID"];
                        $insert["FKDateID"] = $id;
                        $insert["Time"] = $time;
                        $insert["Type"] = $input["Type"];
                        $insert["Date"] = $date;
                        $disableTimes[] = $insert;
                    }
                } else {
                    $insert = array();
                    $insert["FKFranchiseID"] = $input["FKFranchiseID"];
                    $insert["FKDateID"] = $id;
                    $insert["Time"] = $time;
                    $insert["Type"] = $input["Type"];
                    $insert["Date"] = $date;
                    $disableTimes[] = $insert;
                }
                DisableTimes::insert($disableTimes);
            }

            $response["message"] = "Timing added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function delete($franchise_id, $id) {

        DisableTimes::where("FKDateID", $id)->delete();
        DisableDates::where('PKDateID', $id)->delete();
        \Session::flash('error', 'Record deleted successfully!');
        return redirect('franchises/unavailabletimes/' . $franchise_id);
    }

}
