<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banners;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use File;

class BannersController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "banners";
        $this->breadcrumbs[1]['name'] = "Banners";
    }

    public function index() {
        $banners = Banners::paginate(20);

        $data["pageHeader"] = false;
        $data["model"] = $banners;
        $data["title"] = "Banners";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('banners/index', $data);
    }

    public function create() {

        $data["pageHeader"] = false;
        $data["title"] = "Banners";
        $data["subTitle"] = "Create";
        $data['model']['image_route'] = null;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        $data['model']['member_preferences_array'] = null;
        $data['model']['image'] = null;
     
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('banners/create', $data);
    }

    public function save(Request $request) {

        $rules = [
            'Title' => 'required|string',
                //'Image' => 'required|string',
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if ($request->hasFile('ImageName')) {

                //get filename with extension
                $filenamewithextension = $request->file('ImageName')->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $request->file('ImageName')->getClientOriginalExtension();

                //filename to store
                $filenametostore = $filename . '.' . $extension;
                $array = config('params.dir_upload_banners');
                foreach ($array as $row) {
                    //Upload File to external server
                    Storage::disk('ftp')->put($row . $filenametostore, fopen($request->file('ImageName'), 'r+'));
                }
                unset($input["ImageName"]);

                $input['ImageName'] = $filenametostore;

                //Store $filenametostore in the database
            } 
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);

            Banners::insertGetId($input);
            \Session::flash('success', 'Banners Added Succesfully!');
            return redirect('banners');
        }
    }

    public function show($id) {

        $q = new Banners();
        $banners = $q->find($id);

        $data['model'] = $banners;
        $data["title"] = "Banners";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";

        return view('banners/show', $data);
    }

    public function delete($id) {
        $q = new Banners();
        $bann = $q->find($id);
        //get path
        $imagepath=config('params.dir_upload_banners');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$bann->ImageName);
            if($exist==1){
             //delete from storage
                Storage::disk('ftp')->delete($row.$bann->ImageName); 
            }   
        }  
        
        Banners::where('PKBannerID', $id)->delete();
        \Session::flash('error', 'Banner deleted successfully!');
        return redirect('banners');

    }

    public function edit($id) {

        $q = new Banners();
        $banners = $q->find($id);
     
        $data['model'] = $banners;
     
        $data['model']['image'] = $banners->ImageName;

        $data["title"] = "Banners";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('banners/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string',
//            'Content' => 'required|string',
        ];
        $input = $request->all();

        $message = [];

        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
        

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            unset($input["_token"]);
            // mobile image
            if ($request->hasFile('ImageName')) {
                //delete image from ftp start
                //  get image name from id 
                $q = new Banners();
                $bann = $q->find($id);
                
                //get path
                $route=config('params.dir_upload_banners');
                foreach ($route as $row){
                    if($bann->ImageName==null){
                        $imagepath=$row."null";

                    }else{
                        $imagepath=$row.$bann->ImageName;

                    }
                    //file exists
                    $exist=Storage::disk('ftp')->exists($imagepath);
                    if($exist==1){
                        Storage::disk('ftp')->delete($imagepath); 
                    }  
                }
               
                //delete image from ftp end

                //get filename with extension
                $filenamewithextension = $request->file('ImageName')->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $request->file('ImageName')->getClientOriginalExtension();

                //filename to store
                $filenametostore = $filename . '.' . $extension;
                $array = config('params.dir_upload_banners');
                foreach ($array as $row) {
                    //Upload File to external server
                    Storage::disk('ftp')->put($row . $filenametostore, fopen($request->file('ImageName'), 'r+'));
                }
                unset($input["ImageName"]);

                $input['ImageName'] = $filenametostore;

                //Store $filenametostore in the database
            }
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            Banners::where('PKBannerID', '=', $id)->update($input);

            \Session::flash('success', 'Banner Updated successfully!');
            return redirect('banners');
            // d($input);             
        }
    }
    
    public function RemoveImage($id) {
//        $input['ImageName'] = null;
//        
//        $banner=Banners::where('PKBannerID', '=', $id)->update($input);
//
//        return $this->edit($id);
           
        //  get image name from id 
        $q = new Banners();
        $bann = $q->find($id);
        //get path
        $imagepath=config('params.dir_upload_banners');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$bann->ImageName);
            if($exist==1){
             //delete from storage
                Storage::disk('ftp')->delete($row.$bann->ImageName); 
            }   
        }  
        //delete from db
                
        $input['ImageName'] = null;
        Banners::where('PKBannerID', '=', $id)->update($input);
        return $this->edit($id);
        

    }

}

