<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use App\Categories;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ServiceController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "services";
        $this->breadcrumbs[1]['name'] = "Services";
    }
    
     public function index() {

        $services= Services::get();
        $cat = Categories::pluck('Title','PKCategoryID');

        $data["pageHeader"] = false;
        $data["model"] = $services;
        $data["name"] = $cat;
        $data["title"] = "Services";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('service/index', $data);
       
    }  
    
    public function create() {
           
        $data["pageHeader"] = false;
        $data["title"] = "Services";
        $data["subTitle"] = "Create";
        $data["name"]= Categories::pluck('Title','PKCategoryID');
        $data['model']['mobile_image']=null;
        $data['model']['desktop_image']=null;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
   

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('service/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
            'FKCategoryID' => 'required|string',
            'Price' => 'required|string',
            'Title' => 'required|string',
            'PreferencesShow'=>'required|string'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            // mobile image
        if($request->hasFile('MobileImageName')) {
         
                //get filename with extension
            $filenamewithextension = $request->file('MobileImageName')->getClientOriginalName();
 
                 //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
                  //get file extension
            $extension = $request->file('MobileImageName')->getClientOriginalExtension();
 
                //filename to store
            $filenametostore = $filename.'.'.$extension;
            $array=config('params.dir_upload_service');
            
            foreach ($array as $row){
                //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('MobileImageName'), 'r+'));
            }
            unset($input["MobileImageName"]);

              $input['MobileImageName']=$filenametostore;

        }
    
            //desktop image
        if($request->hasFile('DesktopImageName')) {
         
                //get filename with extension
            $filenamewithextension = $request->file('DesktopImageName')->getClientOriginalName();
 
                 //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
                  //get file extension
            $extension = $request->file('DesktopImageName')->getClientOriginalExtension();
 
                //filename to store
            $filenametostore = $filename.'.'.$extension;
             $array=config('params.dir_upload_service');
            foreach ($array as $row){
                //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('DesktopImageName'), 'r+'));
            }
            unset($input["DesktopImageName"]);

              $input['DesktopImageName']=$filenametostore;

        }
         
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
        
            $userId = Services::insertGetId($input);
           

            \Session::flash('success', 'Service Added Succesfully!');
            return redirect('services');
          
        }
    }
    
    public function show($id) {
          
        $q = new Services();
        $service=$q->find($id);
        
        $cat= Categories::find($service->FKCategoryID);
        $data['model'] =$service;
        $data['category']=$cat;
        $data["title"] = "Services";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
   
       return view('service/show', $data);
  
    }
    
    public function delete($id) {

        if ($id != 1) {
            Services::where('PKServiceID', $id)->delete();
        
        }
        \Session::flash('error', 'Service deleted successfully!');
        return redirect('services');
    }
    
    public function edit($id) {

        $q = new Services();
        $service=$q->find($id);
 
        $data['model'] =$service;
        $data["name"]= Categories::pluck('Title','PKCategoryID');

    
        $data['model']['mobile_image']=$service->MobileImageName;
        $data['model']['desktop_image']=$service->DesktopImageName;
        $data["title"] = "Services";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('service/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

           $rules = [
           
            'FKCategoryID' => 'required|string',
            'Price' => 'required|string',
            'Title' => 'required|string',
            'PreferencesShow'=>'required|string'
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
             $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
      
                  
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            
             // mobile image
            if($request->hasFile('MobileImageName')) {
         
                //get filename with extension
            $filenamewithextension = $request->file('MobileImageName')->getClientOriginalName();
 
                 //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
                  //get file extension
            $extension = $request->file('MobileImageName')->getClientOriginalExtension();
           
                //filename to store
            $filenametostore = $filename.'.'.$extension;
             $array=config('params.dir_upload_service');
             foreach ($array as $row){
                //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('MobileImageName'), 'r+'));
             }
            unset($input["MobileImageName"]);

              $input['MobileImageName']=$filenametostore;

        }

            
            //desktop image
            if($request->hasFile('DesktopImageName')) {
         
                    //get filename with extension
                $filenamewithextension = $request->file('DesktopImageName')->getClientOriginalName();

                     //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                      //get file extension
                $extension = $request->file('DesktopImageName')->getClientOriginalExtension();

                    //filename to store
                $filenametostore = $filename.'.'.$extension;
                $array=config('params.dir_upload_service');
                foreach ($array as $row){
                    //Upload File to external server
                Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('DesktopImageName'), 'r+'));
                }
                unset($input["DesktopImageName"]);

                  $input['DesktopImageName']=$filenametostore;

             }
             
           Services::where('PKServiceID', '=', $id)->update($input);
            

            \Session::flash('success', 'Service Updated successfully!');
            return redirect('services');
        }
    }
    
    
    public function RemoveMobileImage($id) {

        //  get image name from id 
         $q = new Services();
        $service=$q->find($id);
        //get path
        $imagepath=config('params.dir_upload_service');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$service->MobileImageName);
            if($exist==1){
             //delete from storage
                Storage::disk('ftp')->delete($row.$service->MobileImageName); 
            }   
        }  
        //delete from db
                
        $input['MobileImageName'] = null;
        Services::where('PKServiceID', '=', $id)->update($input);
        return $this->edit($id);
        
    }
    public function RemoveDesktopImage($id) {

        //  get image name from id 
         $q = new Services();
        $service=$q->find($id);
        //get path
        $imagepath=config('params.dir_upload_service');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$service->DesktopImageName);
            if($exist==1){
             //delete from storage
                Storage::disk('ftp')->delete($row.$service->DesktopImageName); 
            }   
        }  
        //delete from db
                
        $input['DesktopImageName'] = null;
        Services::where('PKServiceID', '=', $id)->update($input);
        return $this->edit($id);
        

    }
}
