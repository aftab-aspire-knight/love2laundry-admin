<?php

namespace App\Http\Controllers;


class BaseController extends Controller
{
    protected $breadcrumbs= array();
    
    
    public function getBreadcrumbs(){
        $this->breadcrumbs[0]['link'] = "/";
        $this->breadcrumbs[0]['name'] = "Home";
        return $this->breadcrumbs;
    }
    
    
}
