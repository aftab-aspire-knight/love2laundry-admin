<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Helpers\Functions;
use App\Helpers\EmailTemplates;
use App\Helpers\StripePayment;
use App\Helpers\NavHelpers;
use App\Helpers\Tookan;
use App\Helpers\SMSGateway;
use App\Invoices;
use App\Franchises;
use App\FranchiseServices;
use App\Preferences;
use App\Members;
use App\InvoiceServices;
use App\MemberCards;
use App\Discounts;
use App\Locations;
use App\InvoicePreferences;
use App\MembersPreference;
use App\PaymentInformations;
use App\InvoiceInformations;
use App\EmailsResponders;
use Illuminate\Support\Facades\Auth;
use DataTables;
use App\FranchisePostCodes;
use App\SMSLogs;
use App\RefundInformations;
use App\SMSResponders;
use App\Services\Country;
use App\Categories;
use Mail;
use PDF;
use App\ConfigHelper\ConfigInterface;
use Dompdf\Dompdf;
use Dompdf\Options;
use Gate;

class InvoicesController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "invoices";
        $this->breadcrumbs[1]['name'] = "Invoices";
    }

    public function index() {

        $data["pageHeader"] = false;
        $data["title"] = "Invoices";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('invoices/index', $data);
    }

    public function json(Request $request) {
        if (Gate::denies('show-invoices')) {
//            return redirect(route('admin.users.index'));        
            $data = Invoices::with('members')->with('franchises');
            return Datatables::of($data)
                            ->addIndexColumn()
                            ->editColumn('OrderPostFrom', function($invoice) {

                                if ($invoice->OrderPostFrom == "Mobile") {
                                    return trim($invoice->OrderPostFrom . " " . $invoice->AppVersion);
                                } else {
                                    return $invoice->OrderPostFrom;
                                }
                            })
                            ->editColumn('members', function($invoice) {

                                if (isset($invoice->members)) {
                                    return $invoice->members->EmailAddress;
                                } else {
                                    return "N/A";
                                }
                            })->editColumn('franchises', function($invoice) {

                                if (isset($invoice->franchises->Title)) {
                                    return $invoice->franchises->Title;
                                } else {
                                    return "N/A";
                                }
                            })->editColumn('GrandTotal', function($invoice) {

                                return env("CURRENCY_SYMBOL") . $invoice->GrandTotal;
                            })
                            ->addColumn('action', function($row) {

                                $id = trim($row->PKInvoiceID);
                                $url = url("invoices/delete/" . $id);

                                $actions = '<div class="btn-group dropdown actions-dropodown">
                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                    <a class="dropdown-item" href="' . url("invoices/show/" . $id) . '" ><i class="feather icon-book"></i>View</a>'
                                        . '<a class="dropdown-item" href="' . url("invoices/edit/" . $id) . '"><i class="feather icon-edit"></i>Edit</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item confirm-delete" onclick=confirmDelete(' . $id . ',"' . $url . '") href="javascript:void(0);" data-href="' . url("invoices/delete/" . $id) . '" data-id="' . $id . '" ><i class="feather icon-trash"></i>Delete</a>
                                </div>
                            </div>';


                                return $actions;
                            })
                            ->addColumn('select', function($row) {
                                
                            })
                            ->rawColumns(['action', 'select'])
                            ->make(true);
        }
        $user = Auth::user();
        $fk_ids = explode(",", $user->FKFranchiseID);

        $data = Invoices::with('members')->with('franchises')->whereIn('FKFranchiseID', $fk_ids);



        return Datatables::of($data)
                        ->addIndexColumn()
                        ->editColumn('OrderPostFrom', function($invoice) {

                            if ($invoice->OrderPostFrom == "Mobile") {
                                return trim($invoice->OrderPostFrom . " " . $invoice->AppVersion);
                            } else {
                                return $invoice->OrderPostFrom;
                            }
                        })
                        ->editColumn('members', function($invoice) {

                            if (isset($invoice->members)) {
                                return $invoice->members->EmailAddress;
                            } else {
                                return "N/A";
                            }
                        })->editColumn('franchises', function($invoice) {

                            if (isset($invoice->franchises->Title)) {
                                return $invoice->franchises->Title;
                            } else {
                                return "N/A";
                            }
                        })->editColumn('GrandTotal', function($invoice) {

                            return env("CURRENCY_SYMBOL") . $invoice->GrandTotal;
                        })
                        ->addColumn('action', function($row) {

                            $id = trim($row->PKInvoiceID);
                            $url = url("invoices/delete/" . $id);

                            $actions = '<div class="btn-group dropdown actions-dropodown">
                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                    <a class="dropdown-item" href="' . url("invoices/show/" . $id) . '" ><i class="feather icon-book"></i>View</a>'
                                    . '<a class="dropdown-item" href="' . url("invoices/edit/" . $id) . '"><i class="feather icon-edit"></i>Edit</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item confirm-delete" onclick=confirmDelete(' . $id . ',"' . $url . '") href="javascript:void(0);" data-href="' . url("invoices/delete/" . $id) . '" data-id="' . $id . '" ><i class="feather icon-trash"></i>Delete</a>
                                </div>
                            </div>';


                            return $actions;
                        })
                        ->addColumn('select', function($row) {
                            
                        })
                        ->rawColumns(['action', 'select'])
                        ->make(true);

// }
    }

    public function member() {

        $members = Members::select("EmailAddress", "PostalCode", "PKMemberID")->where("Status", "Enabled")->get();

        $data["pageHeader"] = false;
        $data["title"] = "Invoice";
        $data["SubTitle"] = "List";
        $data["members"] = $members;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Choose Customer";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('invoices/member', $data);
    }

    public function checkfranchise(Request $request, Country $country) {

        $rules = [
            'Customer' => 'required|string',
            'PostalCode' => 'required|string',
        ];

        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);

        $response = array();
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
        } else {

            $valid = $country->validatePostalCode($input["PostalCode"]);

            if ($valid["validate"] == false) {
                $validator->errors()->add('errors', 'Invalid postal code.');
                $errors = $validator->errors();
                $response['errors'] = $errors;
                $header = 402;
            } else {

                $header = 200;
                $customer = explode("||", $input["Customer"]);


                $response["franchise_id"] = $valid["franchise_id"];
                $response["member_id"] = $customer[0];
                $response["postcode"] = $input["PostalCode"];
            }
            return response()->json($response, $header);
        }
    }

    public function create($member_id, $franchise_id, $postcode) {
        $model = new Invoices();
        $model->PickupDate = date("j F, Y");
        $model->DeliveryDate = date("j F, Y");
        
        $model->PickupTime = "1:00 - 2:00";
        $model->DeliveryTime = "3:00 - 12:00";
        
        
        $model->IPAddress = $_SERVER['REMOTE_ADDR'];
        $model->FKDiscountID = 0;
        $model->FKMemberID = $member_id;
        $model->PostalCode = $postcode;
        $member = Members::find($member_id);
        $model->BuildingName = $member->BuildingName;
        $model->StreetName = $member->StreetName;
        $model->Town = $member->Town;
        $model->FKFranchiseID = $franchise_id;

        $franchises = Franchises::where("Status", "Enabled")->pluck("Title", "PKFranchiseID");
        $cards = MemberCards::where("FKMemberID", $member->PKMemberID)->pluck("Title", "PKCardID");
        $services = FranchiseServices::where('FKFranchiseID', '=', $franchise_id)->get();
        $franchise = Franchises::find($franchise_id);
        $data["franchise"] = $franchise;
        $data["pageHeader"] = false;
        $data["title"] = "Invoice";
        $data["SubTitle"] = "List";
        $data["model"] = $model;
        $data["franchises"] = $franchises;
        $data["member"] = $member;
        $data["services"] = $services;
        $data["cards"] = $cards;
        $data["discountTotal"] = 0.00;
        $data["invoiceDiscount"] = array();
        $data["range"] = Functions::getTimeRanges();
        $data['preferences'] = array();
        $data['preferencesTotal'] = 0.00;
        $data['pref'] = 0.00;
        $data["discounts"] = self::getDiscounts();
        $data["id"] = 0;
        $data["invoice_services"] = array();
        $data["hasPreferences"] = false;
        $data["grandTotal"] = $franchise->MinimumOrderAmount;

        $l = self::getLocations();
        $data["locations"] = $l["locations"];
        $data["lockers"] = $l["lockers"];

        $memberPreferencesModel = MembersPreference::where("FKMemberID", $member_id)->get();
//d($memberPreferencesModel,1);
        $preferencesTotal = 0.00;
        $selectedPreferences = array();
        foreach ($memberPreferencesModel as $memberPreferenceModel) {
//d($memberPreferenceModel->preferences->ParentPreferenceID,1);

            $p = Preferences::find($memberPreferenceModel->preferences->ParentPreferenceID);

            $selectedPreferences[$p->Title] = $memberPreferenceModel->FKPreferenceID;
            $preferencesTotal = $preferencesTotal + $memberPreferenceModel->preferences->Price;
        }
        $categories = Preferences::where("Status", "Enabled")->where("ParentPreferenceID", 0)
                ->orderBy("Position", "asc")
                ->get();

        $preferences = array();
        foreach ($categories as $category) {

            if (count($category->preferences) > 0) {

//d($selectedPreferences[$category->Title],1);
                $preferences[$category->PKPreferenceID]["title"] = $category->Title;
                $preferences[$category->PKPreferenceID]["price_for_package"] = $category->PriceForPackage;
                $preferences[$category->PKPreferenceID]["price"] = $category->Price;
                $preferences[$category->PKPreferenceID]["selected"] = isset($selectedPreferences[$category->Title]) ? $selectedPreferences[$category->Title] : "";
                $childs = array();
                foreach ($category->preferences as $preference) {
                    $childs[$preference->PKPreferenceID] = [
                        "id" => $preference->PKPreferenceID,
                        "title" => $preference->Title,
                        "price" => $preference->Price,
                        "price_for_package" => $preference->PriceForPackage,
                    ];
                }
                $preferences[$category->PKPreferenceID]["preferences"] = $childs;
            }
        }

        $data['preferences'] = $preferences;
        $data['preferencesTotal'] = $preferencesTotal;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('invoices/create', $data)->with('id', 0);
        ;
    }

    public function save(Request $request) {
        $rules = [
            'PickupDate' => 'required|string',
            'DeliveryDate' => 'required|string',
            'IPAddress' => 'required|string',
            'PostalCode' => 'required|string',
        ];

        $input = $request->all();

        $franchise = Franchises::find($input["FKFranchiseID"]);
        $member = Members::find($input["FKMemberID"]);

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
        } else {

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
            $input["Currency"] = htmlentities(env("CURRENCY_SYMBOL"));
            $card = MemberCards::find($input["FKCardID"]);
            $error = true;
            
            

            if ($input["GrandTotal"] >= $franchise->MinimumOrderAmount) {

                $lastInvoice = Invoices::orderBy("PKInvoiceID", "desc")->first();
                $input["InvoiceNumber"] = (int) $lastInvoice->InvoiceNumber + 1;
                $input["InvoiceNumber"] = sprintf('%04d', $input["InvoiceNumber"]);

                $card = MemberCards::find($input["FKCardID"]);
                $data = [
                    'payment_method' => $card->Code,
                    'amount' => Functions::rawAmount($input["GrandTotal"]),
                    'customer' => $member->StripeCustomerID,
                    'payment_method_types' => ['card'],
                    'currency' => env("CURRENCY_CODE"),
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    'setup_future_usage' => 'off_session',
                    'description' => 'Invoice Created ' . $input["InvoiceNumber"]
                ];


                $intent = StripePayment::purchase($data);

                if (!isset($intent["error"])) {
                    $stripeInformation = array(
                        'InvoiceNumber' => $input["InvoiceNumber"],
                        'Content' => json_encode($intent),
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $informationId = PaymentInformations::insertGetId($stripeInformation);
                    $input["PaymentReference"] = $card->Code;
                    $input["PaymentToken"] = $intent->id;

                    $invoiceServices = json_decode($input["invoice_services"], 1);

                    $hasPreference = $input["has_preference"];
                    $preferences = $input["preferences"];
                    
                     $input['PickupTime'] = $input['pickup_time_from'] . "-" . $input['pickup_time_from'];
                         $input['DeliveryTime'] = $input['delivery_time_from'] . "-" . $input['delivery_time_to'];
                    unset($input["_token"]);
                    unset($input["invoice_services"]);
                    unset($input["has_preference"]);
                    unset($input["preferences"]);
                    unset($input["example_length"]);
                    unset($input["customTitle"]);
                    unset($input["customPrice"]);
                    $input["PickupDate"] = date("Y-m-d", strtotime($input["PickupDate"]));
                    $input["DeliveryDate"] = date("Y-m-d", strtotime($input["DeliveryDate"]));
                  
                    
                            unset($input["pickup_time_from"]);
                       unset($input["pickup_time_to"]);

                        unset($input["delivery_time_from"]);
                        unset($input["delivery_time_to"]);
                        
                    if ($hasPreference == "false" || $hasPreference == "") {
                        $input["PreferenceTotal"] = 0.00;
                    }

                    $input["SubTotal"] = $input["ServicesTotal"];

                    if ($input["FKDiscountID"] != "None") {
                        $discount = Discounts::find($input["FKDiscountID"]);
                        $input["DiscountCode"] = $discount->Code;
                        $input["DType"] = $discount->DType;
                        $input["DiscountType"] = $discount->DiscountFor;
                        $input["DiscountWorth"] = $discount->Worth;
                    }

                    $id = Invoices::insertGetId($input);

                    $invoiceInformation = array(
                        'FKInvoiceID' => $id,
                        'FKCardID' => $input["FKCardID"],
                        'Amount' => $input["GrandTotal"],
                        'PaymentReference' => $card->Code,
                        'PaymentToken' => $intent->id,
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $invoiceInformationId = InvoiceInformations::insertGetId($invoiceInformation);

                    if (!empty($invoiceServices)) {

                        $services = array();

                        foreach ($invoiceServices as $service) {
                            unset($service["Allowed"]);
                            $service["FKInvoiceID"] = $id;
                            $services[] = $service;
                        }
                        InvoiceServices::insert($services);
                    }

                    if ($hasPreference == true) {

                        $preferences = Preferences::whereIn("PKPreferenceID", $preferences)->get();
                        foreach ($preferences as $preference) {

                            $arr[] = [
                                "FKInvoiceID" => $id,
                                "FKPreferenceID" => $preference->PKPreferenceID,
                                "ParentTitle" => $preference->category->Title,
                                "Title" => $preference->Title,
                                "Price" => $preference->Price,
                                "Total" => $preference->Price
                            ];
                        }
                        InvoicePreferences::insert($arr);
                    }

                    $invoice = Invoices::find($id);
                    $tookanResponse = Tookan::create($invoice);

                    $update["TookanResponse"] = $tookanResponse;
                    Invoices::where("PKInvoiceID", $id)->update($update);
                    self::sendInvoiceEmail($invoice, "c");
                    self::sendInvoiceEmailToFranchise($invoice, "c");

                    $response["message"] = "Invoice added successfully!";
                    $header = 200;
                    $error = false;
                } else {
                    $validator->errors()->add('errors', "Error in card.");
                    $errors = $validator->errors();
                    $response['errors'] = $errors;

                    $header = 402;
                }
            } else {
                $validator->errors()->add('errors', "Services Total should be > " . $franchise->MinimumOrderAmount . ".");
                $errors = $validator->errors();
                $response['errors'] = $errors;
                $header = 402;
            }
        }
        return response()->json($response, $header);
    }

    public function edit($id) {

        $model = Invoices::find($id);

        if (!empty($model->PickupDate)) {
            $model->PickupDate = date("j F, Y", strtotime($model->PickupDate));
        }
        if (!empty($model->DeliveryDate)) {
            $model->DeliveryDate = date("j F, Y", strtotime($model->DeliveryDate));
        }

        $franchises = Franchises::where("Status", "Enabled")->pluck("Title", "PKFranchiseID");
        $member = $model->members;
        $cards = MemberCards::where("FKMemberID", $member->PKMemberID)->pluck("Title", "PKCardID");

        $discountTotal = 0.00;

        $data["invoiceDiscount"] = array();
        if (isset($model->FKDiscountID)) {
            $invoiceDiscountModel = Discounts::where("PKDiscountID", $model->FKDiscountID)->first();
            $data["invoiceDiscount"] = $invoiceDiscountModel;
        }

        $data["discountTotal"] = $model->DiscountTotal;

        $range = Functions::getTimeRanges();
        $data["range"] = $range;

        $services = FranchiseServices::where('FKFranchiseID', '=', $model->FKFranchiseID)->get();

        $invoiceServicesModel = InvoiceServices::where('FKInvoiceID', '=', $id)->get();

        $invoiceServices = array();
        $hasPreferences = false;
        $i = 1;
        foreach ($invoiceServicesModel as $invoiceServiceModel) {


            if ($invoiceServiceModel->PreferencesShow == "Yes") {
                $hasPreferences = true;
            }

            $array = array();
            $array['FKInvoiceID'] = $invoiceServiceModel->FKInvoiceID;
            $array['FKCategoryID'] = $invoiceServiceModel->FKCategoryID;
            $array['FKServiceID'] = $invoiceServiceModel->FKServiceID;
            $array['Title'] = $invoiceServiceModel->Title;
            $array['ImageURL'] = $invoiceServiceModel->ImageURL;
            $array['DesktopImageName'] = $invoiceServiceModel->DesktopImageName;
            $array['MobileImageName'] = $invoiceServiceModel->MobileImageName;
            $array['IsPackage'] = $invoiceServiceModel->IsPackage;
            $array['PreferencesShow'] = $invoiceServiceModel->PreferencesShow;
            $array['Quantity'] = $invoiceServiceModel->Quantity;
            $array['Price'] = $invoiceServiceModel->Price;
            $array['Total'] = $invoiceServiceModel->Total;
            $array['Allowed'] = $invoiceServiceModel->Quantity;

            if ($invoiceServiceModel->FKServiceID > 0) {
                $invoiceServices[$invoiceServiceModel->FKServiceID] = $array;
            } else {
                $invoiceServices[$invoiceServiceModel->Title . "_" . $i] = $array;
            }
        }
        // d($invoiceServices,1);
        $data["franchise"] = $model->franchises;
        $data["invoice_services"] = $invoiceServices;
        $data["member"] = $member;
        $data["model"] = $model;
        $data["cards"] = $cards;
        $data["discounts"] = self::getDiscounts();
        $data["franchises"] = $franchises;
        $l = self::getLocations();
        $data["locations"] = $l["locations"];
        $data["lockers"] = $l["lockers"];
        $data["hasPreferences"] = $hasPreferences;
        $data["franchise_id"] = $model->FKFranchiseID;
        $data["member_id"] = $model->FKMemberID;
        $data["pageHeader"] = false;
        $data["title"] = "Invoice";
        $data["SubTitle"] = "Edit";
        $data['services'] = $services;
        $data['grandTotal'] = $model->GrandTotal;

        $preferencesTotal = 0.00;

        $selectedPreferences = array();


        if ($hasPreferences) {
            $invoicePreferencesModel = InvoicePreferences::where("FKInvoiceID", $id)->get();
          
            foreach ($invoicePreferencesModel as $invoicePreferenceModel) {

                $selectedPreferences[$invoicePreferenceModel->ParentTitle] = $invoicePreferenceModel->FKPreferenceID;
                $pref=Preferences::where('PKPreferenceID',$invoicePreferenceModel->FKPreferenceID)->first();
                if($pref->PriceForPackage=='Yes'){
                   $preferencesTotal = $preferencesTotal + $invoicePreferenceModel->Price; 
                }
                
            }
            
        } else {
// $invoicePreferencesModel = MembersPreference::where("FKMemberID", $model->FKMemberID)->get();
        }

        $categories = Preferences::where("Status", "Enabled")->where("ParentPreferenceID", 0)
                ->orderBy("Position", "asc")
                ->get();
        

        $preferences = array();
        foreach ($categories as $category) {

            if (count($category->preferences) > 0) {
                $preferences[$category->PKPreferenceID]["title"] = $category->Title;
                $preferences[$category->PKPreferenceID]["price_for_package"] = $category->PriceForPackage;
                $preferences[$category->PKPreferenceID]["price"] = $category->Price;
                $preferences[$category->PKPreferenceID]["selected"] = isset($selectedPreferences[$category->Title]) ? $selectedPreferences[$category->Title] : "";
                $childs = array();
                foreach ($category->preferences as $preference) {
                    $childs[$preference->PKPreferenceID] = [
                        "id" => $preference->PKPreferenceID,
                        "title" => $preference->Title,
                        "price" => $preference->Price,
                        "price_for_package" => $preference->PriceForPackage,
                    ];
                }
                $preferences[$category->PKPreferenceID]["preferences"] = $childs;
            }
        }


        $data['preferences'] = $preferences;
        $data['preferencesTotal'] = $preferencesTotal;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data["breadcrumbs"] = $this->breadcrumbs;


        return view('invoices/edit', $data)->with('id', $id);
    }

    public function getRefunds($invoice_id) {


        $model = Invoices::find($invoice_id);
        $InvoiceNumber = $model->InvoiceNumber;
        $PaymentToken = $model->PaymentToken;
        $total = $model->GrandTotal;
        if ($model->PaymentMethod == "Cash") {
            $response["html"] = "Refund on cash not possible.";
        } else {
            $response = StripePayment::getRefunds($PaymentToken);
            if (count($response->charges->data[0]["refunds"]["data"]) > 0) {
                $error = false;

                $refundHtml = '<table class="table table-striped">
                                                <thead class="cf">
                                                    <tr>
                                                    <th><strong>Amount</strong></th>
                                                    <th><strong>Status</strong></th>
                                                    <th><strong>Date Time</strong></th>
                                                    </tr>
                                                </thead>
                                                <tbody>';

                $refunds = $response->charges->data[0]["refunds"]["data"];

                $amount_refunded = 0.0;
                foreach ($refunds as $refund) {
                    $amount = $refund->amount;
                    $amount_status = $refund->status;
                    $amount_created = $refund->created;

                    $amount = $amount / 100;



                    $refundHtml .= '<tr>'
                            . '<td>' . (env("CURRENCY_SYMBOL") . number_format($amount, 2)) . '</td>'
                            . '<td>' . ucfirst($amount_status) . '</td>'
                            . '<td>' . date('m-d-Y H:i:s', ($amount_created)) . '</td>'
                            . '</tr>';
                    $amount_refunded = $amount_refunded + $amount;
                }
                $refundHtml .= '<tr>'
                        . '<td><strong>' . ucfirst("Total Refunded") . '</strong></td>'
                        . '<td>' . (env("CURRENCY_SYMBOL") . number_format($amount_refunded, 2)) . '</td>
                                            <td>--</td>
                                           
                                            </tr>';

                $refundHtml .= '</tbody>'
                        . '</table>';

                $response["html"] = $refundHtml;
                $total -= $amount_refunded;
            } else {
                $response["html"] = "Payment Refund Data not found in Franchise Payment record 
";
            }
        }
        $response["can_refund"] = $total;
        return response()->json($response, 200);
    }

    public function refund(Request $request) {

        $input = $request->all();
        $model = Invoices::find($input['invoice_id']);
        $rules = [
            'invoice_id' => 'required|numeric',
            'refundAmount' => 'required|numeric|min:2|max:' . $model->GrandTotal,
        ];


        $message = [];
        $validator = Validator::make($input, $rules, $message);
        $response = array();
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
        } else {
            unset($input["_token"]);


            $information = PaymentInformations::where("InvoiceNumber", $model->InvoiceNumber)->first();
            $content = json_decode($information->Content);
            $charge_id = $content->id;
            $amount = Functions::rawAmount($input["refundAmount"]);

            $intent = StripePayment::refund($charge_id, $amount);

            if (!isset($intent['error'])) {

                $stripeInformation = array(
                    'InvoiceNumber' => $model->InvoiceNumber,
                    'Content' => json_encode($intent),
                    'CreatedDateTime' => date('Y-m-d H:i:s')
                );
                $id = RefundInformations::insertGetId($stripeInformation);
                $header = 200;
                $response['message'] = "Invoice Refunded Successfully";
            } else {
                $validator->errors()->add('errors', $response["message"]);
                $errors = $validator->errors();
                $response['errors'] = $errors;
                $header = 402;
            }
        }
        return response()->json($response, $header);
    }

    public function update($id, Request $request) {

        $input = $request->all();

        $rules = [
            'PickupDate' => 'required',
            'DeliveryDate' => 'required|after:PickupDate',
        ];

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        $oldInvoiceModel = Invoices::find($id);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
//return response()->json($response, 402);
        } else {



            $error = false;
/////if change in price 
            $input["SubTotal"] = $input["ServicesTotal"];
            if ($input["GrandTotal"] > $oldInvoiceModel->GrandTotal) {

                if ($input["PaymentMethod"] == "Cash") {
                    $content = $input["PaymentMethod"];
                    $paymentReference = "";
                    $paymentToken = $input["PaymentMethod"];
                    // $intent["error"] = $input["PaymentMethod"];
                } else {
                    $card = MemberCards::find($input["FKCardID"]);

                    $data = [
                        'payment_method' => $card->Code,
                        'amount' => ($input["GrandTotal"] - $oldInvoiceModel->GrandTotal) * 100,
                        'customer' => $oldInvoiceModel->members->StripeCustomerID,
                        'payment_method_types' => ['card'],
                        'currency' => env("CURRENCY_CODE"),
                        'confirmation_method' => 'manual',
                        'confirm' => true,
                        'setup_future_usage' => 'off_session',
                        'description' => 'Invoice Updated ' . $oldInvoiceModel->InvoiceNumber
                    ];
                    $intent = StripePayment::purchase($data);
                    $content = json_encode($intent);
                    $paymentReference = $card->Code;
                    $paymentToken = $intent->id;
                }

                if (!isset($intent["error"])) {

                    $stripeInformation = array(
                        'InvoiceNumber' => $oldInvoiceModel->InvoiceNumber,
                        'Content' => $content,
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );
                    $informationId = PaymentInformations::insertGetId($stripeInformation);

                    $invoiceInformation = array(
                        'FKInvoiceID' => $id,
                        'FKCardID' => $input["FKCardID"],
                        'Amount' => $input["GrandTotal"],
                        'PaymentReference' => $paymentReference,
                        'PaymentToken' => $paymentToken,
                        'CreatedDateTime' => date('Y-m-d H:i:s')
                    );

                    $invoiceInformationId = InvoiceInformations::insertGetId($invoiceInformation);
                    $error = false;
                } else {
                    $error = true;
                    $response["message"] = $intent["message"];
                    $header = 402;
                }
            }

            if ($error == false) {

                $invoiceServices = json_decode($input["invoice_services"], true);
                $hasPreference = $input["has_preference"];
                $preferences = $input["preferences"];

                $input['PickupTime'] = $input['pickup_time_from'] . "-" . $input['pickup_time_from'];
                $input['DeliveryTime'] = $input['delivery_time_from'] . "-" . $input['delivery_time_to'];

                unset($input["_token"]);
                unset($input["invoice_services"]);
                unset($input["has_preference"]);
                unset($input["preferences"]);
                unset($input["example_length"]);
                unset($input["customTitle"]);
                unset($input["customPrice"]);

                unset($input["pickup_time_from"]);
                unset($input["pickup_time_to"]);

                unset($input["delivery_time_from"]);
                unset($input["delivery_time_to"]);

                if (!empty($invoiceServices)) {
//d($invoiceServices,1);
                    $services = array();
                    InvoiceServices::where("FKInvoiceID", $id)->delete();
                    foreach ($invoiceServices as $service) {
                        unset($service["Allowed"]);
                        $service["FKInvoiceID"] = $id;
                        $services[] = $service;
                    }
                    InvoiceServices::insert($services);
                }

                if ($hasPreference == true) {

                    InvoicePreferences::where("FKInvoiceID", $id)->delete();

                    $preferences = Preferences::whereIn("PKPreferenceID", $preferences)->get();
                    foreach ($preferences as $preference) {

                        $arr[] = [
                            "FKInvoiceID" => $id,
                            "FKPreferenceID" => $preference->PKPreferenceID,
                            "ParentTitle" => $preference->category->Title,
                            "Title" => $preference->Title,
                            "Price" => $preference->Price,
                            "Total" => $preference->Price
                        ];
                    }
                    InvoicePreferences::insert($arr);
                }

                $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
                $input["UpdatedBy"] = Auth::user()->PKUserID;
                $input["PickupDate"] = date("Y-m-d", strtotime($input["PickupDate"]));
                $input["DeliveryDate"] = date("Y-m-d", strtotime($input["DeliveryDate"]));


                Invoices::where("PKInvoiceID", $id)->update($input);

                $invoice = Invoices::find($id);
                $tookanResponse = Tookan::update($invoice);
                self::sendInvoiceEmail($invoice, "u");
                self::sendInvoiceEmailToFranchise($invoice, "u");
                if ($input['DeliveryDate'] != $oldInvoiceModel->DeliveryDate || $input['DeliveryTime'] != $oldInvoiceModel->DeliveryTime) {

                    $responderModel = SMSResponders::find(8);

                    $data["subject"] = "";
                    $data["title"] = $responderModel->Title;
                    $data["body"] = $responderModel->Content;
                    $replaces["Invoice_Number"] = $invoice->InvoiceNumber;
                    $replaces["Delivery_Date"] = $input['DeliveryDate'];
                    $replaces["Delivery_Time"] = $input['DeliveryTime'];
                    $replaces["Currency"] = env("CURRENCY_SYMBOL");
                    $replaces["Loyalty_Points_Amount"] = 10;
                    $replaces["Website_Name"] = env("APP_NAME");
                    $template = Functions::replaces($data, $replaces);
                    SMSGateway::send($responderModel->PKResponderID, $replaces["Invoice_Number"], $template["title"], $invoice->members->Phone, $template["body"]);
//d($template, 1);
                }
//die("ssss");
                $response["message"] = "Invoice updated successfully!";
                $header = 200;
            } else {
                $response["message"] = $intent["message"];
                $header = 402;
            }
        }

        return response()->json($response, $header);
    }

    public function getDiscounts() {
        $discountsModel = Discounts::where("Status", "Active")->where("DiscountFor", "Discount")->get();
        $discounts = [];
        foreach ($discountsModel as $discountModel) {
            $discounts[$discountModel->PKDiscountID] = [
                "code" => $discountModel->Code,
                "id" => $discountModel->PKDiscountID,
                "worth" => $discountModel->Worth,
                "type" => $discountModel->DType
            ];
        }
        return $discounts;
    }

    public function getLocations() {

        $locationsModel = Locations::where("Status", "Enabled")->get();

        $locations = array();
        $lockers = array();
        foreach ($locationsModel as $locationModel) {

            if (count($locationModel->lockers) > 0) {
                $locations[$locationModel->Title] = $locationModel->Title;
                $lockers[$locationModel->PKLocationID] = array();
                foreach ($locationModel->lockers as $lockersModel) {
                    $lockers[$locationModel->Title][$lockersModel->Title] = $lockersModel->Title;
                }
            }
        }

        $locations[""] = "None";
        $locations = json_decode(json_encode($locations), true);
        $locations = array_reverse($locations, true);

        $data["lockers"] = $lockers;
        $data["locations"] = $locations;

        return $data;
    }

    public function sendInvoiceEmail($invoice, $action) {

        $discountType = $invoice->DiscountType;
        $discountCode = $invoice->DiscountCode;
        $discountWorth = $invoice->DiscountWorth;
        $discount_id = $invoice->FKDiscountID;

        $emailsResponders = EmailsResponders::find(15);
        $data["body"] = $emailsResponders->Content;
        $data["title"] = $emailsResponders->Title;
        $data["subject"] = $emailsResponders->Subject;

        $settings = Functions::getSettings();

        $replaces['Website_Name'] = $settings["Website Name"];
        $replaces['Email_Address'] = $invoice->members->EmailAddress;
        $replaces['Email_Sender'] = $settings["Website Email Receivers"];
        $replaces['Website_Telephone_Number'] = $settings["Front Telephone Number"];
        $replaces['Website_URL'] = $settings["Front Email Address"];
        $replaces['Footer_Title'] = $settings["Email Footer Title"];
        $replaces['Website_Email_Address'] = $settings["Front Email Address"];
        $replaces['Full_Name'] = $invoice->members->FirstName . " " . $invoice->members->LastName;
        $replaces['Phone_Number'] = $invoice->members->Phone;
        $replaces['Address'] = $invoice->BuildingName . " " . $invoice->members->StreetName . " " . $invoice->members->PostalCode . " " . $invoice->members->Town;

        $replaces['Invoice_Number'] = $invoice->InvoiceNumber;

        $replaces['Payment_Method'] = $invoice->PaymentMethod;
        $replaces['Payment_Status'] = $invoice->PaymentStatus;
        $replaces['Order_Status'] = $invoice->OrderStatus;
        $replaces['Loyalty_Points'] = $invoice->members->TotalLoyaltyPoints;


        if ($action = "c") {
            $replaces['Date_Time_Type'] = "Created Time";
        } else {
            $replaces['Date_Time_Type'] = "Update Time";
        }

        $replaces['Date_Time'] = $invoice->CreatedDateTime;
        $replaces['Pick_Date_Time'] = $invoice->PickupDate;
        $replaces['Delivery_Date_Time'] = $invoice->DeliveryDate;
        $replaces['Order_Notes'] = $invoice->OrderNotes;
        $replaces['Account_Notes'] = $invoice->AccountNotes;
        $replaces['Additional_Instructions'] = $invoice->AdditionalInstructions;

        $replaces['Header_Menus'] = NavHelpers::getEmailHeaderMenus();
        $replaces['Footer_Menus'] = NavHelpers::getEmailFooterMenus();
        $replaces['Currency'] = env("CURRENCY_SYMBOL");

        $html = EmailTemplates::setServicesTemplate($invoice->services);
        $replaces['Invoice_Service_Table'] = $html;


        $html = EmailTemplates::setPreferencesTemplate($invoice->preferences);
        $replaces['Invoice_Preference_Table'] = $html;

        $replaces['Sub_Total'] = $invoice->ServicesTotal;
        $replaces['Preference_Total'] = $invoice->PreferenceTotal;
        $replaces['Discount_Total'] = $invoice->DiscountTotal;

        if ($invoice->DiscountType == "None") {
            $replaces['Discount_Type'] = "";
            $replaces['Discount_Detail'] = " Discount";
        } else {
            
        }

        $replaces['Grand_Total'] = $invoice->GrandTotal;
        $template = Functions::replaces($data, $replaces);

        $pdf = PDF::loadView('emails.template', ['content' => $template['body']]);

        try {
            Mail::send('emails.template', ['content' => $template["body"]], function ($m) use ($invoice, $template, $pdf) {
                $m->from(env("MAIL_FROM_ADDRESS"), env("MAIL_FROM_NAME"));
                $m->to($invoice->members->EmailAddress, $invoice->members->FirstName . " " . $invoice->members->LastName)->subject("Your Invoice#" . $invoice->InvoiceNumber . ' has been updated.');
                $m->attachData($pdf->output(), 'invoice.pdf', ['mime' => 'application/pdf',]);
                
                
            });
            
            
            
            
        } catch (JWTException $exception) {
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }
    }
    public function sendInvoiceEmailToFranchise($invoice, $action) {

        $discountType = $invoice->DiscountType;
        $discountCode = $invoice->DiscountCode;
        $discountWorth = $invoice->DiscountWorth;
        $discount_id = $invoice->FKDiscountID;

        $emailsResponders = EmailsResponders::find(15);
        $data["body"] = $emailsResponders->Content;
        $data["title"] = $emailsResponders->Title;
        $data["subject"] = $emailsResponders->Subject;

        $settings = Functions::getSettings();

        $replaces['Website_Name'] = $settings["Website Name"];
        $replaces['Email_Address'] = $invoice->members->EmailAddress;
        $replaces['Email_Sender'] = $settings["Website Email Receivers"];
        $replaces['Website_Telephone_Number'] = $settings["Front Telephone Number"];
        $replaces['Website_URL'] = $settings["Front Email Address"];
        $replaces['Footer_Title'] = $settings["Email Footer Title"];
        $replaces['Website_Email_Address'] = $settings["Front Email Address"];
        $replaces['Full_Name'] = $invoice->members->FirstName . " " . $invoice->members->LastName;
        $replaces['Phone_Number'] = $invoice->members->Phone;
        $replaces['Address'] = $invoice->BuildingName . " " . $invoice->members->StreetName . " " . $invoice->members->PostalCode . " " . $invoice->members->Town;

        $replaces['Invoice_Number'] = $invoice->InvoiceNumber;

        $replaces['Payment_Method'] = $invoice->PaymentMethod;
        $replaces['Payment_Status'] = $invoice->PaymentStatus;
        $replaces['Order_Status'] = $invoice->OrderStatus;
        $replaces['Loyalty_Points'] = $invoice->members->TotalLoyaltyPoints;


        if ($action = "c") {
            $replaces['Date_Time_Type'] = "Created Time";
        } else {
            $replaces['Date_Time_Type'] = "Update Time";
        }

        $replaces['Date_Time'] = $invoice->CreatedDateTime;
        $replaces['Pick_Date_Time'] = $invoice->PickupDate;
        $replaces['Delivery_Date_Time'] = $invoice->DeliveryDate;
        $replaces['Order_Notes'] = $invoice->OrderNotes;
        $replaces['Account_Notes'] = $invoice->AccountNotes;
        $replaces['Additional_Instructions'] = $invoice->AdditionalInstructions;

        $replaces['Header_Menus'] = NavHelpers::getEmailHeaderMenus();
        $replaces['Footer_Menus'] = NavHelpers::getEmailFooterMenus();
        $replaces['Currency'] = env("CURRENCY_SYMBOL");

        $html = EmailTemplates::setServicesTemplate($invoice->services);
        $replaces['Invoice_Service_Table'] = $html;


        $html = EmailTemplates::setPreferencesTemplate($invoice->preferences);
        $replaces['Invoice_Preference_Table'] = $html;

        $replaces['Sub_Total'] = $invoice->ServicesTotal;
        $replaces['Preference_Total'] = $invoice->PreferenceTotal;
        $replaces['Discount_Total'] = $invoice->DiscountTotal;

        if ($invoice->DiscountType == "None") {
            $replaces['Discount_Type'] = "";
            $replaces['Discount_Detail'] = " Discount";
        } else {
            
        }

        $replaces['Grand_Total'] = $invoice->GrandTotal;
        $template = Functions::replaces($data, $replaces);

        $pdf = PDF::loadView('emails.template', ['content' => $template['body']]);

        try {
            Mail::send('emails.template', ['content' => $template["body"]], function ($m) use ($invoice, $template, $pdf) {
                $m->from(env("MAIL_FROM_ADDRESS"), env("MAIL_FROM_NAME"));
                $m->to($invoice->franchises->EmailAddress, $invoice->franchises->Name )->subject("Your Invoice#" . $invoice->InvoiceNumber . ' has been updated.');
                $m->attachData($pdf->output(), 'invoice.pdf', ['mime' => 'application/pdf',]);
                
                
            });
            
            
            
            
        } catch (JWTException $exception) {
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }
    }
    public function show($id) {

        $model = Invoices::find($id);

        if (!empty($model->PickupDate)) {
            $model->PickupDate = date("j F, Y", strtotime($model->PickupDate));
        }
        if (!empty($model->DeliveryDate)) {
            $model->DeliveryDate = date("j F, Y", strtotime($model->DeliveryDate));
        }

        // $franchises = Franchises::where("Status", "Enabled")->first();
        $member = $model->members;
        $franchise = $model->franchises;

        $cards = MemberCards::where("PKCardID", $model->FKCardID)->first();

        $discounts = Discounts::where("FKMemberID", $member->PKMemberID)->pluck("Code", "PKDiscountID");
        $services = FranchiseServices::where('FKFranchiseID', '=', $franchise->PKFranchiseID)->get();

        //services

        $invoiceServicesModel = InvoiceServices::where('FKInvoiceID', '=', $id)->get();
        $arr = array();
        foreach ($invoiceServicesModel as $key => $row) {
            $arr[$key] = Categories::where('PKCategoryID', $row->FKCategoryID)->pluck('Title');
        }

        //Preference record

        $invoice_preference_records = InvoicePreferences::where("FKInvoiceID", $id)->get();

        $sms_logs = SMSLogs::where("InvoiceNumber", $model->InvoiceNumber)->get();
        $data['preference_records'] = $invoice_preference_records;
        $data["franchise"] = $model->franchises;
        $data["member"] = $member;
        $data["model"] = $model;
        $data["cards"] = $cards;
        $data["pageHeader"] = false;
        $data["title"] = "Invoice";
        $data["SubTitle"] = "View";
        $data['services'] = $invoiceServicesModel;
        $data['franchiseservices'] = $services;
        $data['logs'] = $sms_logs;
        $data['discounts'] = $discounts;
        $data['grandTotal'] = $model->GrandTotal;
        return view('invoices/show', $data)->with('id', $id);
    }

    public function movestatus(Request $request) {

        $invoices = $request->invoices;
        $status = $request->status;


        if (!empty($invoices) > 0) {
            foreach ($invoices as $invoice_id) {

                $invoice = Invoices::find($invoice_id);
                $input = array('OrderStatus' => $status, 'UpdatedDateTime' => date('Y-m-d H:i:s'));
                Invoices::where("PKInvoiceID", $invoice_id)->update($input);
                $invoice->OrderStatus = $status;
                if ($status == "Cancel") {
                    $tookanResponse = Tookan::update($invoice);
                }
            }


            $response["message"] = "Invoice updated successfully!";
            $header = 200;
        } else {
            $response["message"] = "Error try again!";
            $header = 402;
        }
        return response()->json($response, $header);
    }

    public function delete($id) {

        $invoice = Invoices::find($id);
        $status = "Cancel";
        $invoice->OrderStatus = $status;
        Tookan::update($invoice);

        Invoices::where("PKInvoiceID", $id)->delete();
        InvoiceServices::where("FKInvoiceID", $id)->delete();
        InvoicePreferences::where("FKInvoiceID", $id)->delete();
        \Session::flash('error', 'Invoice deleted successfully!');
        return redirect('invoices');
    }

}
