<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Franchises;
use App\Members;
use App\Invoices;
use DB;
use App\Helpers\Functions;
use DataTables;
use DateTime;

class InvoiceFiltersController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "invoicefilters";
        $this->breadcrumbs[1]['name'] = "Invoice Filters";
        $this->from = date("Y-m-d", strtotime("-1 years"));
        $this->to = date("Y-m-d");
        
    }

    public function index() {
        

        $range["from"] = $this->from;
        $range["to"] = $this->to;
        $data = $range;
        
        $data["franchise"] = Franchises::pluck('Title', 'PKFranchiseID');
        $data["members"] = Members::pluck('EmailAddress', 'PKMemberID');
        
        $data["pageHeader"] = false;
        $data["title"] = "Invoice Filters";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        $data["range"] = Functions::getTimeRanges();
        
        return view('invoice_filters/search_form', $data);
    }

    public function search(Request $request) {

//        $pick_date = strtotime($postData['pick_date']);
//        $delivery_date = $postData['delivery_date'];
//        $created_date_time = strtotime($postData['created_date_time']);

        $input = $request->all();
       
        
        $start = $input['from'] . " 00:00:00";
        $end = $input['to']." 23:59:59";
        
        $query = Invoices::whereBetween('CreatedDateTime', [$start, $end]);
        
        
        if (!empty($input['franchise'])) {
            $query->where('FKFranchiseID', $input['franchise']);
        }
        if (!empty($input['member'])) {
            $query->where('FKMemberID', $input['member']);
        }
        if (!empty($input['pick_date'])) {
            $pickDate = date("Y-m-d", strtotime($input['pick_date']));
            $query->where('PickupDate', $pickDate);
        }
        if (!empty($input['pick_time'])) {
            $query->where('PickupTime', $input['pick_time']);
        }
        if (!empty($input['delivery_time'])) {
            $query->where('DeliveryTime', $input['delivery_time']);
        }
        if (!empty($input['delivery_date'])) {
            $deliverDate = date("Y-m-d", strtotime($input['delivery_date']));
            $query->where('DeliveryDate', $deliverDate);
        }
        if (!empty($input['order_post_from'])) {
            $query->where('OrderPostFrom', $input['order_post_from']);
        }
        if (!empty($input['payment_status'])) {
            $query->where('PaymentStatus', $input['payment_status']);
        }
        if (!empty($input['order_status'])) {
            $query->where('OrderStatus', $input['order_status']);
        }
        
        if (!empty($input['invoice_types'])) {
            $query->where('InvoiceType', $input['invoice_types']);
        }
        
        $model = $query->get();
        
        $farnchises_array = array();
        $members_array = array();

        
        $data["pageHeader"] = false;
        $data["model"] = $model;
        $data["franchise"] = $farnchises_array;
        $data["members"] = $members_array;

        $data["title"] = "Invoice Filters";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('invoice_filters/index', $data);
    }

    public function getmembers(Request $request) {
        //$query = DB::table('ws_members')->select('*');

        $input = $request->all();
        $response = array();

        $header = 200;
        $membersModel = Members::where('EmailAddress', 'LIKE', '%' . $input['member'] . '%')->get();
        $members = array();

        foreach ($membersModel as $memberModel) {
            $members[] = array("id" => $memberModel->PKMemberID, "text" => $memberModel->EmailAddress);
        }
        $response = $members;
        return response()->json($response, $header);

        //$query->where('FKFranchiseID', $postData['member']);
    }

}
