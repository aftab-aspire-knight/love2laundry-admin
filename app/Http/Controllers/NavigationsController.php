<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\NavigationSections;
use App\Navigations;
use App\Pages;
use Illuminate\Support\Facades\Auth;
use App\Helpers\NavHelpers;

class NavigationsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "navigations";
        $this->breadcrumbs[1]['name'] = "Navigation Sections";
    }

    public function index() {


        $model = NavigationSections::orderBy("PKSectionID", "desc")->get();
        $data["pageHeader"] = false;
        $data["title"] = "Navigation Sections";
        $data["SubTitle"] = "List";
        $data["model"] = $model;
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('navigations/index', $data);
    }

    public function create() {

        $model = new NavigationSections();

        $data["pageHeader"] = false;
        $data["title"] = "Navigation";
        $data["SubTitle"] = "List";
        $data["model"] = $model;

        $pagesModel = Pages::where("Status", "Enabled")->get();

        $pages = array();
        foreach ($pagesModel as $pageModel) {
            $pages[$pageModel->PKPageID]["ID"] = $pageModel->PKPageID;
            $pages[$pageModel->PKPageID]["AccessURL"] = $pageModel->AccessURL;
            $pages[$pageModel->PKPageID]["Title"] = $pageModel->Title;
        }

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        $data["navigations"] = array();
        $data["pages"] = $pages;
        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('navigations/create', $data);
    }

    public function save(Request $request) {
        $rules = [
            'Title' => 'required|string',
            'p_id' => 'required'
        ];

        $input = $request->all();

        $message = ["p_id.required" => "please add pages."];
        $validator = Validator::make($input, $rules, $message);
        //$validator->errors()->add('pages', 'Something is wrong with this field!');


        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            $ids = $input["p_id"];
            $titles = $input["p_text"];
            $urls = $input["p_url"];
            $pagetype = $input["p_link"];
            
            unset($input["_token"]);
            unset($input["pages"]);
            unset($input["p_id"]);
            unset($input["p_text"]);
            unset($input["p_url"]);
            unset($input["nav_label"]);
            unset($input["assigned_output"]);
            unset($input["title"]);
            unset($input["urls"]);
            unset($input["p_pagetype"]);
            unset($input["p_link"]);
            unset($input["nav_url"]);


            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
            $response["id"] = NavigationSections::insertGetId($input);

            $this->insertNavigations($response["id"], $ids, $titles, $urls, $pagetype);

            $response["message"] = "Navigation added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function edit($id) {

        $model = NavigationSections::find($id);

        $pagesModel = Pages::where("Status", "Enabled")->get();

        $pages = array();
        foreach ($pagesModel as $pageModel) {
            $pages[$pageModel->PKPageID]["ID"] = $pageModel->PKPageID;
            $pages[$pageModel->PKPageID]["AccessURL"] = $pageModel->AccessURL;
            $pages[$pageModel->PKPageID]["Title"] = $pageModel->Title;
        }
        $navigations = array();

        if (count($model->navigations) > 0) {
            foreach ($model->navigations as $navigation) {
                //d($navigation,1);

                if ($navigation->LinkWith == "Page") {
                    $url = $navigation->LinkWithURL;
                } else {
                    $url = $navigation->LinkWithURL;
                }

                $navigations[] = array(
                    "id" => $navigation->FKPageID,
                    "title" => $navigation->Name,
                    "type" => $navigation->LinkWith,
                    "url" => $url
                );
            }
        }

        $data["navigations"] = $navigations;
        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Navigation";
        $data["SubTitle"] = "Edit";
        $data["pages"] = $pages;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('navigations/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string',
            'p_id' => 'required'
        ];

        $input = $request->all();
        
        $message = ["p_id.required" => "please add pages."];
        $validator = Validator::make($input, $rules, $message);


        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            $ids = $input["p_id"];
            $titles = $input["p_text"];
            $urls = $input["p_url"];
            $pagetype = $input["p_link"];

            

            unset($input["_token"]);
            unset($input["pages"]);
            unset($input["p_id"]);
            unset($input["p_text"]);
            unset($input["p_url"]);
            unset($input["nav_label"]);
            unset($input["assigned_output"]);
            unset($input["title"]);
            unset($input["urls"]);
            unset($input["p_pagetype"]);
            unset($input["p_link"]);
            unset($input["nav_url"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["UpdatedBy"] = Auth::user()->PKUserID;

            NavigationSections::where("PKSectionID", $id)->update($input);
            Navigations::where("FKSectionID", $id)->delete();



            $this->insertNavigations($id, $ids, $titles, $urls, $pagetype);

            $response["message"] = "Navigation added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function insertNavigations($id, $pageIds, $titles, $urls, $link) {

        if (!empty($pageIds)) {
            $insert = array();
            $i = 0;
            foreach ($pageIds as $pageId) {

                $insert[$i]["Name"] = $titles[$i];
                $insert[$i]["LinkWithURL"] = $urls[$i];
                $insert[$i]["FKSectionID"] = $id;
                $insert[$i]["Position"] = $i;

                if ($pageId == 0) {
                    $linkWith = 'Url';
                } else {
                    $linkWith = 'Page';
                }

                $insert[$i]["LinkWith"] = $linkWith;


                $insert[$i]["FKPageID"] = $pageId;
                $i++;
            }
            Navigations::insert($insert);
        }
    }

    public function show($id) {

        $model = NavigationSections::find($id);

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Location (" . $model->Title . ")";
        $data["SubTitle"] = "View";

        $data["navigations"] = $model->navigations;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $model->Title;

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('navigations/show', $data)->with('id', $id);
    }

    public function delete($id) {

        NavigationSections::where("PKSectionID", $id)->delete();
        Navigations::where("FKSectionID", $id)->delete();
        \Session::flash('error', 'Navigation deleted successfully!');
        return redirect('navigations');
    }

}
