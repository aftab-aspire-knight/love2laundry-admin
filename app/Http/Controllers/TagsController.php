<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailsResponders;
use App\Tags;
use Illuminate\Support\Facades\Validator;

class TagsController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "tags";
        $this->breadcrumbs[1]['name'] = "Tags";
    }
    
      public function index() {

        $tags= Tags::get();
        $data["pageHeader"] = false;
        $data["model"] = $tags;
        $data["title"] = "Tags";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('tags/index', $data);
    }   
    
      public function create() {

        $data["pageHeader"] = false;
        $data["title"] = "Tags";
        $data["subTitle"] = "Create";
        
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('tags/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
           
            'Title' => 'required|string',
            'Tag' => 'required|string',  
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
           
                       
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
       
            unset($input["_token"]);
             Tags::insertGetId($input);

            \Session::flash('success', 'Tag added successfully!');
            return redirect('tags');

        }
    }
    public function show($id) {
          
        $q = new Tags();
        $tags=$q->find($id);
        $data['model'] =$tags;
    
        $data["title"] = "Tags";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;


       return view('tags/show', $data);
    }
    
      public function delete($id) {
        if ($id != 1) {
            Tags::where('PKTagID', $id)->delete();
        }
        \Session::flash('error', 'Tag deleted successfully!');
        return redirect('tags');
    }
    public function edit($id) {
      
        $q = new Tags();
        $tags=$q->find($id);
        $data['model'] =$tags;
         
        $data["title"] = "Tags";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('tags/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

         $rules = [
           
            'Title' => 'required|string',
            'Tag' => 'required|string',  
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            unset($input["_token"]);

             
            Tags::where('PKTagID', '=', $id)->update($input);
            

            \Session::flash('success', 'Tag Updated successfully!');
            return redirect('tags');
       

        }
    }
}
