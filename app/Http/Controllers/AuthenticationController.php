<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Helpers\PasswordEncryptHelper;
use Session;
use Illuminate\Support\Facades\Input;

class AuthenticationController extends BaseController {

    // Login
    public function login() {

        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/pages/auth-login', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function checklogin(Request $request) {
        $rules = [
            'email' => 'required|string|email',
            'password' => 'required|string',
                //'remember_me' => 'boolean'
        ];

        $message = [];
        $validator = Validator::make($request->all(), $rules, $message);
        $response['error'] = 1;
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            //return response()->json($response, 402);
        } else {
            $credentials["EmailAddress"] = $request->email;
            $credentials["password"] = PasswordEncryptHelper::encrypt_decrypt('encrypt', $request->password);
            $credentials["Status"] = "Enabled";
            //d($credentials,1);

            $data = User::where($credentials)->first();

            // if(!Auth::attempt($credentials))
            if (!isset($data->exists)) {
                $response['errors']["credentials"] = "Unauthorized";
                $header = 401;
                //return response()->json($response, 401);
            } else {

                if ($request->remember_me) {
                    $remember = true;
                } else {
                    $remember = false;
                }

                Auth::loginUsingId($data->PKUserID, $remember);

                $user = $request->user();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;

                if ($request->remember_me) {
                    $token->expires_at = Carbon::now()->addWeeks(1);
                }

                $token->save();
                $response = [
                    'error' => false,
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
                ];
                $header = 200;
            }
        }
        //die($request->ajax());
        if ($request->ajax()) {
            //return redirect()->back()->withErrors($response["errors"])->withInput();
            return response()->json($response, $header);
        } else {

            if ($response['error'] == 1) {

                return redirect()->back()->withErrors($response["errors"])->withInput();
            } else {
                return redirect()->intended('/');
            }
        }
    }

//    public function logout(Request $request) {
//        $request->user()->token()->revoke();
//        return response()->json([
//                    'message' => 'Successfully logged out'
//        ]);
//    }
    // Register
    public function register() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/pages/auth-register', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Forgot Password
    public function forgot_password() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/pages/auth-forgot-password', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Reset Password
    public function reset_password() {
        $token = request()->get('token');
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/pages/auth-reset-password', [
            'pageConfigs' => $pageConfigs,
            'token' => $token
        ]);
    }

    // Lock Screen
    public function lock_screen() {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/pages/auth-lock-screen', [
            'pageConfigs' => $pageConfigs
        ]);
    }

}
