<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Preferences;
use Illuminate\Support\Facades\Auth;

class PreferencesController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "preferences";
        $this->breadcrumbs[1]['name'] = "Preferences";
    }

    public function index() {


        $model = Preferences::orderBy("PKPreferenceID", "desc")->get();

        //d($model->category->Title,1);


        $data["pageHeader"] = false;
        $data["title"] = "Preferences";
        $data["SubTitle"] = "List";
        $data["model"] = $model;
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('preferences/index', $data);
    }

    public function create() {

        $model = new Preferences();


        $categories = Preferences::where("Status", "Enabled")->where("ParentPreferenceID", 0)->pluck("Title", "PKPreferenceID");
        $categories[0] = "None";
        $categories = json_decode(json_encode($categories), true);
        $data["categories"] = array_reverse($categories, true);

        $data["pageHeader"] = false;
        $data["title"] = "Preference";
        $data["SubTitle"] = "List";
        $data["model"] = $model;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('preferences/create', $data);
    }

    public function save(Request $request) {
        $rules = [
            'Title' => 'required|string',
            
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            unset($input["_token"]);

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
            $response["id"] = Preferences::insertGetId($input);

            $response["message"] = "Preference added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function edit($id) {

        $model = Preferences::find($id);
        
        
        $categories = Preferences::where("Status", "Enabled")->where("ParentPreferenceID", 0)->pluck("Title", "PKPreferenceID");
        $categories[0] = "None";
        $categories = json_decode(json_encode($categories), true);
        $data["categories"] = array_reverse($categories, true);

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Preference";
        $data["SubTitle"] = "Edit";
        
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('preferences/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string',
            
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            unset($input["_token"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["UpdatedBy"] = Auth::user()->PKUserID;

            //d($input,1);
            
            Preferences::where("PKPreferenceID", $id)->update($input);
            
            $response["message"] = "Preference updated successfully!";
            $header = 200;
        }
        
        return response()->json($response, $header);
    }

    public function insertLockers($id, $lockers) {

        if (!empty($lockers)) {
            $lockers = explode(",", $lockers);

            $insertLockers = array();
            $i = 0;
            foreach ($lockers as $locker) {
                $insertLockers[$i]["Title"] = $locker;
                $insertLockers[$i]["FKPreferenceID"] = $id;
                $insertLockers[$i]["Position"] = $i;
                $i++;
            }
            Lockers::insert($insertLockers);
        }
    }

    public function show($id) {

        $model = Preferences::find($id);

        $data["model"] = $model;
        $data["category"] = $model->category;
        $data["pageHeader"] = false;
        $data["title"] = "Preference (" . $model->Title . ")";
        $data["SubTitle"] = "View";

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $model->Title;

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('preferences/show', $data)->with('id', $id);
    }

    public function delete($id) {

        Preferences::where("PKPreferenceID", $id)->delete();
        \Session::flash('error', 'Preference deleted successfully!');
        return redirect('preferences');
    }

}
