<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use App\Menus;
use App\UserMenus;
use App\Helpers\PasswordEncryptHelper;
use Auth;

class AdministratorsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "administrators";
        $this->breadcrumbs[1]['name'] = "Administrators";
    }

    public function index() {

        $model = User::where("Role", "Administrator")->get();

        $data["pageHeader"] = false;
        $data["model"] = $model;
        $data["title"] = "Administrator";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('administrators/index', $data);
    }

    public function show($id) {

        $q = new User();
        $data['model'] = $q->find($id);
        $data['userMenus'] = $data['model']->menus->where("ParentMenuID", 0);
        $data["title"] = "Administrators";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $selectedMenus = array();

        if (count($data['userMenus']) > 0) {
            $i = 0;
            foreach ($data['userMenus'] as $val) {
                $menuModel = Menus::find($val->FKMenuID);
                $childrenModel = $data['model']->menus->where("ParentMenuID", $val->PKAdminMenuID)->where("FKUserID", $id);
                $childArray = array();
                $selectedMenus[$i]["id"] = $val->FKMenuID;
                $selectedMenus[$i]["Name"] = $menuModel->Name;
                if (count($childrenModel) > 0) {
                    $j = 0;
                    foreach ($childrenModel as $childModel) {
                        $menuModel = Menus::find($childModel->FKMenuID);
                        $selectedMenus[$i]["children"][$j]["id"] = $menuModel->PKMenuID;
                        $selectedMenus[$i]["children"][$j]["Name"] = $menuModel->Name;

                        $childArray[$j]["id"] = $childModel->FKMenuID;
                        $j++;
                    }
                }

                $i++;
            }
        }

        $data["selectedMenus"] = $selectedMenus;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $data['model']->FirstName . " " . $data['model']->LastName;

        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('administrators/show', $data);
    }

    public function create() {
        $data["pageHeader"] = false;
        $data["model"] = new User();
        $data["title"] = "Administrators";
        $data["subTitle"] = "Create";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $data["assignMenus"] = "";
        $data["selectedMenus"] = array();

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('administrators/create', $data);
    }

    public function save(Request $request) {

        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users',
            'Password' => 'required|string'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $input["Password"] = PasswordEncryptHelper::encrypt_decrypt('encrypt', $request->Password);
            $assignedOutput = $input["assigned_output"];
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            unset($input["assigned_output"]);
            $userId = User::insertGetId($input);
            $this->assignMenus($userId, $assignedOutput);

            \Session::flash('success', 'Administrators added successfully!');
            return redirect('administrators');
        }
    }

    public function assignMenus($userId, $assignedOutput) {

        $menus = json_decode($assignedOutput, true);
        $parentPosition = 0;
        if (sizeof($menus) > 0) {
            foreach ($menus as $menu) {
                $parentMenu = array(
                    'ParentMenuID' => 0,
                    'FKUserID' => $userId,
                    'FKMenuID' => $menu['id'],
                    'Position' => $parentPosition
                );
                $parentMenuId = UserMenus::insertGetId($parentMenu);

                if (isset($menu['children']) && count($menu['children']) > 0) {
                    $chiledPosition = 0;
                    foreach ($menu['children'] as $children) {
                        $childMenu = array(
                            'ParentMenuID' => $parentMenuId,
                            'FKUserID' => $userId,
                            'FKMenuID' => $children['id'],
                            'Position' => $chiledPosition
                        );
                        UserMenus::insertGetId($childMenu);
                        $chiledPosition++;
                    }
                }
                $parentPosition++;
            }
        }
    }

    public function update($id, Request $request) {

        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users,EmailAddress,' . $id . ",PKUserID",
                //'Password' => 'required|string'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $assignedOutput = $input["assigned_output"];
            //echo ''.$assignedOutput;
            unset($input["_token"]);
            unset($input["assigned_output"]);

            if (empty($input["Password"])) {
                unset($input["Password"]);
            }else{
                $input["Password"] = PasswordEncryptHelper::encrypt_decrypt('encrypt', $request->Password);
            }
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            User::where('PKUserID', '=', $id)->update($input);
            UserMenus::where('FKUserID', '=', $id)->delete();

            $this->assignMenus($id, $assignedOutput);


            \Session::flash('message', 'Administrators updated successfully!');
            return redirect('administrators');
        }
    }

    public function edit($id) {

        $q = new User();
        $data['model'] = $q->find($id);
        $data['userMenus'] = $data['model']->menus->where("ParentMenuID", 0);
        $data["title"] = "Administrators";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $assignMenus = array();

        $selectedMenus = array();

        if (count($data['userMenus']) > 0) {
            $i = 0;
            foreach ($data['userMenus'] as $val) {
                $menuModel = Menus::find($val->FKMenuID);
                $jsonArray["id"] = $val->FKMenuID;
                $childrenModel = $data['model']->menus->where("ParentMenuID", $val->PKAdminMenuID)->where("FKUserID", $id);
                $childArray = array();
                $selectedMenus[$i]["id"] = $val->FKMenuID;
                $selectedMenus[$i]["Name"] = $menuModel->Name;
                if (count($childrenModel) > 0) {
                    $j = 0;
                    foreach ($childrenModel as $childModel) {
                        $menuModel = Menus::find($childModel->FKMenuID);
                        $selectedMenus[$i]["children"][$j]["id"] = $menuModel->PKMenuID;
                        $selectedMenus[$i]["children"][$j]["Name"] = $menuModel->Name;

                        $childArray[$j]["id"] = $childModel->FKMenuID;
                        $j++;
                    }
                    $jsonArray["children"] = $childArray;
                }
                $assignMenus[$i] = $jsonArray;
                unset($jsonArray["children"]);
                $i++;
            }
            $assignMenus = json_encode($assignMenus);
        } else {
            $assignMenus = "";
        }

        $data["selectedMenus"] = $selectedMenus;
        $data["assignMenus"] = $assignMenus;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('administrators/edit', $data)->with('id', $id);
    }

    public function delete($id) {


        if ($id != 1) {
            User::where('PKUserID', $id)->delete();
            UserMenus::where('FKUserID', $id)->delete();
        }
        \Session::flash('error', 'Administrators deleted successfully!');
        return redirect('administrators');
    }

}
