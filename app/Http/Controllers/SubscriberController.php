<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribers;
use Illuminate\Support\Facades\Validator;

class SubscriberController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "subscribers";
        $this->breadcrumbs[1]['name'] = "Subscribers";
    }
    
    public function index() {

        $subscribers= Subscribers::get();
        $data["pageHeader"] = false;
        $data["model"] = $subscribers;
        $data["title"] = "Subscribers";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('subscribers/index', $data);
    }  
    
    public function create() {

        $data["pageHeader"] = false;
        $data["title"] = "Subscribers";
        $data["subTitle"] = "Create";
        
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('subscribers/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
           
            'EmailAddress' => 'required|string',
           
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
                     
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }
           
            unset($input["_token"]);
            Subscribers::insertGetId($input);

 
            \Session::flash('success', 'Subscribers added successfully!');
            return redirect('subscribers');

        }
    }
    
    public function show($id) {
          
        $q = new Subscribers();
        $subscribers=$q->find($id);
        $data['model'] =$subscribers;
    
        $data["title"] = "Subscribers";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;


       return view('subscribers/show', $data);
    }
    
    public function delete($id) {
        if ($id != 1) {
            Subscribers::where('PKSubscriberID', $id)->delete();

        }
        \Session::flash('error', 'Subscriber deleted successfully!');
        return redirect('subscribers');
    }
    
    public function edit($id) {
      
        $q = new Subscribers();
        $subscribers=$q->find($id);
        $data['model'] =$subscribers;
        
      
        $data["title"] = "Subscribers";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('subscribers/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {
        $rules = [
           
            'EmailAddress' => 'required|string',
            
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            Subscribers::where('PKSubscriberID', '=', $id)->update($input);
            
            \Session::flash('success', 'Subscriber Updated successfully!');
            return redirect('subscribers');
       

        }
    }    
    
}
