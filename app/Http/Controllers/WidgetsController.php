<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Widgets;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class WidgetsController extends BaseController
{
    public function __construct() {
          
        $this->middleware('auth');
        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "widgets";
        $this->breadcrumbs[1]['name'] = "Widgets";
    }
    
    public function index() {
         
        $widgets= Widgets::get();
        $data["pageHeader"] = false;
        $data["model"] = $widgets;
        $data["title"] = "Widgets";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('widgets/index', $data);
       
    }
    public function create() {
           
        $data["pageHeader"] = false;
        $data["title"] = "Widgets";
        $data["subTitle"] = "Create";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
   
        $data['model']['image']=null;

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('widgets/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
            'Title' => 'required|string',
            'Content' => 'required|string',

        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
           //image name
           if($request->hasFile('ImageName')) {
         
            //get filename with extension
            $filenamewithextension = $request->file('ImageName')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('ImageName')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'.'.$extension;
             $array=config('params.dir_upload_widgets');
            foreach ($array as $row){

            //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('ImageName'), 'r+'));
            }
            unset($input["ImageName"]);

            $input['ImageName']=$filenametostore;

            //Store $filenametostore in the database
            }
            unset($input["_token"]);

            Widgets::insertGetId($input);
//
            \Session::flash('success', 'Widget Added Succesfully!');
            return redirect('widgets');
        //    d($input);
          
        }
    }
    
    public function show($id) {
          
        $q = new Widgets();
        $widgets=$q->find($id);
        
        $data['model'] =$widgets;
        $data["title"] = "Widgets";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
   
       return view('widgets/show', $data);
  
    }
    public function delete($id) {

        if ($id != 1) {
            Widgets::where('PKWidgetID', $id)->delete(); 
        }
        \Session::flash('error', 'Widget deleted successfully!');
        return redirect('widgets');
    }
    
    public function edit($id) {

        $q = new Widgets();
        $widgets=$q->find($id);
 
        $data['model'] =$widgets;
    
        $data['model']['image']=$widgets->ImageName;
        
        $data["title"] = "Widget";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('widgets/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string',
            'Content' => 'required|string',
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            unset($input["_token"]);
             // mobile image
            if($request->hasFile('ImageName')) {
                
                 //delete image from ftp start
                //  get image name from id 
                      $q = new Widgets();
                $widgets = $q->find($id);
                
                //get path
                $route=config('params.dir_upload_widgets');
                foreach ($route as $row){
                    if($widgets->ImageName==null){
                        $imagepath=$row."null";

                    }else{
                        $imagepath=$row.$widgets->ImageName;

                    }
                    //file exists
                    $exist=Storage::disk('ftp')->exists($imagepath);
                    if($exist==1){
                        Storage::disk('ftp')->delete($imagepath); 
                    }  
                }
               
                //delete image from ftp end
         
            //get filename with extension
            $filenamewithextension = $request->file('ImageName')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('ImageName')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'.'.$extension;
            $array=config('params.dir_upload_widgets');
            foreach ($array as $row){
            //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('ImageName'), 'r+'));
            }
            unset($input["ImageName"]);

            $input['ImageName']=$filenametostore;

            //Store $filenametostore in the database
            }
                
            Widgets::where('PKWidgetID', '=', $id)->update($input);
            
           \Session::flash('success', 'Widgets Updated successfully!');
           return redirect('widgets');
           // d($input['Content']);             

        }
    }
    
    public function RemoveImage($id) {
//        $input['ImageName'] = null;
//        
//        $banner=Banners::where('PKBannerID', '=', $id)->update($input);
//
//        return $this->edit($id);
           
        //  get image name from id 
        $q = new Widgets();
        $widgets = $q->find($id);
        //get path
        $imagepath=config('params.dir_upload_widgets');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$widgets->ImageName);
            if($exist==1){
             //delete from storage
                 Storage::disk('ftp')->delete($row.$widgets->ImageName); 
            }   
        }  
        //delete from db
                
        $input['ImageName'] = null;
        Widgets::where('PKWidgetID', '=', $id)->update($input);
        return $this->edit($id);
        

    }

    

}
