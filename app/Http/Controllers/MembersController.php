<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Members;
use App\Preferences;
use Illuminate\Support\Facades\Validator;
use App\Helpers\PasswordEncryptHelper;
use App\MembersPreference;
use DataTables;
use Response;
use App\Invoices;

class MembersController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "members";
        $this->breadcrumbs[1]['name'] = "Members";
    }

    public function index() {

        //$member=Members::paginate(20);
        //$member=Members::get();
        $data["pageHeader"] = false;
        //$data["model"] = $member;
        $data["title"] = "Members";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('members/index', $data);
    }

    public function json(Request $request) {
        $data = Members::get();
        return Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row) {

                            $url = url("members/delete/" . $row->PKMemberID);
                            $id = trim($row->PKMemberID);
                            $actions = '<div class="dropdown">
                                <button type="button" class="btn px-1 py-1 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    
                                </button>
                                <div class="dropdown-menu" x-placement="bottom-start">
                                    <a class="dropdown-item" href="' . url("members/show/" . $row->PKMemberID) . '"><i class="feather icon-book"></i>View</a>
                                    <a class="dropdown-item" href="' . url("members/edit/" . $row->PKMemberID) . '"><i class="feather icon-edit"></i>Edit</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item confirm-delete" onclick=confirmDelete(' . $id . ',"' . $url . '") href="javascript:void(0);"  data-href="' . url("members/delete/" . $row->PKMemberID) . '" data-id="' . $row->PKMemberID . '"><i class="feather icon-trash"></i>Delete</a>
                                </div>
                            </div>';

                            return $actions;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        // }
    }

    public function addAction() {
        
    }

    public function getPreference() {
        $preference_records = Preferences::where('ParentPreferenceID', 0)->get();

        if (sizeof($preference_records) > 0) {
            foreach ($preference_records as $key => $value) {

                $preference_records[$key]['children'] = Preferences::where('ParentPreferenceID', $value->PKPreferenceID)->pluck('Title', 'PKPreferenceID');

                if (sizeof($preference_records[$key]['children']) == 0) {
                    unset($preference_records[$key]);
                }
            }
        }
        return $preference_records;
    }

    public function create() {

        $data["pageHeader"] = false;
        $data["title"] = "Members";
        $data["subTitle"] = "Create";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        $data['model']['member_preferences_array'] = null;

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('members/create', $data);
    }

    public function assignPreferences($member_id, $assign_preferences) {
        if (isset($assign_preferences)) {
            foreach ($assign_preferences as $rec) {
                $insert_member_preference = array(
                    'FKMemberID' => $member_id,
                    'FKPreferenceID' => $rec
                );
                MembersPreference::insertGetId($insert_member_preference);
            }
        }
    }

    public function save(Request $request) {

        $input = $request->all();
        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users',
            'Phone' => 'required|string',
            'PostalCode' => 'required|string',
            'BuildingName' => 'required|string',
            'Town' => 'required|string',
            'StreetName' => 'required|string',
            'TotalLoyaltyPoints' => 'required|string'
        ];

        if (!empty($input["Password"])) {
            $rules["Password"] = 'required|string';
            $rules["AccountNotes"] = 'required|string';
        }


        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            //$input["Password"] = PasswordEncryptHelper::encrypt_decrypt('encrypt', $request->Password);

            $input["Password"] = "YWVDiyHuouAp63Ad2rwCy10ghwkDo2/gMgD6kqzlttw=";

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            unset($input["preference_list"]);
            $member_id = Members::insertGetId($input);

            \Session::flash('success', 'Members added successfully!');
            return redirect('members');
        }
    }

    public function show($id) {

        $q = new Members();
        $member = $q->find($id);
        $data['model'] = $member;
        $model = Invoices::where('FKMemberID', $id)->get();
        $data["orders"] = $model;

        $data["title"] = "Members";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Show";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('members/show', $data);
    }

    public function delete($id) {

        Members::where('PKMemberID', $id)->delete();

        \Session::flash('error', 'Member deleted successfully!');
        return redirect('members');
    }

    public function edit($id) {

        $q = new Members();
        $member = $q->find($id);
        $data['model'] = $member;
        $data["title"] = "Members";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('members/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $input = $request->all();

        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users',
            'Phone' => 'required|string',
            'PostalCode' => 'required|string',
            'BuildingName' => 'required|string',
            'Town' => 'required|string',
            'StreetName' => 'required|string',
            'TotalLoyaltyPoints' => 'required|string'
        ];

        if (!empty($input["Password"])) {
            $rules["Password"] = 'required|string';
            $rules["AccountNotes"] = 'required|string';
        }

        $message = [];

        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            if (isset($input["Password"])) {
                //$input["Password"] = PasswordEncryptHelper::encrypt_decrypt('encrypt', $input["Password"]);
            }


            unset($input["_token"]);

            Members::where('PKMemberID', '=', $id)->update($input);

            \Session::flash('success', 'Members Updated successfully!');
            return redirect('members');
        }
    }

    public function get_export() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="members-' . date('Y-m-d') . '.csv"');
        header("Pragma: no-cache");
        header("Expires: 0");


        $table = Members::get();
        $file = fopen('php://output', 'w');
        fputcsv($file, array(
            'First Name', 'Last Name', 'Email Address', 'Phone Number', 'Address', 'Postcode', 'News Letter'
        ));
        foreach ($table as $row) {
            fputcsv($file, array(
                $row['FirstName'], $row['LastName'], $row['EmailAddress'], $row['Phone'], $row['BuildingName'] . ' ' . $row['StreetName'] . ' ' . $row['Town'], $row['PostalCode'], (isset($row['NewsLetter']) && $row['NewsLetter'] == 1) ? 'Yes' : 'No'
            ));
        }
        fclose($file);
    }

}
