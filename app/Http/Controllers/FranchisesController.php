<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Franchises;
use App\Services;
use App\FranchiseTimings;
use App\FranchisePostCodes;
use App\FranchiseServices;
use Illuminate\Support\Facades\Auth;
use App\Menus;

class FranchisesController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "franchises";
        $this->breadcrumbs[1]['name'] = "Franchises";
    }

    public function index() {

        $model = Franchises::get();
        $data["pageHeader"] = false;
        $data["model"] = $model;
        $data["title"] = "Franchise";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('franchises/index', $data);
    }

    public function show($id) {
        $q = new Franchises();
        $data['model'] = $q->find($id);

        $data['timings'] = $data['model']->timings->where("FKFranchiseID", $id);
        $data['post_codes'] = $data['model']->postCodes->where("FKFranchiseID", $id);
        $data['service'] = $data['model']->services->where("FKFranchiseID", $id);
        $data["title"] = $data['model']->Title;
        $data["SubTitle"] = "List";


        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $data['model']->Title;


        $data['breadcrumbs'] = $this->breadcrumbs;
        $data["pageHeader"] = false;

        return view('franchises/show', $data);
    }

    public function create() {
        $data["pageHeader"] = false;
        $data["model"] = new Franchises();
        $data["title"] = "Franchises";
        $data["subTitle"] = "Create";
        $data["timings"] = array();

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data['breadcrumbs'] = $this->breadcrumbs;
        $data['services'] = Services::where("Status", "Enabled")->get();
        return view('franchises/create', $data);
    }

    public function save(Request $request) {

        $rules = [
            'Title' => 'required|string|unique:ws_franchises',
            'EmailAddress' => 'required|string|email',
            'Name' => 'required|string',
            'Mobile' => 'required|string',
            'DeliveryOption' => 'required|string',
            'DeliveryDifferenceHour' => 'required|numeric',
            'MinimumOrderAmount' => 'required|numeric',
            'PostCode' => 'required|string',
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $timings = array();
            if (!empty($input["timings"])) {
                $timings = $input["timings"];
            }
            if (!empty($input["OffDays"])) {
                $input["OffDays"] = implode(",", $input["OffDays"]);
            }

            $postCodes = $input["PostCode"];

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["PostCode"]);
            unset($input["_token"]);
            unset($input["timings"]);

            $input["CreatedBy"] = Auth::user()->PKUserID;
            
            $id = Franchises::insertGetId($input);

            if (!empty($timings)) {
                $this->insertTimings($id, $timings);
            }

            if (!empty($postCodes)) {
                $this->insertPostCodes($id, $postCodes);
            }

            $services = Services::where("Status", "Enabled")->get();


            $data = array();
            foreach ($services as $service) {
                $data["Title"] = $service->Title;
                $data["Price"] = $service->Price;
                $data["DiscountPercentage"] = 0.00;
                $data["FKFranchiseID"] = $id;
                $data["FKServiceID"] = $service->PKServiceID;
            }

            FranchiseServices::insert($data);
            \Session::flash('success', 'Franchise added successfully!');
            return redirect('franchises');
        }
    }

    public function insertPostCodes($id, $postCodes) {

        $postCodes = explode(",", $postCodes);

        foreach ($postCodes as $postCode) {
            $input = array();
            $input["FKFranchiseID"] = $id;
            $input["Code"] = $postCode;
            FranchisePostCodes::insert($input);
        }
    }

    public function insertTimings($id, $timings) {

        for ($i = 0; $i < count($timings["more_pickup_limit"]); $i++) {

            $inputTimings = array();
            $inputTimings["FKFranchiseID"] = $id;
            $inputTimings["PickupLimit"] = $timings["more_pickup_limit"][$i];
            $inputTimings["DeliveryLimit"] = $timings["more_delivery_limit"][$i];
            $inputTimings["OpeningTime"] = $timings["more_opening_time"][$i];
            $inputTimings["ClosingTime"] = $timings["more_closing_time"][$i];
            $inputTimings["GapTime"] = $timings["gap_time"][$i];
            $inputTimings["created_at"] = date("Y-m-d H:i:s");
            //d($inputTimings,1);
            FranchiseTimings::insert($inputTimings);
        }
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string|unique:ws_franchises,Title,' . $id . ",PKFranchiseID",
            'EmailAddress' => 'required|string|email',
            'Name' => 'required|string',
            'Mobile' => 'required|string',
            'DeliveryOption' => 'required|string',
            'PickupDifferenceHour' => 'required|numeric',
            'DeliveryDifferenceHour' => 'required|numeric',
            'MinimumOrderAmount' => 'required|numeric',
            'PostCode' => 'required|string',
        ];

        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $timings = array();
            if (!empty($input["timings"])) {
                $timings = $input["timings"];
            }

            if (!empty($input["OffDays"])) {
                $input["OffDays"] = implode(",", $input["OffDays"]);
            }

            $postCodes = $input["PostCode"];

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["PostCode"]);
            unset($input["_token"]);
            unset($input["timings"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            
            Franchises::where('PKFranchiseID', '=', $id)->update($input);
            FranchiseTimings::where('FKFranchiseID', '=', $id)->delete();
            FranchisePostCodes::where('FKFranchiseID', '=', $id)->delete();

            if (!empty($timings)) {
                $this->insertTimings($id, $timings);
            }

            if (!empty($postCodes)) {
                $this->insertPostCodes($id, $postCodes);
            }


            \Session::flash('message', 'Franchise updated successfully!');
            return redirect('franchises');
        }
    }

    public function edit($id) {

        $q = new Franchises();
        $data['model'] = $q->find($id);

        $data['timings'] = $data['model']->timings->where("FKFranchiseID", $id);
        $data['post_codes'] = $data['model']->postCodes->where("FKFranchiseID", $id);
        $data["title"] = "Franchises";

        if (!empty($data['post_codes'])) {
            $postcodes = array();
            foreach ($data['post_codes'] as $code) {
                $postcodes[] = $code->Code;
            }
            $data['model']->PostCode = implode(",", $postcodes);
        }

        $days = array();
        if (!empty($data['model']->OffDays)) {
            $days = explode(",", $data['model']->OffDays);
        }
        $data['model']->OffDays = $days;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('franchises/edit', $data)->with('id', $id);
    }

    public function services($id) {

        $q = new Franchises();
        $data['model'] = $q->find($id);
        $data["title"] = "Franchises";

        $data['services'] = FranchiseServices::where('FKFranchiseID', '=', $id)->orderby("PKFranchiseServiceID", "desc")->get();
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Manage Services (" . $data['model']->Title . ")";

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('franchises/services', $data)->with('id', $id);
    }

    public function getservices($id) {

        $data['services'] = FranchiseServices::where('FKFranchiseID', '=', $id)->orderby("PKFranchiseServiceID", "desc")->get();

        foreach ($data['services'] as $s) {
            $services[] = $s->FKServiceID;
        }

        $newServices = Services::where("Status", "Enabled")->whereNotIn("PKServiceID", $services)->pluck("Title", "PKServiceID")->all();

        $data['newServices'] = $newServices;

        return view('franchises/ajax/services', $data)->with('id', $id);
    }

    public function addservice(Request $request) {
        $rules = [
            'FKServiceID' => 'required|numeric',
        ];
        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {
            $id = $input["FKServiceID"];
            unset($input["_token"]);
            $service = Services::where("PKServiceID", $id)->first();
            $input["Price"] = $service->Price;
            $input["Title"] = $service->Title;
            $input["DiscountPercentage"] = 0;
            $response["id"] = FranchiseServices::insertGetId($input);
            $response["message"] = "Franchise service added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function removeservice(Request $request) {

        $rules = [
            'PKFranchiseServiceID' => 'required|numeric',
        ];

        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {
            $id = $input["PKFranchiseServiceID"];
            unset($input["_token"]);
            FranchiseServices::where('PKFranchiseServiceID', '=', $id)->delete();
            $header = 200;
            $response["id"] = $id;
            $response["message"] = "Franchise Service deleted successfully!";
        }
        return response()->json($response, $header);
    }

    public function saveservice(Request $request) {
        $rules = [
            'Title' => 'required|string',
            'DiscountPercentage' => 'required|numeric',
            'Price' => 'required|numeric'
        ];

        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {
            $id = $input["PKFranchiseServiceID"];
            unset($input["_token"]);
            FranchiseServices::where('PKFranchiseServiceID', '=', $id)->update($input);
            $header = 200;
            $response["message"] = "Franchise updated successfully!";
        }

        return response()->json($response, $header);
    }

    public function codes(Request $request) {


        $input = $request->all();
        if (!in_array("", $input["franchises"]) && !empty($input["franchises"])) {
            $franchises = FranchisePostCodes::where("FKFranchiseId", $input["franchises"])->pluck("Code", "Code");
        } else {
            $franchises = FranchisePostCodes::pluck("Code", "Code");
        }
        $header = 200;
        return response()->json($franchises, $header);
    }

    public function delete($id) {


        if ($id != 1) {
            Franchises::where('PKFranchiseID', $id)->delete();
            FranchiseTimings::where('FKFranchiseID', '=', $id)->delete();
            FranchisePostCodes::where('FKFranchiseID', '=', $id)->delete();
        }
        \Session::flash('error', 'Franchise deleted successfully!');
        return redirect('franchises');
    }

}
