<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Storage;
use File;

class CategoriesController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "categories";
        $this->breadcrumbs[1]['name'] = "Categories";
    }
    
    public function index() {

        $categories= Categories::get();
        //$member=Members::get();
        $data["pageHeader"] = false;
        $data["model"] = $categories;
        $data["title"] = "Categories";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('categories/index', $data);
    }  
    
     public function create() {
           
        $data["pageHeader"] = false;
        $data["title"] = "Categories";
        $data["subTitle"] = "Create";
        $data['model']['image_route']=null;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        $data['model']['member_preferences_array'] = null;

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('categories/create', $data);
     
    }
    
    public function save(Request $request) {
         
           $rules = [
            'Title' => 'required|string|',
            'MobileTitle' => 'required|string',
            'DesktopIconClassName' => 'required|string',
            'MobileIcon' => 'required|string',
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
        if($request->hasFile('MobileImageName')) {
         
        //get filename with extension
        $filenamewithextension = $request->file('MobileImageName')->getClientOriginalName();
 
        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
        //get file extension
        $extension = $request->file('MobileImageName')->getClientOriginalExtension();
 
        //filename to store
        $filenametostore = $filename.'.'.$extension;
        $array=config('params.dir_upload_category');
        foreach ($array as $row){
         //Upload File to external server
            Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('MobileImageName'), 'r+'));
        }
     
             unset($input["MobileImageName"]);

              $input['MobileImageName']=$filenametostore;

        //Store $filenametostore in the database
       }
     
        $input["CreatedDateTime"] = date("Y-m-d H:i:s");
             
        if (!isset($input["Status"])) {
            $input["Status"] = "Disabled";
        }
           
        unset($input["_token"]);
      
        Categories::insertGetId($input);

         \Session::flash('success', 'Category added successfully!');
            return redirect('categories');
           // $files = Storage::files($directory);


        }
    }
    
    public function show($id) {
          
        $q = new Categories();
        $cat=$q->find($id);
        $data['model'] =$cat;
        
        $data["title"] = "Categories";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
   
       return view('categories/show', $data);
    }
    
    public function delete($id) {
        
        Categories::where('PKCategoryID', $id)->delete();
        
        \Session::flash('error', 'Category deleted successfully!');
        return redirect('categories');
    }
   
    public function edit($id) {

        $q = new Categories();
        $cat=$q->find($id);
        $data['model'] =$cat;
        $data['model']['image_route']=$cat->MobileImageName;
        $data["title"] = "Categories";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('categories/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

           $rules = [
           
            'Title' => 'required|string',
            'MobileTitle' => 'required|string',
            'DesktopIconClassName' => 'required|string',
            'MobileIcon' => 'required|string',
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
                   
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            
            
         if($request->hasFile('MobileImageName')) {
         
                //get filename with extension
            $filenamewithextension = $request->file('MobileImageName')->getClientOriginalName();
 
                 //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
 
                  //get file extension
            $extension = $request->file('MobileImageName')->getClientOriginalExtension();
 
                //filename to store
            $filenametostore = $filename.'.'.$extension;
            
            $array=config('params.dir_upload_category');
            foreach ($array as $row){
             //Upload File to external server
                Storage::disk('ftp')->put($row.$filenametostore, fopen($request->file('MobileImageName'), 'r+'));
            }
            unset($input["MobileImageName"]);

              $input['MobileImageName']=$filenametostore;

        }
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }
            
           
            unset($input["_token"]);
            unset($input["preference_list"]);
             
            Categories::where('PKCategoryID', '=', $id)->update($input);
            

            \Session::flash('success', 'Category Updated successfully!');
            return redirect('categories');
       

        }
    }
    
    public function RemoveImage($id) {

        //  get image name from id 
       $q = new Categories();
        $cat=$q->find($id);
        //get path
        $imagepath=config('params.dir_upload_category');
        //file exists
        foreach ($imagepath as $row){
            $exist=Storage::disk('ftp')->exists($row.$cat->MobileImageName);
            if($exist==1){
             //delete from storage
                 Storage::disk('ftp')->delete($row.$cat->MobileImageName); 
            }   
        }  
        //delete from db
                
        $input['MobileImageName'] = null;
        Categories::where('PKCategoryID', '=', $id)->update($input);
        return $this->edit($id);
        

    }

}
