<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WidgetSection;
use App\Widgets;
use Illuminate\Support\Facades\Validator;
use App\WidgetNavigationSection;
use Auth;
class WidgetSectionController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "widgetsections";
        $this->breadcrumbs[1]['name'] = "Widget Sections";
    }

    public function index() {

        $widgetsection= WidgetSection::get();
        $data["pageHeader"] = false;
        $data["model"] = $widgetsection;
        $data["title"] = "Widget Sections";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('widgetsections/index', $data);
    }  
   
    public function create() {
        $data['widgets']= Widgets::get();
        $data["pageHeader"] = false;
        $data["title"] = "Widget Sections";
        $data["subTitle"] = "Create";
     
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        $data['selectedwidget']=null;
        $data["navigations"] = null;

        $data["pages_area"] = null;  
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('widgetsections/create', $data);
     
    }
    
    public function setWidgetNavSections($sectionid,$sectionarray){
        $id=$sectionid;
          foreach($sectionarray as $key=>$value){
                $insert_widget_sections= array(
                    'FKWidgetID' => $value,
                    'Position' => $key,
                    'FKSectionID'=>$id
                );
              // d($insert_widget_sections);
               $sectionids= WidgetNavigationSection::insertGetId($insert_widget_sections);
          }
      // echo 'section id'.$sectionids;
    }
    
    public function save(Request $request) {

        $rules = [
           
            'Title' => 'required|string',
                'p_id' => 'required'
        ];
        $input = $request->all();

        $message = ["p_id.required" => "please add pages."];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $ids = $input["p_id"];
            $titles = $input["p_text"];
            $urls = $input["p_url"];
            
            unset($input["_token"]);
            unset($input["pages"]);
            unset($input["p_id"]);
            unset($input["p_text"]);
            unset($input["p_url"]);
            unset($input["nav_label"]);
            unset($input["assigned_output"]);
            
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
            
        
            $sectionid= WidgetSection::insertGetId($input);
            $this->setWidgetNavSections($sectionid, $ids);
//
            \Session::flash('success', 'Widget Section added successfully!');
            return redirect('widgetsections');
            //d($ids);

        }
    }
    
    public function show($id) {
          
        $q = new WidgetSection();
        $widgetsection=$q->find($id);
        $data['model'] =$widgetsection;
        
        $widgetnav= WidgetNavigationSection::where('FKSectionID',$id)->get();
        $widget=array();
        foreach ($widgetnav as $row){
            $widget[]=Widgets::where('PKWidgetID',$row->FKWidgetID)->pluck('Title');
        }

        $data['widgets']=$widget;
        $data["title"] = "Widget Section";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;


      return view('widgetsections/show', $data);
    }
    
    public function delete($id) {
        if ($id != 1) {
            WidgetSection::where('PKSectionID', $id)->delete();
            WidgetNavigationSection::where('FKSectionID', $id)->delete();
        }
        \Session::flash('error', 'Widget Secton deleted successfully!');
        return redirect('widgetsections');
    }
    
    public function edit($id) {

        $q = new WidgetSection();
        $widgetsection=$q->find($id);
        $data['model'] =$widgetsection;
        $data['widgets']= Widgets::get();
            
        $nav_seection= WidgetNavigationSection::where('FKSectionID',$id)->get();
               
        $widgets=array();
        foreach ($nav_seection as $row){
            
            $widgets[]=Widgets::where('PKWidgetID',$row->FKWidgetID)->get();
                  
        }
        $navigations = array();
            
        foreach ($widgets as $key=>$pageModel) {
           $idss=0;
           $navigations[] = array("id" =>  $pageModel[$idss]['PKWidgetID'], "title" => $pageModel[$idss]['Title'], "url" => $pageModel[$idss]['Link']);
       
        }
        $data["navigations"] = $navigations;
        
        $data["title"] = "Widget Section";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('widgetsections/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

           $rules = [
           
            'Title' => 'required|string',
            
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {

            $ids = $input["p_id"];
            $titles = $input["p_text"];
            $urls = $input["p_url"];
            
            unset($input["_token"]);
            unset($input["pages"]);
            unset($input["p_id"]);
            unset($input["p_text"]);
            unset($input["p_url"]);
            unset($input["nav_label"]);
            unset($input["assigned_output"]);
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
                                  
            WidgetSection::where('PKSectionID', '=', $id)->update($input);
            WidgetNavigationSection::where('FKSectionID', '=', $id)->delete();
            
            $this->setWidgetNavSections($id, $ids);

            \Session::flash('success', 'Widget Section Updated successfully!');
           // d($id);
            return redirect('widgetsections');
       
        }
    }



}
