<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmailsResponders;
use App\Tags;
use Illuminate\Support\Facades\Validator;
class EmailrespondersController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "emailresponders";
        $this->breadcrumbs[1]['name'] = "Emailresponders";
    }
    
    public function index() {

        $emails= EmailsResponders::get();
        //$member=Members::get();
        $data["pageHeader"] = false;
        $data["model"] = $emails;
        $data["title"] = "Emails";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('emailresponders/index', $data);
    }   
    
    public function create() {
         $tags= Tags::get();
        $data["pageHeader"] = false;
        $data["title"] = "Emails";
        $data["subTitle"] = "Create";
        $data["model"]["tags"]=$tags;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('emailresponders/create', $data);
     
    }
    
    public function save(Request $request) {

        $rules = [
           
            'Title' => 'required|string',
            'FromEmail' => 'required|string',
            'ToEmail' => 'required|string',
            'Subject' => 'required|string',
            'Content'=>'required|string',
           
      
            
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
           
                       
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }
           
            unset($input["_token"]);
             Emailsresponders::insertGetId($input);

 
            \Session::flash('success', 'Emails added successfully!');
            return redirect('emailresponders');

        }
    }
    public function show($id) {
          
        $q = new Emailsresponders();
        $emails=$q->find($id);
        $data['model'] =$emails;
    
        $data["title"] = "Emails";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;


       return view('emailresponders/show', $data);
    }
    
    public function delete($id) {
        if ($id != 1) {
            Emailsresponders::where('PKResponderID', $id)->delete();

        }
        \Session::flash('error', 'Email deleted successfully!');
        return redirect('emailresponders');
    }
    public function edit($id) {
        $tags= Tags::get();
        $q = new Emailsresponders();
        $emails=$q->find($id);
        $data['model'] =$emails;
        
        $data['model']['tags'] = $tags;
       
        $data["title"] = "Emais";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('emailresponders/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

             $rules = [
           
            'Title' => 'required|string',
            'FromEmail' => 'required|string',
            'ToEmail' => 'required|string',
            'Subject' => 'required|string',
            'Content'=>'required|string',
           
      
            
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

           
            unset($input["_token"]);

             
            Emailsresponders::where('PKResponderID', '=', $id)->update($input);
            

            \Session::flash('success', 'Email Updated successfully!');
            return redirect('emailresponders');
       

        }
    }
}
