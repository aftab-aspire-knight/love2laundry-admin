<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Locations;
use App\Lockers;
use Illuminate\Support\Facades\Auth;

class LocationsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "locations";
        $this->breadcrumbs[1]['name'] = "Locations";
    }

    public function index() {


        $model = Locations::orderBy("PKLocationID", "desc")->get();
        $data["pageHeader"] = false;
        $data["title"] = "Locations";
        $data["SubTitle"] = "List";
        $data["model"] = $model;
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('locations/index', $data);
    }

    public function create() {

        $model = new Locations();

        $data["pageHeader"] = false;
        $data["title"] = "Location";
        $data["SubTitle"] = "List";
        $data["model"] = $model;

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('locations/create', $data);
    }

    public function save(Request $request) {
        $rules = [
            'Title' => 'required|string',
            'PostalCode' => 'required'
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {
            $lockers = $input["Lockers"];
            unset($input["Lockers"]);
            unset($input["_token"]);

            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            $input["CreatedBy"] = Auth::user()->PKUserID;
            $input["FKWebsiteID"] = Auth::user()->FKWebsiteID;
            $response["id"] = Locations::insertGetId($input);

            $this->insertLockers($response["id"], $lockers);

            $response["message"] = "Location added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function edit($id) {

        $model = Locations::find($id);

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Location";
        $data["SubTitle"] = "Edit";
        $data['model']->Lockers = "";
        if (!empty($model->lockers)) {
            $lockers = array();
            foreach ($model->lockers as $locker) {
                $lockers[] = $locker->Title;
            }
            $data['model']->Lockers = implode(",", $lockers);
        }
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('locations/edit', $data)->with('id', $id);
    }

    public function update($id, Request $request) {

        $rules = [
            'Title' => 'required|string',
            'PostalCode' => 'required'
        ];

        $input = $request->all();


        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            //return response()->json($response, 402);
        } else {

            $lockers = $input["Lockers"];
            unset($input["Lockers"]);
            unset($input["_token"]);

            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $input["UpdatedBy"] = Auth::user()->PKUserID;

            Locations::where("PKLocationID", $id)->update($input);
            Lockers::where("FKLocationID", $id)->delete();

            $this->insertLockers($id, $lockers);


            $response["message"] = "Location added successfully!";
            $header = 200;
        }
        return response()->json($response, $header);
    }

    public function insertLockers($id, $lockers) {

        if (!empty($lockers)) {
            $lockers = explode(",", $lockers);

            $insertLockers = array();
            $i = 0;
            foreach ($lockers as $locker) {
                $insertLockers[$i]["Title"] = $locker;
                $insertLockers[$i]["FKLocationID"] = $id;
                $insertLockers[$i]["Position"] = $i;
                $i++;
            }
            Lockers::insert($insertLockers);
        }
    }

    public function show($id) {

        $model = Locations::find($id);

        $data["model"] = $model;
        $data["pageHeader"] = false;
        $data["title"] = "Location (" . $model->Title . ")";
        $data["SubTitle"] = "View";


        $data['model']->Lockers = "";
        if (!empty($model->lockers)) {
            $lockers = array();
            foreach ($model->lockers as $locker) {
                $lockers[] = $locker->Title;
            }
            $data['model']->Lockers = implode(",", $lockers);
        }

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $model->Title;

        $data["breadcrumbs"] = $this->breadcrumbs;

        return view('locations/show', $data)->with('id', $id);
    }

    public function delete($id) {

        Locations::where("PKLocationID", $id)->delete();
        Lockers::where("FKLocationID", $id)->delete();
        \Session::flash('error', 'Location deleted successfully!');
        return redirect('locations');
    }

}
