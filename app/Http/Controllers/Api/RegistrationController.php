<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Members;
use App\Helpers\PasswordEncryptHelper;
use Illuminate\Support\Facades\Validator;
use App\Jobs\NotifyRegister;
use App\EmailsResponders;
use App\Helpers\Functions;
use App\Helpers\NavHelpers;

class RegistrationController extends MainController {

    public function index(Request $request) {
        $rules = [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
            'phone' => 'required|string|max:100',
            'building' => 'required|string|max:100',
            'street' => 'required|string|max:100',
            'postal_code' => 'required|string|max:100',
            'town' => 'required|string|max:100',
            'email' => 'required|string|unique:ws_members,EmailAddress|max:100',
            'password' => 'required|string|max:100'
        ];
        $input = $request->all();
        //$request->validate($validate);

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            return $this->sendError('Error in inputs', $validator->errors(), $header);
        } else {

            $member = new Members([
                'FirstName' => $request->first_name,
                'LastName' => $request->last_name,
                'Phone' => $request->phone,
                'BuildingName' => $request->building,
                'StreetName' => $request->street,
                'PostalCode' => $request->postal_code,
                'Town' => $request->town,
                'EmailAddress' => $request->email,
                'Password' => bcrypt($request->password),
                'CreatedDateTime' => currentDateTime()
            ]);
            $member->save();

            $this->sendMemberEmail($member);
            $response['member'] = $member;

            return $this->sendResponse($response, 'Member added successfully.');
        }
    }
    
        public function sendMemberEmail($member) {


        $emailsResponders = EmailsResponders::find(1);
        $data["body"] = $emailsResponders->Content;
        $data["title"] = $emailsResponders->Title;
        $data["subject"] = $emailsResponders->Subject;
        $settings = Functions::getSettings();

        $replaces['Website_Name'] = $settings["Website Name"];
        $replaces['Email_Address'] = $member->EmailAddress;
        $replaces['Email_Sender'] = $settings["Website Email Receivers"];
        $replaces['Website_Telephone_Number'] = $settings["Front Telephone Number"];
        $replaces['Website_URL'] = $settings["Front Email Address"];
        $replaces['Footer_Title'] = $settings["Email Footer Title"];
        $replaces['Website_Email_Address'] = $settings["Front Email Address"];
        $replaces['Full_Name'] = $member->FirstName . " " . $member->LastName;
        $replaces['Phone_Number'] = $member->Phone;
        $replaces['First_Name'] = $member->FirstName;
        $replaces['Last_Name'] = $member->LastName;
         
        $replaces['Postal_Code'] = $member->PostalCode;
        $replaces['Building_Name'] = $member->BuildingName;
        $replaces['Street_Name'] = $member->StreetName;
        $replaces['Town'] = $member->Town;



        $replaces['Header_Menus'] = NavHelpers::getEmailHeaderMenus();
        $replaces['Footer_Menus'] = NavHelpers::getEmailFooterMenus();

        $template = Functions::replaces($data, $replaces);


        try {
            $view='registration_api_email_template.email_layout';
            $subject='Request Registeration User';
            $fromName=env("MAIL_FROM_NAME");
            $fromEmail="info@love2laundry.com";
            $job = (new NotifyRegister($member->FirstName, $member->LastName,$member->EmailAddress,$view,$subject,$fromName,$fromEmail,$template["body"]));
                dispatch($job); 
        } catch (JWTException $exception) {
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }
        return $template;
    }


}
