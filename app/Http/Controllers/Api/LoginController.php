<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Members;
use Illuminate\Support\Facades\Validator;
use App\Helpers\PasswordEncryptHelper;
class LoginController extends MainController {

    public function __construct() {
        
    }

    public function index(Request $request) {

        $rules = [
             'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ];
        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            return $this->sendError('Error in inputs', $validator->errors(), $header);
        } else {

            $credentials["EmailAddress"] = $request->email;
            $credentials["password"] = bcrypt($request->password);
            $credentials["Status"] = "Enabled";
            $data = Members::where($credentials)->first();


            if (!auth()->guard('api')->check($credentials, false)) {
                return $this->sendError('Unauthorised.', ['error' => 'Unauthorised'], 401);
            }
            if (!isset($data->exists)) {
                $response['errors']["credentials"] = "Unauthorized email or pass not exist";
                $header = 401;
                //return response()->json($response, 401);
            } else {

                if ($request->remember_me) {
                    $remember = true;
                } else {
                    $remember = false;
                }
                $data = auth('api')->user();

                $tokenResult = $data->createToken(env("APP_NAME"));
                $token = $tokenResult->token;
                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                $response['token'] = $tokenResult->accessToken;
                $response['member'] = $data;
            }
            return $this->sendResponse($response, 'Member logined in successfully.');
        }
    }

    public function validateCredentials(UserContract $user, array $credentials) {
        $plain = $credentials['password'];

        return $this->hasher->check($plain, $user->getAuthPassword());
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
                    'message' => 'Successfully logged out'
        ]);
    }

//
//    /**
//     * Get the authenticated User
//     *
//     * @return [json] user object
//     */
    public function user(Request $request) {
        return response()->json($request->user());
    }

}
