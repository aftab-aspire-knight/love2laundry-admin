<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Jobs\NotifyInvoice;

class OrderController extends MainController {
    
    
    
    public function __construct() {
        
    }

    public function checkout() {
        $member = auth("api")->user();
        $this->dispatch(new NotifyInvoice(public_path()));
    }
}