<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use App\Http\Resources\Members as MemberResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Members;
use Illuminate\Support\Facades\Auth;
use App\Token;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mail;
use DB;
use App\EmailsResponders;
use App\Helpers\Functions;
use App\Helpers\NavHelpers;
use App\Jobs\PasswordResetNotify;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\PasswordEncryptHelper;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Jobs\NotifyForgotPassword;
use App\MemberToken;

class AccountController extends MainController {

    public function __construct() {
        
    }

    public function index() {
        $member = auth("api")->user();
        return $this->sendResponse($member, 'Member retrieved successfully.');
    }

    public function changepassword(Request $request) {

        $member = auth("api")->user();

        $rules = [
            'current_password' => ['required', function ($attribute, $value, $fail) use ($member) {
                    if (!\Hash::check($value, $member->Password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }],
            'password' => 'required|string|confirmed|max:100'
        ];

        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            return $this->sendError('Error in inputs', $validator->errors(), $header);
        } else {

            $input = [
                'Password' => bcrypt($request->password),
                'UpdatedDateTime' => currentDateTime()
            ];
            $response['update'] = Members::where('PKMemberID', $member->PKMemberID)->update($input);
            $response['updated_at'] = $input["UpdatedDateTime"];
            return $this->sendResponse($response, 'Password changed successfully.');
        }
    }

    public function logout() {

        $member = auth("api")->user()->token()->revoke();
        return $this->sendResponse($member, 'Member Deleted successfully.');
    }

    public function update(Request $request) {
        $member = auth("api")->user();
        $rules = [
            'first_name' => 'required|string|min:3|max:50',
            'last_name' => 'required|string|min:3|max:50',
            'email' => 'required|string|email|min:3|max:50|unique:ws_members,EmailAddress,' . $member->PKMemberID . ',PKMemberID',
            'phone' => 'required|string|min:8|max:12|unique:ws_members,Phone,' . $member->PKMemberID . ',PKMemberID',
            'building' => 'required|string|min:3|max:60',
            'street' => 'required|string|min:3|max:60',
            'town' => 'required|string|min:3|max:30',
            'account_notes' => 'required|string|min:3|max:100',
        ];

        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            return $this->sendError('Error in inputs', $validator->errors(), $header);
        } else {


            $input = [
                'FirstName' => $request->first_name,
                'LastName' => $request->last_name,
                'Phone' => $request->phone,
                'BuildingName' => $request->building,
                'StreetName' => $request->street,
                'PostalCode' => $request->postal_code,
                'Town' => $request->town,
                'EmailAddress' => $request->email,
                'UpdatedDateTime' => currentDateTime()
            ];

            $response['update'] = Members::where('PKMemberID', '=', $member->PKMemberID)->update($input);
            $response['member'] = $input;
            return $this->sendResponse($member, 'Member added successfully.');
        }
    }

    public function forgotpassword(Request $request) {
        $rules = [
            'email' => 'required|string',
        ];
        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response['errors'] = $errors;
            $header = 402;
            return $this->sendError('Error in inputs', $validator->errors(), $header);
        } else {
            $rand_token = Str::random(12);
            $member = Members::where('EmailAddress', $input['email'])->first();

            if (empty($tokens)) {
                DB::beginTransaction();
                try {
                    $token = new MemberToken();
                    $token->member_id = $member->PKMemberID;
                    $token->token = $rand_token;
                    $token->created_at = date("Y-m-d H:i:s");
                    $token->save();

                    DB::commit();
                    // all good
                } catch (\Exception $e) {
                    DB::rollback();
                    // something went wrong
                }
            } else {
                DB::beginTransaction();
                try {
                    $token_val = MemberToken::find($tokens->PKMemberID);
                    $token_val->token = $rand_token;
                    $token_val->created_at = date("Y-m-d H:i:s");
                    $token_val->save();

                    DB::commit();
                    // all good
                } catch (\Exception $e) {
                    DB::rollback();
                    // something went wrong
                }
            }
            $link = "http://admin.l2l.com/api/auth/account/resetpassword?token=" . $rand_token;
            $this->sendForgotPasswordEmail($member, $link);

            return $this->sendResponse($member, 'Member retrieved successfully.');
        }
    }

    public function resetpasswordview() {
        $token = request()->get('token');
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];

        return view('/members/passwordreset/auth-reset-password', [
            'pageConfigs' => $pageConfigs,
            'token' => $token
            
        ]);
    }

    public function reset(Request $request) {
        $rules = [
            'user-email' => 'required',
            'user-password' => 'required',
            'user-confrim-password' => 'same:user-password'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
        if ($validator->fails()) {
            abort(403, 'Unauthorized action.');
        } else {
            $mytime = Carbon::now();
            $token = $request->get('token');
            $token_val = MemberToken::where("token", "=", $token)->first();
            if (empty($token_val)) {
                abort(403, 'Unauthorized action. Token Invalid');
            } else {
                $user_val = Members::find($token_val->member_id);
                $user_val->Password = bcrypt($request->get('user-password'));
                $user_val->UpdatedDateTime = date("Y-m-d H:i:s");
                $user_val->save();
                 MemberToken::where('token', "=", $token)->delete();
                 try {

                $this->sendPasswordResetEmail($user_val, $request->get('user-password'));
            } catch (DecryptException $e) {
                echo $e->getMessage();
            }
            }
          return $this->resetpasswordview();
        }
    }

    public function sendPasswordResetEmail($member, $pass) {


        $emailsResponders = EmailsResponders::find(3);
        $data["body"] = $emailsResponders->Content;
        $data["title"] = $emailsResponders->Title;
        $data["subject"] = $emailsResponders->Subject;
        $settings = Functions::getSettings();

        $replaces['Website_Name'] = $settings["Website Name"];
        $replaces['Email_Address'] = $member->EmailAddress;
        $replaces['Email_Sender'] = $settings["Website Email Receivers"];
        $replaces['Website_Telephone_Number'] = $settings["Front Telephone Number"];
        $replaces['Website_URL'] = $settings["Front Email Address"];
        $replaces['Footer_Title'] = $settings["Email Footer Title"];
        $replaces['Website_Email_Address'] = $settings["Front Email Address"];
        $replaces['Full_Name'] = $member->FirstName . " " . $member->LastName;
        $replaces['Phone_Number'] = $member->Phone;
        $replaces['Password'] = $pass;


        $replaces['Header_Menus'] = NavHelpers::getEmailHeaderMenus();
        $replaces['Footer_Menus'] = NavHelpers::getEmailFooterMenus();

        $template = Functions::replaces($data, $replaces);


        try {
            $view = 'resetpassword_api_email_template.email_layout';
            $subject = 'Successfull Password Reset';
            $fromName = env("MAIL_FROM_NAME");
            $fromEmail = "info@love2laundry.com";
            $job = (new PasswordResetNotify($member->FirstName, $member->LastName, $member->EmailAddress, $view, $subject, $fromName, $fromEmail, $template["body"]));
            dispatch($job);
        } catch (JWTException $exception) {
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }
        return $template;
    }

    public function sendForgotPasswordEmail($member, $link) {


        $emailsResponders = EmailsResponders::find(17);
        $data["body"] = $emailsResponders->Content;
        $data["title"] = $emailsResponders->Title;
        $data["subject"] = $emailsResponders->Subject;
        $settings = Functions::getSettings();

        $replaces['Website_Name'] = $settings["Website Name"];
        $replaces['Email_Address'] = $member->EmailAddress;
        $replaces['Email_Sender'] = $settings["Website Email Receivers"];
        $replaces['Website_Telephone_Number'] = $settings["Front Telephone Number"];
        $replaces['Website_URL'] = $settings["Front Email Address"];
        $replaces['Footer_Title'] = $settings["Email Footer Title"];
        $replaces['Website_Email_Address'] = $settings["Front Email Address"];
        $replaces['Full_Name'] = $member->FirstName . " " . $member->LastName;
        $replaces['Phone_Number'] = $member->Phone;
        $replaces['Link'] = $link;

        $replaces['Header_Menus'] = NavHelpers::getEmailHeaderMenus();
        $replaces['Footer_Menus'] = NavHelpers::getEmailFooterMenus();

        $template = Functions::replaces($data, $replaces);


        try {
            $view = 'resetpassword_api_email_template.email_layout';
            $subject = 'Request Password Reset';
            $fromName = env("MAIL_FROM_NAME");
            $fromEmail = "info@love2laundry.com";
            $job = (new PasswordResetNotify($member->FirstName, $member->LastName, $member->EmailAddress, $view, $subject, $fromName, $fromEmail, $template["body"]));
            dispatch($job);
        } catch (JWTException $exception) {
            $this->serverstatuscode = "0";
            $this->serverstatusdes = $exception->getMessage();
        }
        return $template;
    }

}
