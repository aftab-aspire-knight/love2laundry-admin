<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Members;
use App\Services\Country;
use App\Franchises;
use App\Http\Resources\Franschises as FranschisesResource;

class HomeController extends MainController {

    public function __construct() {
        
    }

    public function index() {
        
    }

    public function searchfranchise(Request $request, Country $country) {

        $postcode = $request->postcode;

        $rules = [
            'postcode' => 'required|string'
        ];

        $input = $request->all();
        $message = [];
        $validator = Validator::make($input, $rules, $message);



        $valid = $country->validatePostalCode($postcode);

        if ($valid["validate"] == false) {
            $validator->errors()->add('errors', 'Invalid postal code.');
            $errors = $validator->errors();
            $header = 402;
            return $this->sendError('Error in inputs', $errors, $header);
        } else {
            $franchise_id = $valid["franchise_id"];
            $response["franchise_id"] = $valid["franchise_id"];
            $response["prefix"] = $valid["prefix"];
            $response["suffix"] = $valid["suffix"];
            $response["postcode"] = $postcode;

            $franchise = Franchises::find($franchise_id);
            $response["franchise"] = new FranschisesResource($franchise);
        }

        return $this->sendResponse($response, 'franchise found.');
    }

}
