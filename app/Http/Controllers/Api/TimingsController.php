<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\MainController as MainController;
use Illuminate\Http\Request;
use App\FranchiseTimings;
use App\Franchises;
use App\DisableDates;
use App\DisableTimes;

class TimingsController extends MainController {

    public function __construct() {
        
    }

    public function index() {
        
    }

    public function pickup($franchise_id) {

        $model = Franchises::find($franchise_id);
        $openAt = $model->OpeningTime;
        $closeAt = $model->ClosingTime;
        $pickupDifference = $model->PickupDifferenceHour;


        $date = date("Y-m-d H:i:s");
        $pickupDate = date("Y-m-d H:i:s", strtotime("+ $pickupDifference hours"));
        $pickupTime = date("H:i:s", strtotime($pickupDate));
        $closeAt = date("H:i:s", strtotime($closeAt));

        $disableDates = $this->isDisableDateSlot($franchise_id);
        $dates = array();

        $count = 0;

        for ($i = 0; $i <= 30; $i++) {
            $date = date("Y-m-d", strtotime("+" . $i . ' days'));
            $class = "disable";

            if ($this->isOffDay($model, $date) == false) {

                if (!in_array($date, $disableDates)) {
                    $class = "available";
                }
            }
            $dates[$count]["class"] = $class;
            $dates[$count]["date"] = $date;
            $dates[$count]["day"] = date("l", strtotime($date));
            $dates[$count]["string"] = date("d F, Y", strtotime($date));

            $count++;
            if (count($dates) == 20) {
                break;
            }
        }
        return $this->sendResponse($dates, '');
    }

    public function isOffDay(Franchises $model, $date) {

        $day = date("l", strtotime($date));
        $closeDays = explode(",", $model->OffDays);

        if (in_array($day, $closeDays)) {
            return true;
        } else {
            return false;
        }
    }

    public function isDisableDateSlot($id) {

        $table = DisableTimes::table();
        $model = DisableTimes::where("$table.FKFranchiseID", $id)
                        ->join(DisableDates::table() . " as dd", "dd.PKDateID", "=", "FKDateID")
                        ->where("$table.Date", ">", currentDateTime())
                        ->where("$table.Time", "All Day")
                        ->where(function ($query) use ($table) {
                            $query->where("$table.Type", "Both")
                            ->orWhere("$table.Type", "Pickup");
                        })->get();

        $arr = array();
        foreach ($model as $row) {
            $arr[] = $row->Date;
        }

        return $arr;
    }

}
