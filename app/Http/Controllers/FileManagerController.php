<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileManagerController extends BaseController
{
        public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "filemanager";
        $this->breadcrumbs[1]['name'] = "File Manager";
    }
    public function index() {
       // $banners= Banners::paginate(20);

        $data["pageHeader"] = false;
       
        $data["title"] = "File Manager";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('file_manager/index', $data);
    }  

}
