<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostCodeLogs;
use DataTables;

class PostCodeLogsController extends BaseController {

    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "postcodelogs";
        $this->breadcrumbs[1]['name'] = "Post Code";
    }

    public function index() {

        $data["pageHeader"] = false;
        $data["title"] = "Post Code";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('post_code_logs/index', $data);
    }

     public function json(Request $request) {
        $data = PostCodeLogs::with('franchises');
        return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('franchises', function($invoice) {

                            if (isset($invoice->franchises->Title)) {
                                return $invoice->franchises->Title;
                            } else {
                                return "N/A";
                            }
                        })
                    
                ->addColumn('action', function($row) {

                            $id = trim($row->PKSearchID);
                            $url = url("postcodelogs/delete/" . $id);

                            $actions = '<a class="btn btn-success confirm-delete" onclick=confirmDelete(' . $id . ',"' . $url . '") href="javascript:void(0);" data-href="' . url("invoices/delete/" . $id) . '" data-id="' . $id . '" >Delete</a>';


                            return $actions;
                        })
                        ->rawColumns(['action', 'select'])
                        ->make(true);
        // }
    }
    
     public function delete($id) {
  
        PostCodeLogs::where('PKSearchID', $id)->delete();

        \Session::flash('error', 'Post Code deleted successfully!');
        return redirect('postcodelogs');
    }
    public function deleteAll(){
        $data = $_GET['IDS'];

        foreach ($data as $row){
            $post=PostCodeLogs::where('PKSearchID', $row)->delete();
            echo $post;
             
        }
        
    }
}
