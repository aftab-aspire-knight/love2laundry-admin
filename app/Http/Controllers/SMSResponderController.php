<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SMSResponders;
use App\Tags;
use Illuminate\Support\Facades\Validator;

class SMSResponderController extends BaseController
{
    public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "smsresponders";
        $this->breadcrumbs[1]['name'] = "SMS";
    }
    
      public function index() {

        $sms= SMSResponders::get();
        $data["pageHeader"] = false;
        $data["model"] = $sms;
        $data["title"] = "SMS";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('smsresponders/index', $data);
    } 
    
    public function create() {
         $tags= Tags::get();
        $data["pageHeader"] = false;
        $data["title"] = "SMS";
        $data["subTitle"] = "Create";
        $data["model"]["tags"]=$tags;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";
        

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('smsresponders/create', $data);
     
    }
    
      public function save(Request $request) {

        $rules = [
           
            'Title' => 'required|string',
            'Content'=>'required|string', 
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);
//
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
                     
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");
            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }
           
            unset($input["_token"]);
            SMSResponders::insertGetId($input);

 
            \Session::flash('success', 'SMS added successfully!');
            return redirect('smsresponders');

        }
    }
    
    public function show($id) {
          
        $q = new SMSResponders();
        $sms=$q->find($id);
        $data['model'] =$sms;
    
        $data["title"] = "SMS";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "View";
        $data['breadcrumbs'] = $this->breadcrumbs;


       return view('smsresponders/show', $data);
    }
    
    public function delete($id) {
        if ($id != 1) {
            SMSResponders::where('PKResponderID', $id)->delete();

        }
        \Session::flash('error', 'SMS deleted successfully!');
        return redirect('smsresponders');
    }
    
      public function edit($id) {
        $tags= Tags::get();
        $q = new SMSResponders();
        $sms=$q->find($id);
        $data['model'] =$sms;
        
        $data['model']['tags'] = $tags;
       
        $data["title"] = "SMS";
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";
        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('smsresponders/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {
        $rules = [
           
            'Title' => 'required|string',
            'Content'=>'required|string', 
        ];
        $input = $request->all();

        $message = [];
        
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            SMSResponders::where('PKResponderID', '=', $id)->update($input);
            
            \Session::flash('success', 'SMS Updated successfully!');
            return redirect('smsresponders');
       

        }
    }

}
