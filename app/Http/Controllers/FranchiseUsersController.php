<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Menus;
use App\UserMenus;
use App\Helpers\PasswordEncryptHelper;
use App\Franchises;
class FranchiseUsersController extends BaseController
{
  public function __construct() {
        $this->middleware('auth');

        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "users";
        $this->breadcrumbs[1]['name'] = "Users";
    }

    public function index() {

    
        //$user= User::where('FKFranchiseID','!=',0)->get();
        
       $user=User::query()
            ->leftjoin('ws_franchises', 'ws_franchises.PKFranchiseID', '=', 'cms_users.FKFranchiseID')
            ->select('*')
            ->where('FKFranchiseID',"!=",0)
            ->get();
       
        $data["pageHeader"] = false;
        $data["model"] = $user;
        $data["title"] = "Users";
        $data["SubTitle"] = "List";
        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('franchise_users/index', $data);
    }   
    
    public function show($id) {
        $q = new User();
        $user= $q->find($id);
        $fran= Franchises::find($user->FKFranchiseID);
        
        $data['model'] = $user;
        $data['franchise']=$fran;
        $data['userMenus'] = $data['model']->menus->where("ParentMenuID", 0);
        $data["title"] = "Users";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $selectedMenus = array();
        //echo "<pre>".print_r($data['franchise'], true)."</pre>";
        if (count($data['userMenus']) > 0) {
            $i = 0;
            foreach ($data['userMenus'] as $val) {
                $menuModel = Menus::find($val->FKMenuID);
                $childrenModel = $data['model']->menus->where("ParentMenuID", $val->PKAdminMenuID)->where("FKUserID", $id);
                $childArray = array();
                $selectedMenus[$i]["id"] = $val->FKMenuID;
                $selectedMenus[$i]["Name"] = $menuModel->Name;
                if (count($childrenModel) > 0) {
                    $j = 0;
                    foreach ($childrenModel as $childModel) {
                        $menuModel = Menus::find($childModel->FKMenuID);
                        $selectedMenus[$i]["children"][$j]["id"] = $menuModel->PKMenuID;
                        $selectedMenus[$i]["children"][$j]["Name"] = $menuModel->Name;

                        $childArray[$j]["id"] = $childModel->FKMenuID;
                        $j++;
                    }
                    
                }
                
                $i++;
            }
            
        } 

        $data["selectedMenus"] = $selectedMenus;
        
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $data['model']->FirstName." ".$data['model']->LastName;

        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('franchise_users/show', $data);
       // echo ''.$fran;
    }
    
    public function create() {
        $data["pageHeader"] = false;
        $data["model"] = new Franchises();
        $data["title"] = "Users";
        $data["subTitle"] = "Create";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $data["assignMenus"] = "";
        $data["selectedMenus"] = array();
        $data["franchise"]= Franchises::pluck('Title','PKFranchiseID');

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Create";

        $data['breadcrumbs'] = $this->breadcrumbs;
        return view('franchise_users/create', $data);
    }
    
    public function save(Request $request) {

        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users',
            'Password' => 'required|string'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            
            $input["Password"]=PasswordEncryptHelper::encrypt_decrypt('encrypt',$request->Password);
                              
            $fk_franchise= $request->FKFranchiseID;
//            $serialized_array = serialize($fk_franchise); 
//            $unserialized_array = unserialize($serialized_array); 

//            d($unserialized_array,1);
                    
            $assignedOutput = $input["assigned_output"];
            $input["CreatedDateTime"] = date("Y-m-d H:i:s");

            if (!isset($input["Status"])) {
                $input["Status"] = "Disabled";
            }

            unset($input["_token"]);
            unset($input["assigned_output"]);
//            foreach ($fk_franchise as $values){
//                
                $input["FKFranchiseID"]= implode(', ', $fk_franchise);
                
////                $userId = User::insertGetId($input);
////                $this->assignMenus($userId, $assignedOutput);
//               
//            }
                $userId = User::insertGetId($input);
                d($userId,1);
                $this->assignMenus($userId, $assignedOutput);
          

            \Session::flash('success', 'Franchise User added successfully!');
            return redirect('users');
          //  print_r($input);
        }
    }

    
    public function delete($id) {


        if ($id != 1) {
            User::where('PKUserID', $id)->delete();
            UserMenus::where('FKUserID', $id)->delete();
        }
        \Session::flash('error', 'Franchise  User deleted successfully!');
        return redirect('users');
    }
    public function assignMenus($userId, $assignedOutput) {

        $menus = json_decode($assignedOutput, true);
        $parentPosition = 0;
        if (sizeof($menus) > 0) {
            foreach ($menus as $menu) {
                $parentMenu = array(
                    'ParentMenuID' => 0,
                    'FKUserID' => $userId,
                    'FKMenuID' => $menu['id'],
                    'Position' => $parentPosition
                );
                $parentMenuId = UserMenus::insertGetId($parentMenu);

                if (isset($menu['children']) && count($menu['children']) > 0) {
                    $chiledPosition = 0;
                    foreach ($menu['children'] as $children) {
                        $childMenu = array(
                            'ParentMenuID' => $parentMenuId,
                            'FKUserID' => $userId,
                            'FKMenuID' => $children['id'],
                            'Position' => $chiledPosition
                        );
                        UserMenus::insertGetId($childMenu);
                        $chiledPosition++;
                    }
                }
                $parentPosition++;
            }
        }
    }
    public function edit($id) {

        $q = new User();
        $user=   $q->find($id);
        $data["franchise"]= Franchises::pluck('Title','PKFranchiseID');
        $fk_ids= explode(",",$user->FKFranchiseID);
        $fran= Franchises::whereIn('PKFranchiseID',$fk_ids)->get(['PKFranchiseID']);

        $data['pagesSelected']=$fran;
       
        $data['model'] =$user;
        
        $data['userMenus'] = $data['model']->menus->where("ParentMenuID", 0);
        $data["title"] = "Users";
        $menus = Menus::orderBy('Name', 'asc')->get();
        $data["menus"] = $menus;
        $assignMenus = array();

        $selectedMenus = array();

        if (count($data['userMenus']) > 0) {
            $i = 0;
            foreach ($data['userMenus'] as $val) {
                $menuModel = Menus::find($val->FKMenuID);
                $jsonArray["id"] = $val->FKMenuID;
                $childrenModel = $data['model']->menus->where("ParentMenuID", $val->PKAdminMenuID)->where("FKUserID", $id);
                $childArray = array();
                $selectedMenus[$i]["id"] = $val->FKMenuID;
                $selectedMenus[$i]["Name"] = $menuModel->Name;
                if (count($childrenModel) > 0) {
                    $j = 0;
                    foreach ($childrenModel as $childModel) {
                        $menuModel = Menus::find($childModel->FKMenuID);
                        $selectedMenus[$i]["children"][$j]["id"] = $menuModel->PKMenuID;
                        $selectedMenus[$i]["children"][$j]["Name"] = $menuModel->Name;

                        $childArray[$j]["id"] = $childModel->FKMenuID;
                        $j++;
                    }
                    $jsonArray["children"] = $childArray;
                }
                $assignMenus[$i] = $jsonArray;
                unset($jsonArray["children"]);
                $i++;
            }
            $assignMenus = json_encode($assignMenus);
        } else {
            $assignMenus = "";
        }

        $data["selectedMenus"] = $selectedMenus;
        $data["assignMenus"] = $assignMenus;
        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = "Edit";

        $data['breadcrumbs'] = $this->breadcrumbs;

        return view('franchise_users/edit', $data)->with('id', $id);
    }
    
    public function update($id, Request $request) {

        $rules = [
            'FirstName' => 'required|string',
            'LastName' => 'required|string',
            'EmailAddress' => 'required|string|email|unique:cms_users,EmailAddress,' . $id . ",PKUserID",
            'Password' => 'required|string'
        ];
        $input = $request->all();

        $message = [];
        $validator = Validator::make($input, $rules, $message);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        } else {
            $assignedOutput = $input["assigned_output"];
            unset($input["_token"]);
            unset($input["assigned_output"]);

            if (empty($input["Password"])) {
                unset($input["Password"]);
            }
            $input["Password"]=PasswordEncryptHelper::encrypt_decrypt('encrypt',$request->Password);
            $input["UpdatedDateTime"] = date("Y-m-d H:i:s");
            $fk_franchise= $request->FKFranchiseID;

            $input["FKFranchiseID"]= implode(', ', $fk_franchise);

            User::where('PKUserID', '=', $id)->update($input);
            UserMenus::where('FKUserID', '=', $id)->delete();

            $this->assignMenus($id, $assignedOutput);


            \Session::flash('message', 'User updated successfully!');
            return redirect('users');
        }
    }
}
