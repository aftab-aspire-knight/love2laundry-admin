<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Franchises;
use App\Reporting;

class ReportingController extends BaseController {

    // Dashboard - Analytics

    private $from;

    public function __construct() {
        $this->middleware('auth');
        $this->breadcrumbs = $this->getBreadcrumbs();
        $this->breadcrumbs[1]['link'] = "#_";
        $this->breadcrumbs[1]['name'] = "Reporting";

        $this->from = date("01 F, Y", strtotime("-6 month"));
        //date(', F Y', strtotime("-$i month"));
    }

    public function clientretention() {

        $title = "Client Retention";

        $data["pageHeader"] = false;
        $data["title"] = $title;
        $data["SubTitle"] = "List";

        $data["from"] = $this->from;
        $data["to"] = date("j F, Y");

        //$franchises = Franchises::where('Status', "Enabled")->pluck("Title", "PKFranchiseId");


        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $title;
        $data["breadcrumbs"] = $this->breadcrumbs;
        //$data["franchises"] = $franchises;

        return view('/reporting/client_retention', $data);
    }

    public function searchclientretention(Request $request) {

        $from = date("Y-m-d", strtotime($request->from_submit));
        $to = date("Y-m-d", strtotime($request->to_submit));
        $franchises = array();
        $model = Reporting::getClientRetention($from, $to, $franchises);
        $data["model"] = $model;
        
        return view('/reporting/ajax/search_client_retention', $data);
    }

    public function customersretargeting() {

        $title = "Customers Retargeting";
        $data["pageHeader"] = false;
        $data["title"] = $title;
        $data["SubTitle"] = "List";

        $data["date"] = $this->from;
        
        $franchises = Franchises::where('Status', "Enabled")->pluck("Title", "PKFranchiseId");

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $title;
        $data["breadcrumbs"] = $this->breadcrumbs;
        $data["franchises"] = $franchises;
        
        return view('/reporting/customers_retargeting', $data);
    }

    public function searchcustomersretargeting(Request $request) {


        
        $franchises = $request->franchises;

        $date = date("Y-m-d", strtotime($request->date_submit));
        $model = Reporting::getCustomersRetargeting($date,$franchises);
        $data["model"] = $model;
        return view('/reporting/ajax/search_customers_retargeting', $data);
    }

    public function orderanalysis() {

        $title = "Order Analysis";
        $data["pageHeader"] = false;
        $data["title"] = $title;
        $data["SubTitle"] = "List";

        $data["from"] = $this->from;
        $data["to"] = date("j F, Y");

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $title;
        $data["breadcrumbs"] = $this->breadcrumbs;
        return view('/reporting/order_analysis', $data);
    }

    public function searchorderanalysis(Request $request) {

        $from = date("Y-m-d", strtotime($request->from_submit));
        $to = date("Y-m-d", strtotime($request->to_submit));
        $model = Reporting::getOrderAnalysis($from, $to);
        $data["model"] = $model;
        return view('/reporting/ajax/search_order_analysis', $data);
    }

    public function rollingsales() {

        $title = "Rolling Sales";


        $data["pageHeader"] = false;
        $data["title"] = $title;
        $data["SubTitle"] = "List";

        $data["from"] = $this->from;
        $data["to"] = date("j F, Y");

        $this->breadcrumbs[2]['link'] = "#_";
        $this->breadcrumbs[2]['name'] = $title;
        $data["breadcrumbs"] = $this->breadcrumbs;
        return view('/reporting/rolling_sales', $data);
    }

    public function searchrollingsales(Request $request) {

        $from = date("Y-m-d", strtotime($request->from_submit));
        $to = date("Y-m-d", strtotime($request->to_submit));
        $model = Reporting::getRollingSales($from, $to);

        $data["model"] = $model;
        return view('/reporting/ajax/search_rolling_sales', $data);
    }

}
