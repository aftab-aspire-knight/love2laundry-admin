<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FranchiseServices extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        
        
        
        return [
            'id' => $this->PKFranchiseServiceID,
            'title' => $this->Title,
            'price' => $this->Price,
            'discount' => showDecimal($this->DiscountPercentage),
            'discount_type' => "percentage",
            'service_id' => $this->FKServiceID,
            'category_id' => $this->category->PKCategoryID,
            'category' => $this->category->Title,
            'mobile_title' => $this->category->MobileTitle,
            'preferences_show' => $this->category->PreferencesShow,
            'mobile_image_name' => $this->category->MobileImageName,
            'position' => $this->category->Position,
            
        ];
    }
    
    public function withResponse($request, $response)
    {
        $response->header('X-Value', 'True');
    }
}
