<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\FranchiseServices as FranchiseServicesResource;


class Franschises extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        
        return [
            'id' => $this->PKFranchiseID,
            'title' => $this->Title,
            'name' => $this->Name,
            'email' => $this->EmailAddress,
            'address' => $this->Address,
            'phone' => $this->Telephone,
            'mobile' => $this->Mobile,
            'status' => $this->Status,
            'pickup_limit' => $this->PickupLimit,
            'delivery_limit' => $this->DeliveryLimit,
            'opening_time' => $this->OpeningTime,
            'closing_time' => $this->ClosingTime,
            'allow_same_time' => $this->AllowSameTime,
            'delivery_option' => $this->DeliveryOption,
            'delivery_difference_hour' => $this->DeliveryDifferenceHour,
            'minimum_order_amount' => $this->MinimumOrderAmount,
            'minimum_order_amount_later' => $this->MinimumOrderAmountLater,
            'off_days' => $this->OffDays,
            'created_at' => $this->CreatedDateTime,
            'updated_at' => $this->UpdatedDateTime,
            'services' => FranchiseServicesResource::collection($this->services)
        ];
    }
    
    public function arrayServices($services) {
        
        $data = FranchiseServicesResource::collection($services);
    }
}
