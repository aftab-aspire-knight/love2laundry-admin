<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMenus extends Model {

    protected $table = 'cms_admin_menus';
    protected $primaryKey = 'PKAdminMenuID';
    protected $casts = [];
    public $timestamps = false;

    public function menu() {
        //return $this->belongsTo('App\Menus',FKMenuID);
        return $this->belongsTo('App\Menus',"FKMenuID","PKMenuID");
    }

}
