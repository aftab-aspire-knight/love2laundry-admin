<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SMSResponders extends Model
{
    protected $table = 'cms_sms_responders';
    protected $primaryKey = 'PKResponderID';
    public $timestamps = false;
}
