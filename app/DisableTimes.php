<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentTrait;

class DisableTimes extends Model {

    use EloquentTrait;

    protected $table = 'ws_disable_time_slots';
    
    //protected $primaryKey = 'FKFranchiseID';
    
    protected $casts = [];
    public $timestamps = false;
    
    
    
}
