<?php

namespace App\Services;

interface Country{
    
    public function validatePostalCode($postcode);
    public function getLatitudeLongitude($postcode);
    public function getAreaOrders($from,$to);
    public function getAreasList();
    public function ordersByLocations($from, $to,$locations);
    
    public function productsAnalyticsView($range,$franchises);
    public function getProductsAnatytics($input);
    
    
    public function nonCustomersView($range,$franchises);
    public function nonCustomersAction($input);
}
