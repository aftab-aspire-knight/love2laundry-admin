<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discounts extends Model
{
    protected $table = 'ws_discounts';
    protected $primaryKey = 'PKDiscountID';
    public $timestamps = false;
    function member(){
        return $this->hasOne('App\Members', "PKMemberID","FKMemberID");
    }
}
