<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembersPreference extends Model
{
    protected $table = 'ws_member_preferences';
    protected $primaryKey = 'PKMemberPreferenceID';
   
    public $timestamps = false;

    public function preferences() {
        return $this->belongsTo('App\Preferences',"FKPreferenceID","PKPreferenceID");
    }
}
