<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefundInformations extends Model
{
    protected $table = 'ws_payment_refund_informations';
    protected $primaryKey = 'PKRefundID';
   
    public $timestamps = false;

}
