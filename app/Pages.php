<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'cms_pages';
    protected $primaryKey = 'PKPageID';
   
    public $timestamps = false;

}
