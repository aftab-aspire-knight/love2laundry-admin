<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchisePostCodes extends Model {


    protected $table = 'ws_franchise_post_codes';
    
    protected $primaryKey = 'PKPostCodeID';
    
    protected $casts = [];
    public $timestamps = false;
    
}
