<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reporting extends Model {

    public static function getClientRetention($from, $to,$franchises = array()) {
        
        $subSql="";
        if(!empty($franchises)){
            
            $subSql="FKFranchiseID in (".implode(",",$franchises).") AND ";
        }

        $sql = "SELECT 	DATE_FORMAT(m.CreatedDateTime,'%Y-%m') AS JoiningMonth,
		COUNT(o.FKMemberID) AS Joiners,
		SUM(o.Orders) AS Orders,
        SUM(o.TotalOrders) AS TotalOrders,
		SUM(CASE WHEN o.Orders = 1 THEN 1 ELSE 0 END) AS OneOrder,
        SUM(CASE WHEN o.Orders = 1 THEN o.TotalOrders ELSE 0 END) AS OneOrderTotal,
		SUM(CASE WHEN o.Orders BETWEEN 2 AND 5 THEN 1 ELSE 0 END) AS LessThanFiveOrders,
        SUM(CASE WHEN o.Orders BETWEEN 2 AND 5 THEN o.TotalOrders ELSE 0 END) AS LessThanFiveOrdersTotal,
		SUM(CASE WHEN o.Orders > 5 THEN 1 ELSE 0 END) AS MoreThanFiveOrders,
        SUM(CASE WHEN o.Orders > 5 THEN o.TotalOrders ELSE 0 END) AS MoreThanFiveOrdersTotal
        FROM ws_members m 
        JOIN
        (
            SELECT FKMemberID,FKFranchiseID,COUNT(*) AS Orders,SUM(GrandTotal) AS TotalOrders
            FROM ws_invoices 
            WHERE $subSql 
            OrderStatus NOT IN ('Cancel','Pending')
            AND IsTestOrder = 'No'
            AND GrandTotal < 1000
            GROUP BY FKMemberID
        ) o ON (o.FKMemberID = m.PKMemberID)
        WHERE
        CreatedDateTime between '$from' and '$to'
        GROUP BY DATE_FORMAT(m.CreatedDateTime,'%Y-%m') 
        ORDER BY JoiningMonth";

        return $users = DB::select($sql);
    }

    public static function getCustomersRetargeting($date,$franchises = array()) {
        
        $subSql="";
        
        if(!empty($franchises)){
            
            $subSql="m.FKFranchiseID in (".implode(",",$franchises).") AND ";
        }
        

        $inActive = 60;
        $lastOrder = 180;

        $sql = "SELECT 	i.FKMemberID,m.FirstName,m.LastName,m.Phone,m.EmailAddress,UPPER(m.PostalCode) AS PostCode,
		CAST(MAX(i.CreatedDateTime) AS DATE) AS LastOrder,COUNT(*) AS OrderCount,SUM(i.GrandTotal) AS OrderTotal,SUM(i.GrandTotal)/COUNT(*) AS OrderAverage,f.Title
        FROM ws_invoices i 
        LEFT JOIN ws_members m ON (m.PKMemberID = i.FKMemberID)
        LEFT JOIN ws_franchises f ON (f.PKFranchiseID = m.FKFranchiseID)
        WHERE 
        $subSql
        OrderStatus NOT IN ('Cancel','Pending')
        AND IsTestOrder = 'No'
        AND GrandTotal < 1000
        AND i.FKMemberID NOT IN
        (
            SELECT FKMemberID
            FROM ws_invoices
            WHERE OrderStatus NOT IN ('Cancel','Pending')
            AND IsTestOrder = 'No'
            AND GrandTotal < 1000
            AND CreatedDateTime BETWEEN $date - INTERVAL $inActive DAY AND $date
        )
        GROUP BY i.FKMemberID,m.FirstName,m.LastName,m.Phone,m.EmailAddress,UPPER(m.PostalCode)
        HAVING LastOrder > '$date' - INTERVAL $lastOrder DAY
        ORDER BY OrderTotal DESC";

        return $users = DB::select($sql);
    }

    public static function getOrderAnalysis($from, $to) {

        $new = 1;
        $recent = 3;

        $sql = "SELECT 	DATE_FORMAT(i.CreatedDateTime,'%Y-%m') AS Month,COUNT(*) AS Orders,
        SUM(i.GrandTotal) AS TotalOrders,
        SUM(CASE WHEN m.CreatedDateTime BETWEEN i.CreatedDateTime - INTERVAL $new MONTH AND i.CreatedDateTime THEN 1 ELSE 0 END) AS NewOrders,
        SUM(CASE WHEN m.CreatedDateTime BETWEEN i.CreatedDateTime - INTERVAL $new MONTH AND i.CreatedDateTime THEN i.GrandTotal ELSE 0 END) AS TotalNewOrders,
        SUM(CASE WHEN m.CreatedDateTime BETWEEN i.CreatedDateTime - INTERVAL $recent MONTH AND i.CreatedDateTime - INTERVAL $new MONTH THEN 1 ELSE 0 END) AS RecentOrders,
        SUM(CASE WHEN m.CreatedDateTime BETWEEN i.CreatedDateTime - INTERVAL $recent MONTH AND i.CreatedDateTime - INTERVAL $new MONTH THEN i.GrandTotal ELSE 0 END) AS TotalRecentOrders,
        SUM(CASE WHEN m.CreatedDateTime < i.CreatedDateTime - INTERVAL $recent MONTH THEN 1 ELSE 0 END) AS RepeatOrders,
        SUM(CASE WHEN m.CreatedDateTime < i.CreatedDateTime - INTERVAL $recent MONTH  THEN i.GrandTotal ELSE 0 END) AS TotalRepeatOrders
FROM ws_invoices i
JOIN ws_members m ON (m.PKMemberID = i.FKMemberID)
WHERE i.OrderStatus <> 'Cancel'
AND i.GrandTotal < 1000
AND i.CreatedDateTime Between '$from' and '$to'
GROUP BY DATE_FORMAT(i.CreatedDateTime,'%Y-%m') order by i.CreatedDateTime desc ";

        return $users = DB::select($sql);
    }

    public static function getRollingSales($from, $to) {

        $window = 30;

        $sql = "SELECT 	d.Date,
		COUNT(DISTINCT o.FKMemberID) AS Customers,
		COUNT(*) AS Orders,
		SUM(o.GrandTotal) AS OrdersTotal,
                SUM(o.GrandTotal) / COUNT(*) AS AverageOrder
                FROM
                (
                    SELECT DISTINCT DATE(CreatedDateTime) AS Date
                    FROM ws_invoices
                    WHERE CreatedDateTime between '$from' and '$to'
                ) d
                JOIN
                (
                    SELECT CreatedDateTime,FKMemberID,GrandTotal
                    FROM ws_invoices
                    WHERE OrderStatus NOT IN ('Cancel','Pending')
                    AND IsTestOrder = 'No'
                    AND GrandTotal < 1000
                ) o ON (o.CreatedDateTime BETWEEN d.Date - INTERVAL $window DAY AND d.Date)
                GROUP BY d.Date";

        return $users = DB::select($sql);
    }

}
