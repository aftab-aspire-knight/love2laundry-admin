<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Members extends Authenticatable implements MustVerifyEmail {

    use HasApiTokens,
        Notifiable;
    
    

    protected $guard = "api";
    protected $table = 'ws_members';
    protected $primaryKey = 'PKMemberID';
    public $timestamps = false;
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'FirstName', 'LastName', 'EmailAddress', 'Password','BuildingName','StreetName','PostalCode','Town','CreatedDateTime','Phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Password', 'remember_token',
    ];

    public function invoices() {
        return $this->hasMany(Invoices::class, 'FKMemberID', 'PKMemberID');
    }

    public function preference() {
        return $this->hasMany('App\MembersPreference', "FKMemberID");
    }

    public static function checkModel($id) {
        $id = 'PKMemberID';
        return $id;
    }

    public function franchise() {
        return $this->hasOne(Franchises::class, "FKFranchiseID", "PKFranchiseID");
    }

    public function getEmailAttribute() {

        return $this->EmailAddress;
    }

    public function getAuthPassword() {
        return $this->Password;
    }

}
