<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class NotifyForgotPassword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
         protected $firstName,$lastName,$email,$view,$subject,$FromName,$fromEmail,$content;

      public function __construct($firstName, $lastName, $email, $view,$subject,$FromName,$fromEmail,$content)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->view = $view;
        $this->subject = $subject;
        $this->FromName = $FromName;
        $this->fromEmail = $fromEmail; 
        $this->content=$content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $member = [
            'firstName'         => $this->firstName,
            'lastName'         => $this->lastName,
            'email'                => $this->email,
        ];
        $data = array('user'=>$member);
      

         Mail::send( $this->view,  ['content' => $this->content], function ($m) use ($member) {
                $m->from( $this->fromEmail,  $this->FromName);
                $m->to($member["email"], $member["firstName"] . " " . $member["lastName"])->subject($this->subject);
            });
    }
}
