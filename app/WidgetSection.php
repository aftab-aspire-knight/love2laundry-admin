<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WidgetSection extends Model
{
    protected $table = 'cms_widget_sections';
    protected $primaryKey = 'PKSectionID';
    public $timestamps = false;
}
