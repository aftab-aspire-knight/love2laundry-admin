<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentTrait;

class DisableDates extends Model {

    use EloquentTrait;
    protected $table = 'ws_disable_date_slots';
    
    protected $primaryKey = 'PKDateID';
    
    protected $casts = [];
    public $timestamps = false;
    
}
