<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribers extends Model
{
    protected $table = 'ws_subscribers';
    protected $primaryKey = 'PKSubscriberID';
    public $timestamps = false;
}
