<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'cms_tags';
    protected $primaryKey = 'PKTagID';
    public $timestamps = false;
}