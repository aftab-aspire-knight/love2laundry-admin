<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseTimings extends Model {


    protected $table = 'ws_franchise_timings';
    
    //protected $primaryKey = 'FKFranchiseID';
    
    protected $casts = [];
    public $timestamps = false;
    
    
    
}
