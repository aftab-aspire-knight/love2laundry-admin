<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    protected $table = 'ws_preferences';
    
    protected $primaryKey = 'PKPreferenceID';
    
    public $timestamps = false;
    
    function category(){
        return $this->hasOne('App\Preferences', "PKPreferenceID","ParentPreferenceID");
    }
    
    function preferences(){
        return $this->hasMany('App\Preferences', "ParentPreferenceID","PKPreferenceID");
    }
    
    
}
