<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailsResponders extends Model
{
    protected $table = 'cms_email_responders';
    protected $primaryKey = 'PKResponderID';
    public $timestamps = false;
}
