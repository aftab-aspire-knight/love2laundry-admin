<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoicePreferences extends Model
{
    protected $table = 'ws_invoice_preferences';
    protected $primaryKey = 'PKInvoicePreferenceID';
   
    public $timestamps = false;

}
