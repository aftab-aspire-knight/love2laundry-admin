<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navigations extends Model
{
    protected $table = 'cms_navigations';
    protected $primaryKey = 'PKNavigationID';
   
    public $timestamps = false;

}
