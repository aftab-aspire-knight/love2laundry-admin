<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $table = 'ws_locations';
    protected $primaryKey = 'PKLocationID';
    public $timestamps = false;
    
    function lockers(){
        return $this->hasMany('App\Lockers', "FKLocationID","PKLocationID");
    }
}
