<?php

namespace App;
use App\Members;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table = 'ws_invoices';
    protected $primaryKey = 'PKInvoiceID';
    public $timestamps = false;
    
    public function members()
    {
        return $this->belongsTo(Members::class, 'FKMemberID', 'PKMemberID');           
    }
    
    public function franchises()
    {
        return $this->belongsTo(Franchises::class, 'FKFranchiseID', 'PKFranchiseID');           
    }
    
    public function services()
    {
        return $this->hasMany(InvoiceServices::class, 'FKInvoiceID', 'PKInvoiceID');           
    }
    
    public function preferences()
    {
        return $this->hasMany(InvoicePreferences::class, 'FKInvoiceID', 'PKInvoiceID');           
    }
  
    
}
