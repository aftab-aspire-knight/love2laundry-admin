<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model {


    protected $table = 'ws_services';
    
    protected $primaryKey = 'PKServiceID';
    
    protected $casts = [];
    public $timestamps = false;
    
}
