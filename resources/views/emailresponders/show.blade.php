
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('emailresponders')}}" class=" mr-1  btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Title :</td>
                                    <td>{{ $model->Title }}</td>
                                </tr>
                                 <tr>
                                    <td>Email From  :</td>
                                    <td>{{ $model->FromEmail }}</td>
                                </tr>

                                <tr>
                                    <td>Email To  :</td>
                                    <td>{{ $model->ToEmail }}</td>
                                </tr>
                                <tr>
                                    <td>Subject  :</td>
                                    <td>{{ $model->Subject }}</td>
                                </tr>
                                <tr>
                                    <td>Content * :</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="hide">Show</div></td>
                                </tr>
                                <tr id="contentDiv">
                                    <td></td>
                                    <td id="data">{{ $model->Content }}</td>

                                </tr>
                                <tr>
                                    <td>Preview</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="previewclick">Preview</div></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td id="previewdata"><html> <body> <p><?php echo $model->Content;?></p> </body></html></td>
<!--                                    <td id="previewdata"><textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor">{{ $model->Content }}</textarea></td>-->
                                </tr>
                                <tr>
                                    <td>Status:</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
   

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script>
$( document ).ready(function() {
     $("#data").hide();
     $("#previewdata").hide();
     
$("#hide").click(function(){
   $("#data").toggle();
    $(this).toggleClass('class1')
 
});
$("#previewclick").click(function(){
   $("#previewdata").toggle();
    $(this).toggleClass('class1')
 
});


});

</script>
<script>
        CKEDITOR.config.allowedContent = true;

    CKEDITOR.replace( 'summary-ckeditor' );
</script>
    
@endsection