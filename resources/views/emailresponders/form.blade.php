@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Email From') !!}
            {!! Form::text('FromEmail', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Email To') !!}
            {!! Form::text('ToEmail', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Subject') !!}
            {!! Form::text('Subject', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>

</div> 
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Content') !!}
            {!! Form::textarea('Content', null, ['class'=>'form-control mb-20 editor','id'=>'ckview','rows' => 15, 'cols' => 80]) !!}

        </div>
    </div> 
</div>


<div class="col-md-12 col-xs-12 col-lg-12">

<!--    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" >
                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
  <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
</div>
<br clear="all"/>

 <div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('emailresponders')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
<br clear="all"/>

<div>
    <div class="col-md-12 col-xs-12 col-lg-12">
        <table class="table table-bordered no-more-tables">
            <thead class="thead-light ">
                <tr>
                    <th class="text-center" width="40%">Title</th>
                    <th class="text-center" width="60%">Tag</th>
                </tr>
            </thead>
            <tbody class="table-hover-animation table-striped  ">
                <?php
                if (isset($model['tags']) && sizeof($model['tags']) > 0) {
                    foreach ($model['tags'] as $record) {
                        echo '<tr>';
                        echo '<td class="text-center">' . $record['Title'] . '</td>';
                        echo '<td class="text-center">' . $record['Tag'] . '</td>';
                        echo '</tr>';
                    }
                } else {
                    echo '<tr><td class="text-center" colspan="2">No Record Found</td></tr>';
                }
                ?>
            </tbody>
        </table>
    </div>

</div>
