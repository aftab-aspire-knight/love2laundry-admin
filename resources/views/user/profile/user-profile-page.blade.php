
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        {!! Form::open(['action' => 'UserController@update_profile','method' => 'post']) !!}

                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        <div class="row">
                            {!! Form::hidden('invisible', $record->PKUserID) !!}
                            <div class="col-md-6 col-xs-12 col-lg-6 form-label-group">

                                {!! Form::label('fname-label', 'First Name'); !!}
                                <br>
                                {!! Form::text('fname', $value = $record->FirstName, ['class' => 'form-control','Placeholder' => 'First Name']);!!}
                            </div>
                            <div class="col-md-6 col-xs-12 col-lg-6 form-label-group">
                                {!! Form::label('lname-label', 'Last Name'); !!}
                                <br>
                                {!! Form::text('lname', $value = $record->LastName, ['class' => 'form-control','Placeholder' => 'Last Name']);!!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-lg-6 form-label-group">
                                {!! Form::label('email-label', 'E-Mail Address'); !!}
                                <br>
                                {!! Form::email('email', $value = $record->EmailAddress, ['class' => 'form-control','Placeholder' => 'Email Address']);!!}
                            </div>
                            <br clear="all">

                            <div class="col-12">
                                <div class="model-footer">
                                    <i class="btn btn-primary waves-input-wrapper waves-effect waves-light" style="color:rgb(255, 255, 255);background:rgba(0, 0, 0, 0)">
                                        {!! Form::submit('Save!', ['class' => 'waves-button-input']); !!}
                                    </i>
                                    <i class="btn btn-white waves-input-wrapper waves-effect waves-light" style="color:rgb(98, 98, 98);background:rgba(0, 0, 0, 0)">
                                        <input type="Reset" value="Cancel" class="waves-button-input" data-dismiss="modal" style="background-color:rgba(0,0,0,0);"></i>
                                </div>
                            </div>
                            {!! Form::close() !!}   
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection