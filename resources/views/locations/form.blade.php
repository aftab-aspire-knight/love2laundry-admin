<?php
$types = Config::get('params.locations.types');
$statuses = Config::get('params.locations.statuses');
$for = Config::get('params.locations.for');
$used = Config::get('params.locations.used');
$required = "required";
?>
@include("messages")
<div id='errors'></div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('Title') !!}<span class="red-color">*</span>
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Post Code') !!} <span class="red-color">*</span>
            {!! Form::text('PostalCode', null , array('class' => 'form-control') ) !!}
        </div>
    </div>
<!--    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" @if($model->Status=='Enabled') checked="checked" @endif />
                           <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather @if($model->Status=='Enabled') icon-check @endif "></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
  <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('Lockers') !!}
            {!! Form::text('Lockers', null , array("data-role"=>"tagsinput",'id' => 'Lockers','class' => 'form-control') ) !!}
        </div>
    </div>
</div>
<br clear="all" />
<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Save</button>
        <a href="{{url('locations')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
@section('myscript')
@include('vendor_script')


<script>

    $(document).ready(function () {

        $(".save").click(function () {

            var url = $("#form").attr('action');
            var type = "POST";
            var dataType = "json";
            var data = $("#form").serialize();

            var callBack = function (response) {

                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    window.top.location.href = '{{url("locations/")}}';
                }
            };

            ajaxRequest(url, type, dataType, data, callBack, {});

        });
    });
</script>
{{-- Page js files --}}
@endsection