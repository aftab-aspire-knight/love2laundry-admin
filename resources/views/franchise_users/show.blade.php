
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('users')}}" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a>  {{$title}} ({{ $model->FirstName }} {{ $model->LastName }})</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>First Name :</td>
                                    <td>{{ $model->FirstName }}</td>
                                </tr>
                                <tr>
                                    <td>Last Name :</td>
                                    <td>{{ $model->LastName }}</td>
                                </tr>
                                <tr>
                                    <td>Email :</td>
                                    <td>{{ $model->EmailAddress }}</td>
                                </tr>
                                <tr>
                                    <td>Status :</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                                <tr>
                                    <td>Last Updated:</td>
                                    <td>{{ $model->UpdatedDateTime }}</td>
                                </tr>
                                <tr>
                                    <td>Franchise :</td>
                                    <td>{{ $franchise->Title }}</td>
                                </tr>
                                <tr>
                                    <td>Created:</td>
                                    <td>{{ $model->CreatedDateTime }}</td>
                                </tr>
                                <tr>
                                    <td>Last Updated:</td>
                                    <td>{{ $model->UpdatedDateTime }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Roles</h4>
        </div>
        <div class="card-content">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        if (count($selectedMenus) > 0) {
                            echo '<ol class="dd-list">';
                            foreach ($selectedMenus as $rec) {
                                ?>
                                <li class="dd-item" data-id="{{$rec['id']}}">
                                    <div class="dd-handle"><b>{{$rec['Name']}}</b></div>
                                    <?php if (!empty($rec['children'])) { ?>
                                        <ol class="dd-list">
                                            <?php
                                            foreach ($rec['children'] as $child) {
                                                ?>
                                                <li class="dd-item" data-id="{{$child['id']}}"><div class="dd-handle">{{$child['Name']}}</div>
                                                </li>
                                            <?php } ?>
                                        </ol><?php } ?>
                                </li>
                                <?php
                            }
                            echo '</ol>';
                        } else {
                            ?>
                            <div class="dd-empty"></div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
@endsection