@include("messages")
<input type="hidden" id="assigned_output" name="assigned_output" value="<?php echo $assignMenus; ?>" />
<?php
$roles = Config::get('params.roles');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('First Name') !!}
            {!! Form::text('FirstName', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Last Name') !!}
            {!! Form::text('LastName', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('EmailAddress', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>

    <div class="col-sm-6">
           <div class="form-group">
            {!! Form::label('Franchise') !!}
            {!! Form::select('FKFranchiseID[]', $franchise,$pagesSelected,['class' => 'form-control select2','multiple'=>'multiple',$required,'id'=>'FKFranchiseID'] ) !!}

<!--            {!! Form::select('FKFranchiseID', $franchise,null,['class' => 'form-control','name'=>'FKFranchiseID'] ) !!}-->
        </div>
    </div>
</div>
<div class="row">
<!--    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" >
                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
    <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Password') !!}
            {!! Form::password('Password',array('placeholder'=>'Password','class' => 'form-control')); !!}
        </div>
    </div>

    <!--
    <div class="row">

        <h3 class="card-title">Administrator <span class="semi-bold">Menus</span></h3>
        <p>Left Side is user assigned menu list and the Right Side is user unassigned menu list</p>

    </div>-->

</div>
<div class="row">

    <div class="col-md-12 col-xs-12 col-lg-12">
        <h3>Administrator <span class="semi-bold">Menus</span></h3>
        <p>Left Side is user assigned menu list and the Right Side is user unassigned menu list</p>
        <br>
        <div class="cf nestable-lists">
            <div class="dd col-md-6 col-xs-12 col-lg-6" id="assigned_menus" style="margin-left:0px;height:300px !important;overflow-y: auto;">
                <?php
                if (count($selectedMenus) > 0) {
                    echo '<ol class="dd-list">';
                    foreach ($selectedMenus as $rec) {
                        ?>
                        <li class="dd-item" data-id="{{$rec['id']}}">
                            <div class="dd-handle">{{$rec['Name']}}</div>
                            <?php if (!empty($rec['children'])) { ?>
                                <ol class="dd-list">
                                    <?php
                                    foreach ($rec['children'] as $child) {
                                        ?>
                                        <li class="dd-item" data-id="{{$child['id']}}"><div class="dd-handle">{{$child['Name']}}</div>
                                        </li>
                                    <?php } ?>
                                </ol><?php } ?>
                        </li>
                        <?php
                    }
                    echo '</ol>';
                } else {
                    ?>
                    <div class="dd-empty"></div>
                <?php } ?>
            </div>
            <div class="dd col-md-5 col-xs-12 col-lg-5" id="unassigned_menus" style="margin-left:20px;height:300px !important;overflow-y: auto;">
                <ol class="dd-list dark">
                    <?php
                    if (isset($menus) && sizeof($menus) > 0) {
                        foreach ($menus as $rec) {
                            ?>
                            <li class="dd-item" data-id="{{$rec->PKMenuID}}"><div class="dd-handle">{{$rec->Name}}</div></li>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="dd-empty"></div>
                        <?php
                    }
                    ?>
                </ol>                                            
            </div>
        </div>
    </div>
</div>
<br clear="all" />
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('users')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>