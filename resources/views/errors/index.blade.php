
@extends('layouts/contentLayoutMaster')
<title>Under Construction</title>

<style>
body, html {
  height: 100%;
  margin: 0;
}
.topleft {
  position: absolute;
  top: 0;
  left: 16px;
}

.bottomleft {
  position: absolute;
  bottom: 0;
  left: 16px;
}

.middle {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

hr {
  margin: auto;
  width: 40%;
}
</style>
@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row" style="background-image: url('{{ asset('images/pages/vuexy-login-bg.jpg')}}');    height: 100%; background-position: center;   background-size: cover; position: relative; color: white; font-family: "Courier New", Courier, monospace; font-size: 25px;">
 
  <div class="middle">
    <h1>COMING SOON</h1>
  </div>

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

@endsection
