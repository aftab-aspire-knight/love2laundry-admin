@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('content')

<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add New {{$title}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::open(array( 'id' => 'form','class' => 'number-tab-steps wizard-circle wizard clearfix','url' => 'franchises/unavailabletimes/save')) !!}
                            @include('unavailable_times.form')

                            {!! Form::close() !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection