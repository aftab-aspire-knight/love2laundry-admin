
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <a href="{{url("franchises/unavailabletimes/create/$id")}}" class="btn btn-primary mb-2"><i class="feather icon-plus"></i></a>
                        <a href="{{url('franchises/')}}" class="btn btn-danger mb-2">Back</a>
                        <div class="table-responsive">

                            @if(count($model))
                            <table class="table add-rows">
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th >Date From</th>
                                        <th >Date To</th>
                                        <th >Type</th>
                                        <th ></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr>
                                        <td>{{$row->PKDateID}}</td>
                                        <td>{{$row->DisableDateFrom}}</td>
                                        <td>{{$row->DisableDateTo}}</td>
                                        <td>{{$row->Type}}</td>
                                        <td>
                                            <div class="btn-group dropdown actions-dropodown">
                                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>                                               
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{url('franchises/unavailabletimes/edit/'.$row->FKFranchiseID."/".$row->PKDateID)}}"><i class="feather icon-edit"></i> Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="confirm-delete dropdown-item" data-href="{{url('franchises/unavailabletimes/delete/'.$row->FKFranchiseID."/".$row->PKDateID)}}" data-id="{{$row->PKDateID}}" ><i class="fa fa-trash-o"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th >Date From</th>
                                        <th >Date To</th>
                                        <th >Type</th>
                                        <th ></th>
                                    </tr>
                                </tfoot>
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<!--
<script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
-->
<script>
    $(document).ready(function () {
        $('.add-rows').DataTable();
    });

    $('.confirm-delete').on('click', function () {
        var id = $(this).data('id');
        var href = $(this).data('href');
        //alert(href);
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                // alert(id);
                window.top.location = href;
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                    title: 'Cancelled',
                    text: '',
                    type: 'error',
                    confirmButtonClass: 'btn btn-success',
                })
            }
        })
    });

</script>
@endsection