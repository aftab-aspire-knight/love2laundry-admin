@include("messages")
<div id='errors'></div>
<?php
$types = Config::get('params.unavailable_times_types');
$required = "required";
?>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('Date From') !!}<span class="red-color">*</span>
            {!! Form::text('DisableDateFrom', null , array('class' => 'form-control pickadate',$required) ) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('Date To') !!}<span class="red-color">*</span>
            {!! Form::text('DisableDateTo', null , array('class' => 'form-control pickadate',$required) ) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('Type') !!}<span class="red-color">*</span>
            {!! Form::select('Type', $types,null,['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('Time') !!}<span class="red-color">*</span>
            {!! Form::select('Time', array("All Day"=>"All Day","Specific"=>"Specific"),$time,['class' => 'form-control','id' => 'Time']) !!}
        </div>
    </div>
</div>
<div class="row" id="div_timings" @if($time=="All Day") style="display: none;" @endif >
     <div class="col-sm-12">

        <ul class="list-unstyled mb-0">
            @foreach($range as $key => $val)

            <li class="col-sm-12 col-md-3 col-xs-6 d-inline-block mr-2">
                <fieldset>
                    <label>
                        <input type="checkbox" name="timings[]" id="timings" value="{{$key}}"

                               <?php if (in_array($key, $selectedTime)) { ?>
                                   checked="checked"
                               <?php } ?>
                               >
                        {{$key}} 
                    </label>
                </fieldset>
            </li>
            @endforeach
        </ul>

    </div>
</div>
{{ Form::hidden('FKFranchiseID', $franchise_id, array('id' => 'FKFranchiseID')) }}

<br clear="all" />
<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Save</button>
        <a href="{{url('franchises/unavailabletimes')}}/{{$franchise_id}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
@section('myscript')
@include('vendor_script')

<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
<script>

$(document).ready(function () {
    $('.select2').select2();
    $('#Time').change(function () {


        var Time = $('#Time').val();

        if (Time === "Specific") {

            $('#div_timings').show();
        } else {
            $('#div_timings').hide();
        }

    });

    $(".save").click(function () {

        var url = $("#form").attr('action');
        var type = "POST";
        var dataType = "json";
        var data = $("#form").serialize();

        var callBack = function (response) {

            if (response.error === 1) {
                displayErrors(response, "errors");
            } else
            {
                window.top.location.href = '{{url("franchises/unavailabletimes/$franchise_id")}}';
            }
        };

        ajaxRequest(url, type, dataType, data, callBack, {});

    });
});
</script>

<script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
{{-- Page js files --}}
@endsection