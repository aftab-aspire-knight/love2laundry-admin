<div class="card">

    <div class="card-header">
        <h4 class="card-title">Customer who hasn't place the order </h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="users-list-filter">
                <form id="filter" name="filter" action="{{url('dashboard/membersnotinorders')}}">

                    <div class="row align-items-end">
                        <div class="col-md-3 col-lg-3 col-sm-12">
                            {!! Form::label('Franchise') !!}
                            {!! Form::select('franchises[]', $franchises,[],['class' => 'form-control franchises select2',"multiple"=>"multiple"]) !!}
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12" id="codes_div" style="display: none;">
                            {!! Form::label('Districts') !!} 
                            {!! Form::select('codes[]', array(),null,['id' =>'codes', 'class' => 'form-control codes select2',"multiple"=>"multiple"]) !!}
                        </div>

                        <div class="col-md-2 col-lg-2 col-sm-12">
                            {!! Form::label('Limit') !!} 
                            {!! Form::select('limit', array("7 days"=>"7 days","30 days"=>"30 days","60 days"=>"60 days","90 days"=>"90 days","6 months"=>"6 Months","1 year"=>"Last Year",""=>"Never Ordered"),null,['id' =>'limit', 'class' => 'form-control select2']) !!}
                        </div>

                        <div class="col-md-3 col-lg-3 col-sm-12">
                            <label>Range</label>
                            <div class="input-group">
                                <input type="text" class="form-control daterange"  name="range" value="{{date("m/d/Y",strtotime($from))}} - {{date("m/d/Y",strtotime($to))}}" />

                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="fa fa-calendar-o"></span>
                                    </span>
                                </div>
                            </div>
                            <input type="hidden" name="from" id="from" value="{{$from}}" />
                            <input type="hidden" name="to" id="to" value="{{$to}}" />

                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12">
                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-light" id="members_not_in_orders_btn">Search</button>
                        </div>

                    </div>
                    <input type="hidden" name="div" id="div" value="members_not_in_orders" />

                </form>
                <div class="row">
                    <div class="col-md-12" id="members_not_in_orders">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    $(".franchises").change(function () {
        var value = this.value;
        var values = $(this).val();

        //console.log(values);

        getCodesOptions(values);
    });

    function getCodesOptions(values) {

        var url = "{{url('franchises/codes')}}";
        var type = "POST";
        var dataType = "json";
        $('#codes_div').hide();
        var callBack = function (response) {
            html = "<option value=''>All</option>";
            $.each(response, function (key, value) {
                html += "<option value=" + key + ">" + value + "</option>";
            });
            $('#codes').html(html);
            $('#codes_div').show();
            return false;
        };

        var errorCallBack = function (response) {
            //$('.search').prop('disabled', false);
        };

        ajaxRequest(url, type, dataType, {franchises: values, _token: "<?php echo csrf_token(); ?>"}, callBack, errorCallBack);

    }

    $(document).ready(function () {
        getCodesOptions("");
    });
</script>