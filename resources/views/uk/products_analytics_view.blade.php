<div class="card">
    <div class="card-header">
        <h4 class="card-title">Products </h4>
        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
        <div class="heading-elements">
            <ul class="list-inline mb-0">
                <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="users-list-filter">
                <form id="filter" name="filter" action="{{url('dashboard/productsanatytics')}}">

                    <div class="row align-items-end">
                        <div class="col-md-6 col-lg-6 col-sm-12">
                            {!! Form::label('Franchise') !!} 
                            {!! Form::select('franchise', $franchises,null,['class' => 'form-control select2']) !!}
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <label>Range</label>
                            <div class="input-group">
                                <input type="text" class="form-control daterange"  name="range" value="{{date("m/d/Y",strtotime($from))}} - {{date("m/d/Y",strtotime($to))}}" />

                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <span class="fa fa-calendar-o"></span>
                                    </span>
                                </div>
                            </div>
                            <input type="hidden" name="from" id="from" value="{{$from}}" />
                            <input type="hidden" name="to" id="to" value="{{$to}}" />

                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12">
                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-light" id="orders_franchises_btn">Search</button>
                        </div>

                    </div>
                    <input type="hidden" name="div" id="div" value="products_anatytics" />

                </form>
                <div class="row">
                    <div class="col-md-12" id="products_anatytics">

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>