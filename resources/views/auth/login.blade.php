@extends('layouts/fullLayoutMaster')

@section('title', 'Login')

@section('mystyle')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

<?php /* ?>
  @section('myscript')
  <script src="{{ asset(mix('js/scripts/auth/login.js')) }}"></script>
  @endsection
  <?php */ ?>
@section('content')
<section class="row flexbox-container">
    <div class="col-xl-8 col-11 d-flex justify-content-center">
        <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                    <img src="{{ asset('assets/images/front/login-logo.png') }}" alt="<?php echo env("APP_NAME"); ?>">
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Login</h4>
                            </div>
                        </div>
                        <p class="px-2">Welcome back, please login to your account.</p>
                        <div class="card-content">
                            @include("messages")
                            <div class="card-body pt-1">
                                <form method="POST" action="check-login">
                                    @csrf
                                    <fieldset class="form-label-group form-group position-relative has-icon-left">

                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="E-Mail Address" value="{{ old('email') }}" required autocomplete="true" autofocus >

                                        <div class="form-control-position">
                                            <i class="feather icon-user"></i>
                                        </div>
                                        <label for="email">E-Mail Address</label>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </fieldset>

                                    <fieldset class="form-label-group position-relative has-icon-left">
                                        <div class="input-group">
                                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="" name="password" placeholder="Password"required autocomplete="true" data-toggle="password">
                                            <div class="form-control-position">
                                                <i class="feather icon-lock"></i>
                                            </div>

                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-eye"><a href="#_"></a></i>
                                                </span>
                                            </div>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </fieldset>
                                    <div class="form-group d-flex justify-content-between align-items-center">
                                        <div class="text-left">
                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input name="remember_me" value="true" id="remember_me" type="checkbox" {{ old('remember_me') ? 'checked' : '' }}>
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span class="">Remember me</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        @if (Route::has('password.request'))
                                        <div class="text-right"><a class="card-link" href="auth-forgot-password">
                                                Forgot Password?
                                            </a></div>
                                        @endif

                                    </div>

                                    <button type="submit" class="btn btn-primary float-right btn-inline">Login</button>
                                </form>
                            </div>
                        </div>
                        <div class="login-footer">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".input-group-text").on('click', function (event) {
            event.preventDefault();
            if ($('#password').attr("type") == "text") {
                $('#password').attr('type', 'password');
                $('.input-group-text i').addClass("fa-eye-slash");
                $('.input-group-text i').removeClass("fa-eye");
            } else if ($('#password').attr("type") == "password") {
                $('#password').attr('type', 'text');
                $('.input-group-text i').removeClass("fa-eye-slash");
                $('.input-group-text i').addClass("fa-eye");
            }
        });
    });
</script>

@endsection


@include('vendor_script')


@section('myscript')
{{-- Page js files --}}