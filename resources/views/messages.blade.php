@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissible fade show" role="alert">

    @foreach ($errors->all() as $error)
    <p class="mb-0">{{ $error }}</p>
    @endforeach
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
    </button>
</div>
@endif
@if (Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <p class="mb-0">{!! session('success') !!}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
    </button>
</div>
@endif

@if (Session::has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Whoops! There were some problems with your input.</strong>
    <p class="mb-0">{!! session('danger') !!}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
    </button>
</div>
@endif

@if (Session::has('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <p class="mb-0">{!! session('error') !!}</p>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
    </button>
</div>
@endif
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">  
        <div id="errors"></div>
    </div> 
</div>