@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('mystyle')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection
@section('content')
<section class="row flexbox-container">
    <div class="col-xl-8 col-11 d-flex justify-content-center">
        <div class="card bg-authentication rounded-0 mb-0">
            <div class="row m-0">
                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                    <img src="{{ asset('assets/images/front/login-logo.png') }}" alt="branding logo">
                </div>
                <div class="col-lg-6 col-12 p-0">
                    <div class="card rounded-0 mb-0 px-2">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="mb-0">Login</h4>
                            </div>
                        </div>
                        <p class="px-2">Welcome back, please login to your account.</p>
                        <div class="card-content">
                            <div id="errors"></div>
                            <div class="card-body pt-1">

                                <form action="check-login" method="post">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                        <input type="text" class="form-control" name="email" id="email" placeholder="Email Address..." required value="admin@love2laundry.com">
                                        <div class="form-control-position">
                                            <i class="feather icon-user"></i>
                                        </div>
                                        <label for="user-name">Username</label>
                                    </fieldset>

                                    <fieldset class="form-label-group position-relative has-icon-left">
                                        <input type="password" class="form-control" id="user-password" placeholder="Password" required value="passpass">
                                        <div class="form-control-position">
                                            <i class="feather icon-lock"></i>
                                        </div>
                                        <label for="user-password">Password</label>
                                    </fieldset>
                                    <div class="form-group d-flex justify-content-between align-items-center">
                                        <div class="text-left">
                                            <fieldset class="checkbox">
                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                    <input type="checkbox">
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span class="">Remember me</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="text-right"><a href="auth-forgot-password" class="card-link">Forgot Password?</a></div>
                                    </div>
                                    
                                    <button  id="login" type="button" class="btn btn-primary float-right btn-inline">Login</button>
                                </form>
                            </div>
                        </div>
                        <div class="login-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('#login').click(function ()
    {

        var email = $("#email").val();
        var password = $("#user-password").val();
        var token = $("#_token").val();

        $.ajax({url: "<?php echo url("check-login"); ?>",
            type: 'post',
            dataType: 'json',
            data: {email: email, password: password, _token: token},
            success: function (response) {
                //alert(response.error)
                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    window.location = "<?php echo url('dashboard'); ?>";
                }

            },
            error: function (jqXHR, error, errorThrown) {
                displayErrors(jqXHR.responseJSON, "errors");
            }
        });
    });

    

</script>

@endsection
