
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard Analytics')

@section('vendor-style')
        <!-- vednor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/tether-theme-arrows.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/tether.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/shepherd-theme-default.css')) }}">
@endsection
@section('mystyle')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-analytics.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/tour/tour.css')) }}">
  @endsection

  @section('content')
    {{-- Dashboard Analytics Start --}}
    <section id="dashboard-analytics">
        <div class="row">
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
              <div class="card-header d-flex flex-column align-items-start pb-0">
                  <div class="avatar bg-rgba-primary p-50 m-0">
                      <div class="avatar-content">
                          <i class="feather icon-users text-primary font-medium-5"></i>
                      </div>
                  </div>
                  <h2 class="text-bold-700 mt-1 mb-25">{{$count_member}}</h2>
                  <p class="mb-0">No of Members</p>
              </div>
              <div class="card-content">
                  <div id="line-area-chart-1"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices}}</h2>
                    <p class="mb-0">No of Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-2"></div>
                </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices_pending}}</h2>
                    <p class="mb-0">No of Pending Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-3"></div>
                </div>
            </div>
          </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices_completed}}</h2>
                    <p class="mb-0">No of Completed Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-4"></div>
                </div>
            </div>
          </div>
      </div>             
      <div class="row">
          <div class="col-md-6 col-12">
              <div class="card">
                   <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
             <div class="card-content">
                    <div class="card-body">
                        
                        <div class="table-responsive">

                            @if(count($model))
                            <table id="example" class="table add-rows">
                                <thead>
                                    <tr>
                                        <th data-hide="phone,tablet">Email</th>
                                        <th data-hide="phone,tablet">Phone</th>
                                       
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr>
                                        <td>{{$row->EmailAddress}}</td>
                                        <td>{{$row->Phone}}</td>
                                   
                                    </tr>
                                    @endforeach
                                </tbody>
                     
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                               {{ $model->onEachSide(1)->links() }}
                        </div>
                    </div>
                </div>
                </div>
          </div>
          <div class="col-md-6 col-12">
                   <div class="card">
                   <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
             <div class="card-content">
                    <div class="card-body">
                     
                        <div class="table-responsive">

                            @if(count($model))
                            <table id="example2" class="table add-rows">
                                <thead>
                                    <tr>
                                        <th data-hide="phone,tablet">Email</th>
                                        <th data-hide="phone,tablet">Phone</th>
                                       
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    
                                    <tr>
                                        <td>{{$row->EmailAddress}}</td>
                                        <td>{{$row->Phone}}</td>
                                   
                                    </tr>
                                    @endforeach
                                </tbody>
                             
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                             {{ $model->onEachSide(1)->links() }}
                        </div>
                    </div>
                </div>
                </div>
          </div>
      </div>
      <div class="row">
        <div class="col-12">
        <div class="card">
        <div class="card-header">
              <h4 class="mb-0">Statistics</h4>
        </div>
            <div class="row">
               <div class="col-12 col-lg-4 col-md-6">
                            <label>Period</label>
                            <select id="period" name="period" class="form-control">
                                        <option value="7">Last 7 Days</option>
                                        <option value="30">Last 30 Days</option>
                                    </select>
                        </div>
            </div>
            <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
              <div class="card-header d-flex flex-column align-items-start pb-0">
                  <div class="avatar bg-rgba-primary p-50 m-0">
                      <div class="avatar-content">
                          <i class="feather icon-users text-primary font-medium-5"></i>
                      </div>
                  </div>
                  <h2 class="text-bold-700 mt-1 mb-25" id='new-customer'>00</h2>
                  <p class="mb-0">No of New Customers</p>
              </div>
              <div class="card-content">
                  <div id="line-area-chart-5"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25" id="no-of-orders" >00</h2>
                    <p class="mb-0">No of Orders</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-6"></div>
                </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25" id='amount-of-orders'>00</h2>
                    <p class="mb-0">Amount of orders</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-7"></div>
                </div>
            </div>
          </div>
            </div>
            
            <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
              <div class="card-header d-flex flex-column align-items-start pb-0">
                  <div class="avatar bg-rgba-primary p-50 m-0">
                      <div class="avatar-content">
                          <i class="feather icon-users text-primary font-medium-5"></i>
                      </div>
                  </div>
                  <h2 class="text-bold-700 mt-1 mb-25" id="order-of-new-customers">00</h2>
                  <p class="mb-0">No of Orders New Customers</p>
              </div>
              <div class="card-content">
                  <div id="line-area-chart-1"></div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25" id="amount-of-order-of-new-Customers">00</h2>
                    <p class="mb-0">Amount of Orders of new Customer</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-2"></div>
                </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25" id="repeated-customer">00</h2>
                    <p class="mb-0">No of Repeated Customer</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-3"></div>
                </div>
            </div>
          </div>
            </div>
     
         </div>
      </div>
   
    </section>
  <!-- Dashboard Analytics end -->
  @endsection


@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

<script>
    $(document).ready(function(){
	$('#example').dataTable( {
            "destroy":true,
            "paging":   false,
            "ordering": true,
            "info":    true,
             dom: 'Bfrtip',
            buttons: [
            {
                extend: 'csvHtml5',
                text: 'Export'
               
            }
            
        ]
    } );
    	$('#example2').dataTable( {
            "destroy":true,
            "paging":   false,
            "ordering": true,
            "info":    true,
             dom: 'Bfrtip',
            buttons: [
            {
                extend: 'csvHtml5',
                text: 'Export'
               
            }
            
        ]
    } );
   var periodval = $('#period').find(":selected").val();

    //    alert("You have selected the country - " + periodval);
    $.ajax({
    url: "{{url('get_statistics')}}",
    type: "GET",
    data: {Period:periodval},
    dataType: "json",
    success: function (result) {
        //alert(result);
	$('#new-customer').html('<h2> ' + result.New_customers + '</h2>');
        $('#no-of-orders').html('<h2> ' + result.No_of_orders + '</h2>');
        $('#amount-of-orders').html('<h2> ' + result.Amount_of_orders + '</h2>');
        $('#order-of-new-customers').html('<h2> ' + result.No_of_order_of_new_Customers + '</h2>');
        $('#amount-of-order-of-new-Customers').html('<h2> ' + result.Amount_of_order_of_new_Customers + '</h2>');
	$('#repeated-customer').html('<h2> ' + result.Repeated_Customers + '</h2>');
    }
});

    $('#period').on('change', function() {
      var value = $(this).val();

    $.ajax({
        url: "{{url('get_statistics')}}",
        type: "GET",
        data: {Period:value},
        dataType: "json",
        success: function (result) {
    //alert(result.New_customers);
         $('#new-customer').html('<h2> ' + result.New_customers + '</h2>');
        $('#no-of-orders').html('<h2> ' + result.No_of_orders + '</h2>');
        $('#amount-of-orders').html('<h2> ' + result.Amount_of_orders + '</h2>');
        $('#order-of-new-customers').html('<h2> ' + result.No_of_order_of_new_Customers + '</h2>');
        $('#amount-of-order-of-new-Customers').html('<h2> ' + result.Amount_of_order_of_new_Customers + '</h2>');
	$('#repeated-customer').html('<h2> ' + result.Repeated_Customers + '</h2>');
        }
    });
    });
});
</script>
@endsection
