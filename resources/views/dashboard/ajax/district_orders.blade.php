<div class="card" >
    <div class="card-header">
        <h4 class="card-title">Orders By {{$text}} ({{$title}})</h4>
    </div>
    <div class="card-content">
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
                    <thead>
                        <tr>
                            <th class="hidden"></th>
                            <th>{{$text}}</th>
                            <th>Franchise</th>
                            <th>Orders</th>
                            <th>Total ({{env('CURRENCY_SYMBOL')}})</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key=>$row)
                        <tr>
                            <td class="hidden">{{$row['orders']}}</td>
                            <td>{{ strtoupper($key) }}</td>
                            <td>{{$row['franchise']}}</td>
                            <td>{{$row['orders']}}</td>
                            <td><?php echo env("CURRENCY_SYMBOL"); ?>{{$row['total']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <!-- 
                    <tfoot>
                        <tr>
                            <th>Totals</th>
                            <th>{{$orders}}</th>
                            <th><?php echo env("CURRENCY_SYMBOL"); ?>{{$total}}</th>
                        </tr>
                    </tfoot> 
                    -->
                </table>
            </div>
        </div>
    </div>
</div>