<div class="row" >
    <div class="col-lg-12 col-sm-12 col-md-12" id="<?php echo $chart ?>">
    </div>
</div>

<script>
    
    var $primary = '#7367F0',
            $success = '#28C76F',
            $danger = '#EA5455',
            $warning = '#FF9F43',
            $info = '#00cfe8',
            $label_color_light = '#dae1e7';

    var themeColors = [$primary, $success, $danger, $warning, $info];
    var yaxis_opposite = false;

    var lineAreaOptions = {
        chart: {
            height: 450,
            type: 'line',
        },
        colors: themeColors,
        dataLabels: {
            enabled: true
        },
        stroke: {
            curve: 'smooth'
        },
        series: <?php echo $series; ?>,
        legend: {
            offsetY: -10
        },
        xaxis: {
            title: "Dates",
            format: 'numeric',
            categories: <?php echo $categories; ?>,
            title: {
                text: "Dates",
            }
        },
        yaxis: {
            seriesName: "No. of Orders",
            floating: false,
            decimalsInFloat: 0,
            format: 'numeric',
            opposite: yaxis_opposite,
            labels: {
                formatter: function (val, opts) {
                    return val + ""
                }
            },
            title: {
                text: "No. of Orders",
                rotate: 90,
                offsetX: 0,
                offsetY: 0,
                style: {
                    color: "#000",
                    fontSize: '14px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    cssClass: 'apexcharts-yaxis-title',
                },
            }
        },
        tooltip: {
            x: {
                format: 'numeric'
            },

        }
    }
    var lineAreaChart = new ApexCharts(
            document.querySelector("#<?php echo $chart ?>"),
            lineAreaOptions
            );


    $(document).ready(function () {

        lineAreaChart.render();


    });
</script>