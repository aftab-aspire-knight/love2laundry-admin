<div class="table-responsive">
    <br clear="all"/>
    @if(count($model)>0)
    <table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
        <thead>
            <tr>
                <th>Invoice Number</th>
                <th>Franchise</th>
                <th>Order By</th>
                <th>Amount</th>
                <th>{{$text}}</th>
                <th>Ordered</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($model as $row)
            <?php
            ?>
            <tr>
                <td>{{ $row->InvoiceNumber}}</td>
                <td>{{$row->franchises->Title}}</td>
                <td>{{isset($row->members->EmailAddress)? $row->members->EmailAddress :""}}</td>
                <td>{{trim(env('CURRENCY_SYMBOL'))}}{{$row->GrandTotal}}</td>
                <td>{{$row->PostalCode}}</td>
                <td>{{$row->CreatedDateTime}}</td>
                <td>
                    <div class="btn-group dropdown actions-dropodown">
                        <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        </button>                                               
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" target="_blank" href="{{url('invoices/show/'.$row->PKInvoiceID)}}"><i class="feather icon-eye"></i> View</a>
                            <a  class="dropdown-item" target="_blank" href="{{url('invoices/edit/'.$row->PKInvoiceID)}}"><i class="feather icon-edit"></i> Edit</a>

                        </div>
                    </div>

                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    @else
    <div class="alert alert-primary" role="alert">
        <i class="feather icon-info mr-1 align-middle"></i>
        <span>No record found</span>
    </div>
    @endif
</div>