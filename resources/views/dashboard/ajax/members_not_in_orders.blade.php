<br clear="all"/>
<table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
    <thead>
        <tr>
            <th class="hidden">ID</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Order Since</th>
            <th>Joined</th>
            <th>District</th>
            <th>Franchise</th>
            <th>Postal Code</th>
            
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($model as $row)


        <tr>
            <td class="hidden">{{$row->PKMemberID}}</td>
            <td >{{$row->PKMemberID}}</td>
            <td>{{$row->FirstName}} {{$row->LastName}}</td>
            <td>{{$row->EmailAddress}}</td>
            <td>{{$row->Phone}}</td>
            <td>{{ !empty($row->lastOrdered)? $row->lastOrdered: "-" }}</td>
            <td>{{$row->CreatedDateTime}}</td>
            <td>{{$row->district}}</td>
            <td>{{$row->Title}}</td>
            <td>{{$row->PostalCode}}</td>
            
            <td>
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    </button>                                               
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" target="_blank" href="{{url('members/show/'.$row->PKMemberID)}}"><i class="feather icon-eye"></i> View</a>
                        <a  class="dropdown-item" target="_blank" href="{{url('members/edit/'.$row->PKMemberID)}}"><i class="feather icon-edit"></i> Edit</a>

                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>