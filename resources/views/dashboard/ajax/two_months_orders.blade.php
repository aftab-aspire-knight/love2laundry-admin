<?php
$from = date("Y-m", strtotime($from));
$to = date("Y-m", strtotime($to));

if (!empty($orders[$to])) {
    if (count($orders[$to]) > count($orders[$from])) {
        $keys = array_keys($orders[$to]);
    } else {
        $keys = array_keys($orders[$from]);
    }
} else {
    $orders[$to] = array();
    $orders[$from] = array();
    $keys = array();
}
?>
<div class="row" >
    <div class="col-lg-12 col-sm-12 col-md-12" id="chart1">
    </div>
</div>

<script>


    var $primary = '#7367F0',
            $success = '#28C76F',
            $danger = '#EA5455',
            $warning = '#FF9F43',
            $info = '#00cfe8',
            $label_color_light = '#dae1e7';

    var themeColors = [$primary, $success, $danger, $warning, $info];
    var yaxis_opposite = false;

    var lineAreaOptions = {
        chart: {
            height: 350,
            type: 'area',
        },
        colors: themeColors,
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        series: [{
                name: "<?php echo date("F", strtotime($from)); ?>",
                data: [<?php echo implode(",", $orders[$from]) ?>]
            }, {
                name: "<?php echo date("F", strtotime($to)); ?>",
                data: [<?php echo implode(",", $orders[$to]) ?>]
            }],
        legend: {
            offsetY: -10
        },
        xaxis: {
            title: "Dates",
            format: 'numeric',
            categories: [<?php echo implode(",", $keys) ?>],
            title: {
                text: "Dates",
            }
        },
        yaxis: {
            seriesName: "No. of Orders",
            floating: false,
            decimalsInFloat: 0,
            format: 'numeric',
            opposite: yaxis_opposite,
            labels: {
                formatter: function (val, opts) {
                    return val + ""
                }
            },
            title: {
                text: "No. of Orders",
                rotate: 90,
                offsetX: 0,
                offsetY: 0,
                style: {
                    color: "#000",
                    fontSize: '14px',
                    fontFamily: 'Helvetica, Arial, sans-serif',
                    cssClass: 'apexcharts-yaxis-title',
                },
            }
        },
        tooltip: {
            x: {
                format: 'numeric'
            },

        }
    }
    var lineAreaChart = new ApexCharts(document.querySelector("#chart1"),lineAreaOptions);
    $(document).ready(function () {
        lineAreaChart.render();
    });
</script>