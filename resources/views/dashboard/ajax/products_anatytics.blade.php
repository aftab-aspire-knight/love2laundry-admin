<?php
$config = Helper::getconfigData();
?>
<div class="table-responsive">
    @if(count($model)>0)
    <br clear="all"/>
    <table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
        <thead>
            <tr>
                <th>Service</th>
                <th>Orders</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($model as $row)
            <?php
            ?>
            <tr>
                <td>{{ $row->title}}</td>
                <td>{{ $row->orders}}</td>
                <td>{{ env("CURRENCY_SYMBOL")}}{{ $row->total}}</td>    

                
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="alert alert-primary" role="alert">
        <i class="feather icon-info mr-1 align-middle"></i>
        <span>No record found</span>
    </div>
    @endif
</div>