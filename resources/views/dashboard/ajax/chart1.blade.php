<div class="row" >
    <div class="col-lg-12 col-sm-12 col-md-12" id="<?php echo $chart ?>">
    </div>
</div>

<script>





    // console.log(data);
    var series = <?php echo json_encode($series); ?>;




    $(document).ready(function () {

        var data = {};
        for (i = 0; i < series.length; i++) {
            data[i] = {"name": series[i].name, "data": []};
            //data[i].data = series[i].dates;
            var j = 0;
            $.each(series[i].dates, function (key, value) {

                data[i].data[j] = [key, value];
                j++;
            });

        }
        //console.log(data);
        //var chart = new ApexCharts(document.querySelector("#<?php echo $chart ?>"), options);
        
        render(data);
    });


    function render(series) {
        console.log(JSON.stringify(series));
        var options = {
            chart: {
                height: 380,
                width: "100%",
                type: "line",
                animations: {
                    initialAnimation: {
                        enabled: false
                    }
                }
            },
            series: series,
            xaxis: {
                type: 'datetime'
            }
        };
        
        $("#data").html(data);
        var chart = new ApexCharts(document.querySelector("#<?php echo $chart ?>"), options);
        chart.render();
    }


</script>

<div id="data"> </div>