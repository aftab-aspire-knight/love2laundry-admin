<div class="card" >
    <div class="card-header">
        <h4 class="card-title">Members By {{$text}} ({{$title}})</h4>
    </div>
    <div class="card-content">
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
                    <thead>
                        <tr>
                            <th class="hidden">Members</th>
                            <th>{{$text}}</th>
                            <th>Members</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key=>$row)
                        <tr>
                            <td class="hidden">{{$row['members']}}</td>
                            <td>{{ strtoupper($key) }}</td>
                            <td>{{$row['members']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>