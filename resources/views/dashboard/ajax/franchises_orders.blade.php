<?php
$config = Helper::getconfigData();
?>
<div class="table-responsive">
    <br clear="all"/>
    @if(count($model)>0)
    <table class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer dashboard_tables">
        <thead>
            <tr>
                <th><?php echo $config["location_col_title"]?></th>
                <th>Franchise</th>
                <th>Orders</th>
            </tr>
        </thead>
        <tbody>
            @foreach($model as $row)
            <?php
            ?>
            <tr>
                <td>{{ $row['title']}}</td>
                <td>{{ $row['franchise']}}</td>
                <td>{{ $row['orders']}}</td>
            </tr>
            @endforeach

        </tbody>
    </table>
    @else
    <div class="alert alert-primary" role="alert">
        <i class="feather icon-info mr-1 align-middle"></i>
        <span>No record found</span>
    </div>
    @endif
</div>