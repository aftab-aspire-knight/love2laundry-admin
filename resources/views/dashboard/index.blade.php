
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard Analytics')

@section('vendor-style')
<!-- vednor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/charts/apexcharts.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/tether-theme-arrows.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/tether.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/shepherd-theme-default.css')) }}">
@endsection
@section('mystyle')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/pages/dashboard-analytics.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/pages/card-analytics.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/plugins/tour/tour.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/forms/select/select2.css')) }}">
<?php
$type = "hidden";
?>
@endsection

@section('content')
{{-- Dashboard Analytics Start --}}
<section id="dashboard-analytics">
    
   

    <div class="row">
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-primary p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-users text-primary font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_member}}</h2>
                    <p class="mb-0">No of Members</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-1"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices}}</h2>
                    <p class="mb-0">No of Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-2"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices_pending}}</h2>
                    <p class="mb-0">No of Pending Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-3"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-12">
            <div class="card">
                <div class="card-header d-flex flex-column align-items-start pb-0">
                    <div class="avatar bg-rgba-warning p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-package text-warning font-medium-5"></i>
                        </div>
                    </div>
                    <h2 class="text-bold-700 mt-1 mb-25">{{$count_invoices_completed}}</h2>
                    <p class="mb-0">No of Completed Invoices</p>
                </div>
                <div class="card-content">
                    <div id="line-area-chart-4"></div>
                </div>
            </div>
        </div>
    </div>
    @if(Gate::denies('show-invoices'))
    
    <div class="row">
        <div class="col-md-12 col-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Orders Statistics</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="users-list-filter">
                            <form id="filter" name="filter" action="{{url('dashboard/franchisesorders')}}">

                                <div class="row align-items-end">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        {!! Form::label('Franchise') !!} 
                                        {!! Form::select('franchises[]', $franchises,[],['class' => 'form-control select2',"multiple"=>"multiple"]) !!}
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-12">
                                        <label>Range</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control daterange"  name="range" value="{{date("m/d/Y",strtotime($from))}} - {{date("m/d/Y",strtotime($to))}}" />

                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <span class="fa fa-calendar-o"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <input type="<?php echo $type; ?>" name="from" id="from" value="{{$from}}" />
                                        <input type="<?php echo $type; ?>" name="to" id="to" value="{{$to}}" />

                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <button type="submit" class="btn btn-primary mr-1 waves-effect waves-light" id="orders_franchises_btn">Search</button>
                                    </div>

                                </div>
                                <input type="hidden" name="div" id="div" value="orders_franchises" />

                            </form>
                            <div class="row">
                                <div class="col-md-12" id="orders_franchises">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12 col-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Orders By {{$areas['title']}}</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="users-list-filter">
                            <form id="filter" name="filter" action="{{url('dashboard/ordersbylocations')}}">

                                <div class="row align-items-end">
                                    <div class="col-md-6 col-lg-6 col-sm-12">
                                        {!! Form::label($areas['title']) !!} 
                                        {!! Form::select('locations[]', $areas['list'],array(),['class' => 'form-control select2',"multiple"=>"multiple"]) !!}
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-12">
                                        <label>Range</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control daterange"  name="range" value="{{date("m/d/Y",strtotime($from))}} - {{date("m/d/Y",strtotime($to))}}" />
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <span class="fa fa-calendar-o"></span>
                                                </span>
                                            </div>

                                        </div>
                                        <input type="<?php echo $type; ?>" name="from" id="from" value="{{$from}}" />
                                        <input type="<?php echo $type; ?>" name="to" id="to" value="{{$to}}" />

                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-12">
                                        <button type="submit"  class="btn btn-primary mr-1 waves-effect waves-light" id="orders_locations_btn">Search</button>
                                    </div>

                                </div>
                                <input type="hidden" name="div" id="div" value="orders_locations" />

                            </form>
                            <div class="row">
                                <div class="col-md-12" id="orders_locations">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12 col-12 col-lg-12">
            <?php echo ($only_members_view) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-12 col-lg-12">
            <?php echo ($products_analytics_view) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">

                        <div class="table-responsive" id="table_body">

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-6 col-6 col-lg-6" id="district_orders_month" >

        </div>
        <div class="col-md-6 col-6 col-lg-6" id="district_orders_week" >

        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-6 col-lg-6" id="district_members_month" >

        </div>
        <div class="col-md-6 col-6 col-lg-6" id="district_members_week" >

        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0">Statistics</h4>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4 col-md-6">
                        <label>Period</label>
                        <select id="period" name="period" class="form-control">
                            <option value="7">Last 7 Days</option>
                            <option value="30">Last 30 Days</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-primary p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-users text-primary font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id='new-customer'>00</h2>
                                <p class="mb-0">No of New Customers</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-warning p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-package text-warning font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id="no-of-orders" >00</h2>
                                <p class="mb-0">No of Orders</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-6"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-warning p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-package text-warning font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id='amount-of-orders'>00</h2>
                                <p class="mb-0">Amount of orders</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-7"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-primary p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-users text-primary font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id="order-of-new-customers">00</h2>
                                <p class="mb-0">No of Orders New Customers</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-warning p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-package text-warning font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id="amount-of-order-of-new-Customers">00</h2>
                                <p class="mb-0">Amount of Orders of new Customer</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="card">
                            <div class="card-header d-flex flex-column align-items-start pb-0">
                                <div class="avatar bg-rgba-warning p-50 m-0">
                                    <div class="avatar-content">
                                        <i class="feather icon-package text-warning font-medium-5"></i>
                                    </div>
                                </div>
                                <h2 class="text-bold-700 mt-1 mb-25" id="repeated-customer">00</h2>
                                <p class="mb-0">No of Repeated Customer</p>
                            </div>
                            <div class="card-content">
                                <div id="line-area-chart-3"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
   
</section>
<!-- Dashboard Analytics end -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}


<script>

    function search(form) {

        $(this).prop('disabled', true);

        var url = form.attr('action');
        var type = "GET";
        var dataType = "html";
        var data = form.serializeArray();
        var values = {};

        $(data).each(function (i, field) {
            values[field.name] = field.value;
        });

        var getValue = function (valueName) {
            return values[valueName];
        };
        var div = getValue("div");

        var callBack = function (response) {

            $("#" + div).html(response);
            $('.search').prop('disabled', false);
            loadDashboadTables();
            return false;
        };

        var errorCallBack = function (response) {
            $('.search').prop('disabled', false);
        };
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

    }


    $("form").submit(function (event) {
        event.preventDefault();
        search($(this));
    });

    $(document).ready(function () {
        $('.daterange').daterangepicker({
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                'Beginning': [moment().subtract(6, 'year').startOf('year'), moment()]
            }
        }
        , function (start, end, label) {
            // console.log(label + " A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });

        //$("#orders_franchises_btn").trigger("click");
        //$("#orders_locations_btn").trigger("click");
    });

    $('input[name="range"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).parent(".input-group").next("#from").val(picker.startDate.format('YYYY-MM-DD'));
        $(this).parent(".input-group").next("#from").next("#to").val(picker.endDate.format('YYYY-MM-DD'));
    });
</script>

<script>

    function twomonthsorders() {

        var url = '{{ url("dashboard/twomonthsorders") }}';
        var type = "GET";
        var dataType = "html";
        var callBack = function (response) {
            $("#table_body").html(response);

        };

        var errorCallBack = function (response) {};
        var data = {};

        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

    }

    function districtmembers(div, from, to, title) {

        var url = '{{ url("dashboard/districtmembers") }}';
        var type = "GET";
        var dataType = "html";
        var callBack = function (response) {
            $("#" + div).html(response);
            loadDashboadTables();
        };
        var errorCallBack = function (response) {};
        var data = {from: from, to: to, title: title};
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }


    function districtorders(div, from, to, title) {

        var url = '{{ url("dashboard/districtorders") }}';
        var type = "GET";
        var dataType = "html";
        var callBack = function (response) {
            $("#" + div).html(response);
            loadDashboadTables();
        };
        var errorCallBack = function (response) {};
        var data = {from: from, to: to, title: title};
        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
    }


    $(document).ready(function () {

        twomonthsorders();
        districtorders("district_orders_month", "<?php echo date("Y-m") . "-01 00:00:00" ?>", "<?php echo date("Y-m-d h:i:s") ?>", "This Month");
        districtorders("district_orders_week", "<?php echo date("Y-m-d", strtotime("this week")) . " 00:00:00" ?>", "<?php echo date("Y-m-d h:i:s") ?>", "This Week");

        districtmembers("district_members_month", "<?php echo date("Y-m") . "-01 00:00:00" ?>", "<?php echo date("Y-m-d h:i:s") ?>", "This Month");
        districtmembers("district_members_week", "<?php echo date("Y-m-d", strtotime("this week")) . " 00:00:00" ?>", "<?php echo date("Y-m-d h:i:s") ?>", "This Week");

        var periodval = $('#period').find(":selected").val();

        $.ajax({
            url: "{{url('get_statistics')}}",
            type: "GET",
            data: {Period: periodval},
            dataType: "json",
            success: function (result) {
                
                $('#new-customer').html('<h2> ' + result.New_customers + '</h2>');
                $('#no-of-orders').html('<h2> ' + result.No_of_orders + '</h2>');
                $('#amount-of-orders').html('<h2> ' + result.Amount_of_orders + '</h2>');
                $('#order-of-new-customers').html('<h2> ' + result.No_of_order_of_new_Customers + '</h2>');
                $('#amount-of-order-of-new-Customers').html('<h2> ' + result.Amount_of_order_of_new_Customers + '</h2>');
                $('#repeated-customer').html('<h2> ' + result.Repeated_Customers + '</h2>');
            }
        });

        $('#period').on('change', function () {
            var value = $(this).val();
            
            $.ajax({
                url: "{{url('get_statistics')}}",
                type: "GET",
                data: {Period: value},
                dataType: "json",
                success: function (result) {
                    
                    $('#new-customer').html('<h2> ' + result.New_customers + '</h2>');
                    $('#no-of-orders').html('<h2> ' + result.No_of_orders + '</h2>');
                    $('#amount-of-orders').html('<h2> ' + result.Amount_of_orders + '</h2>');
                    $('#order-of-new-customers').html('<h2> ' + result.No_of_order_of_new_Customers + '</h2>');
                    $('#amount-of-order-of-new-Customers').html('<h2> ' + result.Amount_of_order_of_new_Customers + '</h2>');
                    $('#repeated-customer').html('<h2> ' + result.Repeated_Customers + '</h2>');
                }
            });
        });

        var start = moment().subtract(29, 'days');
        var end = moment();


        $('input[name="dates"]').daterangepicker();
    });

    function loadDashboadTables() {

        var table = $('.dashboard_tables');
        table.DataTable().destroy();
        table.DataTable({
            "fixedHeader": true,
            "responsive": true,
            "paging": 10,
            "debug": false,
            "bInfo": true,
            "scrollX": true,
            "dom": 'Bfrtip',
            "buttons": {
                "buttons": [
                    {
                        extend: 'csvHtml5',
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                    }
                ]
            },
            order: [[0, 'desc']]
        });

    }
</script>
@endsection
