
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('categories')}}" class=" mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Title :</td>
                                    <td>{{ $model->Title }}</td>
                                </tr>
                                 <tr>
                                    <td>Mobile Title  :</td>
                                    <td>{{ $model->MobileTitle }}</td>
                                </tr>
                                <tr>
                                    <td>Desktop Icon Class Name :</td>
                                    <td>{{ $model->DesktopIconClassName }}</td>
                                </tr>
                                <tr>
                                    <td>Mobile Icon Name :</td>
                                    <td>{{ $model->MobileIcon }}</td>
                                </tr>
                                <tr>
                                    <td>Pop Up Message :</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="hide">Show</div></td>

                                </tr>
                                <tr id="contentDiv">
                                    <td></td>
                                    @if( $model->PopupMessage== null)
                                       <td id="data">None</td>

                                    @else
                                    <td id="data">{{  $model->PopupMessage }}</td>
                                    @endif

                                </tr>
                                 <tr>
                                    <td>Preview</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="previewclick">Preview</div></td>

                                </tr>
                                   <tr>
                                    <td></td>
                                    @if( $model->PopupMessage== null)
                                       <td id="previewdata">None</td>

                                    @else
                                    <td id="previewdata"><html> <body> <p><?php echo  $model->PopupMessage;?></p> </body></html></td>
                                    @endif
                                </tr>
                          
                                <tr>
                                    <td>Position :</td>
                                    @if ($model->Position==null)
                                           <td>None</td>
                                        @else
                                    <td>{{ $model->Position }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Status :</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                                <tr>
                                    <td>Image Preview:</td>
                                        @if ($model->MobileImageName==null)
                                           <td>None</td>
                                        @else
                                        <td>  <img  height="40" width="40" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_category.thumb') }}{{$model->MobileImageName }}"></td>
                                        @endif
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
   

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script>
$( document ).ready(function() {
     $("#data").hide();
     $("#previewdata").hide();
    
     
$("#hide").click(function(){
   $("#data").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick").click(function(){
   
   $("#previewdata").toggle();
    $(this).toggleClass('class1')
 
});

$("#hide0").click(function(){
   $("#data0").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick0").click(function(){

   $("#previewdata0").toggle();
    $(this).toggleClass('class1')
 
});
$("#hide1").click(function(){
   $("#data1").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick1").click(function(){

   $("#previewdata1").toggle();
    $(this).toggleClass('class1')
 
});

});

</script>
<script>
    CKEDITOR.config.allowedContent = true;

    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection