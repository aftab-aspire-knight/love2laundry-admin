@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Title ') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Mobile Title ') !!}
            {!! Form::text('MobileTitle', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Desktop Icon Class Name ') !!}
            {!! Form::text('DesktopIconClassName', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
     <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Mobile Icon Name ') !!}
            {!! Form::text('MobileIcon', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
  
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Popup Message') !!}
            {!! Form::textarea('PopupMessage', null, ['class'=>'form-control editor', 'rows' => 5]) !!}
        </div>
    </div>
</div>

<div class="row">
   <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Image') !!}
            {!! Form::file('MobileImageName',array('class'=>'form-control','id'=>'image')); !!}
        </div>
    </div>   
<div class="col-sm-6">
        @if($model['image_route']==null)

        @else
        <img  height="120" width="150" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_category.thumb') }}{{$model['image_route'] }}"></td>
        <br/>
         <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('categories/remove/'.$model->PKCategoryID)}}">Remove</a>

        @endif  
       </div>    
</div>
 <div class="row">
    <div class="col-sm-6">
        <div class="d-inline-block mb-1">
            <div class="form-group">
                {!! Form::label('Position') !!}

                <div class="input-group">
                    {!! Form::text('Position', null , array('class' => 'touchspin-min-max form-control',$required, 'id'=>'position') ) !!} 
                </div>

            </div>
        </div>

        </div>
</div>
       
<!--    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" >
                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
  <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('categories')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>