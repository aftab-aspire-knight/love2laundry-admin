@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
    
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Tag') !!}
            {!! Form::text('Tag', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('tags')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>