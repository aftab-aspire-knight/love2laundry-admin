@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/forms/select/select2.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<?php
$orders_from = Config::get('params.orders_from');
$timings = Config::get('params.timings');
$payment_status = Config::get('params.payment_status');
$invoiceTypes = Config::get('params.invoice_types');
$invoiceTypes = ["After" => "After", "Items" => "Items"];
$required = "";
?>
<section id="add-row">
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header" style="padding-bottom: 1.5rem;">
                    <h4 class="card-title">{{$title}}</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse" class=""><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::open(array( 'method' => 'GET','class' => 'number-tab-steps wizard-circle wizard clearfix','id' =>"form",'url' => 'invoicefilters/search')) !!}
                            @include("messages")
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('Franchise') !!}
                                        {!! Form::select('FKFranchiseID', $franchise,null,['class' => 'form-control select2 ','name'=>'franchise','id'=>'franchise','placeholder'=>'Select'] ) !!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('Members') !!}
                                        {!! Form::select('FKMemberID', $members,null,['class' => 'form-control search-member','name'=>'member','id'=>'member','placeholder'=>'Select'] ) !!}
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Pick Date') !!}
                                        {!! Form::text('PickupDate', null , array('class' => 'form-control pickadate2',$required,'name'=>'pick_date') ) !!}

                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Pick Time') !!}
                                        {!! Form::select('PickupTime',$range,null,['class' => 'form-control','name'=>'pick_time','id' => 'PickupTime','placeholder'=>'All']) !!}
                                    </div>

                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Delivery Date') !!}
                                        {!! Form::text('DeliveryDate', null , array('class' => 'form-control pickadate2',$required,'name'=>'delivery_date') ) !!}

                                    </div>

                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Delivery Time') !!}
                                        {!! Form::select('DeliveryTime',$range,null,['class' => 'form-control','id' => 'DeliveryTime','name'=>'delivery_time','placeholder'=>'All']) !!}
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Order From') !!}
                                        {!! Form::select('OrderPostFrom', $orders_from,null,['class' => 'form-control','name'=>'order_post_from','id'=>'order_post_from','placeholder'=>'Both'] ) !!}
                                    </div>

                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Invoice Types') !!}
                                        {!! Form::select('InvoiceTypes', $invoiceTypes,null,['class' => 'form-control','name'=>'invoice_types','id'=>'invoice_types','placeholder'=>'All'] ) !!}
                                    </div>

                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Payment Status') !!}
                                        {!! Form::select('PaymentStatus', $payment_status,null,['class' => 'form-control','name'=>'payment_status','id'=>'payment_status','placeholder'=>'All'] ) !!}
                                    </div>

                                </div>


                                <div class="col-sm-3">
                                    <div class="form-group">
                                        {!! Form::label('Order Status') !!}
                                        {!! Form::select('OrderStatus', $payment_status,null,['class' => 'form-control','name'=>'order_status','id'=>'order_status','placeholder'=>'All'] ) !!}
                                    </div>

                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">

                                        {!! Form::label('OrderedDate') !!}
                                        {!! Form::text('CreatedDateTime', null , array('class' => 'form-control daterange','name'=>'created_date_time','íd'=>'created_date_time') ) !!}
                                        <input type="hidden" name="from" id="from" value="{{$from}}" />
                                        <input type="hidden" name="to" id="to" value="{{$to}}" />
                                    </div>

                                </div>

                            </div>


                            <br clear="all"/>
                            <div class="row">
                                <div class="col-md-8">
                                    <button id="search" name="search" type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Search</button>
                                    <a href="{{url('invoicefilters')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</a>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12" id="result">


        </div>

    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection

@section('myscript')
@include('vendor_script')
<script>

    $("#search").click(function () {
        $('.search').prop('disabled', true);
        var url = $("#form").attr('action');
        var type = "GET";
        var dataType = "html";
        var data = $("#form").serialize();
        var callBack = function (response) {
            $('#result').html(response);
            return false;
        };

        var errorCallBack = function (response) {
            $('#result').html(response);
            return false;
        };

        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
        $(document).ajaxError(function () {
            $('.search').prop('disabled', false);
        });
    });

    $(document).ready(function () {
        $('.search-member').select2({
            maximumSelectionLength: 2,
            minimumInputLength: 3,
            width: 'resolve',
            allowClear: true,
            ajax: {
                url: "{{url('invoicefilters/getmembers')}}",
                type: 'get',
                delay: 250,
                dataType: 'json',
                data: function (parms) {
                    return {
                        member: parms.term
                    };
                },
                processResults: function (response) {
                    console.log(response);
                    return {
                        results: response
                    };
                },
                cache: true
            },

        });


    });

</script>
<script>
    $('.pickadate2').pickadate({
        labelMonthNext: 'Go to the next month',
        labelMonthPrev: 'Go to the previous month',
        labelMonthSelect: 'Pick a month from the dropdown',
        labelYearSelect: 'Pick a year from the dropdown',
        selectMonths: true,
        selectYears: true,
        editable: true
    });
</script>
<script>

    $(function () {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('input[name="created_date_time"]').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));


        }
    });
    $(document).ready(function () {
        $('.daterange').daterangepicker({
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'This Year': [moment().startOf('year'), moment().endOf('year')],
                'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
                'Beginning': [moment().subtract(6, 'year').startOf('year'), moment()]
            }
        }
        , function (start, end, label) {

            $("#from").val(start.format('YYYY-MM-DD'));
            $("#to").val(end.format('YYYY-MM-DD'));
        });

        //$("#orders_franchises_btn").trigger("click");
        //$("#orders_locations_btn").trigger("click");
    });
</script>


@endsection