<div class="card">
    <div class="card-content">
        <div class="card-body">
            <div class="table-responsive">
                @if(count($model))
                <table id="example" class="table">
                    <thead>
                        <tr>
                            <th >Invoice</th>
                            <th data-hide="phone,tablet">Type</th>
                            <th data-hide="phone,tablet">Franchise</th>
                            <th data-hide="phone,tablet">Member</th>
                            <th data-hide="phone,tablet">Phone</th>
                            <th data-hide="phone,tablet">P-Status</th>
                            <th >Address</th>
                            <th data-hide="phone,tablet">C-Date</th>
                            <th data-hide="phone,tablet">P-Date</th>
                            <th data-hide="phone,tablet">P-Time</th>
                            <th data-hide="phone">D-Date</th>
                            <th>D-Time</th>
                            <th></th>
                        </tr>

                    </thead>
                    <tbody>
                        @foreach($model as $row)

                        <?php
                        //d($row->members->EmailAddress,1);
                        ?>
                        <tr>
                            <td>{{$row->InvoiceNumber}}</td>
                            <td>{{$row->InvoiceType}}</td>
                            <td>{{$row->franchises->Title}}</td>

                            <td>{{isset($row->members->EmailAddress) ? $row->members->EmailAddress : "-"}}</td>
                            <td>{{isset($row->members->Phone) ? $row->members->Phone : "-"}}</td>
                            <td>{{$row->PaymentStatus}}</td>
                            <td>{{$row->BuildingName}} {{$row->StreetName}}</td>
                            <td>{{date("Y-m-d",strtotime($row->CreatedDateTime))}}</td>
                            <td>{{$row->PickupDate}}</td>
                            <td>{{$row->PickupTime}}</td>
                            <td>{{$row->DeliveryDate}}</td>
                            <td>{{$row->DeliveryTime}}</td>
                            <td><div class="btn-group dropdown actions-dropodown">
                                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    </button>                                               
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="{{url('invoices/show/'.$row->PKInvoiceID)}}"><i class="feather icon-eye"></i> View</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>



                    <tfoot>
                        <tr>
                            <th>Invoice</th>
                            <th data-hide="phone,tablet">Franchise</th>
                            <th data-hide="phone,tablet">Member</th>
                            <th data-hide="phone,tablet">Phone</th>
                            <th data-hide="phone,tablet">P-Status</th>
                            <th >Address</th>
                            <th data-hide="phone,tablet">P-Date</th>
                            <th data-hide="phone,tablet">P-Time</th>
                            <th data-hide="phone">D-Date</th>
                            <th>D-Time</th>
                            <th></th                                      
                        </tr>
                    </tfoot>
                </table>
                @else
                <div class="alert alert-primary" role="alert" >
                    <i class="feather icon-info mr-1 align-middle"></i>
                    <span>No record found</span>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $('#example').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'csvHtml5',
                text: 'Export',

                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                }
            }
        ]
    });
</script>