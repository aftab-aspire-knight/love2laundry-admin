@section('vendor-style')
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/forms/select/select2.css')) }}">
@endsection
@include("messages")
<?php
$orders_from = Config::get('params.orders_from');
$timings = Config::get('params.timings');
$payment_status = Config::get('params.payment_status');
$invoiceTypes = Config::get('params.invoice_types');
$invoiceTypes = ["After" => "After", "Items" => "Items"];

$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
//d($invoiceTypes,1);
?>
