@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset('ckeditor/contents.css') }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">

@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit {{$title}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::model($model, ['files' => true,'class' => 'number-tab-steps wizard-circle wizard clearfix','url' => ['pages/update', $id], 'method' => 'post']) !!}
                            @include('cms.pages.form')


                            

                            {!! Form::close() !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script src="{{ asset(mix('/vendors/js/extensions/jquery.nestable.js')) }}"></script>
<script>
var updateOutput = function (e) {
    var list = e.length ? e : $(e.target);
    if ((list.attr("id") == "nestable_menus") || (list.attr("id") == "assigned_menus") || (list.attr("id") == "nestable_widget") || (list.attr("id") == "nestable_locker") || (list.attr("id") == "nestable_franchise_post_code") || (list.attr("id") == "nestable_time")) {
        var output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            //output.html('JSON browser support required for this demo.');
        }
    }
};
$(document).ready(function ()
{

    // activate Nestable for list 1
    $('#assigned_menus').nestable({
        group: 1,
        maxDepth: 2
    }).on('change', updateOutput);

    // activate Nestable for list 2
    $('#unassigned_menus').nestable({
        group: 1,
        maxDepth: 1
    }).on('change', updateOutput);

    updateOutput($('#assigned_menus').data('output', $('#assigned_output')));



});
</script>

 <script>
    var ckview = document.getElementById("ckview");
    CKEDITOR.config.allowedContent = true;
  CKEDITOR.replace(ckview, {filebrowserImageBrowseUrl: '/file-manager/ckeditor',
    });
     var ckview1 = document.getElementById("ckview1");
    //CKEDITOR.replace(ckview, options);
CKEDITOR.config.allowedContent = true;
CKEDITOR.replace(ckview1, {filebrowserImageBrowseUrl: '/file-manager/ckeditor',
    });
    var ckview2 = document.getElementById("ckview2");
    //CKEDITOR.replace(ckview, options);
CKEDITOR.config.allowedContent = true;
 CKEDITOR.replace(ckview2, {filebrowserImageBrowseUrl: '/file-manager/ckeditor',
    });


</script>
<script>
    $( document ).ready(function() {
        
        if($("#template").val() =="None"){
            $("#sectionpages").hide();
            $("#pageAreaDetailSection").hide();
            $("#latitudelongitudeSection").hide();
        }else  if($( "#template" ).val()=="Area"){
            $("#hideAreaStatus").hide();
            $("#sectionpages").show();
            $("#pageAreaDetailSection").show();
            $("#latitudelongitudeSection").show();
        }else{
            $("#hideAreaStatus").hide();
            $("#sectionpages").show();
            $("#pageAreaDetailSection").hide();  
            $("#latitudelongitudeSection").show();
        }
        
        $('#template').on('change', function() {
        var value = $(this).val();
        if(value== "None"){
            $("#sectionpages").hide();
            $("#pageAreaDetailSection").hide();
            $("#hideAreaStatus").show();
            $("#latitudelongitudeSection").hide();

        }else if(value == "Area"){
            
            $("#hideAreaStatus").hide();
            $("#sectionpages").show();
            $("#pageAreaDetailSection").show();
            $("#latitudelongitudeSection").show();

        }else{
            $("#hideAreaStatus").hide();
            $("#sectionpages").show();
            $("#pageAreaDetailSection").hide();  
            $("#latitudelongitudeSection").show();
        }
    });


    });
</script>
@endsection
