
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('pages')}}" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Title  :</td>
                                    <td>{{ $model->Title  }}</td>
                                </tr>
                                 <tr>
                                    <td>Meta Title :</td>
                                    <td>{{ $model->MetaTitle }}</td>
                                </tr>

                                <tr>
                                    <td>Meta Keyword :</td>
                                    <td>{{ $model->MetaKeyword }}</td>
                                </tr>
                                <tr>
                                    <td>Meta Description :</td>
                                    <td>{{ $model->MetaDescription }}</td>
                                </tr>
                                <tr>
                                    <td>Is Permanent Redirect? :</td>
                                    <td>{{ $model->IsPermanentRedirect }}</td>
                                </tr>
                                 <tr>
                                    <td>Permanent Redirect URL:</td>
                                    <td>{{ $model->PermanentRedirectURL }}</td>
                                </tr>
                                 <tr>
                                    <td>No Index Follow Tag :</td>
                                    <td>{{ $model->NoIndexFollowTag }}</td>
                                </tr>
                                <tr>
                                    <td>Template  :</td>
                                    <td>{{ $model->Template }}</td>
                                </tr>
                                <tr>
                                    <td>Area Detail  :</td>
                                    <td>
                                        @foreach($details as $row)
                                        {{$row->Title}}, 
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status  :</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                                <tr>
                                    <td>URL :</td>
                                    <td>{{ $model->AccessURL }}</td>
                                </tr>
                                 <tr>
                                    <td>Content :</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="hide">Show</div></td>

                                </tr>
                                <tr id="contentDiv">
                                    <td></td>
                                    @if($model->Content== null)
                                       <td id="data">None</td>

                                    @else
                                    <td id="data">{{ $model->Content }}</td>
                                    @endif

                                </tr>
                                 <tr>
                                    <td>Preview</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="previewclick">Preview</div></td>

                                </tr>
                                   <tr>
                                    <td></td>
                                    @if($model->Content== null)
                                       <td id="previewdata">None</td>

                                    @else
                                    <td id="previewdata"><html> <body> <p><?php echo $model->Content;?></p> </body></html></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Latitude  :</td>
                                    <td>{{ $model->Latitude }}</td>
                                </tr>
                                <tr>
                                    <td>Longitude :</td>
                                    <td>{{ $model->Longitude }}</td>
                                </tr>
                                <tr>
                                    <td>Header Title :</td>
                                    <td>{{ $model->HeaderTitle }}</td>
                                </tr>
                                    @foreach($pages_area as $key=>$row)
                                 <tr>
                                    <td>Section  Title :</td>
                                    <td>{{ $row->Title }}</td>
                                </tr>
                                 <tr>
                                    <td>Section  Content :</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="hide{{$key}}">Show</div></td>
                                </tr>
                                <tr id="contentDiv">
                                    <td></td>
                                    @if($row->Content== null)
                                       <td id="data{{$key}}">None</td>

                                    @else
                                    <td id="data{{$key}}">{{ $row->Content }}</td>
                                    @endif

                                </tr>
                                         <tr>
                                    <td>Preview</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="previewclick{{$key}}">Preview</div></td>

                                </tr>
                                   <tr>
                                    <td></td>
                                    @if($row->Content== null)
                                       <td id="previewdata{{$key}}">None</td>

                                    @else
                                    <td id="previewdata{{$key}}"><html> <body> <p><?php echo $row->Content;?></p> </body></html></td>
                                    @endif
                                </tr>
                                  @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script>
$( document ).ready(function() {
     $("#data").hide();
     $("#previewdata").hide();
     
      $("#data0").hide();
     $("#previewdata0").hide();
      $("#data1").hide();
     $("#previewdata1").hide();
     
$("#hide").click(function(){
   $("#data").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick").click(function(){
   
   $("#previewdata").toggle();
    $(this).toggleClass('class1')
 
});

$("#hide0").click(function(){
   $("#data0").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick0").click(function(){

   $("#previewdata0").toggle();
    $(this).toggleClass('class1')
 
});
$("#hide1").click(function(){
   $("#data1").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick1").click(function(){

   $("#previewdata1").toggle();
    $(this).toggleClass('class1')
 
});

});

</script>
<script>
    CKEDITOR.config.allowedContent = true;

    CKEDITOR.replace( 'summary-ckeditor' );
</script>
@endsection