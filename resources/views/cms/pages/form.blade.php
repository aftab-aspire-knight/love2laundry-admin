@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Meta Title') !!}
            {!! Form::text('MetaTitle', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Meta Keyword') !!}
            {!! Form::text('MetaKeyword', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Meta Description') !!}
            {!! Form::textarea('MetaDescription', null, ['class'=>'form-control','id' => 'MetaDescription', 'rows' => 5, 'cols' => 100]) !!}
        </div>
    </div>

    
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Is Permanent Redirect ?') !!}
            {!! Form::select('IsPermanentRedirect', $yes_no,null,['class' => 'form-control','name'=>'IsPermanentRedirect'] ) !!}
        </div>
    </div>
    
    
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Permanent Redirect URL') !!}
            {!! Form::text('PermanentRedirectURL', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>
              
<div class="row">
     <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('No Index Follow Tag') !!}
            {!! Form::select('NoIndexFollowTag', $yes_no,null,['class' => 'form-control','name'=>'NoIndexFollowTag'] ) !!}
        </div>
    </div>
      <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('Template') !!}
            {!! Form::select('Template', $template,null,['class' => 'form-control','name'=>'Template','id'=>'template'] ) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('URL') !!}
            {!! Form::text('AccessURL', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>    
</div>
<div class="row" id="pageAreaDetailSection">
    
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Area Detail Pages') !!}
            {!! Form::select('PKPageAreaID[]', $pages,$pagesSelected,['class' => 'form-control select2','multiple'=>'multiple',$required,'id'=>'PKPageAreaID'] ) !!}
        </div>
    </div>   
</div>
<div id="hideAreaStatus">
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
           {!! Form::label('Content') !!}
           {!! Form::textarea('Content', null, ['class'=>'form-control mb-20 editor','id'=>'ckview','rows' => 15, 'cols' => 80]) !!}
        </div>
    </div>    
</div>


 <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Header  Image') !!}
            {!! Form::file('HeaderImageName',array('class'=>'form-control')); !!}
        </div>
    </div>    
    <div class="col-sm-6">
        @if($model['HeaderImageName']==null)

        @else
        <img  height="150" width="120" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_banners.thumb') }}{{$model['HeaderImageName1'] }}">
        <br>
        <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('pages/remove-header/'.$model->PKPageID)}}">Remove</a>

        @endif
    </div>    
</div>
</div>   
<div id="latitudelongitudeSection" class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Latitude ') !!}
            {!! Form::text('Latitude', null , array('class' => 'form-control',$required) ) !!}

        </div>
    </div> 
      <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Longitude ') !!}
            {!! Form::text('Longitude', null , array('class' => 'form-control',$required) ) !!}

        </div>
    </div>
    
</div>
<div id="sectionpages">
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Header Title') !!}
            {!! Form::text('HeaderTitle',null, array('class' => 'form-control',$required) ) !!}

        </div>
    </div>  
</div>
 <div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Section 1 Title') !!}
            {!! Form::text('SectionTitle1', null , array('class' => 'form-control',$required) ) !!}

        </div>
    </div>  
</div>
 
  <div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Content 1') !!}
            {!! Form::textarea('Content1', null, ['class'=>'form-control mb-20 editor','id'=>'ckview1','rows' => 15, 'cols' => 80]) !!}
        </div>
    </div>    
</div>

 <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::hidden('ImageName1',$model['HeaderImageName1'] ) !!}
            {!! Form::label('Header 1 Image') !!}
            {!! Form::file('HeaderImageName1',array('class'=>'form-control')); !!}
        </div>
    </div> 
    <div class="col-sm-6">
        @if($model['HeaderImageName1']==null)

        @else
        <img  height="150" width="120" class="round" src="{{ "http://www.graspdeal.com/l2llondon"."/".config('params.dir_upload_banners.thumb') }}{{$model['HeaderImageName1'] }}">
        <br>
        <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('pages/remove-header1/'.$model->PKSectionID1)}}">Remove</a>

        @endif
    </div>   
</div>

    
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Section 2 Title') !!}
            {!! Form::text('SectionTitle2', null , array('class' => 'form-control',$required) ) !!}

        </div>
    </div>  
</div>
  <div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Content 2') !!}
           {!! Form::textarea('Content2', null, ['class'=>'form-control mb-20 editor','id'=>'ckview2','rows' => 15, 'cols' => 80]) !!}
        </div>
    </div>    
</div>
 <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::hidden('ImageName2',$model['HeaderImageName2'] ) !!}
            {!! Form::label('Header 2 Image') !!}
            {!! Form::file('HeaderImageName2',array('class'=>'form-control')); !!}
        </div>
    </div>  
      <div class="col-sm-6">
        @if($model['HeaderImageName2']==null)

        @else
        <img  height="150" width="120" class="round" src="{{  "http://www.graspdeal.com/l2llondon"."/".config('params.dir_upload_banners.thumb') }}{{$model['HeaderImageName2'] }}">
        <br>
        <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('pages/remove-header2/'.$model->PKSectionID2)}}">Remove</a>

        @endif
    </div>   
</div>


</div>
<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('pages')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
