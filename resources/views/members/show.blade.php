
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<?php
$backButton = '<a href="' . url("members") . '" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a>';
?>
<section id="add-row">

    <div class="row">
        
        <div class="col-md-3 mb-2 mb-md-0">
            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 active" id="nav-personal" data-toggle="pill" href="#personal" aria-expanded="true">
                        <i class="feather icon-user mr-50 font-medium-3"></i>
                       Personal Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-address" data-toggle="pill" href="#address" aria-expanded="false">
                        <i class="fa fa-location-arrow mr-50 font-medium-3"></i>
                       Address Information
                    </a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-orders" data-toggle="pill" href="#orders" aria-expanded="false">
                        <i class="feather icon-book mr-50 font-medium-3"></i>
                       Orders Information
                    </a>
                </li>
     
            </ul>
        </div>  <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                           
                            
                            <div class="tab-pane active" id="personal" role="tabpanel" aria-labelledby="personal" aria-expanded="true">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?>Personal Information</h4>
                                        </div>
                                        <hr/>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>Register From :</td>
                                                    <td>{{ $model->RegisterFrom }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Referral Code :</td>
                                                    <td>{{ $model->ReferralCode }}</td>
                                                </tr>

                                                <tr>
                                                    <td>First Name :</td>
                                                    <td>{{ $model->FirstName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name :</td>
                                                    <td>{{ $model->LastName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email :</td>
                                                    <td>{{ $model->EmailAddress }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Password :</td>
                                                    <td>{{ $model->Password }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone :</td>
                                                    <td>{{ $model->Phone }}</td>
                                                </tr>

                                            </tbody>
                                        </table> 

                                    </div>
                                </div>
                            </div>
                             
                            <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address" aria-expanded="false">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Address Information</h4>
                                        </div>
                                        <hr/>
                                        <table class="table table-bordered">
                                            <tbody>
                              
                                                <tr>
                                                    <td>Postal Code :</td>
                                                    <td>{{ $model->PostalCode }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Building Name:</td>
                                                    <td>{{ $model->BuildingName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Street Name :</td>
                                                    <td>{{ $model->StreetName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Town:</td>
                                                    <td>{{ $model->Town }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Account Notes:</td>
                                                    <td>{{ $model->AccountNotes }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Total Loyality Points:</td>
                                                    <td>{{ $model->TotalLoyaltyPoints }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Status:</td>
                                                    <td>{{ $model->Status }}</td>
                                                </tr>

                                            </tbody>
                                        </table> 

                                    </div>
                                </div>
                            </div>
                            
                            <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="orders" aria-expanded="false">
                                <div id="add-row" class="data-list-view-header">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card card-transparent card-minimal">
                                                <div class="media">
                                                    <h4 class="card-title"><?php echo $backButton; ?> Orders</h4>
                                                </div>
                                                <hr>
                                                <div class="card-content">
                                                    <div class="table-responsive">
                                                        @if(count($orders))
                                                        <table id="example" class="table data-list-view">
                                                        <thead>
                                                            <tr role="row">
                                                                <th>Order No:</th>
                                                                <th>Payment Method:</th>
                                                                <th>Payment Status:</th>
                                                                <th>Order Status:</th>
                                                                <th>Grand Total:</th>
                                                                <th>Created Date Time:</th>
                                                                <th>Updated Date Time:</th>
                                                                <th>Actions</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($orders as $row)
                                                        <tr>
                                                            <td>{{ $row->PKInvoiceID}}</td>
                                                            <td>{{ $row->PaymentMethod }}</td>
                                                            <td>{{ $row->PaymentStatus }}</td>
                                                            <td>{{ $row->OrderStatus}}</td>
                                                            <td>{{env("CURRENCY_SYMBOL")}}{{ $row->GrandTotal }}</td>
                                                            <td>{{$row->CreatedDateTime}}</td>
                                                            <td>{{ $row->UpdatedDateTime}}</td>
                                                            <td>
                                                                <div class="btn-group dropdown actions-dropodown">
                                                                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                                    </button>                                               
                                                                    <div class="dropdown-menu dropdown-menu-right">
                                                                        <a class="dropdown-item" href="{{url('invoices/show/'.$row->PKInvoiceID)}}" target="_blank"><i class="feather icon-eye"></i> View</a>
                                                                        <a class="dropdown-item" href="{{url('invoices/edit/'.$row->PKInvoiceID)}}" target="_blank"><i class="feather icon-edit"></i> Edit</a>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        @endforeach 

                                                        </tbody>
                                                    </table>
                                                    @else

                                                    <div class="alert alert-primary" role="alert">
                                                        <i class="feather icon-info mr-1 align-middle"></i>
                                                        <span>No record found</span>
                                                    </div>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        
    </div>

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script>
$(document).ready(function () {
        table = $('#example').DataTable({
            "fixedHeader": true,
            
            "processing": true,
            responsive: true,
           
               "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            bInfo: false,
        });
	
        // to check and uncheck checkboxes on click of <td> tag
        
    });
</script>
@endsection