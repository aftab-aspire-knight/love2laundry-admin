
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor files --}}
@endsection
@section('mystyle')
{{-- Page css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row" class="data-list-view-header">
    <div class="row">
        <div class="col-12">
            <div class="card card-transparent card-minimal">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">
                    @include("messages")
                    <div class="dataTables_wrapper dt-bootstrap4 no-footer table-responsive">
                        <table id="example" class="table table-bordered mb-0 dataTable data-list-view no-footer dt-checkboxes-select">
                            <thead>
                                <tr role="row">
                                    <th class="dt-checkboxes-cell dt-checkboxes-select-all"></th>
                                    <th>Register From</th>
                                    <th>Referral Code</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Loyalty</th>
                                    <th>Status</th>
                                    <th>Registered At</th>
                                    <th></th>
                                </tr>

                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

<script>

    $(document).ready(function () {

        $('#example').DataTable({

            "fixedHeader": true,
            "pageLength": pageLength,
            "processing": true,
            "serverSide": true,
            responsive: true,
            "oLanguage": {
                "sLengthMenu": "_MENU_",
                "sSearch": ""
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            columnDefs: [{
                    searchable: false,
                    orderable: false,
                    targets: 0,
                    checkboxes: {selectRow: true},
                }],
            select: {
                selector: 'td:first-child',
                style: 'multi'
            },
            bInfo: false,
            "dom": '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
            "buttons": {
                "buttons": [
                    {
                        text: '<i class="feather icon-plus"></i> Add New',
                        className: 'select-btn btn btn-outline-primary',
                        action: function (e, dt, node, config) {
                            window.top.location = "{{url('members/create')}}";
                        }
                    },
                    {
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                        action: function (e, dt, node, config) {
                            window.top.location = "{{url('members/export')}}";
                        }
                    },
  
                ],
                dom: {
                    button: {
                        tag: "button",
                        className: "select-btn"
                    },
                    buttonLiner: {
                        tag: null
                    },
                    container: {
                        tag: 'div',
                        className: "dt-buttons"
                    },
                }
            },
            "ajax": {
                url: "{{url('members/json')}}",
                dataType: 'JSON'
            },
            columns: [
                {data: 'PKMemberID', name: 'PKMemberID'},
                {data: 'RegisterFrom', name: 'RegisterFrom'},
                {data: 'ReferralCode', name: 'ReferralCode'},
                {data: 'FirstName', name: 'FirstName'},
                {data: 'LastName', name: 'LastName'},
                {data: 'EmailAddress', name: 'EmailAddress'},
                {data: 'Phone', name: 'Phone'},
                {data: 'TotalLoyaltyPoints', name: 'TotalLoyaltyPoints'},
                {data: 'Status', name: 'Status'},
                {data: 'CreatedDateTime', name: 'CreatedDateTime'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            "order": [[1, 'desc']]
        });
        // to check and uncheck checkboxes on click of <td> tag
        $(".data-list-view").on("click", "tbody td", function () {
            var dtCheckbox = $(this).parent("tr").find(".dt-checkboxes-cell .dt-checkboxes")
            $(this).closest("tr").toggleClass("selected");
            dtCheckbox.prop("checked", !dtCheckbox.prop("checked"))
        });

        $(".dt-checkboxes").on("click", function () {
            $(this).closest("tr").toggleClass("selected");
        })
        $(".dt-checkboxes-select-all input").on("click", function () {
            $(".data-list-view").find("tbody tr").toggleClass("selected")
        })

        // mac chrome checkbox fix
        if (navigator.userAgent.indexOf('Mac OS X') != -1) {
            $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox");
        }
    });


</script>

@endsection