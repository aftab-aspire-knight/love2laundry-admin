@include("messages")
<?php
$register_from = Config::get('params.register_from');
$roles = Config::get('params.register_from');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Register From') !!}
            {!! Form::select('RegisterFrom', $register_from,null,['class' => 'form-control','name'=>'RegisterFrom'] ) !!}
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Referral Code') !!}
            {!! Form::text('ReferralCode', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('First Name') !!}
            {!! Form::text('FirstName', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>


    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Last Name') !!}
            {!! Form::text('LastName', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Email') !!}
            {!! Form::text('EmailAddress', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
<!--
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Password') !!}
            {!! Form::password('Password',array('placeholder'=>'Password','class' => 'form-control')); !!}
        </div>
    </div>
    -->

</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Phone') !!}
            {!! Form::text('Phone', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>


</div>

<div class="row">

    <div class="col-md-12 col-xs-12 col-lg-12">
        <h3>Address <span class="semi-bold">Information</span></h3>
        <br>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Post Code') !!}
                    {!! Form::text('PostalCode', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Building Name or Number') !!}
                    {!! Form::text('BuildingName', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Street Code') !!}
                    {!! Form::text('StreetName', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>


            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Town') !!}
                    {!! Form::text('Town', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
        </div>   


    </div>
</div>
<br clear="all" />

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Account Notes') !!}
            {!! Form::textarea('AccountNotes', null, ['class'=>'form-control','id' => 'AccountNotes', 'rows' => 5, 'cols' => 100]) !!}
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Total Loyalty Points') !!}
            {!! Form::text('TotalLoyaltyPoints', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <!--<div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" >
                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
    <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
</div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button  type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('members')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>

