
@extends('layouts/contentLayoutMaster')

@section('title', $title)
@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ asset('vendor/file-manager/css/file-manager.css') }}">

@endsection
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div style="height: 600px;">
    <div id="fm"></div>
</div>
@endsection

@section('myscript')
@include('vendor_script')
<script src="{{ asset('vendor/file-manager/js/file-manager.js') }}"></script>

@endsection
    
