
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- <div class="card-header"><h4 class="card-title">{{$title}}</h4></div> -->
                <div class="card-content">
                    <div class="card-body">
                        @include("messages")
                        <div class="table data-list-view dataTable no-footer dt-checkboxes-select">

                            @if(count($model))
                            <table id="listing" class="table data-list-view dataTable no-footer dt-checkboxes-select">
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Role</th>
                                        <th data-hide="phone,tablet">First Name</th>
                                        <th data-hide="phone,tablet">Last Name</th>
                                        <th >Email Address</th>
                                        <th data-hide="phone">Status</th>
                                        <th></th>
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr class="odd">
                                        <td class="product-name">{{$row->PKUserID}}</td>
                                        <td class="">{{$row->Role}}</td>
                                        <td>{{$row->FirstName}}</td>
                                        <td>{{$row->LastName}}</td>
                                        <td>{{$row->EmailAddress}}</td>
                                        <td>{{$row->Status}}</td>
                                        <td>
                                            <div class="btn-group dropdown actions-dropodown">
                                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>                                               
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{url('administrators/'.$row->PKUserID)}}"><i class="feather icon-eye"></i> View</a>
                                                    <a class="dropdown-item" href="{{url('administrators/edit/'.$row->PKUserID)}}"><i class="feather icon-mail"></i> Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item confirm-delete" href="javascript:void(0);" data-href="{{url('administrators/delete/'.$row->PKUserID)}}" data-id="{{$row->PKUserID}}" ><i class="fa fa-trash-o"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Role</th>
                                        <th data-hide="phone,tablet">First Name</th>
                                        <th data-hide="phone,tablet">Last Name</th>
                                        <th >Email Address</th>
                                        <th data-hide="phone">Status</th>
                                        <th >Options</th>
                                    </tr>
                                </tfoot>
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

<script>

    $(document).ready(function () {
        $('#listing').DataTable({
            "orderCellsTop": true,
            "fixedHeader": true,
            "pageLength": pageLength,
            "responsive": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            "dom": 'Bfrtip',
            "buttons": {
                "buttons": [
                    {
                        extend: "pageLength",
                        className: 'form-control',
                    },
                    {
                        text: '<i class="feather icon-plus"></i> Add New',
                        className: 'select-btn btn btn-outline-primary',
                        action: function (e, dt, node, config) {
                            window.top.location = "{{url('administrators/create')}}";
                        }
                    }
                ],
                dom: {
                    button: {
                        tag: "button",
                        className: "select-btn"
                    },
                    buttonLiner: {
                        tag: null
                    }
                }
            }
        });
    });
</script>
@endsection
