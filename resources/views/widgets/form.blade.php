@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Image') !!}
            {!! Form::file('ImageName',array('class'=>'form-control')); !!}
        </div>
   
    </div>
        <div class="col-sm-6">
                    @if($model['image']==null)
                    @else
                    <img  height="150" width="120" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_widgets.thumb') }}{{$model['image'] }}"></td>
                     <br/>
                    <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('/widgets/remove/'.$model->PKWidgetID)}}" data-id="{{$model->PKWidgetID}}" >Remove</a>

                    @endif
        </div>   
</div>
<div class="card-content collapse show">
<div class="row" >
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group " >
                
            {!! Form::label('Content') !!}    
           {!! Form::textarea('Content', null, ['class'=>'form-control mb-20 editor','id'=>'ckview','rows' => 15, 'cols' => 80]) !!}

        </div>
    </div> 
</div>

</div>
<br clear="all"/>
<br clear="all">
<br clear="all">
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Link') !!}
            {!! Form::text('Link', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>

</div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('widgets')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
