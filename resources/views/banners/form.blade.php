@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="card-content collapse show">
<div class="row" >
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group " >
                
            {!! Form::label('Content') !!}    
           {!! Form::textarea('Content', null, ['class'=>'form-control mb-20 editor','id'=>'ckview','rows' => 15, 'cols' => 80]) !!}

        </div>
    </div> 
</div>

</div>

<div class="row">
    <div class="col-md-12 col-xs-12 col-lg-12">
        <div class="form-group ">
            {!! Form::label('Link') !!}    
            {!! Form::text('Link', null , array('class' => 'form-control',$required) ) !!}

        </div>
    </div>

</div>

 <div class="row">
   
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Image') !!}
            {!! Form::file('ImageName',array('class'=>'form-control','id'=>'image')); !!}
        </div>
    </div>    


     <div class="col-sm-6" id="image-preview-div">
                    @if($model['image']==null)
                    @else
                    <img id="img-preview" name="img-preview" height="150" width="120" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_banners.thumb') }}{{$model['image'] }}">
                    <br/>
                    <a  class=" btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" href="{{url('banners/remove/'.$model->PKBannerID)}}">Remove</a>

                    @endif
    </div>   
</div>
 <div class="row">
    <div class="col-sm-6">
        <div class="d-inline-block mb-1">
            <div class="form-group">
                {!! Form::label('Position') !!}

                <div class="input-group">
                    {!! Form::text('Position', null , array('class' => ' touchspin-min-max form-control',$required, 'id'=>'position') ) !!} 
                </div>

            </div>
        </div>
    </div>
       
<!--      <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset class="checkbox">
                <div class="vs-checkbox-con vs-checkbox-primary">
                    <input type="checkbox" name="Status" id="Status" value="Enabled" >
                    <span class="vs-checkbox">
                        <span class="vs-checkbox--check">
                            <i class="vs-icon feather icon-check"></i>
                        </span>
                    </span>
                    <span class="">Status</span>
                </div>
            </fieldset>
        </div>
    </div>-->
    <div class="col-sm-6">
        <div class="d-flex justify-content-start flex-wrap">
            <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                <p class="mb-0">Status</p>
                <input type="checkbox" 
                       
                       @if(isset($model->Status) && $model->Status=="Enabled")
                       checked 
                       @endif
                       class="custom-control-input" name="Status" id="Status" value="Enabled">
                <label class="custom-control-label" for="Status">Status</label>
            </div>
        </div>
    </div>
</div>


<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('banners')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>

<!--<script>
$('#image').click(function(){  

    $("#img-preview").removeAttr("src");
    $("#image-preview-div").hide();

});
    
</script>-->