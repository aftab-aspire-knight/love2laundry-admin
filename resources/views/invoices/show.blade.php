@extends('layouts/contentLayoutMaster')
@section('title', $title)
@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<?php
$backButton = '<a href="' . url("invoices") . '" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a>';
?>
<section id="page-account-settings">
    <div class="row">
        <!-- left menu section -->
        <div class="col-md-3 mb-2 mb-md-0">
            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 active" id="nav-postcodes" data-toggle="pill" href="#information" aria-expanded="false">
                        <i class="feather icon-info mr-50 font-medium-3"></i>
                        Information
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-time" data-toggle="pill" href="#time" aria-expanded="false">
                        <i class="feather icon-truck mr-50 font-medium-3"></i>
                        Pickup and Delivery
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-preferences" data-toggle="pill" href="#preferences" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>

                        Preferences
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 " id="nav-summary" data-toggle="pill" href="#summary" aria-expanded="true">
                        <i class="feather icon-book mr-50 font-medium-3"></i>
                        Order Summary
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-preferences" data-toggle="pill" href="#logs" aria-expanded="false">
                        <i class="feather icon-message-square mr-50 font-medium-3"></i>
                        SMS Logs
                    </a>
                </li>
            </ul>
        </div>
        <!-- right content section -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                            
                            <div class="tab-pane active" id="information" role="tabpanel" aria-labelledby="
                                 information" aria-expanded="false">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Information</h4>
                                        </div>
                                        <hr/>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>Franchise :</td>
                                                    <td>{{ $franchise->Title }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Member :</td>
                                                    <td>{{ $member->EmailAddress }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Card :</td>
                                                    <td>{{$cards->Title}}</td>

                                                </tr>

                                                <tr>
                                                    <td>Discount Code</td>
                                                    <td>{{$model->DiscountCode}}</td>
                                                </tr>

                                                <tr>
                                                    <td>Payment Method  :</td>
                                                    <td>{{ $model->PaymentMethod }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Payment Status :</td>
                                                    <td>{{ $model->PaymentStatus }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Order Status :</td>
                                                    <td>{{ $model->OrderStatus }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Order From</td>
                                                    <td>{{ $model->OrderPostFrom }}</td>
                                                </tr>
                                                <tr>
                                                    <td>IP Address</td>
                                                    <td>{{ $model->IPAddress }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Regularly :</td>
                                                    <td>{{ $model->Regularly }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Account Notes :</td>
                                                    <td>{{ $model->AccountNotes }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Order Notes :</td>
                                                    <td>{{ $model->OrderNotes }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Additional Instructions :</td>
                                                    <td>{{ $model->AdditionalInstructions }}</td>
                                                </tr>

                                            </tbody>
                                        </table> 

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="time" role="tabpanel" aria-labelledby="time" aria-expanded="false">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Pickup and Delivery</h4>
                                        </div>
                                        <hr/>

                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                    <td>Pickup Date :</td>
                                                    <td>{{ $model->PickupDate }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Pickup Time :</td>
                                                    <td>{{ $model->PickupTime }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Delivery Date :</td>
                                                    <td>{{ $model->DeliveryDate }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Delivery Time :</td>
                                                    <td>{{ $model->DeliveryTime }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Name :</td>
                                                    <td>{{ $model->members->FirstName }} {{ $model->members->LastName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email :</td>
                                                    <td>{{ $model->members->EmailAddress }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone :</td>
                                                    <td>{{ $model->members->Phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Post Codes :</td>
                                                    <td>{{$model->PostalCode}}</td>
                                                </tr>

                                                @if(!empty($model->Location))
                                                <tr>
                                                    <td>Location</td>
                                                    <td>{{$model->Location}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Locker	</td>
                                                    <td>{{$model->Locker}}</td>
                                                </tr>
                                                @else
                                                <tr>
                                                    <td>Building Name Or Number :</td>
                                                    <td>{{ $model->BuildingName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Street Name :</td>
                                                    <td>{{ $model->StreetName }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Town :</td>
                                                    <td>{{ $model->Town }}</td>
                                                </tr>
                                                @endif

                                            </tbody>
                                        </table>

                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane fade" id="preferences" role="tabpanel" aria-labelledby="preferences" aria-expanded="false">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Preferences</h4>
                                        </div>
                                        <hr/>

                                        <table class="table table-bordered">
                                            <tbody>
                                                @foreach($preference_records as $row)
                                                <tr>
                                                    <td>{{$row->ParentTitle}}</td>
                                                    <td>{{$row->Title}}</td>
                                                </tr>                          
                                                @endforeach


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div role="tabpanel" class="tab-pane" id="summary" aria-labelledby="summary" aria-expanded="true">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Summary</h4>
                                        </div>
                                        <hr>

                                        @if(count($services))
                                        <table class="table ">
                                            <tbody>
                                            <thead>
                                            <td>Title:</td>
                                            <td>Quantity:</td>
                                            <td>Price:</td>
                                            </thead>


                                            @foreach($services as $row)
                                            <tr>
                                                <td>{{ $row->Title}}</td>
                                                <td>{{ $row->Quantity }}</td>
                                                <td>{{env("CURRENCY_SYMBOL")}}{{ $row->Price }}</td>
                                            </tr>

                                            @endforeach

                                            <tr>
                                                <td></td>
                                                <td>Service Total </td>
                                                <td>{{env("CURRENCY_SYMBOL")}}{{ $model->GrandTotal }}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Sub Total</td>
                                                <td>{{env("CURRENCY_SYMBOL")}}{{$model->SubTotal}}</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Discount Total</td>
                                                <td>{{env("CURRENCY_SYMBOL")}}{{$model->DiscountTotal}}</td>
                                            </tr>  

                                            </tbody>
                                        </table>
                                        @else

                                        <div class="alert alert-primary" role="alert">
                                            <i class="feather icon-info mr-1 align-middle"></i>
                                            <span>There is no items</span>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs" aria-expanded="false">
                                <div id="add-row" class="data-list-view-header">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="card card-transparent card-minimal">
                                                <div class="media">
                                                    <h4 class="card-title"><?php echo $backButton; ?> Logs</h4>
                                                </div>
                                                <hr>
                                                <div class="card-content">
                                                    <div class="table-responsive">
                                                        @if(count($logs))
                                                        <table id="example" class="table data-list-view">
                                                        <thead>
                                                            <tr role="row">
                                                                <th>ID:</th>
                                                                <th>Invoice Number:</th>
                                                                <th>Title:</th>
                                                                <th>Content:</th>
                                                                <th>Response:</th>
                                                                <th>Phone:</th>
                                                                <th>IPAddress:</th>
                                                                <th>Created Date Time:</th>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($logs as $key=>$row)
                                                        <tr>
                                                            <td>{{ $row->PKSmsLogID}}</td>
                                                            <td>{{ $row->InvoiceNumber }}</td>
                                                            <td>{{ $row->Title }}</td>
                                                            <td>
                                                               
                                                               <div id="contentsdata{{$key}}" class="hide"> <html> <body> <p><?php echo  $row->Content;?></p> </body></html></div></div>
                                                                <a  id="contentclick{{$key}}" href="javascript:void(0);">Show</a>
                                                            </td>
                                                            <td>
                                                                
                                                                
                                                               <div id="response{{$key}}" class="hide">
                                                                   <html> <body> <p><?php echo  $row->Response;?></p> </body></html></div>
                                                               <a id="responseclick{{$key}}" href="javascript:void(0);">Show</a>
                                                            </td>
                                                            <td>{{ $row->Phone}}</td>
                                                            <td>{{ $row->IPAddress}}</td>
                                                            <td>{{$row->CreatedDateTime}}</td>
                                                           
                                                           
                                                        </tr>

                                                        @endforeach 

                                                        </tbody>
                                                    </table>
                                                    @else

                                                    <div class="alert alert-primary" role="alert">
                                                        <i class="feather icon-info mr-1 align-middle"></i>
                                                        <span>No record found</span>
                                                    </div>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

<script>
$(document).ready(function () {
        table = $('#example').DataTable({
            "fixedHeader": true,
            
            "processing": true,
            responsive: true,
           
               "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            bInfo: false,
        });
	
        // to check and uncheck checkboxes on click of <td> tag
        
    });
</script>
<script>
$( document ).ready(function() {
    
     var logs = <?php echo $logs ?>;
     var i;
     for (i = 0; i < logs.length; i++) {
        $("#response"+[i]).hide();
        $("#contentsdata"+[i]).hide();
        
        $("#contentclick"+[i]).click(function(){
            
          var num = this.id.match(/-?\d+\.?\d*/);
            $('#contentsdata'+num).toggle();
              $(this).text() === 'Show' ? $(this).text('Hide') : $(this).text('Show');

         });
         $("#responseclick"+[i]).click(function(){
            var num = this.id.match(/-?\d+\.?\d*/);
            $("#response"+num).toggle();
             $(this).text() === 'Show' ? $(this).text('Hide') : $(this).text('Show');



         });
     }


});

</script>
@endsection
