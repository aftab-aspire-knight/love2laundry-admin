@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('content')
<section id="add-row" class="data-list-view-header">
    <div class="row">
        <div class="col-12">
            <div class="card card-transparent card-minimal">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">
                    @include("messages")
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered mb-0 data-list-view">
                            <thead>
                                <tr role="row">
                                    <th></th>
                                    <th>No</th>
                                    <th>Invoice</th>
                                    <th>From</th>
                                    <th>Franchise</th>
                                    <th>Member</th>
                                    <th>Payment Status</th>
                                    <th>Order Status</th>
                                    <th>Amount</th>
                                    <th>Created</th>
                                   
                                    
                                    <th width="100px">Action</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@include('vendor_script')
@section('myscript')
<script>
    //
    var table;
    $(document).ready(function () {
        table = $('#example').DataTable({
            "fixedHeader": true,
            "pageLength": pageLength,
            "processing": true,
            "serverSide": true,
            responsive: true,
            "oLanguage": {
                "sLengthMenu": "_MENU_",
                "sSearch": ""
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            columnDefs: [{
                    searchable: false,
                    orderable: false,
                    targets: 0,
                    render: function (data, type, full, meta) {
                        //console.log(full.PKInvoiceID)
                        return '<input type="checkbox" class="dt-checkboxes" name="invoices[]" value="' + full.PKInvoiceID + '">';
                    },
                    checkboxes: {selectRow: true},
                }],
            select: {
                selector: 'td:first-child',
                style: 'multi'
            },
            bInfo: false,
            "dom": '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
            "buttons": {
                "buttons": [
                    {
                        text: '<i class="feather icon-plus"></i> Add New',
                        className: 'select-btn btn btn-outline-primary',
                        action: function (e, dt, node, config) {
                            window.top.location = "{{url('/invoices/member')}}";
                        }
                    },
                    {
                        text: 'Mark Completed',
                        attr: {
                            title: 'Completed',
                            id: 'Completed',
                            value: 'Completed'
                        },
                        className: 'action-btn btn btn-primary'
                    },
                    {
                        text: 'Mark Canceled',
                        attr: {
                            title: 'Canceled',
                            id: 'Canceled',
                            value: 'Cancel'
                        },
                        className: 'action-btn btn btn-primary'
                    }
                ],
                dom: {
                    button: {
                        tag: "button",
                        className: "select-btn"
                    },
                    buttonLiner: {
                        tag: null
                    },
                    container: {
                        tag: 'div',
                        className: "dt-buttons"
                    },
                }
            },
            "ajax":
                    {
                        url: "{{url('invoices/json')}}",
                        dataType: 'JSON'

                    },
            columns: [
                {data: 'select', name: 'select', orderable: false, searchable: false},
                {data: 'PKInvoiceID', name: 'PKInvoiceID'},
                {data: 'InvoiceNumber', name: 'InvoiceNumber'},
                {data: 'OrderPostFrom', name: 'OrderPostFrom'},
                {data: 'franchises', name: 'franchises.Title'},
                {data: 'members', name: 'members.EmailAddress'},
                {data: 'PaymentStatus', name: 'PaymentStatus'},
                {data: 'OrderStatus', name: 'OrderStatus'},
                {data: 'GrandTotal', name: 'GrandTotal'},
                {data: 'CreatedDateTime', name: 'CreatedDateTime'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            "order": [[1, 'desc']]
        });

        // to check and uncheck checkboxes on click of <td> tag
        $(".data-list-view").on("click", "tbody td", function () {
            var dtCheckbox = $(this).parent("tr").find(".dt-checkboxes-cell .dt-checkboxes")
            $(this).closest("tr").toggleClass("selected");
            dtCheckbox.prop("checked", !dtCheckbox.prop("checked"))
            $(".action-btn").show();
        });
        $(".dt-checkboxes-select-all input").on("click", function () {
            $(".data-list-view").find("tbody tr").toggleClass("selected");
            $(".action-btn").show();
        })
        $(".action-btn").hide();
        $('.action-btn').on('click', function (e) {
            e.preventDefault();
            var status = $(this).val();
            alert(status)
            Swal.fire({
                title: 'Are you sure?',
                text: "Are you sure you want to perform this action?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, do it!',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-warning ml-1',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    moveInvoiceStatus(status);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire({
                        title: 'Cancelled',
                        text: '',
                        type: 'error',
                        confirmButtonClass: 'btn btn-success',
                    })
                }
            });

        });
    });
	$(document).on('change', '.dt-checkboxes[type="checkbox"]', function(e){
		var dtCheckbox = $('.dt-checkboxes[type="checkbox"]:checked');
		$(".action-btn").show();
		if(dtCheckbox.length == 0){
			$(".action-btn").hide();
		}
		$(this).closest("tr").toggleClass("selected");
		
	});
    function moveInvoiceStatus(status) {

        var data = $("input[name='invoices[]']").serializeArray();
        data.push({'name': 'status', 'value': status});
        data.push({'name': '_token', 'value': "{{ csrf_token() }}"});

        $.ajax({
            url: "{{url('/invoices/move-status')}}",
            type: "POST",
            data: data
        }).done(function (response) {
            alertMessage("", response.message, "success");
        }).fail(function (response) {
            alertMessage("", "Try again!", "danger");
        });

    }
</script>
{{-- Page js files --}}
@endsection
