@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/forms/select/select2.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Check Franchise</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::open(array( 'id' => 'form','class' => 'number-tab-steps wizard-circle wizard clearfix','url' => 'invoices/checkfranchise')) !!}
                                @include("messages")
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('Customer') !!}
                                        <select name="Customer" id='Customer' class="form-control select2 members" >
                                            <option>Select</option>
                                            @foreach($members as $member)
                                            <option value="{{$member->PKMemberID}}||{{$member->PostalCode}}" >{{$member->EmailAddress}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        {!! Form::label('Postal Code') !!}
                                        {!! Form::text('PostalCode', null , array('class' => 'form-control','id' => 'PostalCode') ) !!}
                                    </div>
                                </div>
                            </div>
                            <br clear="all" />
                            <div class="row">
                                <div class="col-md-8">
                                    <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Check</button>
                                    <a href="{{url('invoices')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="row" id="success" style="display: none;">
                                
                                    
                                    
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {

        $('.select2').select2();
        $(".members").change(function () {
            var val = $(this).val();
            val = val.split("||");
            var id = val[0];
            var postcode = val[1];
            $("#PostalCode").val(postcode);
        });


        $(".save").click(function () {

            var url = $("#form").attr('action');
            var type = "POST";
            var dataType = "json";
            var data = $("#form").serialize();
            var callBack = function (response) {

                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    var url= "<?php echo url("invoices/create");?>";
                    url = url+"/"+response.member_id+"/"+response.franchise_id+"/"+response.postcode;
                    var html = "<div class='col-md-12'><a href='"+url+"' class='btn btn-outline-warning'>Click here </a></div>";
                    $("#success").html(html).show();
                }
            };
            ajaxRequest(url, type, dataType, data, callBack, {});
        });


    });
</script>
@include('vendor_script')
{{-- Page js files --}}
@endsection