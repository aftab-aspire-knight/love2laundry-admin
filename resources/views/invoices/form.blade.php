<?php
$regularly = Config::get('params.regularly');

$orderFrom = Config::get('params.order_from');
$paymentMethods = Config::get('params.payment_methods');
$invoiceStatuses = Config::get('params.invoice_statuses');
$required = "required";


$paymentMethods = Helper::getPaymentMethods();
?>
@include("messages")
<div class="row">
    <div class="col-sm-8">

        <div id='errors'></div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="order-tab-fill" data-toggle="tab" href="#order-fill" role="tab" aria-controls="order-fill" aria-selected="true">Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="timings-tab-fill" data-toggle="tab" href="#timings-fill" role="tab" aria-controls="timings-fill" aria-selected="false">Pickup/Delivery</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="cart-tab-fill" data-toggle="tab" href="#cart-fill" role="tab" aria-controls="cart-fill" aria-selected="false">Services</a>
            </li>
            <li class="nav-item" id="preferences-tab-fill-li">
                <a class="nav-link" id="preferences-tab-fill" data-toggle="tab" href="#preferences-fill" role="tab" aria-controls="preferences-fill" aria-selected="false">Preferences</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="notes-tab-fill" data-toggle="tab" href="#notes-fill" role="tab" aria-controls="notes-fill" aria-selected="false">Notes</a>
            </li>
            <?php if ($id != 0) { ?>
                <li class="nav-item">
                    <a class="nav-link" id="refund-tab-fill" data-toggle="tab" href="#refund-fill" role="tab" aria-controls="notes-fill" aria-selected="false">Refund</a>
                </li>
            <?php } ?>
        </ul>
        <div class="tab-content pt-1">
            <div class="tab-pane active" id="order-fill" role="tabpanel" aria-labelledby="order-tab-fill">
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Order From') !!} <span class="red-color">*</span>
                        {!! Form::select('OrderPostFrom', $orderFrom,null,['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('IP Address') !!} <span class="red-color">*</span>
                        {!! Form::text('IPAddress', null , array('class' => 'form-control',$required) ) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Franchise') !!} <span class="red-color">*</span>
                        {!! Form::select('FKFranchiseID', $franchises,null,['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Member') !!} <span class="red-color">*</span>
                        {!! Form::text('Member', $member->EmailAddress , array('disabled' => 'disabled','class' => 'form-control') ) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Payment Method') !!} <span class="red-color">*</span>
                        {!! Form::select('PaymentMethod', $paymentMethods,$model->PaymentMethod,['class' => 'form-control','id' => 'PaymentMethod']) !!}
                    </div>
                </div>
                <div class="col-sm-12" id="cards_list" style="@if($model->PaymentMethod=="Cash") display:none;  @else display:block; @endif">
                     <div class="form-group">
                        {!! Form::label('Card') !!} <span class="red-color">*</span>
                        {!! Form::select('FKCardID', $cards,null,['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Discount') !!}
                        <select name="FKDiscountID" id='FKDiscountID' class="form-control select2 discounts" >
                            <option>None</option>
                            @foreach($discounts as $key=>$discount)
                            <option value="{{$key}}"  @if($key==$model->FKDiscountID) selected=selected  @endif  >{{$discount['code']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Payment Status') !!}
                        {!! Form::select('PaymentStatus', $invoiceStatuses,null,['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Order Status') !!}
                        {!! Form::select('OrderStatus', $invoiceStatuses,null,['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Location') !!} 
                        {!! Form::select('Location', $locations,null,['class' => 'form-control select2','id' => 'location']) !!}
                    </div>
                </div>
                <div class="col-sm-12" id="lockers">
                    <div class="form-group">
                        {!! Form::label('Locker') !!}
                        {!! Form::select('Locker', [],null,['class' => 'form-control select2','id' => 'locker']) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Post Code') !!} <span class="red-color">*</span>
                        {!! Form::text('PostalCode', null , array('class' => 'form-control',$required) ) !!}
                    </div>
                </div>
                <div class="" id="address">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('Building Name or Number') !!} <span class="red-color">*</span>
                            {!! Form::text('BuildingName', null , array('class' => 'form-control','id'=>'BuildingName') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('Street Name') !!} <span class="red-color">*</span>
                            {!! Form::text('StreetName', null , array('class' => 'form-control', 'id'=>'StreetName') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('Town') !!} <span class="red-color">*</span>
                            {!! Form::text('Town', null , array('class' => 'form-control','id'=>'Town') ) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('Regularly') !!} <span class="red-color">*</span>
                        {!! Form::select('Regularly', $regularly,null,['class' => 'form-control select2','id' => 'regularly']) !!}
                    </div>
                </div>

            </div>
            <div class="tab-pane " id="timings-fill" role="tabpanel" aria-labelledby="timings-tab-fill">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('Pickup Date') !!}
                            {!! Form::text('PickupDate', null , array('class' => 'form-control pickadate2') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Pickup Time') !!}<span class="red-color">*</span>
                            <?php
                            list($from, $to) = explode("-", $model->PickupTime);
                            //$from = sprintf("%02d", $from). ":00";
                            //$to = sprintf("%02d", $to). ":00";
                            ?>
                            <select name="pickup_time_from" id="pickup_time_from" class="form-control">
                                <?php
                                for ($i = 0; $i < 24; $i++) {
                                    $hour = $i . ":00";
                                    ?>
                                    <option value="<?php echo $hour ?>"
                                            <?php if ($hour == trim($from)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option><?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('') !!}<span class="red-color"></span>
                            <select name="pickup_time_to" id="pickup_time_to" class="form-control">

                                <?php
                                for ($i = 0; $i < 24; $i++) {
                                    $hour = $i . ":00";
                                    ?>
                                    <option value="<?php echo $hour ?>"
                                            <?php if ($hour == trim($to)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('Delivery Date') !!}
                            {!! Form::text('DeliveryDate', null , array('class' => 'form-control pickadate2') ) !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('Delivery Time') !!}<span class="red-color">*</span>
                            <?php
                            list($from, $to) = explode("-", $model->DeliveryTime);
                            ?>
                            <select name="delivery_time_from" id="delivery_time_from" class="form-control">
                                <?php
                                for ($i = 0; $i < 24; $i++) {
                                    $hour = $i . ":00";
                                    ?>
                                    <option value="<?php echo $hour ?>"
                                            <?php if ($hour == trim($from)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option><?php } ?>
                            </select>

                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('') !!}<span class="red-color"></span>
                            <select name="delivery_time_to" id="delivery_time_to" class="form-control">
                                <?php
                                for ($i = 0; $i < 24; $i++) {
                                    $hour = $i . ":00";
                                    ?>
                                    <option value="<?php echo $hour ?>"
                                            <?php if ($hour == trim($to)) { ?> selected="selected" <?php } ?>><?php echo $hour ?></option><?php } ?>
                            </select>

                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane" id="cart-fill" role="tabpanel" aria-labelledby="cart-tab-fill">
                <div class="table-responsive">

                    <div class="col-md-12 col-xs-12 col-lg-12">
                        <h5>Add Custom Service</h5>

                        <div class="form-controls">
                            <label class="form-label">Title <span class="red-color">*</span></label>
                            <input type="text" name="customTitle" id="customTitle" class="form-control">
                        </div>
                        <br clear="all">

                        <div class="form-controls">
                            <label class="form-label">Price <span class="red-color">*</span></label>
                            <input type="number" name="customPrice" id="customPrice" class="form-control auto" data-a-sep="," data-a-dec=".">
                        </div>
                        <br clear="all">
                        <button type="button" class="btn btn-small btn-primary" id="add_custom_service">Add</button>
                    </div>

                    @if(count($services))
                    <div class="col-md-12 col-xs-12 col-lg-12">
                        <table id="example" class="table add-rows">
                            <thead>
                                <tr role="row">
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Discount (%)</th>
                                    <th></th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($services as $row)
                                <?php
                                if ($row->DiscountPercentage > 0.00) {

                                    $price = $row->Price - ($row->Price * ($row->DiscountPercentage / 100));
                                    $price = number_format($price, 2);
                                } else {
                                    $price = $row->Price;
                                }
                                ?>

                                <tr>
                                    <td>{{$row->Title}}</td>
                                    <td><?php echo isset($row->category->Title) ? $row->category->Title : ""; ?></td>
                                    <td>{{ env('CURRENCY_SYMBOL')}}{{$price}}</td>
                                    <td>{{$row->DiscountPercentage}}%</td>
                                    <td>
                                        <button type="button"
                                                id="addservice"
                                                data-invoice_id="{{$id}}"
                                                data-franchise_service_id="{{$row->PKFranchiseServiceID}}"
                                                data-service_id="{{$row->FKServiceID}}"
                                                data-desktop_image="{{ isset($row->service->DesktopImageName)? $row->service->DesktopImageName :"" }}"
                                                data-mobile_image="{{ isset($row->service->MobileImageName)? $row->service->MobileImageName :"" }}"
                                                data-image_url=""
                                                data-title="{{$row->Title}}"
                                                data-price="{{$price}}"
                                                data-category_id="{{ isset($row->category->PKCategoryID)? $row->category->PKCategoryID :"" }}"
                                                data-is_package="{{ isset($row->service->IsPackage)? $row->service->IsPackage :"" }}"
                                                data-preference="{{ isset($row->service->PreferencesShow)? $row->service->PreferencesShow :"" }}"
                                                data-discount="{{$row->DiscountPercentage}}"
                                                data-franchise_id="{{$row->FKFranchiseID}}"
                                                class="btn btn-success add">ADD
                                        </button>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Discount (%)</th>
                                    <th></th>                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    @else

                    <div class="alert alert-primary" role="alert">
                        <i class="feather icon-info mr-1 align-middle"></i>
                        <span>No record found</span>
                    </div>

                    @endif
                </div>
            </div>
            <div class="tab-pane" id="preferences-fill" role="tabpanel" aria-labelledby="preferences-tab-fill">
                <div class="row">
                    @foreach($preferences as $key=>$preference)
                    <div class="col-sm-6">
                        <div class="form-group">
                            <b>{!! Form::label($preference['title']) !!}</b>

                            <select name="preferences[{{$key}}]" id='preferences_{{$key}}' class="form-control preferences" >

                                @foreach($preference['preferences'] as $pid=>$child)
                                <option value="{{$child['id']}}"  @if($child['id']==$preference['selected']) selected=selected  @endif  >{{$child['title']}} @if($child['price_for_package']=='Yes')({{$child['price']}}) @endif</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
            <div class="tab-pane" id="notes-fill" role="tabpanel" aria-labelledby="notes-tab-fill">
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label("Account Notes") !!}
                        {!! Form::textarea('AccountNotes', null, ['class'=>'form-control','rows' => 4]) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label("Order Notes") !!}
                        {!! Form::textarea('OrderNotes', null, ['class'=>'form-control','rows' => 4]) !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label("Additional Instructions") !!}
                        {!! Form::textarea('AdditionalInstructions', null, ['class'=>'form-control','rows' => 4]) !!}
                    </div>
                </div>
            </div>

            <div class="tab-pane " id="refund-fill" role="tabpanel" aria-labelledby="refund-tab-fill">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('Refund') !!}<span class="red-color">*</span>
                            <input class="form-control" id="refundAmount" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger refund">Refund</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="refund_html">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="col-md-12 col-xs-12 col-lg-12" id="service_cart_area">
            <table class="table table-striped" id="invoice_summary" 
            <?php if (count($invoice_services) > 0) { ?>
                       style="display:none;" 
                   <?php } ?>
                   >
                <thead class="cf">
                    <tr>
                        <th><strong>Title</strong></th>
                        <th><strong>Qty</strong></th>
                        <th><strong>Price</strong></th>
                        <th><strong>#</strong></th></tr>
                </thead>
                <tbody id="invoice_summary_body">

                </tbody>
            </table>

            <div id ="franchise_order"><b>Minimum Grand Total (Required):</b> {{ env('CURRENCY_SYMBOL').$grandTotal}}</div>

        </div>
        <br clear="all">
    </div>
</div>
<br clear="all" />

<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Save</button>
        <a href="{{url('invoices')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>


{{ Form::hidden('invoice_services', count($invoice_services)>0 ? json_encode($invoice_services) : "{}", array('id' => 'invoice_services')) }}
{{ Form::hidden('PreferenceTotal', $preferencesTotal, array('id' => 'PreferenceTotal')) }}
{{ Form::hidden('DiscountTotal', $discountTotal, array('id' => 'DiscountTotal')) }}
{{ Form::hidden('FKMemberID', $model->FKMemberID, array('id' => 'FKMemberID')) }}
{{ Form::hidden('has_preference', $hasPreferences, array('id' => 'has_preference')) }}
{{ Form::hidden('GrandTotal', $grandTotal, array('id' => 'GrandTotal')) }}
{{ Form::hidden('ServicesTotal', null, array('id' => 'ServicesTotal')) }}
@section('myscript')
@include('vendor_script')

<script>

    var servicesTotal = 0.00;
    var discountTotal = <?php echo isset($discountTotal) ? $discountTotal : 0 ?>;
    var preferencesTotal = <?php echo $preferencesTotal ?>;
    var preferences = <?php echo json_encode($preferences); ?>;
    var discounts = <?php echo json_encode($discounts); ?>;
    var discount_id = <?php echo isset($model->FKDiscountID) ? $model->FKDiscountID : 0 ?>;


    function addCustomService()
    {
        var title = $("#customTitle").val();
        var price = $("#customPrice").val();
        var services = JSON.parse($("#invoice_services").val());
        var invoice_id = $(this).data('invoice_id');

        var array = {};
        array["FKInvoiceID"] = invoice_id;
        array["FKCategoryID"] = 0;
        array["FKServiceID"] = 0;
        array["Title"] = title;
        array["ImageURL"] = "";
        array["DesktopImageName"] = "";
        array["MobileImageName"] = "";
        array["IsPackage"] = "No";
        array["PreferencesShow"] = "No";
        array["Quantity"] = 1;
        array["Allowed"] = 0;
        array["Price"] = price;
        array["Total"] = price;
        services[title] = array;

        $("#invoice_services").val(JSON.stringify(services))
        calculatePreferences();
        arrangeServices();
    }

    function calculateDiscount(id) {
        discount_id = id;
        arrangeServices();
    }

    function calculatePreferences()
    {
        var total = 0.00;
        var hasPreference = $("#has_preference").val();
        $.each(preferences, function (i, item) {
            var val = $("#preferences_" + i).val();
            if(item["preferences"][val]["price_for_package"]=='Yes'){
            var price = item["preferences"][val]["price"];
            }
            if (price != null) {
                total += parseFloat(price);
            }
        });
        preferencesTotal = total;


        $("#PreferenceTotal").val(preferencesTotal);
        arrangeServices();
    }

    $(document).ready(function () {

        $("#add_custom_service").click(function () {
            addCustomService();
        });


        $(".discounts").change(function () {

            var id = $(this).val();
            calculateDiscount(id);
        });

        $(".preferences").change(function () {
            calculatePreferences();
        });

        $(".save").click(function () {

            $(this).prop('disabled', true);

            if ($('#locker').val() == "") {

            } else {
                $('#StreetName').val('');
                $('#BuildingName').val('');
                $('#Town').val('');
                //
            }
            var url = $("#form").attr('action');
            var type = "POST";
            var dataType = "json";
            var data = $("#form").serialize();
            var check = false;
            var callBack = function (response) {
                //add one loader here   
                if (response.error === 1) {
                    alertMessage("error");
                } else
                {
                    window.top.location.href = '{{url("invoices/")}}';
                }

            };

            var errorCallBack = function (response) {
                alertMessage('success');


            };


            ajaxRequest(url, type, dataType, data, callBack, errorCallBack);
            $(document).ajaxError(function () {
                $('.save').prop('disabled', false);
            });
        });

        $(".refund").click(function () {

            var refundAmount = $("#refundAmount").val();
            var invoice_id = <?php echo $id ?>;
            var type = "POST";
            var dataType = "json";
            var data = {invoice_id: invoice_id, refundAmount: refundAmount, _token: "{{ csrf_token() }}"};
            var url = "<?php echo url("invoices/refund"); ?>";
            var callBack = function (response) {

                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    alertMessage(response.message);
                    // window.top.location.href = '{{url("invoices/")}}';
                }
            };
            ajaxRequest(url, type, dataType, data, callBack, {});
        });

        $(".btn-success").click(function () {

            var services = JSON.parse($("#invoice_services").val());
            var invoice_id = $(this).data('invoice_id');
            var franchise_service_id = $(this).data('franchise_service_id');
            var service_id = $(this).data('service_id');
            var category_id = $(this).data('category_id');
            var title = $(this).data('title');
            var price = $(this).data('price');
            var preference = $(this).data('preference');
            var discount = $(this).data('discount');
            var franchise_id = $(this).data('franchise_id');
            var is_package = $(this).data('is_package');
            var mobile_image = $(this).data('mobile_image');
            var desktop_image = $(this).data('desktop_image');
            var image_url = $(this).data('image_url');


            if (services[franchise_service_id]) {

                var quantity = services[franchise_service_id].Quantity;
                services[franchise_service_id].Quantity = quantity + 1;
                services[franchise_service_id].Total = parseFloat(services[franchise_service_id].Price) * parseInt(services[franchise_service_id].Quantity);
                services[franchise_service_id].Total = parseFloat(services[franchise_service_id].Total).toFixed(2);
            } else {

                var array = {};
                array["FKInvoiceID"] = invoice_id;
                array["FKCategoryID"] = category_id;
                array["FKServiceID"] = franchise_service_id;
                array["Title"] = title;
                array["ImageURL"] = image_url;
                array["DesktopImageName"] = desktop_image;
                array["MobileImageName"] = image_url;
                array["IsPackage"] = is_package;
                array["PreferencesShow"] = preference;
                array["Quantity"] = 1;
                array["Allowed"] = 0;
                array["Price"] = price;
                array["Total"] = price;
                services[franchise_service_id] = array;

            }
            $("#invoice_services").val(JSON.stringify(services))
            calculatePreferences();
            arrangeServices();
        });
        $('.select2').select2();
        arrangeServices();

        $('#example').DataTable();

        $('.pickadate2').pickadate({
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true,
            editable: true
        });

<?php if ($id > 0) { ?>
            getRefunds();
<?php } ?>
    });

    function getRefunds() {
        var invoice_id = <?php echo $id; ?>;

        var type = "GET";
        var dataType = "json";
        var data = "";
        var url = "<?php echo url("invoices/get-refunds"); ?>/" + invoice_id;
        var callBack = function (response) {

            if (response.error === 1) {
                displayErrors(response, "errors");
            } else
            {
                $("#refund_html").html(response.html);
                $("#refundAmount").val(response.can_refund);

            }
        };
        ajaxRequest(url, type, dataType, data, callBack, {});

    }


    function arrangeServices() {
        var services = JSON.parse($("#invoice_services").val());
        var servicesCount = Object.keys(services).length;

        var hasPreference = false;
        servicesTotal = 0.00;
        var discount = discounts[discount_id];
        var discountAmount = 0.00;
        var total = 0.00;

        if (servicesCount > 0) {
            var html = "";
            $.each(services, function (i, item) {

                var title = item.Title + " - " + item.IsPackage;
                html = html + "<tr><td>" + title + "</td><td>" + item.Quantity + "</td><td>" + showPrice(item.Total) + "</td><td><a href='#' data-id='" + i + "' onclick='removeCart(\"" + i + "\")' class='remove_cart fa fa-remove' ></a></td></tr>";
                if (item.PreferencesShow == "Yes") {

                    hasPreference = true;
                }

                if (item.IsPackage == "No") {

                    total += parseFloat(item.Total);
                }
                servicesTotal += parseFloat(item.Total);

            });

            if (discount_id > 0) {
                if (discount["type"] == "Percentage") {
                    discountAmount = total * (parseFloat(discount["worth"]) / 100);
                } else {
                    discountAmount = parseFloat(discount["worth"]);
                }

                discountTotal = discountAmount;
                $("#DiscountTotal").val(discountTotal);
            }

            $("#has_preference").val(hasPreference);

            html = html + "<tr><td><b>Service Total</b></td><td></td><td>" + showPrice(servicesTotal) + "</td><td></td></tr>";
            $("#invoice_summary_body").html(html).show();
            $("#invoice_summary").show();

        } else if (servicesCount == 0) {
            $("#invoice_summary_body").html("").hide();
            $("#invoice_summary").hide();
        }

        calculateGrandTotal();
    }

    function calculateGrandTotal() {

        var html = "";
        var hasPreference = $("#has_preference").val();

        if (discountTotal > 0.00) {

            var code = discounts[discount_id]['code'];
            var type = discounts[discount_id]['type'];
            var worth = discounts[discount_id]['worth'];

            if (type == "Percentage") {
                worth = worth + "%"
            } else {
                worth = currency_symbol.worth
            }

            html = html + "<tr><td><b>Discount Total: (" + code + ")</b></td><td></td><td>" + showPrice(discountTotal) + " " + (worth) + "</td><td></td></tr>";
        }

        if (hasPreference == "true") {

            html = html + "<tr><td><b>Preferences Total</b></td><td></td><td>" + showPrice(preferencesTotal) + "</td><td></td></tr>";

            //$("#preferences-fill").show();
            //$("#preferences-tab-fill").show();
            $("#preferences-tab-fill-li").show();

        } else
        {
            // $("#preferences-fill").hide();
            // $("#preferences-tab-fill").hide();
            $("#preferences-tab-fill-li").hide();
            preferencesTotal = 0.00;
        }
        var grandTotal = 0.00;
        if (servicesTotal > 0) {
            var grandTotal = servicesTotal + preferencesTotal - discountTotal;
            html += "<tr><td><b>Grand Total</b></td><td></td><td>" + showPrice(grandTotal) + "</td><td></td></tr>";
        } else {
            grandTotal = <?php echo $franchise->MinimumOrderAmount ?>;
        }
        $("#GrandTotal").val(grandTotal);
        $("#ServicesTotal").val(servicesTotal);

        $("#invoice_summary_body").append(html).show();
    }

    function showPrice(price) {
        return currency_symbol + parseFloat(price).toFixed(2);
    }

    function removeCart(id) {


        var services = JSON.parse($("#invoice_services").val());
        var quantity = parseInt(services[id].Quantity);
        var allowed = parseInt(services[id].Allowed);

        if (quantity > allowed) {

            if (quantity == 1) {

                delete services[id];
            } else {
                quantity--;
                services[id].Quantity = quantity;
                services[id].Total = parseFloat(services[id].Price) * quantity;
            }
            $("#invoice_services").val(JSON.stringify(services));
            arrangeServices();
        }

    }

</script>

<script>
    $(document).ready(function () {

        $("#PaymentMethod").change(function () {

            displayCreditsCards();
        });

        $("#location").change(function () {
            var lockers = <?php echo json_encode($lockers); ?>;
            var locker = $(this).val();
            var html = "<option>Select</option>";
            if (lockers[locker]) {

                $.each(lockers[locker], function (i, item) {
                    if (item != "") {
                        html = html + "<option value='" + item + "' selected>" + item + "</option>";
                    }
                });

                $("#address").hide();


                $("#lockers").show();
            } else {
                $("#address").show();
                $("#lockers").hide();

            }
            $("#locker").html(html);

        });

        displayCreditsCards();
    });


    function displayCreditsCards() {

        var option = $("#PaymentMethod").val();

        if (option == "Cash") {
            $("#cards_list").hide();
        } else {
            $("#cards_list").show();
        }

    }

</script>

{{-- Page js files --}}
@endsection