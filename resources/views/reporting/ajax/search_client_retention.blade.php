<?php
$options["series"] = array();
$options["chart"] = array();
$options["chart"]["type"] = "bar";
$options["chart"]["height"] = "400";
$options["plotOptions"] = array();
$options["plotOptions"]["bar"]["horizontal"] = false;
$options["plotOptions"]["bar"]["dataLabels"]["position"] = "top";
$options["dataLabels"]["enabled"] = true;
$options["dataLabels"]["offsetX"] = -6;
$options["dataLabels"]["style"]["fontSize"] = "12px";
$options["dataLabels"]["style"]["colors"] = ["#000"];
$options["stroke"]["show"] = true;
$options["stroke"]["width"] = 1;
$options["stroke"]["colors"] = ["#fff"];
$options["xaxis"]["categories"] = array();
//$options = json_encode($options);
?>
@if(count($model))

<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <ul id="tabsJustified" class="nav nav-tabs">
            <li class="nav-item"><a href="#_" data-target="#tabular" data-toggle="tab" class="nav-link small text-uppercase active">Tabular</a></li>
            <li class="nav-item"><a href="#_" data-target="#graph" data-toggle="tab" class="nav-link small text-uppercase ">Graph</a></li>
        </ul>
    </div>
</div>
<div id="tabsJustifiedContent" class="tab-content">
    <div id="tabular" class="tab-pane fade active show">
        <table id="example" class="table table-fixed table-bordered mb-0 dataTable data-list-view no-footer">
            <thead>
                <tr>
                    <th>Joining Month</th>
                    <th>Joiners</th>
                    <th>Orders</th>
                    <th>Total Orders</th>
                    <th>One Order</th>
                    <th>One Order Total</th>
                    <th>Less Than Five Orders</th>
                    <th>Less Than Five Orders Total</th>
                    <th>More Than Five Orders</th>
                    <th>More Than Five Orders Total</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 0;
                ?>
                @foreach($model as $row)
                <?php
                /*
                  $options["series"][]['data'] = array($row->Joiners,$row->Orders,$row->OneOrder,$row->LessThanFiveOrders,$row->MoreThanFiveOrders);
                 * 
                 */

                $joiners[] = $row->Joiners;
                $orders[] = $row->Orders;
                $oneOrder[] = $row->OneOrder;
                $lessThanFiveOrders[] = $row->LessThanFiveOrders;
                $moreThanFiveOrders[] = $row->MoreThanFiveOrders;

                $options["xaxis"]["categories"][] = showYearMonth($row->JoiningMonth);
                ?>
                <tr>
                    <td>{{ showYearMonth($row->JoiningMonth)}}</td>
                    <td>{{$row->Joiners}}</td>
                    <td>{{$row->Orders}}</td>
                    <td>{{trim(env('CURRENCY_SYMBOL'))}}{{$row->TotalOrders}}</td>
                    <td>{{$row->OneOrder}}</td>
                    <td>{{trim(env('CURRENCY_SYMBOL'))}}{{$row->OneOrderTotal}}</td>
                    <td>{{trim($row->LessThanFiveOrders)}}</td>
                    <td>{{trim(env('CURRENCY_SYMBOL'))}}{{$row->LessThanFiveOrdersTotal}}</td>
                    <td>{{$row->MoreThanFiveOrders}}</td>
                    <td>{{trim(env('CURRENCY_SYMBOL'))}}{{$row->MoreThanFiveOrdersTotal}}</td>
                </tr>

                <?php
                if ($i == 5) {
                    //  break;
                }
                $i++;
                ?>
                @endforeach

            </tbody>
        </table>
    </div>
    <?php
    $options["series"][0]['name'] = "Joiners";
    $options["series"][0]['data'] = $joiners;

    $options["series"][1]['name'] = "Orders";
    $options["series"][1]['data'] = $orders;

    $options["series"][2]['name'] = "One Orders";
    $options["series"][2]['data'] = $oneOrder;

    $options["series"][3]['name'] = "Less Than Five Orders";
    $options["series"][3]['data'] = $lessThanFiveOrders;

    $options["series"][4]['name'] = "More Than Five Orders";
    $options["series"][4]['data'] = $moreThanFiveOrders;
    ?>
    <div id="graph" class="tab-pane fade">
        @include('reporting.ajax.chart')
    </div>
</div>


@else

<div class="alert alert-primary" role="alert">
    <i class="feather icon-info mr-1 align-middle"></i>
    <span>No record found</span>
</div>

@endif




<script>
    var options = <?php echo json_encode($options) ?>;

    var chart = new ApexCharts(document.querySelector("#chart"), options);

    $(document).ready(function () {

        chart.render();

        $('#example').DataTable({
            "fixedHeader": true,
            "responsive": true,
            "paging": false,
            "bInfo": false,
            "dom": 'Bfrtip',
            "buttons": {
                "buttons": [
                    {
                        extend: 'csvHtml5',
						charset: 'utf8',
						bom: true,
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                    }
                ]
            },
        });
    });
</script>