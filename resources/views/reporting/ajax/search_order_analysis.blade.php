<?php
$options["series"] = array();
$options["chart"] = array();
$options["chart"]["type"] = "bar";
$options["chart"]["height"] = "400";
$options["plotOptions"] = array();
$options["plotOptions"]["bar"]["horizontal"] = false;
$options["plotOptions"]["bar"]["dataLabels"]["position"] = "top";
$options["dataLabels"]["enabled"] = true;
$options["dataLabels"]["offsetX"] = -6;
$options["dataLabels"]["style"]["fontSize"] = "12px";
$options["dataLabels"]["style"]["colors"] = ["#000"];
$options["stroke"]["show"] = true;
$options["stroke"]["width"] = 1;
$options["stroke"]["colors"] = ["#fff"];
$options["xaxis"]["categories"] = array();
?>
@if(count($model))

<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <ul id="tabsJustified" class="nav nav-tabs">
            <li class="nav-item"><a href="#_" data-target="#tabular" data-toggle="tab" class="nav-link small text-uppercase active">Tabular</a></li>
            <li class="nav-item"><a href="#_" data-target="#graph" data-toggle="tab" class="nav-link small text-uppercase ">Graph</a></li>
        </ul>
    </div>
</div>
<div id="tabsJustifiedContent" class="tab-content">
    <div id="tabular" class="tab-pane fade active show">
        <table id="example" class="table table-bordered mb-0 dataTable data-list-view no-footer">
            <thead>
                <tr>
                    <th>Month</th>
                    <th>Orders</th>
                    <th>Total Orders</th>
                    <th>New Orders</th>
                    <th>Total New Orders</th>
                    <th>Recent Orders</th>
                    <th>Total Recent Orders</th>
                    <th>Repeat Orders</th>
                    <th>Total Repeat Orders</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                ?>

                @foreach($model as $row)
                <?php
                $totalOrders[] = $row->Orders;
                $newOrders[] = $row->NewOrders;
                $recentOrders[] = $row->RecentOrders;
                $repeatOrders[] = $row->RepeatOrders;

                $options["xaxis"]["categories"][] = showYearMonth($row->Month);
                ?>
                <tr>
                    <td >{{showYearMonth($row->Month)}}</td>
                    <td>{{$row->Orders}}</td>
                    <td>{{env('CURRENCY_SYMBOL')}}{{$row->TotalOrders}}</td>
                    <td>{{$row->NewOrders}}</td>
                    <td>{{env('CURRENCY_SYMBOL')}}{{$row->TotalNewOrders}}</td>
                    <td>{{$row->RecentOrders}}</td>
                    <td>{{env('CURRENCY_SYMBOL')}}{{$row->TotalRecentOrders}}</td>
                    <td>{{$row->RepeatOrders}}</td>
                    <td>{{env('CURRENCY_SYMBOL')}}{{$row->TotalRepeatOrders}}</td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
    <div id="graph" class="tab-pane fade">
        @include('reporting.ajax.chart')
    </div>
    <?php
    $options["series"][0]['name'] = "Orders";
    $options["series"][0]['data'] = $totalOrders;

    $options["series"][1]['name'] = "New Orders";
    $options["series"][1]['data'] = $newOrders;

    $options["series"][2]['name'] = "Recent Orders";
    $options["series"][2]['data'] = $recentOrders;

    $options["series"][3]['name'] = "Repeat Orders";
    $options["series"][3]['data'] = $repeatOrders;
    ?>
</div>
@else

<div class="alert alert-primary" role="alert">
    <i class="feather icon-info mr-1 align-middle"></i>
    <span>No record found</span>
</div>

@endif
<script>
    var options = <?php echo json_encode($options) ?>;

    var chart = new ApexCharts(document.querySelector("#chart"), options);

    $(document).ready(function () {

        chart.render();

        $('#example').DataTable({
            "fixedHeader": true,
            "responsive": true,
            "paging": 30,
            "bInfo": false,
            "dom": 'Bfrtip',
            columnDefs: [{
                    searchable: true,
                    orderable: true,
                    targets: 0,
                }],
            "buttons": {
                "buttons": [
                    {
                        extend: 'csvHtml5',
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                    }
                ]
            }
        });
    });
</script>