@if(count($model)>0)

<table id="example" class="table table-bordered mb-0 dataTable data-list-view no-footer">
    <thead>
        <tr>
            <th>Date</th>
            <th>Customers</th>
            <th>Orders</th>
            <th>Orders Total</th>
            <th>AverageOrder</th>
        </tr>
    </thead>
    <tbody>
        @foreach($model as $row)
        <tr>
            <td>{{$row->Date}}</td>
            <td>{{$row->Customers}}</td>
            <td>{{env('CURRENCY_SYMBOL')}}{{$row->Orders}}</td>
            <td>{{env('CURRENCY_SYMBOL')}}{{$row->OrdersTotal}}</td>
            <td>{{env('CURRENCY_SYMBOL')}}{{$row->AverageOrder}}</td>
        </tr>
        @endforeach

    </tbody>
</table>

@else

<div class="alert alert-primary" role="alert">
    <i class="feather icon-info mr-1 align-middle"></i>
    <span>No record found</span>
</div>

@endif
<script>
    $(document).ready(function () {

        $('#example').DataTable({
            "fixedHeader": true,
            "responsive": true,
            "paging": 30,
            "bInfo": false,
            "dom": 'Bfrtip',
            "buttons": {
                "buttons": [
                    {
                        extend: 'csvHtml5',
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                    }
                ]
            }
        });
    });
</script>