
@if(count($model))

<table id="example" class="table table-bordered mb-0 dataTable data-list-view no-footer">
    <thead>
        <tr>
            <th>Member ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Franchise</th>
            <th>Post Code</th>
            <th>Last Order</th>
            <th>Order Count</th>
            <th>Order Total</th>
            <th>Order Average</th>
        </tr>
    </thead>
    <tbody>
        @foreach($model as $row)
        <tr>
            <td>{{$row->FKMemberID}}</td>
            <td>{{$row->FirstName}}</td>
            <td>{{$row->LastName}}</td>
            <td>{{$row->Phone}}</td>
            <td>{{$row->EmailAddress}}</td>
            <td>{{$row->Title}}</td>
            <td>{{$row->PostCode}}</td>
            <td>{{$row->LastOrder}}</td>
            <td>{{$row->OrderCount}}</td>
            <td>{{env('CURRENCY_SYMBOL')}}{{$row->OrderTotal}}</td>
            <td>{{env('CURRENCY_SYMBOL')}}{{$row->OrderAverage}}</td>
        </tr>
        @endforeach

    </tbody>
</table>
@else

<div class="alert alert-primary" role="alert">
    <i class="feather icon-info mr-1 align-middle"></i>
    <span>No record found</span>
</div>

@endif



<script>
    $(document).ready(function () {

        $('#example').DataTable({
            "fixedHeader": true,
            "responsive": true,
            "paging": 30,
            "bInfo": false,
            "dom": 'Bfrtip',
            "buttons": {
                "buttons": [
                    {
                        extend: 'csvHtml5',
                        text: 'Export',
                        className: 'select-btn btn btn-primary',
                    }
                ]
            }
        });
    });
</script>