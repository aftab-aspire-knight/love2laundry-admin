
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Search</h4>
                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="feather icon-chevron-down"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="users-list-filter">
                            <form id="filter" name="filter" action="{{url('reporting/search-order-analysis')}}">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('From') !!}
                                            {!! Form::text('from', $from , array('class' => 'form-control pickadate2') ) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            {!! Form::label('To') !!}
                                            {!! Form::text('to', $to , array('class' => 'form-control pickadate2') ) !!}
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light search">Search</button>
                                        <a href="{{url('reporting/order-analysis')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <div class="dataTables_wrapper dt-bootstrap4 no-footer table-responsive" id="table_body">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}


<script>

    $(document).ready(function () {

        $('.pickadate2').pickadate({
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true,
            editable: true,
            formatSubmit: 'yyyy-mm-dd'
        });


        $(".search").click(function () {

            search();

        });
        search();
    });

    function search() {

        $(this).prop('disabled', true);

        var url = $("#filter").attr('action');
        var type = "GET";
        var dataType = "html";
        var data = $("#filter").serialize();
        var check = false;
        var callBack = function (response) {

            $("#table_body").html(response);
            $('.search').prop('disabled', false);
        };

        var errorCallBack = function (response) {
            //alertMessage('success');
            $('.search').prop('disabled', false);
        };


        ajaxRequest(url, type, dataType, data, callBack, errorCallBack);

    }
</script>


@endsection
