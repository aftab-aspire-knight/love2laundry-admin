<?php
$required = "";
?>
@include("messages")
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Franchise') !!}
            {!! Form::select('FKFranchiseID[]', $franchise,null,['class' => 'form-control select2','multiple'=>'multiple',$required,'id'=>'FKFranchiseID'] ) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('Start Date') !!}
            {!! Form::text('StartDate', $date , array('class' => 'form-control pickadate2',$required) ) !!}
        </div>

    </div>

    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('End Date') !!}
            {!! Form::text('EndDate', $date , array('class' => 'form-control pickadate2',$required) ) !!}

        </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-2">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset>
                  <div class="vs-checkbox-con vs-checkbox-primary">
                      <input type="checkbox" id="paid" name="paid" id="paid" value="Yes">
                    <span class="vs-checkbox">
                      <span class="vs-checkbox--check">
                        <i class="vs-icon feather icon-check"></i>
                      </span>
                    </span>
                    <span class="">Paid</span>
                  </div>
            </fieldset>
        </div>
        
    </div>
    
    <div class="col-sm-2">
        <div class="form-group">
            {!! Form::label(' ') !!}
            <fieldset>
                  <div class="vs-checkbox-con vs-checkbox-primary">
                      <input type="checkbox" value="No" id="not-paid" id="not-paid" name="not-paid">
                    <span class="vs-checkbox">
                      <span class="vs-checkbox--check">
                        <i class="vs-icon feather icon-check"></i>
                      </span>
                    </span>
                    <span class="">Not Paid</span>
                  </div>
            </fieldset>
        </div>
    </div>
 
</div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save" id="btn_submit">Save</button>
        <a href="{{url('payments')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
@section('myscript')
@include('vendor_script')
<script>
    
    $( document ).ready(function() {
        $("#index").hide();
        

});

function loadDataTable(table_data){
    $("#example").DataTable().destroy();
        var table=$('#example').DataTable({
         "data": table_data,
        "fixedHeader": true,
        "pageLength": pageLength,
        "processing": true,
        responsive: true,
        "cache" : false,
      
    destroy: true,
    retrieve: true,

        "oLanguage": {
        "sLengthMenu": "_MENU_",
        "sSearch": ""
         },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
        columnDefs: [{
            searchable: false,
            orderable: false,
            data:"PKInvoiceID",
            targets: 0, 
            checkboxes: {select: true},
            
            
        }],
   
        select: {
            selector: 'td:first-child',
            style: 'os'
        },
        bInfo: false,
        "dom": '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
        "buttons": {
            "buttons": [ 
                {
                        text: '<i class="feather "></i> Move to Paid',
                        className: 'select-btn btn btn-outline-primary hidden buttonsToHide',
                        action: function () {
//                            selectedIds.push($(this).val());
                        var count = table.rows( { selected: true } ).count();
                        //alert( 'Rows '+table.rows( '.selected' ).count()+' are selected' );                        
                        //    alert(table.rows('.selected').data().length + ' row(s) selected');
                            var ids = $.map(table.rows('.selected').data(), function (item) {
                                return item['PKInvoiceID']

                            });
                            
                              $.ajax({
                                url: 'payments/update',
                                type: "GET",
                                data: {IDS:ids},
                                success: function (result) {
                                  if(result>=1){
                                    window.top.location = "{{url('payments')}}";
                                   }else{
                                       alert('Status does not updated');
                                   }

                                }
                            });

                        }
                 },
                 {
                        text: '<i class="feather "></i> Move to Un-Paid',
                        className: 'select-btn btn btn-outline-primary hidden buttonToHide',
                        action: function () {
//                            selectedIds.push($(this).val());
                        var count = table.rows( { selected: true } ).count();
                        //alert( 'Rows '+table.rows( '.selected' ).count()+' are selected' );                        
                        //    alert(table.rows('.selected').data().length + ' row(s) selected');
                            var ids = $.map(table.rows('.selected').data(), function (item) {
                                return item['PKInvoiceID']

                            });
                            
                              $.ajax({
                                url: 'payments/update-unpaid',
                                type: "GET",
                                data: {IDS:ids},
                                success: function (result) {
                                  if(result>=1){
                                    window.top.location = "{{url('payments')}}";
                                   }else{
                                       alert('Status does not updated');
                                   }

                                }
                            });

                        }
                 },
               
                {

                    extend: 'csvHtml5',
                                charset: 'UTF-16LE',
                    bom: true,
                    text: 'Export',
                    className: 'select-btn btn btn-primary',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6],
                        
                    }
                 }
                ],
                dom: {
                    button: {
                        tag: "button",
                        className: "select-btn"
                    },
                    buttonLiner: {
                        tag: null
                    },
                    container: {
                        tag: 'div',
                        className: "dt-buttons"
                    },
                }
            },
           
            columns: [
          
                {data: 'PKInvoiceID', name: 'PKInvoiceID'},
                {data: 'InvoiceNumber', name: 'InvoiceNumber'},
                {data: 'members.FirstName', name: 'members.FirstName', "defaultContent": "<i>Not set</i>"},
                {data: 'OwnerPaymentStatus', name: 'OwnerPaymentStatus'},
                {data: 'GrandTotal', name: 'GrandTotal'},
                {data: 'PickupDate', name: 'PickupDate'},
                {data: 'PickupTime', name: 'PickupTime'},
                {
                data: 'PKInvoiceID',
                name:'PKInvoiceID',
                className: "center",
                   "render": function (item) {
                                return "<button onclick='ViewAction(" + item + ");'  class=\"btn btn-success\" return false>View</button>&nbsp;&nbsp;"

                                }
                      

                }
            ],
            "order": [[1, 'desc']]
        });
 
      
        // to check and uncheck checkboxes on click of <td> tag
        $(".data-list-view").on("click", "tbody td", function () {
//            var dtCheckbox = $(this).parent("tr").find(".dt-checkboxes-cell .dt-checkboxes")
//            $(this).closest("tr").toggleClass("selected");
//            dtCheckbox.prop("checked", !dtCheckbox.prop("checked"));

        });

        $(".dt-checkboxes").on("click", function () {
            $(this).closest("tr").toggleClass("selected");
            
        })
        $(".dt-checkboxes-select-all input").on("click", function () {
            $(".data-list-view").find("tbody tr").toggleClass("selected")
            

        })  
        $(".dt-checkboxes-cell").on("click", function () {
            if ($(".data-list-view").find("tbody tr").hasClass("selected")) {
                table.buttons('.buttonsToHide').nodes().removeClass('hidden');
                table.buttons('.buttonToHide').nodes().removeClass('hidden');

            }else{
                table.buttons('.buttonsToHide').nodes().addClass('hidden');
                table.buttons('.buttonToHide').nodes().addClass('hidden');

            }
        }) 
        // mac chrome checkbox fix
        if (navigator.userAgent.indexOf('Mac OS X') != -1) {
            $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox");
        }
    
    }
        function ViewAction(id) {
		var url='<?php echo url('invoices/show');?>'+'/'+id; 
                window.open(url, '_blank');


        }
  

$( ".save" ).click(function() {
      $(this).prop('disabled', true);
    var url = $("#form").attr('action');
            var type = "POST";
            var dataType = "json";
            var data = $("#form").serialize();
      
             var callBack = function (response) {
                //add one loader here   
                if (response.errors === 1) {
                    alert("error");
                } else
                {
                    $("#index").show();
                    var table_data = JSON.parse(response.value);
                    loadDataTable(table_data);
                $('.save').prop('disabled', false);

       
               }
            };


            ajaxRequest(url, type, dataType, data, callBack,[]);
            
             $(document).ajaxError(function () {
                $('.save').prop('disabled', false);
            });
    });
    $('.pickadate2').pickadate({
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true,
            editable: true
        });
</script>

@endsection