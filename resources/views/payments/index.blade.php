
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
  <section>
      
      
    <div class="row">
        <div class="col-12">
            
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Invoices</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">

                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::model($model, ['id' => 'form','files' => true,'class' => 'number-tab-steps wizard-circle wizard clearfix','url' => ['payments/save'], 'method' => 'post']) !!}
 
                                @include('payments.form')


                            {!! Form::close() !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
      
        <div id="add-row" class="data-list-view-header">
            <div class="row" id="index">
                    <div class="col-12">
                        <div class="card card-transparent card-minimal">
                            <div class="card-content">
                                <div class="dataTables_wrapper dt-bootstrap4 no-footer table-responsive">
                                    <table id="example" class="table table-bordered mb-0 dataTable data-list-view no-footer dt-checkboxes-select ">
                                        <thead>
                                            <tr role="row">
                                                <th class="dt-checkboxes-cell dt-checkboxes-select-all"></th>
                                                <th>Invoice Number</th>
                                                <th>Customer Name</th>
                                                <th>Owner Payment Status</th>
                                                <th>Total</th>
                                                <th>P-Date</th>
                                                <th>P-Time</th>
                                                <th></th>                  
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
 
</section>


<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

@endsection
