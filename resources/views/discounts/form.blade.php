<?php
$types = Config::get('params.discounts.types');
$statuses = Config::get('params.discounts.statuses');
$for = Config::get('params.discounts.for');
$used = Config::get('params.discounts.used');
$required = "required";
?>
@include("messages")
<div id='errors'></div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Code') !!}<span class="red-color">*</span>
            {!! Form::text('Code', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Worth') !!}<span class="red-color">*</span>
            {!! Form::text('Worth', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Start Date') !!}
            {!! Form::text('StartDate', null , array('class' => 'form-control pickadate') ) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Expire Date') !!}
            {!! Form::text('ExpireDate', null , array('class' => 'form-control pickadate') ) !!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Type') !!}
            {!! Form::select('DType', $types,null,['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Status') !!}
            {!! Form::select('Status', $statuses,null,['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Discount For') !!}
            {!! Form::select('DiscountFor', $for,null,['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Code Used') !!}
            {!! Form::select('CodeUsed', $used,null,['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Member') !!}
            {!! Form::select('FKMemberID', $members,null,['class' => 'form-control select2']) !!}
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Minimum Order Amount') !!}<span class="red-color">*</span>
            {!! Form::text('MinimumOrderAmount', null , array('class' => 'form-control') ) !!}
        </div>
    </div>
</div>
<br clear="all" />
<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Save</button>
        <a href="{{url('discounts')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
@section('myscript')
@include('vendor_script')

<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
<script>

$(document).ready(function () {
    $('.select2').select2();

    $(".save").click(function () {

        var url = $("#form").attr('action');
        var type = "POST";
        var dataType = "json";
        var data = $("#form").serialize();

        var callBack = function (response) {

            if (response.error === 1) {
                displayErrors(response, "errors");
            } else
            {
                window.top.location.href = '{{url("discounts/")}}';
            }
        };

        ajaxRequest(url, type, dataType, data, callBack, {});

    });
});
</script>

<script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
{{-- Page js files --}}
@endsection