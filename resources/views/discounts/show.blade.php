@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->

<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('discounts')}}" class=" mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Code:</td>
                                    <td>{{ $model->Code }}</td>
                                </tr>
                                <tr>
                                    <td>Discount For :</td>
                                    <td>{{ $model->DiscountFor }}</td>
                                </tr>
                                <tr>
                                    <td>Worth :</td>
                                    <td>{{$model->DType=="Price" ? env('CURRENCY_SYMBOL'): ""}}{{$model->Worth}}{{$model->DType=="Percentage" ? "%" : ""}}</td>
                                </tr>
                                <tr>
                                    <td>Code Used:</td>
                                    <td>{{ $model->CodeUsed }}</td>
                                </tr>
                                <tr>
                                    <td>Member:</td>
                                    <td>{{ isset($member->EmailAddress) ? $member->EmailAddress  : "N/A" }}</td>
                                </tr>
                                <tr>
                                    <td>Minimum Order Amount:</td>
                                    <td>{{ env('CURRENCY_SYMBOL') }}{{ $model->MinimumOrderAmount }}</td>
                                </tr>
                                <tr>
                                    <td>Start Date :</td>
                                    <td>{{ $model->StartDate }}</td>
                                </tr>
                                <tr>
                                    <td>Expire Date :</td>
                                    <td>{{ $model->ExpireDate }}</td>
                                </tr>
                                <tr>
                                    <td>Status :</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                                <tr>
                                    <td>Created:</td>
                                    <td>{{ $model->CreatedDateTime }}</td>
                                </tr>
                                <tr>
                                    <td>Last Updated:</td>
                                    <td>{{ $model->UpdatedDateTime }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
@endsection