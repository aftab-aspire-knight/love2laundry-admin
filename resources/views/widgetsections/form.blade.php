@include("messages")
<?php
$yes_no = Config::get('params.yes_no');
$template = Config::get('params.template');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="col-sm-12">
        <div class="form-group">
           {!! Form::label('Widget') !!}  
                <select name="pages" id='pages' class="form-control select2" >
                    @foreach($widgets as $key=>$row)
                    <option value="{{$key}}" >{{$row['Title']}}</option>
                    @endforeach
                </select>
<!--           {!! Form::text('Widget[]', $widgets , array("data-role"=>"tagsinput",'id' => 'Lockers','class' => 'form-control') ) !!}-->
        </div>
        </div>
        <div class="dd col-md-12 col-xs-12 col-lg-12">
            <a href="javascript:void(0);" class="btn btn-outline-primary add">Add</a>
        </div>

    </div>
    <div class="col-md-6 col-xs-6 col-lg-6">
        <div class="cf nestable-lists">
            <div class="dd col-md-12 col-xs-12 col-lg-12" id="unassigned_menus">
                <ol class="dd-list dark"></ol>                                            
            </div>
        </div>
    </div>

</div>

<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('widgetsections')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>

<script>
    var pages =<?php echo json_encode($widgets); ?>;
    var navigations =<?php echo json_encode($navigations); ?>;

    $(document).ready(function () {
            $(".add").click(function () {
            var page = $("#pages").val();
            var page = pages[page];
            addPage(page['PKWidgetID'], page['Title'], page['AccessURL']);
    });
               var navigationsCount = Object.keys(navigations).length;

      if (navigationsCount > 0) {
            // console.log(navigationsCount);
            $.each(navigations, function (i, item) {

                addPage(item['id'], item['title'], item['url']);
            });
        }
    });
    
    function remove(id) {
        $("#" + id).remove();
    }

    function addPage(id, title, url) {

        var num = parseInt((Math.random() * 100), 10);
        
        var dd = '<li id="' + num + '" class="dd-item dd3-item" data-id="' + id + '" data-name="' + title + '" data-url="' + url + '"><div class="dd-handle dd3-handle"></div><input type="hidden" id="p_id" name="p_id[]" value="' + id + '"><input type="hidden" id="p_text" name="p_text[]" value="' + title + '"><input type="hidden" id="p_url" name="p_url[]" value="' + url + '"><div class="dd3-content"><div class="accordion accordianpanel" aria-expanded="false"><div class="accordion-group"><div class="accordion-heading"> <a href="#collapse' + num + '" data-toggle="collapse" class="accordion-toggle collapsed">' + title + '<i class="fa fa-plus"></i> </a> </div><div class="accordion-body collapse" id="collapse' + num + '" style="height: 0px;"><div class="accordion-inner"><div class="form-group"><label class="form-label">Title</label><div class="controls"><input type="text" id="nav_label' + num + '" name="nav_label[]" class="form-control nav-title" value="' + title + '"></div></div><div class="control-button"><a href="javascript:void(0);" onclick="remove(' + num + ')" data-id="' + num + '" class="remove-nav">Remove</a><!--<a data-id="' + num + '" class="cancel-nav">Close</a>--></div></div></div></div></div></div></li>';

        $(".dd-list").append(dd);
    }
</script>