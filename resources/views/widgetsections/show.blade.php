
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('widgetsections')}}" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Title :</td>
                                    <td>{{ $model->Title }}</td>
                                </tr>
                                 <tr>
                                    <td>Widget :</td>
                                    <td>
                                        @foreach($widgets as $key=>$value)
                                        <tr>
                                            <td></td>
                                            <td>{{$value}}</td>
                                        </tr>
                                        @endforeach
                                    </td>
                                </tr>

                            </tbody>
                        </table

                    </div>
                </div>
            </div>
        </div>
    </div>
  
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
@endsection