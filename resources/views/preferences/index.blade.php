
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <a href="{{url("preferences/create")}}" class="btn btn-primary mb-2"><i class="feather icon-plus"></i></a>
                        <div class="table-responsive">

                            @if(count($model))
                            <table class="table listing">
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th >Title</th>
                                        <th >Parent</th>
                                        <th >Price For Package</th>
                                        <th >Price</th>
                                        <th >Status</th>
                                        <th ></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr>
                                        <td>{{$row->PKPreferenceID}}</td>
                                        <td>{{$row->Title}}</td>
                                        <td>{{ isset($row->category->Title) ? $row->category->Title : "None" }}</td>
                                        <td>{{$row->PriceForPackage}}</td>
                                        <td>{{ isset($row->Price)? env("CURRENCY_SYMBOL").$row->Price : "" }}</td>
                                        <td>{{$row->Status}}</td>
                                        <td>
                                            <div class="btn-group dropdown actions-dropodown">
                                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>                                               
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{url('preferences/show/'.$row->PKPreferenceID)}}"><i class="feather icon-book-open"></i> View</a>
                                                    <a class="dropdown-item" href="{{url('preferences/edit/'.$row->PKPreferenceID)}}"><i class="feather icon-edit"></i> Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item confirm-delete" href="javascript:void(0);" data-href="{{url('preferences/delete/'.$row->PKPreferenceID)}}" data-id="{{$row->PKPreferenceID}}" ><i class="fa fa-trash-o"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th >ID</th>
                                        <th >Title</th>
                                        <th >Parent</th>
                                        <th >Status</th>
                                        <th ></th>
                                    </tr>
                                </tfoot>
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<!--
<script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
-->
<script>
    $(document).ready(function () {
        $('.add-rows').DataTable();
    });
</script>
@endsection