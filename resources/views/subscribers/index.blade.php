
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <a href="{{url('subscribers/create')}}" class="btn btn-primary mb-2">Add</a>


                        <div class="table-responsive">

                            @if(count($model))
                            <table  class="table listing">
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Email Address</th>
                                        <th data-hide="phone,tablet">Source</th>
                                        <th></th>
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr>
                                        <td>{{$row->PKSubscriberID}}</td>

                                        <td>{{$row->EmailAddress}}</td>
                                        @if ($row->Source==null)
                                        <td>None</td>
                                        @else
                                        <td>{{$row->Source}}</td>
                                        @endif
                                        <td>
                                            <div class="btn-group dropdown actions-dropodown">
                                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>                                               
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{url('subscribers/'.$row->PKSubscriberID)}}"><i class="feather icon-eye"></i> View</a>
                                                    <a class="dropdown-item" href="{{url('subscribers/edit/'.$row->PKSubscriberID)}}"><i class="feather icon-edit"></i> Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="confirm-delete dropdown-item" href="javascript:void(0);" data-href="{{url('subscribers/delete/'.$row->PKSubscriberID)}}" data-id="{{$row->PKSubscriberID}}" ><i class="fa fa-trash-o"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>



                                <tfoot>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Email Address</th>
                                        <th data-hide="phone,tablet">Source</th>
                                        <th></th>                                
                                    </tr>
                                </tfoot>
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

@endsection
