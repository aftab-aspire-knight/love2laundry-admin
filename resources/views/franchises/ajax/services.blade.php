
<div class="table-responsive">
    <button type="button" class="btn btn-outline-success waves-effect waves-light" data-toggle="modal" data-target="#inlineForm">
        Add Services
    </button>

    @if(count($services))
    <table class="table" id="table_services">
        <thead>
            <tr><th scope="col"></th>
                <th scope="col">Service</th>
                <th scope="col">Price</th>
                <th scope="col">Discount(%)</th>


                <th scope="col">Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach($services as $row)

            <tr id="tr_<?php echo $row->PKFranchiseServiceID; ?>" data-service="<?php echo $row->PKFranchiseServiceID; ?>" >
                <td scope="row"><?php echo $row->Title; ?></td>
                <td >
                    <input type="text" id="Title{{$row->PKFranchiseServiceID}}" name="Title{{$row->PKFranchiseServiceID}}"  class="form-control" value="<?php echo $row->Title; ?>" aria-invalid="false" >
                </td>
                <td ><input type="text" id="Price{{$row->PKFranchiseServiceID}}" name="Price{{$row->PKFranchiseServiceID}}"  class="form-control " value="<?php echo $row->Price; ?>" aria-invalid="false"></td>
                <td><input type="text" id="DiscountPercentage{{$row->PKFranchiseServiceID}}" name="DiscountPercentage{{$row->PKFranchiseServiceID}}"  class="form-control " value="<?php echo isset($row->DiscountPercentage) ? $row->DiscountPercentage : 0.00; ?>" aria-invalid="false"></td>

                <td><a data-id="{{$row->PKFranchiseServiceID}}" class="btn btn-danger remove" style="color:white" > Remove </a>
                    <input data-id="{{$row->PKFranchiseServiceID}}" data-service-id="{{$row->FKServiceID}}" class="btn btn-primary save"  type="button" name="Save" value="Submit" >
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <div class="alert alert-primary" role="alert">
        <i class="feather icon-info mr-1 align-middle"></i>
        <span>No record found</span>
    </div>
    @endif


</div>
<div class="modal fade text-left" id="inlineForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel33">Add New Services </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="number-tab-steps wizard-circle wizard clearfix" action="#">
                <input type="hidden" name="franchise_id" id="franchise_id" value="{{$id}}"/>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('Services') !!}
                            {!! Form::select('service_id', $newServices,null,['class' => 'form-control select2',"id"=>"service_id" ]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary waves-effect waves-light add-service" data-dismiss="modal">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@csrf
<script>
    $(document).ready(function () {

        $('.select2').select2();

        $('#table_services').DataTable({
            "paging": false,
            "ordering": false,
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": true
                }
            ]
        });

        $(".add-service").click(function () {

            var id = $("#service_id").val();
            var franchise_id = $("#franchise_id").val();

            var url = '{{url("franchises/addservice")}}';
            var type = "POST";
            var dataType = "json";
            var token = $("[name=_token]").val();
            var data = {FKServiceID: id, FKFranchiseID: franchise_id, _token: token};

            var callBack = function (response) {
                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    alertMessage("Success", response.message, "success");
                    getServices();
                }
            };

            ajaxRequest(url, type, dataType, data, callBack, {});

        });

        $(".remove").click(function () {
            var id = $(this).data("id");
            var url = '{{url("franchises/removeservice")}}';
            var type = "POST";
            var dataType = "json";
            var token = $("[name=_token]").val();
            var data = {PKFranchiseServiceID: id, _token: token};

            var callBack = function (response) {
                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    $("#tr_" + id).hide();
                    alertMessage("", response.message, "success");
                }
            };

            ajaxRequest(url, type, dataType, data, callBack, {});

        });

        $(".save").click(function () {
            var id = $(this).data("id");
            var service_id = $(this).data("service-id");
            var title = $("#Title" + id).val();
            var price = $("#Price" + id).val();
            var discountPercentage = $("#DiscountPercentage" + id).val();
            var token = $("[name=_token]").val();
            var data = {PKFranchiseServiceID: id, FKServiceID: service_id, Title: title, Price: price, DiscountPercentage: discountPercentage, _token: token};

            var url = '{{url("franchises/saveservice")}}';
            var type = "POST";
            var dataType = "json";
            var callBack = function (response) {
                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    alertMessage("", response.message, "success");
                }
            };

            ajaxRequest(url, type, dataType, data, callBack, {});
        });
    });
</script>