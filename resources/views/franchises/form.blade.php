@include("messages")
<?php
$deliveryOptions = Config::get('params.delivery_options');
$allowSameTime = Config::get('params.yes_no');
$franchiseTimings = Config::get('params.franchise_timings');
$days = Config::get('params.days');
$required = "required";
?>
<div class="row">
    <ul id="tabsJustified" class="nav nav-tabs">
        <li class="nav-item"><a href="#_" data-target="#about" data-toggle="tab" class="nav-link small text-uppercase active">About</a></li>
        <li class="nav-item"><a href="#_" data-target="#timings" data-toggle="tab" class="nav-link small text-uppercase ">Timings</a></li>
    </ul>
</div>
<div id="tabsJustifiedContent" class="tab-content">

    <div id="about" class="tab-pane fade active show">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('Title') !!} <span class="red-color">*</span>
                    {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('Contact Person Name') !!}<span class="red-color">*</span>
                    {!! Form::text('Name', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    {!! Form::label('Contact Person Email Address') !!}<span class="red-color">*</span>
                    {!! Form::text('EmailAddress', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Mobile') !!} <span class="red-color">*</span>
                    {!! Form::text('Mobile', null , array('class' => 'form-control') ) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::label('Telephone') !!} 
                    {!! Form::text('Telephone', null , array('class' => 'form-control') ) !!}
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Delivery Option') !!} <span class="red-color">*</span>
                    {!! Form::select('DeliveryOption', $deliveryOptions,null,['class' => 'form-control','name'=>'DeliveryOption']) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Pickup Difference Hour') !!} <span class="red-color">*</span>
                    {!! Form::text('PickupDifferenceHour', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Delivery Difference Hour') !!} <span class="red-color">*</span>
                    {!! Form::text('DeliveryDifferenceHour', null , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Min. Order Amount') !!} <span class="red-color">*</span>
                    {!! Form::text('MinimumOrderAmount', 15 , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Address') !!}
                    {!! Form::textarea('Address', null, ['size' => '105x2','class' => 'form-control',$required]) !!}
                </div>
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('PostCode') !!}
                    {!! Form::text('PostCode', null , array("data-role"=>"tagsinput",'id' => 'PostCode','class' => 'form-control') ) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Off Days') !!}
                    {!! Form::select('OffDays[]', $days,null,['class' => 'form-control select2',"multiple"=>"multiple" ]) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Allow Same Time') !!}
                    {!! Form::select('AllowSameTime', $allowSameTime,null,['class' => 'form-control','name'=>'AllowSameTime']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <div class="d-flex justify-content-start flex-wrap">
                    <div class="custom-control custom-switch custom-switch-success mr-2 mb-1">
                        <p class="mb-0">Status</p>
                        <input type="checkbox" 

                               @if(isset($model->Status) && $model->Status=="Enabled")
                               checked 
                               @endif
                               class="custom-control-input" name="Status" id="Status" value="Enabled">
                               <label class="custom-control-label" for="Status">Status</label>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="timings" class="tab-pane fade">
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Pickup Limit') !!} <span class="red-color">*</span> at one time
                    {!! Form::text('PickupLimit', 1 , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::label('Delivery Limit') !!} <span class="red-color">*</span> at one time
                    {!! Form::text('DeliveryLimit', 1 , array('class' => 'form-control',$required) ) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('Opening Time') !!} <span class="red-color">*</span>
                    {!! Form::select('OpeningTime', $franchiseTimings,null,['class' => 'form-control','name'=>'OpeningTime']) !!}
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::label('Closing Time') !!} <span class="red-color">*</span>
                    {!! Form::select('ClosingTime', $franchiseTimings,null,['class' => 'form-control','name'=>'ClosingTime']) !!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    {!! Form::label('Gap') !!} <span class="red-color">*</span>
                    {!! Form::text('GapTime', 1,['class' => 'form-control',$required]) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                <h3>Add <span class="semi-bold">More</span></h3>
                <p><button type="button" class="add_field_button btn btn-info">Add More Fields</button></p>
                <br>
                <div class="cf nestable-lists" id="franchise_timmings_div">

                    <div class="input_fields_wrap">

                        <?php
                        $i = 1;
                        ?>
                        @foreach($timings as $timing)
                        <div class="row" id="{{$i}}">
                            <div class="col-md-3 col-xs-12 col-lg-3" >
                                <div class="form-group">
                                    <label class="form-label">Pickup Limit</label><span class="help"> at one time</span><div class="controls"><input type="text" id="more_pickup_limit" name="timings[more_pickup_limit][]" maxlength="3" class="form-control only-number not-zero" value="{{$timing->PickupLimit}}"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-12 col-lg-3">
                                <div class="form-group">
                                    <label class="form-label">Delivery Limit</label><span class="help"> at one time</span>
                                    <div class="controls"><input type="text" id="more_delivery_limit" name="timings[more_delivery_limit][]" maxlength="3" class="form-control only-number not-zero" value="{{$timing->DeliveryLimit}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 col-lg-2">
                                <div class="form-group">
                                    <label class="form-label">Opening Time</label>
                                    <div class="controls">
                                        {!! Form::select('timings[more_opening_time][]', $franchiseTimings,$timing->OpeningTime,['class' => 'form-control','id'=>'more_opening_time']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 col-lg-2">
                                <div class="form-group">
                                    <label class="form-label">Closing Time</label>
                                    <div class="controls">
                                        {!! Form::select('timings[more_closing_time][]', $franchiseTimings,$timing->ClosingTime,['class' => 'form-control','id'=>'more_closing_time']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 col-lg-2">
                                <div class="form-group">
                                    {!! Form::label('Gap') !!} <span class="red-color">*</span>
                                    {!! Form::text('timings[gap_time][]', 1,['class' => 'form-control',$required]) !!}
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 col-lg-2">
                                <a href="#" data-id="{{$i}}" class="remove_field">Remove</a>
                            </div>
                        </div>
                        <?php
                        $i++;
                        ?>
                        @endforeach


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<br clear="all" />
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('franchises')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>

<?php
$disabled_attribute = "";
?>
<script>
    $(document).ready(function () {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = <?php echo isset($id) ? $i : 1 ?>; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();

            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="row" id="' + x + '">'
                        + '' +
                        '<div class="col-md-3 col-xs-12 col-lg-3">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Pickup Limit <span class="red-color">*</span></label><span class="help">at one time</span>' +
                        '<div class="controls">' +
                        '<input type="text" id="more_pickup_limit" name="timings[more_pickup_limit][]" maxlength="3" class="form-control only-number not-zero"<?php
echo $disabled_attribute;
echo ' value=""'
?> />' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-3 col-xs-12 col-lg-3">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Delivery Limit </label><span class="help">at one time</span>' +
                        '<div class="controls">' +
                        '<input type="text" id="more_delivery_limit" name="timings[more_delivery_limit][]" maxlength="3" class="form-control only-number not-zero"<?php
echo $disabled_attribute;
echo ' value=""'
?> />' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-xs-12 col-lg-2">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Opening Time</label>' +
                        '<div class="controls">' +
                        '<select id="more_opening_time" name="timings[more_opening_time][]" class="form-control"<?php echo $disabled_attribute; ?>>' +
                        '<?php
for ($i = 5; $i <= 12; $i++) {
    $opening_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $opening_value = $i . ':00';
    if ($i == 12) {
        $opening_value .= ' pm';
    } else {
        $opening_value .= ' am';
    }
    /* if(isset($record['OpeningTime'])){
      if($record['OpeningTime'] == $opening_value){
      $opening_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
}
for ($i = 1; $i <= 11; $i++) {
    $opening_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $opening_value = $i . ':00';
    if ($i == 12) {
        $opening_value .= ' am';
    } else {
        $opening_value .= ' pm';
    }
    /* if(isset($record['OpeningTime'])){
      if($record['OpeningTime'] == $opening_value){
      $opening_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $opening_value . '"' . $opening_time_selected . '>' . $opening_value . '</option>';
}
?>' +
                        '</select>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="col-md-2 col-xs-12 col-lg-2">' +
                        '<div class="form-group">' +
                        '<label class="form-label">Closing Time </label>' +
                        '<div class="controls">' +
                        '<select id="more_closing_time" name="timings[more_closing_time][]" class="form-control"<?php echo $disabled_attribute; ?>>' +
                        '<?php
for ($i = 5; $i <= 12; $i++) {
    $closing_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $closing_value = $i . ':00';
    if ($i == 12) {
        $closing_value .= ' pm';
    } else {
        $closing_value .= ' am';
    }
    /* if(isset($record['ClosingTime'])){
      if($record['ClosingTime'] == $closing_value){
      $closing_time_selected = ' selected="selected"';
      }
      } */
    echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
}
for ($i = 1; $i <= 11; $i++) {
    $closing_time_selected = "";
    if (strlen($i) == 1) {
        $i = '0' . $i;
    }
    $closing_value = $i . ':00';
    if ($i == 12) {
        $closing_value .= ' am';
    } else {
        $closing_value .= ' pm';
    }
    echo '<option value="' . $closing_value . '"' . $closing_time_selected . '>' . $closing_value . '</option>';
}
?>' +
                        '</select>' +
                        '</div>' +
                        '</div>'
                        + '</div>'
                        + '<div class="col-md-2 col-xs-12 col-lg-2"><label class="form-label">Gap Time <span class="red-color">*</span></label><div class="form-group"><input type="text" id="timings_gap_time" name="timings[gap_time][]" class="form-control only-number not-zero"/></div></div>'
                        + '<div class="col-md-1 col-xs-1 col-lg-2"><a href="#" data-id="' + x + '" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            e.preventDefault();

            var id = $(this).data("id");
            $("#" + id).remove();
            x--;
        })
    }
    );
</script>

@section('myscript')
@include('vendor_script')
<script>

    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
{{-- Page js files --}}
@endsection