@extends('layouts/contentLayoutMaster')
@section('title', $title)
@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<?php
$backButton = '<a href="' . url("franchises") . '" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a>';
?>
<section id="page-account-settings">
    <div class="row">
        <!-- left menu section -->
        <div class="col-md-3 mb-2 mb-md-0">
            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 active" id="nav-general" data-toggle="pill" href="#general" aria-expanded="true">
                        <i class="feather icon-globe mr-50 font-medium-3"></i>
                        General
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-time" data-toggle="pill" href="#time" aria-expanded="false">
                        <i class="feather icon-clock mr-50 font-medium-3"></i>
                        Time & Info
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-postcodes" data-toggle="pill" href="#postcodes" aria-expanded="false">
                        <i class="feather icon-globe mr-50 font-medium-3"></i>
                        Post Codes
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="nav-services" data-toggle="pill" href="#services" aria-expanded="false">
                        <i class="feather icon-truck mr-50 font-medium-3"></i>
                        Services
                    </a>
                </li>
            </ul>
        </div>
        <!-- right content section -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="general" aria-labelledby="general" aria-expanded="true">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> General</h4>
                                        </div>
                                        <hr>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td><strong> Title : </strong></td>
                                                    <td>{{ $model->Title }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong> Person Name : </strong></strong></td>
                                                    <td>{{ $model->Name }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Person Email :</strong></td>
                                                    <td>{{ $model->EmailAddress }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Mobile :</strong></td>
                                                    <td>{{ $model->Mobile }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Telephone :</strong></td>
                                                    <td>{{ $model->Telephone }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Delivery Options :</strong></td>
                                                    <td>{{ $model->DeliveryOption }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Difference Hours</strong></td>
                                                    <td>{{ $model->DeliveryDifferenceHour }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Minimum order amount:</strong></td>
                                                    <td>{{ $model->MinimumOrderAmount }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Address:</strong></td>
                                                    <td>{{ $model->Address }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Off Days:</strong></td>
                                                    <td>{{ $model->OffDays }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Allow Same Time:</strong></td>
                                                    <td>{{ $model->AllowSameTime }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Status:</strong></td>
                                                    <td>{{ $model->Status }}</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="time" role="tabpanel" aria-labelledby="time" aria-expanded="false">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Time & Info</h4>
                                        </div>
                                        <hr/>
                                        <table class="table mb-0">
                                            <thead>
                                            <th>Pickup Limit * at one time</th>
                                            <th>Delivery Limit * at one time</th>
                                            <th>Opening Time *</th>
                                            <th>Closing Time *</th>
                                            </thead>
                                            <tbody>
                                                @foreach($timings as $row)
                                                <tr>
                                                    <td>{{ $row->PickupLimit}}</td>
                                                    <td>{{ $row->DeliveryLimit }}</td>
                                                    <td>{{ $row->OpeningTime }}</td>
                                                    <td>{{ $row->ClosingTime }}</td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="postcodes" role="tabpanel" aria-labelledby="
                                 postcodes" aria-expanded="false">

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Post Codes</h4>
                                        </div>
                                        <hr/>

                                        @foreach($post_codes as $row)
                                        <span class="badge badge-pill badge-light-primary badge-md px-2 mr-1 mb-1">{{ $row->Code}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services" aria-expanded="false">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="media">
                                            <h4 class="card-title"><?php echo $backButton; ?> Services</h4>
                                        </div>
                                        <hr/>

                                        @if($preference_records->isEmpty())
                                        @else
                                        <table class="table mb-0">
                                            <thead>
                                            <th>Title</th>
                                            <th>Price</th>
                                            <th>Discount </th>
                                            </thead>
                                            <tbody>
                                                @foreach($service as $row)
                                                <tr>
                                                    <td>{{ $row->Title}}</td>
                                                    <td><?php echo env("CURRENCY_SYMBOL") ?>{{ $row->Price }}</td>
                                                    <td>{{ $row->DiscountPercentage }}%</td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                        @elseif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
@endsection
