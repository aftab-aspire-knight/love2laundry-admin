
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('/vendors/css/forms/select/select2.css')) }}">
@endsection
@section('content')

<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <div id="errors"></div>
                        <div id="message"></div>
                        <div id="services" class="tab-pane">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<!--
<script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
-->

<script>
    $(document).ready(function () {
        getServices();
    });

    function getServices() {
        var url = '{{url("franchises/getservices/$id")}}';
        var type = "GET";
        var dataType = "html";
        var callBack = function (result) {
            $("#services").html(result);
        };

        ajaxRequest(url, type, dataType, {}, callBack, {});
    }


</script>


@endsection