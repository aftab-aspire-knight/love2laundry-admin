
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row" class="data-list-view-header">
    <div class="row">
        <div class="col-12">
            <div class="card card-transparent card-minimal">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">
                    @include("messages")
                    <div class="dataTables_wrapper dt-bootstrap4 no-footer table-responsive">
                        <table id="example"  class="table table-bordered mb-0 dataTable data-list-view no-footer dt-checkboxes-select ">
                            <thead>
                                <tr role="row">
                                    <th class="dt-checkboxes-cell dt-checkboxes-select-all"></th>
                                   
                                    <th>Franchise</th>
                                    <th>Post Code</th>
                                    <th>IPAddress</th>
                                    <th>CreatedDateTime</th>
                                    <th></th>
                                </tr>

                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
<script>

    $(document).ready(function () {

       var table= $('#example').DataTable({

            "fixedHeader": true,
            "pageLength": pageLength,
            "processing": true,
            "serverSide": true,
            responsive: true,
            destroy: true,
            retrieve: true,
            "oLanguage": {
                "sLengthMenu": "_MENU_",
                "sSearch": ""
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 100]], // page length options
            columnDefs: [{
                    searchable: false,
                    orderable: false,
                     data:"PKSearchID",
                     targets: 0, 
                    checkboxes: {selectRow: true},
                }],
            select: {
                selector: 'td:first-child',
                style: 'multi'
            },
            bInfo: false,
            "dom": '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
            "buttons": {
                "buttons": [
                        {
                        text: 'Delete all',
                        className: 'select-btn btn btn-outline-primary hidden buttonToHide',
                        action: function () {
                        var count = table.rows( { selected: true } ).count();
                            var ids = $.map(table.rows('.selected').data(), function (item) {
                                return item['PKSearchID']

                            });
                            alert(ids);
                  
                              $.ajax({
                                url: 'postcodelogs/deleteAll',
                                type: "GET",
                                data: {IDS:ids},
                                success: function (result) {
                                  if(result>=1){
                                    window.top.location = "{{url('postcodelogs')}}";
                                   }else{
                                       alert('Status does not updated');
                                   }

                                }
                            });

                        }
                 },
  
                ],
                dom: {
                    button: {
                        tag: "button",
                        className: "select-btn"
                    },
                    buttonLiner: {
                        tag: null
                    },
                    container: {
                        tag: 'div',
                        className: "dt-buttons"
                    },
                }
            },
            "ajax": {
                url: "{{url('postcodelogs/json')}}",
                dataType: 'JSON'
            },
            columns: [
                {data: 'PKSearchID', name: 'PKSearchID'},
                {data: 'franchises', name: 'franchises.Title'},
                {data: 'Code', name: 'Code'},
                {data: 'IPAddress', name: 'IPAddress'},
                {data: 'CreatedDateTime', name: 'CreatedDateTime'},
                {data: 'action', name: 'action', orderable: false, searchable: false},

            
            ],
            "order": [[1, 'desc']]
        });
    // to check and uncheck checkboxes on click of <td> tag
        $(".data-list-view").on("click", "tbody td", function () {
            var dtCheckbox = $(this).parent("tr").find(".dt-checkboxes-cell .dt-checkboxes")
            $(this).closest("tr").toggleClass("selected");
            dtCheckbox.prop("checked", !dtCheckbox.prop("checked"));

        });

        $(".dt-checkboxes").on("click", function () {
            $(this).closest("tr").toggleClass("selected");
            
        })
        $(".dt-checkboxes-select-all input").on("click", function () {
            $(".data-list-view").find("tbody tr").toggleClass("selected")
            

        })  
        $(".dt-checkboxes-cell").on("click", function () {
            if ($(".data-list-view").find("tbody tr").hasClass("selected")) {
                table.buttons('.buttonsToHide').nodes().removeClass('hidden');
                table.buttons('.buttonToHide').nodes().removeClass('hidden');

            }else{
                table.buttons('.buttonsToHide').nodes().addClass('hidden');
                table.buttons('.buttonToHide').nodes().addClass('hidden');

            }


        }) 
        // mac chrome checkbox fix
        if (navigator.userAgent.indexOf('Mac OS X') != -1) {
            $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox");
        }
    });


</script>
@endsection
