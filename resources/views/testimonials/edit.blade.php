@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
<link rel="stylesheet" href="{{ asset('ckeditor/contents.css') }}">


@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit {{$title}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::model($model, ['files' => true,'class' => 'number-tab-steps wizard-circle wizard clearfix','url' => ['testimonials/update', $id], 'method' => 'post']) !!}
                            @include('testimonials.form')

                            {!! Form::close() !!}
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}
 <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
<script src="{{ asset(mix('/vendors/js/extensions/jquery.nestable.js')) }}"></script>
<script>
var updateOutput = function (e) {
    var list = e.length ? e : $(e.target);
    if ((list.attr("id") == "nestable_menus") || (list.attr("id") == "assigned_menus") || (list.attr("id") == "nestable_widget") || (list.attr("id") == "nestable_locker") || (list.attr("id") == "nestable_franchise_post_code") || (list.attr("id") == "nestable_time")) {
        var output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            //output.html('JSON browser support required for this demo.');
        }
    }
};
$( document ).ready(function() {
        $('.pickadate2').pickadate({
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true,
            editable: true
        });

});
</script>
<script>
 $("#position").TouchSpin({
        min: 0,
        max: 5,
        verticalbuttons:false,
        boostat: 5,
        maxboostedstep: 10,
        initval:1,
        verticalbuttons:false

    });
</script>
 <script>
    var ckview = document.getElementById("ckview");
    CKEDITOR.config.allowedContent = true;
 CKEDITOR.replace(ckview, {filebrowserImageBrowseUrl: '/file-manager/ckeditor',
    });
</script>

@endsection