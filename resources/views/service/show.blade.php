
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">


    <div class="card">
        <div class="card-header">
            <h4 class="card-title"><a href="{{url('services')}}" class="mr-1 btn-icon btn btn-primary btn-round btn-sm"><i class="feather icon-arrow-left"></i></a> {{$title}}</h4>
        </div>
        <div class="card-content">
            <div class="card-body">

                <div class="row">
                    <div class="col-sm-12">

                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Category :</td>
                                    <td>{{ $category->Title }}</td>
                                </tr>
                                 <tr>
                                    <td>Price  :</td>
                                    <td>{{ $model->Price }}</td>
                                </tr>
                                <tr>
                                    <td>Title:</td>
                                    @if ($model->Tile==null)
                                           <td>None</td>
                                        @else
                                        <td>{{ $model->Tile }}</td>
                                        @endif
                                    
                                </tr>
                           <tr>
                                    <td>Content :</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="hide">Show</div></td>

                                </tr>
                                <tr id="contentDiv">
                                    <td></td>
                                    @if($model->Content== null)
                                       <td id="data">None</td>

                                    @else
                                    <td id="data">{{ $model->Content }}</td>
                                    @endif

                                </tr>
                                 <tr>
                                    <td>Preview</td>
                                    <td><div class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light" id="previewclick">Preview</div></td>

                                </tr>
                                   <tr>
                                    <td></td>
                                    @if($model->Content== null)
                                       <td id="previewdata">None</td>

                                    @else
                                    <td id="previewdata"><html> <body> <p><?php echo $model->Content;?></p> </body></html></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Desktop Image:</td>
                                        @if ($model->DesktopImageName==null)
                                           <td>None</td>
                                        @else
                                        <td>  <img  height="40" width="40" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_service.thumb') }}{{$model->DesktopImageName }}"></td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>Mobile Image:</td>
                                        @if ($model->MobileImageName==null)
                                           <td>None</td>
                                        @else
                                        <td>  <img  height="40" width="40" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_service.thumb') }}{{$model->MobileImageName }}"></td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>Is Package:</td>
                                    <td>{{ $model->IsPackage }}</td>
                                     
                                </tr>
                                 <tr>
                                    <td>Preferences Show :</td>
                                    <td>{{ $model->PreferencesShow }}</td>
                                     
                                </tr>
                                <tr>
                                    <td>Position :</td>
                                    @if ($model->Position==null)
                                           <td>None</td>
                                        @else
                                    <td>{{ $model->Position }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Status :</td>
                                    <td>{{ $model->Status }}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
   

</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
{{-- Page js files --}}

<script>
$( document ).ready(function() {
     $("#data").hide();
     $("#previewdata").hide();

     
$("#hide").click(function(){
   $("#data").toggle();
   $(this).toggleClass('class1')
 
});
$("#previewclick").click(function(){
   
   $("#previewdata").toggle();
    $(this).toggleClass('class1')
 
});

});

</script>
<script>
    CKEDITOR.config.allowedContent = true;

    CKEDITOR.replace( 'summary-ckeditor' );
</script>

@endsection