
@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                        @include("messages")
                        <a href="{{url('services/create')}}" class="btn btn-primary mb-2">Add</a>


                        <div class="table-responsive">

                            @if(count($model))
                            <table  class="table listing">
                                <thead>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Category</th>
                                        <th data-hide="phone,tablet">Title</th>
                                        <th data-hide="phone,tablet">Desktop Image</th>
                                        <th data-hide="phone,tablet">Mobile Image</th>
                                        <th data-hide="phone,tablet">Price</th>
                                        <th data-hide="phone,tablet">Preferences Show</th>
                                        <th data-hide="phone,tablet">Position</th>
                                        <th data-hide="phone,tablet">Status</th>
                                        <th></th>
                                    </tr>

                                </thead>
                                <tbody>
                                    @foreach($model as $row)
                                    <tr>
                                        <td>{{$row->PKServiceID}}</td>
                                        <td>{{$name[$row->FKCategoryID]}}</td>
                                        <td>{{$row->Title}}</td>
                                        @if ($row->DesktopImageName==null)
                                        <td>None</td>
                                        @else
                                        <td>  <img  height="40" width="40" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_service.thumb') }}{{$row->DesktopImageName }}"></td>
                                        @endif
                                        @if ($row->MobileImageName==null)
                                        <td>None</td>
                                        @else                                  
                                        <td>  <img  height="40" width="40" class="round" src="{{  env('APP_URL')."/".config('params.dir_upload_service.thumb') }}{{$row->MobileImageName }}"></td>

                                        @endif
                                        <td>{{$row->Price}}</td>
                                        <td>{{$row->PreferencesShow}}</td>
                                        <td>{{$row->Position}}</td>
                                        <td>{{$row->Status}}</td>


                                        <td>
                                            <div class="btn-group dropdown actions-dropodown">
                                                <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                </button>                                               
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{url('services/'.$row->PKServiceID)}}"><i class="feather icon-eye"></i> View</a>
                                                    <a class="dropdown-item" href="{{url('services/edit/'.$row->PKServiceID)}}"><i class="feather icon-mail"></i> Edit</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="confirm-delete dropdown-item" href="{{url('services/delete/'.$row->PKServiceID)}}" data-id="{{$row->PKServiceID}}" ><i class="fa fa-trash-o"></i> Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>



                                <tfoot>
                                    <tr>
                                        <th >ID</th>
                                        <th data-hide="phone,tablet">Category</th>
                                        <th data-hide="phone,tablet">Title</th>
                                        <th data-hide="phone,tablet">Desktop Image</th>
                                        <th data-hide="phone,tablet">Mobile Image</th>
                                        <th data-hide="phone,tablet">Price</th>
                                        <th data-hide="phone,tablet">Preferences Show</th>
                                        <th data-hide="phone,tablet">Position</th>
                                        <th data-hide="phone,tablet">Status</th>
                                        <th></th>                             
                                    </tr>
                                </tfoot>
                            </table>
                            @else

                            <div class="alert alert-primary" role="alert">
                                <i class="feather icon-info mr-1 align-middle"></i>
                                <span>No record found</span>
                            </div>

                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
@endsection
