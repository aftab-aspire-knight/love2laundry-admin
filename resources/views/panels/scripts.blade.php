{{-- Vendor Scripts --}}
<script>
    var currency = "<?php echo env("CURRENCY_CODE") ?>";
    var currency_symbol = "<?php echo env("CURRENCY_SYMBOL") ?>";
    var pageLength = 25;
</script>
@yield('vendor-script')
{{-- Theme Scripts --}}
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/components.js')) }}"></script>
@if($configData['blankPage'] == false)
<script src="{{ asset(mix('js/scripts/customizer.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/footer.js')) }}"></script>
@endif
<script src="{{ asset(mix('js/scripts/common.js')) }}"></script>
<script src="https://www.jqueryscript.net/demo/Bootstrap-4-Tag-Input-Plugin-jQuery/tagsinput.js"></script>
