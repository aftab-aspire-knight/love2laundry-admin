@php
$configData = Helper::applClasses();
@endphp


<?php
//d($menuData,1);
//echo Auth::user()->PKUserID;
$adminId = 36;
if (Auth::user()->PKUserID != $adminId) {
    $menuData = Helper::getSideBar(Auth::user(), request());
}
?>
<div class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{url('dashboard') }}">
                    <!--                    <div class="brand-logo"></div>-->
                    <img class="round" src="{{asset('assets/images/front/logo3.png') }}" alt="{{env('APP_NAME')}}" height="40" width="40" /><span></span>

                    <h5 class="brand-text mb-0">Love2<br>Laundary </h5>

                </a></li>
            <li class="nav-item"><a class="nav-link modern-nav-toggle pr-0" data-toggle=""><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon" data-ticon="icon-disc"></i>
                </a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">

        @if(Auth::user()->PKUserID == $adminId)
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            {{-- Foreach menu item starts --}}
            @foreach($menuData[0]->menu as $menu)
            @if(isset($menu->navheader))
            <li class="navigation-header">
                <span>{{ $menu->navheader }}</span>
            </li>
            @else
            {{-- Add Custom Class with nav-item --}}
            @php
            $custom_classes = "";
            if(isset($menu->classlist)) {
            $custom_classes = $menu->classlist;
            }
            $translation = "";
            if(isset($menu->i18n)){
            $translation = $menu->i18n;
            }
            @endphp
            <li class="nav-item {{ (request()->is($menu->url)) ? 'active' : '' }} {{ $custom_classes }}">
                <a href="{{ url($menu->url) }}">
                    <i class="{{ $menu->icon }}"></i>
                    <span class="menu-title" data-i18n="{{ $translation }}">{{ $menu->name }}</span>
                    @if (isset($menu->badge))
                    <?php $badgeClasses = "badge badge-pill badge-primary float-right" ?>
                    <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass.' test' : $badgeClasses.' notTest' }} ">{{$menu->badge}}</span>
                    @endif
                </a>
                @if(isset($menu->submenu))
                @include('panels/submenu', ['menu' => $menu->submenu])
                @endif
            </li>
            @endif
            @endforeach
            {{-- Foreach menu item ends --}}
        </ul>
        @else

        <?php echo html_entity_decode($menuData); ?>

        @endif
    </div>
</div>
<!-- END: Main Menu-->
