@include("messages")
<?php
$live_test= Config::get('params.live_test');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div class="row"> 
    <div class="col-md-12 col-xs-12 col-lg-12">
     
    <p>Please fill all the required fields with <span class="danger media-heading yellow darken-3">*</span> mark.<br/>Use <span class="danger media-heading yellow darken-3">#</span> for blank.</p>

    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active" id="tab_website">
        <div class="row">
            <div class="col-sm-6">
                @foreach($model as $key=>$row)
                    <div class="form-group">
                   
                    <input type="hidden" name="IDS[]" value="{{$row->PKSettingID}}" />
                {!! Form::label($row->Title) !!}
                   <?php  $status = Config::get('settings.'.$row->Title);
                   ?>
                    @if($row->Type=="dropdown")
                    
                       {!! Form::select('Content[]', $status,null,['class' => 'form-control',$required]) !!}

                         

                    @else
                        {!! Form::text('Content[]', $row->Content , array('class' => 'form-control',$required) ) !!}       
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        
    </div>
</div>
    
<br clear="all"/>
<div class="row">
    <div class="col-md-8">
        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
        <a href="{{url('settings')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
