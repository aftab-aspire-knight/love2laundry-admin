@extends('layouts/contentLayoutMaster')
@section('content-sidebar')
@section('title', $title)

@section('vendor-style')
{{-- vednor css files --}}
<link rel="stylesheet" href="">
@endsection
@section('content')
<!-- Add rows table -->
<section id="add-row">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-content">

                    <div class="card-body">
                       
                           <fieldset id="steps-uid-0-p-0" role="tabpanel" aria-labelledby="steps-uid-0-h-0" class="body current" aria-hidden="false" style="">
                            {!! Form::model($model, ['files' => true,'class' => 'number-tab-steps wizard-circle wizard clearfix','url' => ['settings/update'], 'method' => 'post']) !!}
                            @include('settings.form')

                            {!! Form::close() !!}
                      

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ Add rows table -->
<!--/ Scroll - horizontal and vertical table -->
@endsection
@include('vendor_script')
@section('myscript')
<script>
$(document).ready(function () {
//alert('hello');
    $('div li a').click(function(e) {
    
        $('div li a').removeClass('active');
        $('.tab-content div').removeClass('active');
         var id_name = this.hash.substr(1); //gets the text after # in href
        $("#" + id_name).addClass('active'); 
        $('#active_tab').attr('val',id_name.toString());

        $("#active_tab").val("#"+id_name);
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active');
        }
        //e.preventDefault();
    });
});
</script>

@endsection