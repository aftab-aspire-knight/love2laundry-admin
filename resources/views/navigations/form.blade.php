@include("messages")
<input type="hidden" id="assigned_output" name="assigned_output" value="" />
<?php
$roles = Config::get('params.roles');
$required = "";
//d($roles,1);
//$currency = $currencies[Config::get('params.currency_default')]['symbol'];
?>
<div id="errors"></div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('Title', null , array('class' => 'form-control',$required) ) !!}
        </div>
    </div>
</div>
 <div class="row">
    <div class="col-md-6 col-xs-6 col-lg-6">
        <div class="col-md-12 col-xs-12 col-lg-12">
            <div class="form-group">
                {!! Form::label('Page') !!}
                <select name="pages" id='pages' class="form-control select2" >
                    @foreach($pages as $key=>$page)
                    <option value="{{$page['ID']}}"  >{{$page['Title']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="dd col-md-12 col-xs-12 col-lg-12">
            <a href="javascript:void(0);" class="btn btn-outline-primary add">Add</a>
        </div>
    </div>
    <div class="col-md-6 col-xs-6 col-lg-6">
        <div class="cf nestable-lists">
            <div class="dd col-md-12 col-xs-12 col-lg-12" id="unassigned_menus">
                <ol class="dd-list dark"></ol>                                            
            </div>
        </div>
    </div>
</div>
<br clear="all" />
<br clear="all"/>
<h3>Add Custom Link</h3>
<div class="row">
  <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('Title') !!}
            {!! Form::text('CustomTitle', null , array('class' => 'form-control',$required, 'id'=>'title', 'name'=>'title') ) !!}
        </div>
</div>  
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            {!! Form::label('URL') !!}
            {!! Form::text('URL', null , array('class' => 'form-control',$required, 'id'=>'urls', 'name'=>'urls') ) !!}
        </div>
    </div>
  
</div>
<div class="row">
    <div class="col-sm-6">
        <div id="error"></div>

    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <a href="javascript:void(0);" class="btn btn-outline-primary " id="add">Add</a>
    </div>
</div>
<br clear="all" />

<div class="row">
    <div class="col-md-8">
        <button type="button" class="btn btn-primary mr-1 mb-1 waves-effect waves-light save">Save</button>
        <a href="{{url('navigations')}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Cancel</a>
    </div>
</div>
<br clear="all" />
<?php 
// echo json_encode($navigations); 
?>
<script>
var pages =<?php echo json_encode($pages); ?>;
var navigations =<?php echo json_encode($navigations); ?>;
</script>
<script>

    
    var count = 0;
    $(document).ready(function () {

        $(".add").click(function () {
            var page = $("#pages").val();
            var page = pages[page];
            var pagtype="Page"
            addPage(count,page['ID'], page['Title'], page['AccessURL'],pagtype);
        });
        
        $("#add").click(function () {
            var title = $("#title").val();
            var url = $("#urls").val();
            var pageid=0;
            var pagetype="Url";

            if(title=="" || url==""){
            $( "#error" ).html( "<p>Title/URL is empty</p>" );
            }else{

                $('#error').hide();
                $("#title").val('');
                $("#urls").val('');

                addCustomPage(count, pageid, title , url, pagetype);

            }
        });
          $(".save").click(function () {

            var url = $("#form").attr('action');
            var type = "POST";
            var dataType = "json";
            var data = $("#form").serialize();
            var callBack = function (response) {

                if (response.error === 1) {
                    displayErrors(response, "errors");
                } else
                {
                    window.top.location.href = '{{url("navigations/")}}';
                }
            };
            ajaxRequest(url, type, dataType, data, callBack, {});
        });

        var navigationsCount = Object.keys(navigations).length;

        if (navigationsCount > 0) {
            
            
            $.each(navigations, function (i, item) {
                console.log(item);
                
                if(item['id']==0){
                    addCustomPage(count,item['id'], item['title'], item['url'],item['type']);
                }else{
                    addPage(count,item['id'], item['title'], item['url'],item['type']);
                }
            });
        }

    });

    function remove(id) {
        $("#" + id).remove();
    }

    function addPage(num, id, title, url, pagetype) {
        
        var dd = '<li id="' + num + '" class="dd-item dd3-item" data-id="' + id + '" data-name="' + title + '" data-url="' + url + '"><div class="dd-handle dd3-handle"></div><input type="hidden" id="p_id" name="p_id[]" value="' + id + '"><input type="hidden" id="p_link" name="p_link[]" value="' + pagetype + '"><input type="hidden" id="p_text" name="p_text[]" value="' + title + '"><input type="hidden" id="p_url" name="p_url[]" value="' + url + '"><div class="dd3-content"><div class="accordion accordianpanel" aria-expanded="false"><div class="accordion-group"><div class="accordion-heading"> <a href="#collapse' + num + '" data-toggle="collapse" class="accordion-toggle collapsed">' + title + '<i class="fa fa-plus"></i> </a> </div><div class="accordion-body collapse" id="collapse' + num + '" style="height: 0px;"><div class="accordion-inner"><div class="form-group"><label class="form-label">Title</label><div class="controls"><input type="text" id="nav_label' + num + '" name="nav_label[]" class="form-control nav-title" value="' + title + '"></div></div><div class="original-name"><p>Original : ' + title + ' </p></div><div class="control-button"><a href="javascript:void(0);" onclick="remove(' + num + ')" data-id="' + num + '" class="remove-nav">Remove</a><!--<a data-id="' + num + '" class="cancel-nav">Close</a>--></div></div></div></div></div></div></li>';

        $(".dd-list").append(dd);
        count++;
    }
    
    function addCustomPage(num, id, title, url, pagetype) {
         
        var dd = '<li id="' + num + '" class="dd-item dd3-item" data-id="' + id + '" data-name="' + title + '" data-url="' + url + '"><div class="dd-handle dd3-handle"></div><input type="hidden" id="p_id" name="p_id[]" value="' + id + '"><input type="hidden" id="p_link" name="p_link[]" value="' + pagetype + '"><div class="dd3-content"><div class="accordion accordianpanel" aria-expanded="false"><div class="accordion-group"><div class="accordion-heading"> <a href="#collapse' + num + '" data-toggle="collapse" class="accordion-toggle collapsed">' + title + '<i class="fa fa-plus"></i> </a> </div><div class="accordion-body collapse" id="collapse' + num + '" style="height: 0px;"><div class="accordion-inner"><div class="form-group"><label class="form-label">Title</label><div class="controls"><input type="text" id="nav_label' + num + '" name="p_text[]" class="form-control nav-title" value="' + title + '"></div></div><div class="accordion-body collapse" id="collapse' + num + '" style="height: 0px;"><div class="accordion-inner"><div class="form-group"><label class="form-label">URL</label><div class="controls"><input type="text" id="nav_label' + num + '" name="p_url[]" class="form-control nav-title" value="' + url + '"></div></div><div class="control-button"><a href="javascript:void(0);" onclick="remove(' + num + ')" data-id="' + num + '" class="remove-nav">Remove</a><!--<a data-id="' + num + '" class="cancel-nav">Close</a>--></div></div></div></div></div></div></li>';

        $(".dd-list").append(dd);
        count++;
    }

</script>